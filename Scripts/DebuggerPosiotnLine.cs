

using UnityEngine;

namespace RCPU
{
    public class DebuggerPosiotnLine : MonoBehaviour
    {
#if UNITY_EDITOR
        [SerializeField] Color color;
        [SerializeField] float duration;

        Vector3 lsPos;


        void Update()
        {
            Debug.DrawLine(transform.position, lsPos, color, duration);
            lsPos = transform.position;
        }
#endif
    }
}

