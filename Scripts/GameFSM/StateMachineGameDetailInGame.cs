﻿
using RCPU.Attributes;
using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.NotificationSystem;
using RCPU.StateMachine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RCPU.Game
{
    [InjectToSystem(typeof(IInGameDetailFSM))]
    public class StateMachineGameDetailInGame : StateMachineGameDetailBase, IGameDetailFSM, IInGameDetailFSM
    {
        Dictionary<int, IReflexCause> reflexCause;
        protected Dictionary<EnumInGameCauciousState, InGameStateDataModel> cauciousStateModels;
        protected Dictionary<EnumInGameUiState, InGameUiStateDataModel> uiStateModels;
        [SerializeField] protected InGameStateDataModel_SO model_SO;

        protected EnumInGameUiState lastUiState;
        protected EnumInGameCauciousState lastCauciousState;

        InGameUiStateDataModel UiStateModel => uiStateModels[UiState];
        InGameStateDataModel CauciousStateModel => cauciousStateModels[CauciousState];
        public override EnumGameOveralState OveralState => EnumGameOveralState.InGame;
        protected override float EnterTime
        {
            get { return CauciousStateModel.EnterTime; }
            set { CauciousStateModel.EnterTime = value; }
        }
        protected override int EnterFrame
        {
            get { return CauciousStateModel.EnterFrame; }
            set { CauciousStateModel.EnterFrame = value; }
        }
        protected override float TimeScale => UiStateModel.timeScale * CauciousStateModel.timeScale;

        public override void Init()
        {
            base.Init();
            var instance = DInjector.Instantiate<InGameStateDataModel_SO>(model_SO);
            var ingameModels = instance.models;
            cauciousStateModels = new Dictionary<EnumInGameCauciousState, InGameStateDataModel>();
            for (int i = 0; i < ingameModels.Length; i++)
            {
                cauciousStateModels.Add(ingameModels[i].State, ingameModels[i]);
            }
            uiStateModels = new Dictionary<EnumInGameUiState, InGameUiStateDataModel>();
            var uiModels = instance.uiModels;
            for (int i = 0; i < uiModels.Length; i++)
            {
                uiStateModels.Add(uiModels[i].State, uiModels[i]);
            }
            reflexCause = new Dictionary<int, IReflexCause>();
            Register(EnumNotifType.StartReflexMode, OnStartReflex);
            Register(EnumNotifType.SetWeaponSelectionEnable, OnWeaponSelectionChanged);
            Register(EnumNotifType.SetScanUiEnable, OnScanUiEnableChanged);
        }

        private void OnScanUiEnableChanged(NotificationParameter obj)
        {
            bool value = obj.Get<bool>(EnumNotifParam.value);
            if (value)
            {
                ChangeState(EnumInGameUiState.Scan);
            }
            else
            {
                ChangeState(lastUiState);
            }
        }

        private void OnWeaponSelectionChanged(NotificationParameter obj)
        {
            bool value = obj.Get<bool>(EnumNotifParam.value);
            if (value)
            {
                ChangeState(EnumInGameUiState.WeaponSelection);
            }
            else
            {
                ChangeState(lastUiState);
            }
        }

        private void OnStartReflex(NotificationParameter obj)
        {
            IReflexCause owner = obj.Get<IReflexCause>(EnumNotifParam.owner);
            if (!reflexCause.ContainsKey(owner.Id))
                reflexCause.Add(owner.Id, owner);

            ChangeState(EnumInGameCauciousState.Reflex);

            //if (DetailState != EnumGameDetailState.Reflex)
            //{
            //    var notifParam = new NotificationParameter(EnumNotifParam.oldState, lastState, EnumNotifParam.newState, DetailState);
            //    Notification.Notify(EnumNotifType.InGameStateChanged, notifParam);

            //    lastState = DetailState;
            //    DetailState = EnumGameDetailState.Reflex;
            //    TimeController.timeScale = model.timeScale; 
            //}
        }

        protected void ChangeState(EnumInGameCauciousState newState)
        {
            if (CauciousState != newState)
            {
                var notifParam = new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.oldState, EnumNotifParam.newState }, new object[] { lastCauciousState, CauciousState });
                lastCauciousState = CauciousState;
                CauciousState = newState;
                TimeController.timeScale = TimeScale;
                Notification.Notify(EnumNotifType.InGameStateChanged, notifParam);
            }
        }

        protected void ChangeState(EnumInGameUiState newState)
        {
            if (UiState != newState)
            {
                var notifParam = new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.oldState, EnumNotifParam.newState }, new object[] { lastUiState, UiState });
                lastUiState = UiState;
                UiState = newState;
                TimeController.timeScale = TimeScale;
                Notification.Notify(EnumNotifType.InGameUiStateChanged, notifParam);
            }
        }

        public override void EnterState(StateChangeParameter param)
        {
            base.EnterState(param);
            if (!CommonHelper.IsNull(param))
            {
                if (param.ContainsKey(EnumStateChangeParameter.stateDetail))
                {
                    ChangeState(param.Get<EnumInGameCauciousState>(EnumStateChangeParameter.stateDetail));
                }
                if (param.ContainsKey(EnumStateChangeParameter.inGameStateUi))
                {
                    ChangeState(param.Get<EnumInGameUiState>(EnumStateChangeParameter.inGameStateUi));
                }
            }
            TimeController.timeScale = TimeScale;
        }

        public IReflexCause ReflexCause
        {
            get
            {
                if (reflexCause.Count > 0)
                    return reflexCause.First().Value;
                return null;
            }
        }



        protected override void UpdateStateMachine()
        {
            base.UpdateStateMachine();
            if (reflexCause.Count > 0)
            {
                List<int> remove = null;
                foreach (var item in reflexCause)
                {
                    if (item.Value.ReflexTime + GameSetting.ReflexDuration < TimeController.gameTime)
                    {
                        //CLog.Error("Reflex ChangeStateIf");
                        ChangeState(EnumInGameCauciousState.Alert);
                        return;
                    }
                    if (!item.Value.IsAlive)
                    {
                        if (CommonHelper.IsNull(remove))
                            remove = new List<int>();
                        remove.Add(item.Key);
                    }
                }
                if (!CommonHelper.IsNull(remove))
                {
                    //CLog.Error("Reflex remove");
                    for (int i = 0; i < remove.Count; i++)
                    {
                        reflexCause.Remove(remove[i]);
                    }
                }
            }
            if (reflexCause.Count == 0)
            {
                ChangeState(EnumInGameCauciousState.Normal);
            }
        }
    }
}
