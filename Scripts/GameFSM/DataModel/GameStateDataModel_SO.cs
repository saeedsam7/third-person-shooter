﻿
using UnityEngine;

namespace RCPU.Game
{
    [CreateAssetMenu(fileName = "GameState", menuName = "RCPU/DataModel/Game/GameState", order = 3)]
    public class GameStateDataModel_SO : ScriptableObject
    {
        public GameStateDataModel model;
    }

}
