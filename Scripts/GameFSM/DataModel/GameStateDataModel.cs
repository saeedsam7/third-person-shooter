﻿

using System;
using UnityEngine;

namespace RCPU.Game
{
    [Serializable]
    public class GameStateDataModel
    {
        [HideInInspector]
        public float EnterTime;
        [HideInInspector]
        public int EnterFrame;
        public EnumGameOveralState State;
        public float timeScale;
    }
}
