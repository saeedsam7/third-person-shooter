﻿
using UnityEngine;

namespace RCPU.Game
{
    [CreateAssetMenu(fileName = "InGameState", menuName = "RCPU/DataModel/Game/InGameState", order = 4)]
    public class InGameStateDataModel_SO : ScriptableObject
    {
        public InGameStateDataModel[] models;
        public InGameUiStateDataModel[] uiModels;
    }
}
