﻿

using System;
using UnityEngine;

namespace RCPU.Game
{
    [Serializable]
    public class InGameStateDataModel
    {
        [HideInInspector]
        public float EnterTime;
        [HideInInspector]
        public int EnterFrame;
        public EnumInGameCauciousState State;
        public float timeScale;
    }

    [Serializable]
    public class InGameUiStateDataModel
    {
        [HideInInspector]
        public float EnterTime;
        [HideInInspector]
        public int EnterFrame;
        public EnumInGameUiState State;
        public float timeScale;
    }
}
