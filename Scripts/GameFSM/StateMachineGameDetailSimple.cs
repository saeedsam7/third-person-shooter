﻿using RCPU.DependencyInjection;
using UnityEngine;

namespace RCPU.Game
{


    public class StateMachineGameDetailSimple : StateMachineGameDetailBase
    {

        protected GameStateDataModel model;
        [SerializeField] protected GameStateDataModel_SO model_SO;

        public override EnumGameOveralState OveralState => model.State;
        protected override float EnterTime
        {
            get { return model.EnterTime; }
            set { model.EnterTime = value; }
        }
        protected override int EnterFrame
        {
            get { return model.EnterFrame; }
            set { model.EnterFrame = value; }
        }

        protected override float TimeScale => model.timeScale;


        public override void Init()
        {
            base.Init();
            this.model = DInjector.Instantiate<GameStateDataModel_SO>(model_SO).model;
        }


    }
}