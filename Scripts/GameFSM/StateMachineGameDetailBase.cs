﻿

using RCPU.Attributes;
using RCPU.StateMachine;
using System;

namespace RCPU.Game
{

    [InitOrder(EnumInitOrder.GameState)]
    [InjectToInstance(typeof(IGameDetailFSM))]
    public class StateMachineGameDetailBase : StateMachineBase, IGameDetailFSM
    {

        [InjectFromSystem] protected IGameFSM GameFSM { get; set; }
        public EnumInGameCauciousState CauciousState { get; protected set; }
        public EnumInGameUiState UiState { get; protected set; }
        public virtual EnumGameOveralState OveralState { get => throw new Exception(); }
        protected virtual float EnterTime { get => throw new Exception(); set { } }
        protected virtual int EnterFrame { get => throw new Exception(); set { } }
        protected virtual float TimeScale { get => throw new Exception(); }

        protected override bool IsEnable { get { return this.enabled && (EnterFrame != TimeController.frameCount); } set { this.enabled = value; } }

        public override void Init()
        {
            base.Init();
            UiState = EnumInGameUiState.None;
            CauciousState = EnumInGameCauciousState.None;
            IsEnable = false;
        }

        //[InputRegisterButtonDown(EnumInputPriority.Escape, EnumUnityEvent.Update, "IsActive", EnumGameOveralState.InGame, EnumButton.Escape)]
        //protected void Escape()
        //{
        //    //if (currentState != GameState.Pause)
        //    {
        //        ChangeStateIf(EnumGameOveralState.Pause);
        //    }
        //}


        public virtual void EnterState(StateChangeParameter param)
        {
            this.IsEnable = true;
            EnterTime = TimeController.gameTime;
            EnterFrame = TimeController.frameCount;
            TimeController.timeScale = TimeScale;
        }

        //void ChangeStateIf(EnumGameOveralState next)
        //{
        //    GameFSM.ChangeStateIf(OveralState, next, new StateChangeParameter());
        //}

        public void ExitState(StateChangeParameter stateChangeParameter)
        {
            this.IsEnable = false;
        }

    }
}