using RCPU.Attributes;
using RCPU.InputSystem;
using RCPU.NotificationSystem;
using RCPU.StateMachine;
using System.Collections.Generic;

namespace RCPU.Game
{

    [InitOrder(EnumInitOrder.GameFSM)]
    [InjectToSystem(typeof(IGameFSM))]
    [RequireInput]
    public class StateMachineGame : StateMachineBase, IGameFSM
    {
        private EnumGameOveralState lastState;
        public EnumGameOveralState State { get; private set; }

        public EnumInGameCauciousState CauciousState => allState[State].CauciousState;
        public EnumInGameUiState InGameUIState => allState[State].UiState;

        //public EnumGameOveralState CurrentState => State;
        protected Dictionary<EnumGameOveralState, IGameDetailFSM> allState;
        [InjectFromInstance] protected IGameDetailFSM[] GameStates { get; set; }
        protected Dictionary<EnumGameOveralState, EnumGameOveralState> lastStateBeforeChange;


        [InputRegisterButtonDown(EnumInputPriority.Escape, EnumUnityEvent.Update, "IsActive", EnumGameOveralState.InGame, EnumInGameUiState.Scan, EnumButton.Escape)]
        protected void EscapeScan() => Notification.Notify(EnumNotifType.SetScanUiEnable, new NotificationParameter(EnumNotifParam.value, false));

        [InputRegisterButtonDown(EnumInputPriority.Escape, EnumUnityEvent.Update, "IsActive", EnumGameOveralState.Pause, EnumInGameUiState.None, EnumButton.Escape)]
        protected void Resume() => OnStateChanged(EnumGameOveralState.Pause, false);

        [InputRegisterButtonDown(EnumInputPriority.Escape, EnumUnityEvent.Update, "IsActive", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Escape)]
        protected void Pause() => OnStateChanged(EnumGameOveralState.Pause, true);

        [InputRegisterButtonDown(EnumInputPriority.Escape, EnumUnityEvent.Update, "IsActive", EnumGameOveralState.InGame, EnumInGameUiState.WeaponSelection, EnumButton.Escape)]
        protected void EscapeWeaponSelection() => Notification.Notify(EnumNotifType.SetWeaponSelectionEnable, new NotificationParameter(EnumNotifParam.value, false));




        public override void Init()
        {
            base.Init();
            allState = new Dictionary<EnumGameOveralState, IGameDetailFSM>();
            lastStateBeforeChange = new Dictionary<EnumGameOveralState, EnumGameOveralState>();

            for (int i = 0; i < GameStates.Length; i++)
            {
                lastStateBeforeChange.Add(GameStates[i].OveralState, GameStates[i].OveralState);
                allState.Add(GameStates[i].OveralState, GameStates[i]);
            }

            Register(EnumNotifType.PlayerDied, OnPlayerDie);
            Register(EnumNotifType.GameInitializationDone, OnGameInitializationDone);
            Register(EnumNotifType.SetMenuEnable, OnMenuEnableChanged);
            //Register(EnumNotifType.SetInventoryEnable, OnInventoryEnableChanged);
            //Register(EnumNotifType.SetWeaponSelectionEnable, OnWeaponSelectionEnableChanged);
        }
        //private void OnWeaponSelectionEnableChanged(NotificationParameter obj)
        //{
        //    OnUIStateChanged(EnumGameOveralState.WeaponSelection, obj.Get<bool>(EnumNotifParam.value));
        //}
        private void OnStateChanged(EnumGameOveralState newState, bool value)
        {
            if (value)
            {
                lastStateBeforeChange[newState] = State;
                ChangeState(new StateChangeParameter(EnumStateChangeParameter.newState, newState));
            }
            else
            {
                ChangeState(new StateChangeParameter(EnumStateChangeParameter.newState, lastStateBeforeChange[newState]));
            }
        }
        //private void OnInventoryEnableChanged(NotificationParameter obj)
        //{
        //    OnUIStateChanged(EnumGameOveralState.Inventory, obj.Get<bool>(EnumNotifParam.value));
        //}
        private void OnMenuEnableChanged(NotificationParameter obj)
        {
            OnStateChanged(EnumGameOveralState.Menu, obj.Get<bool>(EnumNotifParam.value));
        }

        private void OnGameInitializationDone(NotificationParameter obj)
        {
            var param = new StateChangeParameter(new EnumStateChangeParameter[] { EnumStateChangeParameter.newState, EnumStateChangeParameter.stateDetail, EnumStateChangeParameter.inGameStateUi },
                new object[] { EnumGameOveralState.InGame, EnumInGameCauciousState.Normal, EnumInGameUiState.HUD });

            ChangeState(param);
        }

        private void OnPlayerDie(NotificationParameter obj)
        {
            ChangeState(new StateChangeParameter(EnumStateChangeParameter.newState, EnumGameOveralState.GameOver));
        }




        protected void ChangeState(StateChangeParameter param)
        {
            EnumGameOveralState newState = param.Get<EnumGameOveralState>(EnumStateChangeParameter.newState);
            if (State != newState)
            {
                Logger.Log(" Game   " + State + " ---> " + newState, EnumLogColor.GrayLight);
                if (allState.ContainsKey(newState))
                {
                    if (allState.ContainsKey(State))
                    {
                        allState[State].ExitState(new StateChangeParameter(EnumStateChangeParameter.newState, newState));
                    }
                    var notifParam = new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.oldState, EnumNotifParam.newState }, new object[] { State, newState });
                    lastState = State;
                    State = newState;
                    allState[State].EnterState(param);
                    Notification.Notify(EnumNotifType.GameStateChanged, notifParam);
                }
            }
        }

        protected override void UpdateStateMachine()
        {
            //allState[State].UpdateState();
        }


        //public void ChangeStateIf(EnumGameOveralState current, EnumGameOveralState next, StateChangeParameter param)
        //{
        //    //Ddebug.Log(" ChangeStateIf currentState " + currentState + " current " + current + " next " + next);
        //    if (State == current)
        //    {
        //        param.Add(EnumStateChangeParameter.newState, next);
        //        ChangeState(param);
        //    }
        //}


    }
}