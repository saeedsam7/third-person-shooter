﻿namespace RCPU.Game
{
    public interface IGameFSM : IInitializable/*, IInputRegisterer*/
    {
        EnumInGameUiState InGameUIState { get; }
        EnumInGameCauciousState CauciousState { get; }
        EnumGameOveralState State { get; }

        //IReflexCause ReflexCause { get; }

        //void ChangeStateIf(EnumGameOveralState current, EnumGameOveralState next, StateChangeParameter param);
    }
}
