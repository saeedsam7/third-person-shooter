﻿namespace RCPU.Game
{
    public interface IInGameDetailFSM
    {
        IReflexCause ReflexCause { get; }
        EnumInGameCauciousState CauciousState { get; }
        EnumInGameUiState UiState { get; }
        bool IsActive();
    }
}
