﻿
using RCPU.Agent;

namespace RCPU.Game
{
    public interface IReflexCause : IAlive
    {
        float ReflexTime { get; }
    }
}


