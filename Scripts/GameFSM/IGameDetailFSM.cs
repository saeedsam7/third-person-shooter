﻿using RCPU.StateMachine;

namespace RCPU.Game
{
    public interface IGameDetailFSM : IInitializable/*, IInputRegisterer*/
    {
        EnumGameOveralState OveralState { get; }
        EnumInGameCauciousState CauciousState { get; }
        EnumInGameUiState UiState { get; }

        void EnterState(StateChangeParameter param);
        void ExitState(StateChangeParameter stateChangeParameter);
        //void UpdateState(StateChangeParameter obj);
        //IReflexCause ReflexCause { get; }

    }
}
