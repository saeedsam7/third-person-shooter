﻿

using UnityEngine;

namespace RCPU.UI
{
    public interface IUiState : IInitializable
    {
        EnumGameOveralState Index { get; }
        bool Active { get; set; }
        bool IsOverHudLayer { get; }
    }

    public interface IUiCanvas
    {
        RectTransform Trans { get; }
    }
}
