﻿
using RCPU.Attributes;
using RCPU.CameraFSM;
using RCPU.DependencyInjection;
using RCPU.StateMachine;
using UnityEngine;

namespace RCPU.UI
{
    [InjectToSystemArray(typeof(IUiState))]
    [InjectToInstance(typeof(IUiCanvas))]
    [InitOrder(EnumInitOrder.UiState)]
    [RequireComponent(typeof(Canvas))]
    public class UiStateBase : StateBase, IUiState, IUiCanvas
    {
        [InjectFromSystem] IUiCamera uiCamera { get; set; }
        [SerializeField] protected UiStateDataModel_SO dataModel_SO;
        protected UiStateDataModel dataModel;
        protected RectTransform trans;

        public EnumGameOveralState Index => dataModel.state;

        public bool Active { get => gameObject.activeSelf; set => gameObject.SetActive(value); }

        public RectTransform Trans => trans;

        public bool IsOverHudLayer => dataModel.IsOverHudLayer;

        public override void Init()
        {
            base.Init();
            trans = GetComponent<RectTransform>();
            GetComponent<Canvas>().worldCamera = uiCamera.cam;
            dataModel = DInjector.Instantiate<UiStateDataModel_SO>(dataModel_SO).model;
        }
    }
}
