﻿
using UnityEngine;

namespace RCPU.UI
{
    [CreateAssetMenu(fileName = "UI", menuName = "RCPU/DataModel/UI/UiDataModel", order = 1)]
    public class UiDataModel_SO : ScriptableObject
    {
        public UiDataModel model;
    }
}