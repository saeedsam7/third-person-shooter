﻿

using System;

namespace RCPU.UI
{
    [Serializable]
    public struct UiDataModel
    {
        public UiColor[] uiColors;
    }
}
