﻿

using UnityEngine;

namespace RCPU.UI
{
    [CreateAssetMenu(fileName = "UiState", menuName = "RCPU/DataModel/UI/UiStateDataModel", order = 1)]
    public class UiStateDataModel_SO : ScriptableObject
    {
        public UiStateDataModel model;
    }
}