﻿
using System;

namespace RCPU.UI
{
    [Serializable]
    public class UiStateDataModel
    {
        public EnumGameOveralState state;
        public bool IsOverHudLayer;
    }
}
