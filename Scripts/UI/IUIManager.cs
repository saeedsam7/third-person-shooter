﻿

using RCPU.Agent;
using RCPU.Damage;
using UnityEngine;

namespace RCPU.UI
{
    public interface IUIManager
    {
        void SetCrosshair(EnumCrosshairType cursorType);
        void SetCrosshairSize(float val);
        void SetCrosshairEnable(bool val);
        Vector2 WorldToCanvasPosition(RectTransform canvas, Vector3 position);
        Quaternion WorldToCanvasRotation(Vector3 position);
        void SetText(EnumUiTextIndex bulletCounter, string bulletCount);

        void SetImageSprite(EnumUiImageIndex uiIndex, EnumSpriteIndex sprite);
        void SetShadowCrosshair(Vector3 pos);
        void CLearShadowCrosshair();
        void SetColor(EnumUiColor color);
        void SetColor(EnumUiTextIndex uiIndex, EnumUiColor color);
        void SetColor(EnumUiImageIndex uiIndex, EnumUiColor color);
        void SetBarValue(EnumUiBarIndex index, float value);
        void SetEnable(EnumUiBarIndex index, bool value);
        void SetEnable(EnumUiImageIndex index, bool value);

        void BindWidgetToAgent(IAiPublic owner);
        void BindDamageText(IAiPublic owner, Vector3 position, EnumDamageType damageType, int damagePower, float damageMultiplier);
        void BindIndicatorToDamage(Vector3 hitPoint);
        void BindIndicatorToThreat(IAiPublic alive);
        void SetWeaponMenuEnable(bool value);
        Sprite GetSprite(EnumSpriteIndex index);
        //void Scan(IAiPublic scanTarget);
        void Scan(float hold, int targetID);
        void SetImagePosition(EnumUiImageIndex pickup, Vector3 position);
        void SetTextPosition(EnumUiTextIndex bulletCounter, Vector3 position);
    }
}
