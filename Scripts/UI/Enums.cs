namespace RCPU.UI
{
    public enum EnumSpriteIndex
    {
        None,
        AmmoPistol01, AmmoPistol02, AmmoPistol03, AmmoPistol04, AmmoPistol05,
        AmmoAssaultRifle01, AmmoAssaultRifle02, AmmoAssaultRifle03, AmmoAssaultRifle04, AmmoAssaultRifle05,
        AmmoSniperRifle01, AmmoSniperRifle02, AmmoSniperRifle03, AmmoSniperRifle04, AmmoSniperRifle05,
        AmmoShotgunRifle01, AmmoShotgunRifle02, AmmoShotgunRifle03, AmmoShotgunRifle04, AmmoShotgunRifle05,
        AmmoRocketLauncher01, AmmoRocketLauncher02, AmmoRocketLauncher03, AmmoRocketLauncher04, AmmoRocketLauncher05,
        Grenade01, Grenade02, Grenade03, Grenade04, Grenade05,
        WeaponPistol01, WeaponPistol02, WeaponPistol03, WeaponPistol04, WeaponPistol05,
        WeaponAssaultRifle01, WeaponAssaultRifle02, WeaponAssaultRifle03, WeaponAssaultRifle04, WeaponAssaultRifle05,
        WeaponSniperRifle01, WeaponSniperRifle02, WeaponSniperRifle03, WeaponSniperRifle04, WeaponSniperRifle05,
        WeaponShotgunRifle01, WeaponShotgunRifle02, WeaponShotgunRifle03, WeaponShotgunRifle04, WeaponShotgunRifle05,
        WeaponRocketLauncher01, WeaponRocketLauncher02, WeaponRocketLauncher03, WeaponRocketLauncher04, WeaponRocketLauncher05,
        Door, Teleport, HealthPack, Vehicle, Cover,
        DamageShock,
        DamageHealth,
        DamageLink,
    }

    public enum EnumUiImageIndex
    {
        Cursor, ThreatIndicator, DamageIndicator, Indicator,
        Weapom,
        Pickup,
    }

    //public enum EnumUiGameStateIndex
    //{
    //    Pause, GameOver,  HUD,Menu
    //}
    public enum EnumUiGameObjectIndex
    {
        Pause, GameOver
    }
    public enum EnumUiTextIndex
    {
        BulletCounter, HealthCounter, Interact, Pickup, Cover,
        Debug01, Debug02, Debug03, Debug04, Debug05, Debug06, Debug07, Debug08, Debug09, Debug10,
        Debug11, Debug12, Debug13, Debug14, Debug15, Debug16, Debug17, Debug18, Debug19, Debug20,
    }
    public enum EnumUi3DTextIndex
    {
        Damage
    }
    public enum EnumCrosshairType
    {
        None, Pistol, Rifle, Shotgun, Sniper
    }
    public enum EnumCrossCrosshairIndex
    {
        Top, Bottom, Left, Right, Ceneter
    }

    public enum EnumUiBarIndex
    {
        Reload, Scan
    }

    public enum EnumUiColor
    {
        CrosshairEnemy, CrosshairFriend, CrosshairNeutral, CrosshairDisable, CrosshairShadow,
        UiElementActive, UiElementDisable, UiElementError, UiElementWarning, UiElementLog,
        IndicatorDamage, IndicatorThreat,
        SimpleDamage,
        CriticalDamage
    }


    public enum EnumMenuState { None, FadeIn, Show, FadeOut }
}