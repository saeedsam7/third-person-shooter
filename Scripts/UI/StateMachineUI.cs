using RCPU.Agent;
using RCPU.Attributes;
using RCPU.CameraFSM;
using RCPU.Component;
using RCPU.Damage;
using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.InputSystem;
using RCPU.NotificationSystem;
using RCPU.StateMachine;
using RCPU.Trigger;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Color = UnityEngine.Color;

namespace RCPU.UI
{
    [InitOrder(EnumInitOrder.UiManager)]
    [InjectToSystem(typeof(IUIManager))]
    public class StateMachineUI : StateMachineBase, IUIManager
    {
        [InjectFromSystem] protected IUiImage[] injectedUiImage { get; set; }
        [InjectFromSystem] protected IUiText[] injectedUiText { get; set; }
        [InjectFromSystem] protected IUiGameObject[] injectedUiGameObject { get; set; }
        [InjectFromSystem] protected IUiAiWidget[] injectedAgentWidget { get; set; }
        [InjectFromSystem] protected IUiAiComponentWidget[] injectedAgentComponentWidget { get; set; }
        [InjectFromSystem] protected IUiDamageWidget[] injectedDamageWidget { get; set; }
        [InjectFromSystem] protected IUiThreatWidget[] injectedThreatWidget { get; set; }
        [InjectFromSystem] protected IUICrosshairWidget[] injectedCrosshairWidget { get; set; }
        [InjectFromSystem] protected IUIShadowCrosshairWidget[] injectedShadowCrosshairWidget { get; set; }
        [InjectFromSystem] protected IUiState[] injectedUiState { get; set; }
        [InjectFromSystem] protected IUiBar[] injectedUiBar { get; set; }
        [InjectFromSystem] protected IUiRadialMenu injectedUiWeaponMenu { get; set; }
        [InjectFromSystem] protected ILevelManager levelManager { get; set; }
        [InjectFromSystem] protected IUiDamageTextWidget[] injectedUiDamageTextWidget { get; set; }

        [SerializeField] protected UiSpriteView[] injectedUiSprite;
        [InjectFromSystem] protected ICamera Cam { get; set; }

        protected Dictionary<EnumGameOveralState, IUiState> allUiState;

        protected Dictionary<EnumUiImageIndex, IUiImage> allUiImage;
        protected Dictionary<EnumUiTextIndex, IUiText> allUiText;
        protected Dictionary<EnumUiGameObjectIndex, IUiGameObject> allUiGameObject;
        protected Dictionary<EnumSpriteIndex, IUiSprite> allUiSprite;
        protected Dictionary<EnumCrosshairType, IUICrosshairWidget> allCrosshairs;
        protected Dictionary<EnumCrosshairType, IUIShadowCrosshairWidget> allShadowCrosshairs;
        protected Dictionary<EnumUiBarIndex, IUiBar> allUiBars;



        [SerializeField] protected UiDataModel_SO dataModel_SO;
        protected UiDataModel model;
        protected Dictionary<EnumUiColor, Color> colors;

        public override void Init()
        {
            base.Init();

            Notification.Register(EnumNotifType.GameStateChanged, Id, GameStateChanged);

            model = DInjector.Instantiate<UiDataModel_SO>(dataModel_SO).model;
            colors = new Dictionary<EnumUiColor, Color>();
            for (int i = 0; i < model.uiColors.Length; i++)
            {
                colors.Add(model.uiColors[i].index, model.uiColors[i].color);
            }

            allUiSprite = new Dictionary<EnumSpriteIndex, IUiSprite>();
            if (!CommonHelper.IsNull(injectedUiSprite))
                for (int i = 0; i < injectedUiSprite.Length; i++)
                {
                    allUiSprite.Add(injectedUiSprite[i].Index, injectedUiSprite[i]);
                }

            allUiImage = new Dictionary<EnumUiImageIndex, IUiImage>();
            if (!CommonHelper.IsNull(injectedUiImage))
                for (int i = 0; i < injectedUiImage.Length; i++)
                {
                    allUiImage.Add(injectedUiImage[i].Index, injectedUiImage[i]);
                }

            allUiText = new Dictionary<EnumUiTextIndex, IUiText>();
            if (!CommonHelper.IsNull(injectedUiText))
                for (int i = 0; i < injectedUiText.Length; i++)
                {
                    allUiText.Add(injectedUiText[i].Index, injectedUiText[i]);
                }

            allUiGameObject = new Dictionary<EnumUiGameObjectIndex, IUiGameObject>();
            if (!CommonHelper.IsNull(injectedUiGameObject))
                for (int i = 0; i < injectedUiGameObject.Length; i++)
                {
                    allUiGameObject.Add(injectedUiGameObject[i].Index, injectedUiGameObject[i]);
                }

            allUiState = new Dictionary<EnumGameOveralState, IUiState>();
            if (!CommonHelper.IsNull(injectedUiState))
                for (int i = 0; i < injectedUiState.Length; i++)
                {
                    allUiState.Add(injectedUiState[i].Index, injectedUiState[i]);
                    injectedUiState[i].Active = false;
                }

            allCrosshairs = new Dictionary<EnumCrosshairType, IUICrosshairWidget>();
            if (!CommonHelper.IsNull(injectedCrosshairWidget))
                for (int i = 0; i < injectedCrosshairWidget.Length; i++)
                {
                    allCrosshairs.Add(injectedCrosshairWidget[i].Index, injectedCrosshairWidget[i]);
                }

            allShadowCrosshairs = new Dictionary<EnumCrosshairType, IUIShadowCrosshairWidget>();
            if (!CommonHelper.IsNull(injectedShadowCrosshairWidget))
                for (int i = 0; i < injectedShadowCrosshairWidget.Length; i++)
                {
                    allShadowCrosshairs.Add(injectedShadowCrosshairWidget[i].Index, injectedShadowCrosshairWidget[i]);
                }


            allUiBars = new Dictionary<EnumUiBarIndex, IUiBar>();
            if (!CommonHelper.IsNull(injectedUiBar))
                for (int i = 0; i < injectedUiBar.Length; i++)
                {
                    allUiBars.Add(injectedUiBar[i].Index, injectedUiBar[i]);
                }
        }

        public Sprite GetSprite(EnumSpriteIndex index)
        {
            if (allUiSprite.ContainsKey(index))
            {
                return allUiSprite[index].Element;
            }
            return null;
        }

        EnumCrosshairType selectedCrosshair;
        public void SetCrosshair(EnumCrosshairType crosshairType)
        {
            foreach (var item in allCrosshairs)
            {
                item.Value.Active = false;
            }
            foreach (var item in allShadowCrosshairs)
            {
                item.Value.Active = false;
            }
            selectedCrosshair = crosshairType;

        }

        public void SetCrosshairEnable(bool val)
        {
            if (selectedCrosshair != EnumCrosshairType.None)
                allCrosshairs[selectedCrosshair].Active = val;
        }

        public void SetCrosshairSize(float val)
        {
            if (selectedCrosshair != EnumCrosshairType.None)
                allCrosshairs[selectedCrosshair].Size = val;
        }



        //public void SetColor(EnumUiImageIndex uiIndex, Color color)
        //{
        //    if (allUiImage.ContainsKey(uiIndex))
        //    {
        //        allUiImage[uiIndex].Color = (color);
        //    }
        //}

        public void SetText(EnumUiTextIndex uiIndex, string txt)
        {
            if (allUiText.ContainsKey(uiIndex))
            {
                allUiText[uiIndex].Text = (txt);
                allUiText[uiIndex].Active = (true);
            }
        }



        public void SetGameObject(EnumUiGameObjectIndex uiIndex, bool val)
        {
            if (allUiGameObject.ContainsKey(uiIndex))
            {
                allUiGameObject[uiIndex].Active = (val);
            }
        }




        private void PlayerEnterInteractableTrigger(NotificationParameter obj)
        {
            string message = obj.Get<string>(EnumNotifParam.message);
            EnumSpriteIndex icon = obj.Get<EnumSpriteIndex>(EnumNotifParam.icon);
            EnumTriggerType triggerType = obj.Get<EnumTriggerType>(EnumNotifParam.type);
            EnumButton interactType = obj.Get<EnumButton>(EnumNotifParam.interactType);

            //SetIcon(icon);
            switch (interactType)
            {
                case EnumButton.Action:
                    SetText(EnumUiTextIndex.Interact, message);
                    break;
                case EnumButton.Pickup:
                    SetText(EnumUiTextIndex.Pickup, message);
                    break;
                case EnumButton.Cover:
                    SetText(EnumUiTextIndex.Cover, message);
                    break;
            }
        }

        //private void SetIcon(EnumSpriteIndex icon)
        //{
        //    //Debug.LogError(" SetIcon " + icon);
        //    //if (icon == EnumSpriteIndex.None)
        //    //{
        //    //    SetImageSprite(EnumUiImageIndex.Indicator, null);
        //    //}
        //    //else
        //        SetImageSprite(EnumUiImageIndex.Indicator, icon);
        //}

        private void PlayerExitInteractableTrigger(NotificationParameter obj)
        {
            EnumTriggerType triggerType = obj.Get<EnumTriggerType>(EnumNotifParam.type);
            EnumButton interactType = obj.Get<EnumButton>(EnumNotifParam.interactType);
            //SetIcon(EnumSpriteIndex.None);
            switch (interactType)
            {
                case EnumButton.Action:
                    SetText(EnumUiTextIndex.Interact, "");
                    break;
                case EnumButton.Pickup:
                    SetText(EnumUiTextIndex.Pickup, "");
                    break;
                case EnumButton.Cover:
                    SetText(EnumUiTextIndex.Cover, "");
                    break;
            }
            // CLog.Error(" PlayerExitPickupableTrigger "); 
        }

        public void SwitchState(EnumGameOveralState state)
        {

        }

        private void GameStateChanged(NotificationParameter obj)
        {
            //Debug.Log(" GameStateChanged " + ((CStateMachine_Game.GameState)obj[CNotificationCenter.NotifParam.newState]));
            foreach (var item in allUiState)
            {
                item.Value.Active = false;
            }
            var state = obj.Get<EnumGameOveralState>(EnumNotifParam.newState);
            allUiState[state].Active = true;
            if (allUiState[state].IsOverHudLayer)
                allUiState[EnumGameOveralState.InGame].Active = true;
            allUiState[EnumGameOveralState.Debug].Active = true;
        }

        public void SetTextPosition(EnumUiTextIndex uiIndex, Vector3 position)
        {
            if (allUiText.ContainsKey(uiIndex))
            {
                allUiText[uiIndex].SetPosition(WorldToCanvasPosition(allUiText[uiIndex].canvas.Trans, position));
            }
        }
        public void SetImagePosition(EnumUiImageIndex uiIndex, Vector3 position)
        {
            if (allUiImage.ContainsKey(uiIndex))
            {
                allUiImage[uiIndex].SetPosition(WorldToCanvasPosition(allUiImage[uiIndex].canvas.Trans, position));
            }
        }
        public void SetImageSprite(EnumUiImageIndex uiIndex, EnumSpriteIndex sprite)
        {
            if (allUiImage.ContainsKey(uiIndex))
            {
                if (sprite == EnumSpriteIndex.None)
                {
                    allUiImage[uiIndex].Active = (false);
                }
                else
                {
                    allUiImage[uiIndex].Sprite = GetSprite(sprite);
                    allUiImage[uiIndex].Active = (true);
                }
            }
        }

        //private void UpdateUiElementColor(NotificationParameter obj)
        //{
        //    EnumUiImageIndex uiIndex = obj.Get<EnumUiImageIndex>(EnumNotifParam.uiIndex);
        //    Color color = obj.Get<Color>(EnumNotifParam.color);
        //    SetColor(uiIndex, color);
        //}



        //private void PlayerChangeWeapon(NotificationParameter obj)
        //{
        //    UpdateHUD(obj);
        //}
        //private void PlayerStateChanged(NotificationParameter obj)
        //{
        //    UpdateHUD(obj);
        //}

        //private void UpdateHUD(NotificationParameter obj)
        //{
        //    //Ddebug.Log(" UpdateHHUD " + haveCurrentState);
        //    if (!haveCurrentState)
        //        return;
        //    var state = obj.Get<EnumAgentState>(EnumNotifParam.newState);
        //    var dic = new MStateChangeParameter();
        //    IWeapon weapon = null;
        //    switch (state)
        //    {
        //        case EnumAgentState.Aim:
        //        case EnumAgentState.Shoot:
        //        case EnumAgentState.CrouchAim:
        //        case EnumAgentState.CrouchShoot:
        //            weapon = obj.Get<IWeapon>(EnumNotifParam.currentWeapon);
        //            dic.Add(StateChangeParameter.currentWeapon, weapon);
        //            dic.Add(StateChangeParameter.shoot, state == EnumAgentState.Shoot);
        //            SwitchState(EnumHudState.Aim, dic);
        //            break;
        //        case EnumAgentState.Normal:
        //        case EnumAgentState.Crouch:
        //            weapon = obj.Get<IWeapon>(EnumNotifParam.currentWeapon);
        //            dic.Add(StateChangeParameter.currentWeapon, weapon);
        //            dic.Add(StateChangeParameter.shoot, false);
        //            SwitchState(EnumHudState.Normal, dic);
        //            break;
        //        default:
        //            SwitchState(EnumHudState.None, dic);
        //            break;
        //    }
        //    UpdateBullets(weapon);
        //}

        //private void UpdateBullets(IWeaponComponent weapon)
        //{
        //    if (CommonHelper.IsNull(weapon) || weapon.attckType == EnumAttckType.Melee)
        //        SetText(EnumUiTextIndex.BulletCounter, "");
        //    else
        //        SetText(EnumUiTextIndex.BulletCounter, weapon.CurrentClipSize + "/" + weapon.CurrentAmmoCount);
        //}



        //public void SwitchState(EnumHudState newState, MStateChangeParameter obj = null)
        //{
        //    if (currentState == newState)
        //        allState[currentState].UpdateState(obj);
        //    else
        //        ChangeState(newState, obj);
        //}

        //protected void ChangeState(EnumHudState newState, MStateChangeParameter param = null)
        //{
        //    //Logger.Log(" ChangeState currentState "+ currentState + " newState " + newState);
        //    if (currentState != newState && allState.ContainsKey(newState))
        //    {
        //        if (allState.ContainsKey(currentState))
        //        {
        //            allState[currentState].ExitState(new MStateChangeParameter(StateChangeParameter.newState, newState));
        //        }

        //        haveCurrentState = allState.ContainsKey(newState);
        //        if (haveCurrentState)
        //        {
        //            currentState = newState;
        //            allState[currentState].EnterState(param);
        //        }
        //        else
        //        {
        //            CLog.Break(name + " has no " + newState + " state ");
        //        }
        //    }
        //}


        public Quaternion WorldToCanvasRotation(Vector3 position)
        {
            Vector3 relativePos = position - Cam.Position;
            Quaternion rotation = Quaternion.LookRotation(relativePos);
            float rot = Quaternion.Angle(rotation, Cam.LocalRotation);
            if (Vector3.Dot(Cam.Right, relativePos) > 0f)
                rot = -rot;
            return Quaternion.Euler(0, 0, rot);
        }

        public Vector2 WorldToCanvasPosition(RectTransform canvas, Vector3 position)
        {
            //Vector position (percentage from 0 to 1) considering camera size.
            //For example (0,0) is lower left, middle is (0.5,0.5)
            Vector2 temp = Cam.WorldToViewportPoint(position);

            //Calculate position considering our percentage, using our canvas size
            //So if canvas size is (1100,500), and percentage is (0.5,0.5), current value will be (550,250)
            temp.x *= canvas.sizeDelta.x;
            temp.y *= canvas.sizeDelta.y;
            //The result is ready, but, this result is correct if canvas recttransform pivot is 0,0 - left lower corner.
            //But in reality its middle (0.5,0.5) by default, so we remove the amount considering cavnas rectransform pivot.
            //We could multiply with constant 0.5, but we will actually read the value, so if custom rect transform is passed(with custom pivot) , 
            //returned value will still be correct.

            temp.x -= canvas.sizeDelta.x * canvas.pivot.x;
            temp.y -= canvas.sizeDelta.y * canvas.pivot.y;

            //Debug.Log(" 1 position " + position + " temp " + temp + " WorldToScreenPoint " + camera.WorldToScreenPoint(position));


            return temp;
        }
        //private Vector2 WorldToCanvasPosition(Vector3 position)
        //{
        //    //Vector position (percentage from 0 to 1) considering camera size.
        //    //For example (0,0) is lower left, middle is (0.5,0.5)
        //    Vector2 temp = cam.WorldToViewportPoint(position);


        //    return temp;
        //}


        //public static Vector3 WorldToCanvasPosition(RectTransform canvasRect, Vector3 worldPosition, Camera camera)
        //{
        //    var viewportPosition = camera.WorldToViewportPoint(worldPosition);
        //    var centerBasedViewPortPosition = viewportPosition - new Vector3(0.5f, 0.5f, 0);
        //    return Vector3.Scale(centerBasedViewPortPosition, canvasRect.sizeDelta);
        //}



        public void SetShadowCrosshair(Vector3 pos)
        {
            if (selectedCrosshair != EnumCrosshairType.None)
            {
                allShadowCrosshairs[selectedCrosshair].Active = true;
                allShadowCrosshairs[selectedCrosshair].Position = WorldToCanvasPosition(allShadowCrosshairs[selectedCrosshair].canvas.Trans, pos);
            }

        }
        public void CLearShadowCrosshair()
        {
            if (selectedCrosshair != EnumCrosshairType.None)
                allShadowCrosshairs[selectedCrosshair].Active = false;
        }

        public void SetColor(EnumUiColor color)
        {
            if (selectedCrosshair != EnumCrosshairType.None)
                allCrosshairs[selectedCrosshair].Color = colors[color];
        }

        public void SetColor(EnumUiTextIndex index, EnumUiColor color)
        {
            if (allUiText.ContainsKey(index))
            {
                allUiText[index].Color = colors[color];
            }
        }

        public void SetColor(EnumUiImageIndex index, EnumUiColor color)
        {
            if (allUiImage.ContainsKey(index))
            {
                allUiImage[index].Color = colors[color];
            }
        }



        public void SetBarValue(EnumUiBarIndex index, float value)
        {
            if (allUiBars.ContainsKey(index))
            {
                allUiBars[index].Value = value;
            }
        }

        public void SetEnable(EnumUiBarIndex index, bool value)
        {
            if (allUiBars.ContainsKey(index))
            {
                allUiBars[index].Active = value;
            }
        }

        public void BindWidgetToAgent(IAiPublic owner)
        {
            var last = injectedAgentWidget.FirstOrDefault(x => x.Active && x.OwnerId == owner.Id);
            if (CommonHelper.IsNull(last))
            {
                var free = injectedAgentWidget.FirstOrDefault(x => !x.Active);
                if (!CommonHelper.IsNull(free))
                {
                    free.Bind(owner);
                }
            }
            else
            {
                last.Refresh();
            }
        }

        public void BindWidgetComponent(IDamageableComponentPublic owner)
        {
            var last = injectedAgentComponentWidget.FirstOrDefault(x => x.Active && x.OwnerId == owner.Id);
            if (CommonHelper.IsNull(last))
            {
                var free = injectedAgentComponentWidget.FirstOrDefault(x => !x.Active);
                if (!CommonHelper.IsNull(free))
                {
                    free.Bind(owner);
                }
            }
            else
            {
                last.Refresh();
            }
        }

        int lastDamageTextRandomDirection;
        float lastDamageTextRequest;
        public void BindDamageText(IAiPublic owner, Vector3 position, EnumDamageType damageType, int damagePower, float damageMultiplier)
        {
            var free = injectedUiDamageTextWidget.FirstOrDefault(x => !x.Active);
            if (!CommonHelper.IsNull(free))
            {
                if (lastDamageTextRequest + 1 > TimeController.gameTime)
                {
                    lastDamageTextRandomDirection++;
                    if (lastDamageTextRandomDirection > 1)
                        lastDamageTextRandomDirection = -1;
                }
                else
                {
                    lastDamageTextRandomDirection = 0;
                }
                free.Bind(owner, position, GetSprite(damageType), damagePower, GetColor(damageMultiplier), lastDamageTextRandomDirection);
                lastDamageTextRequest = TimeController.gameTime;
            }
        }

        public Sprite GetSprite(EnumDamageType damageType)
        {
            EnumSpriteIndex res = damageType switch
            {
                EnumDamageType.Shock => EnumSpriteIndex.DamageShock,
                EnumDamageType.Health => EnumSpriteIndex.DamageHealth,
                EnumDamageType.Link => EnumSpriteIndex.DamageLink,
                _ => throw new System.NotImplementedException(),
            };
            return GetSprite(res);
        }

        public Color GetColor(float damageMultiplier)
        {
            Color res = damageMultiplier switch
            {
                >= 2 => colors[EnumUiColor.CriticalDamage],
                _ => colors[EnumUiColor.SimpleDamage],
            };
            return res;
        }

        public void BindIndicatorToDamage(Vector3 hitPoint)
        {
            var same = injectedDamageWidget.FirstOrDefault(x => x.Active && x.Distance(hitPoint) < 1);
            if (!CommonHelper.IsNull(same))
            {
                same.Refresh(hitPoint);
            }
            else
            {
                var free = injectedDamageWidget.FirstOrDefault(x => !x.Active);
                if (!CommonHelper.IsNull(free))
                {
                    free.Bind(hitPoint);
                }
            }
        }

        public void BindIndicatorToThreat(IAiPublic alive)
        {
            if (injectedThreatWidget.Count(x => x.Active && x.OwnerId == alive.Id) == 0)
            {
                var free = injectedThreatWidget.FirstOrDefault(x => !x.Active);
                if (!CommonHelper.IsNull(free))
                {
                    free.Bind(alive);
                }
            }
        }

        public void SetWeaponMenuEnable(bool val)
        {
            //if (val)
            //    Notification.Notify(EnumNotifType.SetWeaponSelectionEnable, new NotificationParameter(EnumNotifParam.value, true));
            //Debug.LogError(" SetWeaponMenuEnable " + val);
            if (val)
                injectedUiWeaponMenu.Show();
            else
                injectedUiWeaponMenu.Hide();
        }

        public void SetEnable(EnumUiImageIndex index, bool value)
        {
            if (allUiImage.ContainsKey(index))
            {
                allUiImage[index].Active = value;
            }
        }

        protected void Scan(IAiPublic scanTarget)
        {
            BindWidgetToAgent(scanTarget);
            foreach (var component in scanTarget.DamageMangerPublicData.GetAllDamageableComponent())
            {
                BindWidgetComponent(component);
            }
        }

        int lastTargetID;
        IAlive scanTarget;
        float scanHoldTime;
        public void Scan(float hold, int targetID)
        {
            if (hold > 0 && targetID != lastTargetID)
            {
                lastTargetID = targetID;
                var damageManager = levelManager.GetDamageManager(targetID);
                if (!CommonHelper.IsNull(damageManager))
                {
                    scanTarget = damageManager.GetOwner();
                }
            }
            if (!CommonHelper.IsNull(scanTarget))
            {
                scanHoldTime += TimeController.deltaTime;
            }
            else
            {
                scanHoldTime -= TimeController.deltaTime;
            }
            scanHoldTime = Mathf.Clamp01(scanHoldTime);
            SetEnable(EnumUiBarIndex.Scan, (scanHoldTime < 1 && scanHoldTime > 0));
            if (Mathf.Clamp01(scanHoldTime) == 1)
            {
                Scan(scanTarget as IAiPublic);
                scanHoldTime = 0;
                scanTarget = null;
                Notification.Notify(EnumNotifType.SetScanUiEnable, new NotificationParameter(EnumNotifParam.value, true));
            }
            else
            {
                SetBarValue(EnumUiBarIndex.Scan, scanHoldTime);
            }
        }
    }
}