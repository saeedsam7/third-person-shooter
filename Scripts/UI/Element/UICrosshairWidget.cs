
using RCPU.Attributes;
using UnityEngine;

namespace RCPU.UI
{
    [InjectToSystemArray(typeof(IUICrosshairWidget))]
    public class UICrosshairWidget : UiWidget, IUICrosshairWidget
    {
        [SerializeField] EnumCrosshairType index;

        protected Color color;
        protected float size;

        public EnumCrosshairType Index => index;
        public Color Color { get { return color; } set { SetColor(value); } }

        public float Size { get { return size; } set { SetSize(value); } }

        protected virtual void SetColor(Color c)
        {
            color = c;
        }
        protected virtual void SetSize(float r)
        {
            size = r;
        }
    }
}