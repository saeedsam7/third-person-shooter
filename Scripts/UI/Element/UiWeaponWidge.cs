﻿
using RCPU.Attributes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RCPU.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    [InjectToInstance(typeof(IUiWeaponWidge))]
    public class UiWeaponWidge : UiWidget, IUiWeaponWidge
    {
        [SerializeField] protected TMP_Text currentClip;
        [SerializeField] protected TMP_Text totalAmmo;
        [SerializeField] protected Image icon;
        [SerializeField] protected Image select;
        protected RectTransform trans;
        protected CanvasGroup canvasGroup;
        public string CurrentClip { set => currentClip.text = value; }
        public string TotalAmmo { set => totalAmmo.text = value; }
        public Sprite Icon { set => icon.sprite = value; }
        public bool Select { set => select.enabled = value; }
        public Vector2 AnchoredPosition { get => trans.anchoredPosition; set => trans.anchoredPosition = value; }
        public float Alpha { set => canvasGroup.alpha = value; }

        public override void Init()
        {
            base.Init();
            Select = false;
            canvasGroup = GetComponent<CanvasGroup>();
            trans = GetComponent<RectTransform>();
        }

    }
}