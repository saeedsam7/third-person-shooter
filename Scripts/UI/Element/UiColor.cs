﻿using System;
using UnityEngine;

namespace RCPU.UI
{
    [Serializable]
    public struct UiColor
    {
        public EnumUiColor index;
        public Color color;

    }
}
