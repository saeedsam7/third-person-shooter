using System;
using UnityEngine;

namespace RCPU.UI
{
    [Serializable]
    public class UiSpriteView : IUiSprite
    {
        public EnumSpriteIndex Index => index;

        public Sprite Element => element;

        [SerializeField] EnumSpriteIndex index;
        [SerializeField] Sprite element;
    }
}