﻿using RCPU.Attributes;
using RCPU.Helper;
using RCPU.InputSystem;
using RCPU.NotificationSystem;
using RCPU.Weapon;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.UI
{
    [InjectToSystem(typeof(IUiRadialMenu))]
    [RequireInput]
    public class UIRadialMenu : UiWidget, IUiRadialMenu
    {
        [InjectFromSystem] IUIManager UIManager { get; set; }
        [InjectFromInstance] IUiWeaponWidge[] InjectedIUiWeaponWidge { get; set; }

        private int pieceCount = 8;

        [Range(0.2f, 1f)][SerializeField] private float appearanceDuration = .3f;
        [Range(0.2f, 1f)][SerializeField] private float disappearanceDuration = .3f;
        [SerializeField] private float pieceDist = 180f;


        [SerializeField] private RectTransform arrow;

        [Range(0, 1)][SerializeField] protected float HoverAlpha = 1;
        [Range(0, 1)][SerializeField] protected float NotHoverAlpha = 0.3f;

        private Vector2[] pieceDirections;

        private float arrowRotationZ;
        private int prevSelectedIndex = -1;
        private int tempSelectedIndex = -1;
        private int selectedIndex = -1;

        float fadeInTImer = 0;
        float fadeOutTImer = 0;
        Vector2 pointer;
        Coroutine fadeIn;
        Coroutine fadeOut;
        EnumMenuState menuState;
        List<IWeaponUiData> allWeapons;

        public override void Init()
        {
            base.Init();
            menuState = EnumMenuState.None;
            pieceDirections = new Vector2[InjectedIUiWeaponWidge.Length];
            SetActive(false);
            Notification.Register(EnumNotifType.PlayerWeaponsUpdated, Id, OnPlayerWeaponsUpdated);
        }
        protected void OnPlayerWeaponsUpdated(NotificationParameter obj)
        {
            this.allWeapons = obj.Get<List<IWeaponUiData>>(EnumNotifParam.dataModel);
            InitWidgetsDirections();
        }

        private void InitWidgetsDirections()
        {
            pieceCount = allWeapons.Count;
            float angle = 360f / pieceCount;

            for (int i = 0; i < InjectedIUiWeaponWidge.Length; i++)
            {
                if (i < pieceCount)
                {
                    InjectedIUiWeaponWidge[i].Icon = UIManager.GetSprite(allWeapons[i].Icon);
                    InjectedIUiWeaponWidge[i].Active = true;
                    pieceDirections[i] = CommonHelper.GetPolarCoord(1f, angle * i);
                }
                else
                {
                    InjectedIUiWeaponWidge[i].Active = false;
                }
            }
            UpdatWidgetsData();
        }

        private void UpdatWidgetsData()
        {
            for (int i = 0; i < pieceCount; i++)
            {
                InjectedIUiWeaponWidge[i].TotalAmmo = allWeapons[i].TotalAmmo.ToString();
                InjectedIUiWeaponWidge[i].CurrentClip = allWeapons[i].AmmoInClip.ToString();
            }
        }



        private void ResetAllWidgetsColors()
        {
            for (int i = 0; i < pieceCount; i++)
            {
                InjectedIUiWeaponWidge[i].Select = (selectedIndex == i);
                InjectedIUiWeaponWidge[i].Alpha = (tempSelectedIndex == i) ? HoverAlpha : NotHoverAlpha;
            }
        }


        private void SetArrowRotation()
        {
            arrow.eulerAngles = Vector3.forward * arrowRotationZ;
        }



        IEnumerator CoroutineFadeIn()
        {
            while (fadeInTImer < appearanceDuration)
            {
                float dist = fadeInTImer * pieceDist / appearanceDuration;

                for (int i = 0; i < pieceCount; i++)
                {
                    InjectedIUiWeaponWidge[i].AnchoredPosition = pieceDirections[i] * dist;
                }

                fadeInTImer += Time.deltaTime;

                if (fadeInTImer >= appearanceDuration)
                {
                    menuState = EnumMenuState.Show;
                }
                yield return new WaitForEndOfFrame();
            }

        }
        IEnumerator CoroutineFadeOut()
        {
            while (fadeOutTImer > 0)
            {
                float dist = fadeOutTImer * pieceDist / disappearanceDuration;

                for (int i = 0; i < pieceCount; i++)
                {
                    InjectedIUiWeaponWidge[i].AnchoredPosition = pieceDirections[i] * dist;
                }

                fadeOutTImer -= Time.deltaTime;
                if (fadeOutTImer <= 0)
                {
                    SetActive(false);
                    menuState = EnumMenuState.None;
                }
                yield return new WaitForEndOfFrame();
            }
        }

        [InputRegisterAxis(EnumInputPriority.UI, EnumUnityEvent.Update, "IsActive", EnumGameOveralState.InGame, EnumInGameUiState.WeaponSelection)]
        private void UpdateMenu(float[] axis, bool[] btn)
        {
            pointer.x += axis[(int)EnumAxis.AimX] * TimeController.deltaTime;
            pointer.y += axis[(int)EnumAxis.AimY] * TimeController.deltaTime;
            pointer.x = Mathf.Clamp(pointer.x, -pieceDist, pieceDist);
            pointer.y = Mathf.Clamp(pointer.y, -pieceDist, pieceDist);


            var angle = CommonHelper.GetAngleFromPolarCoord(pointer);
            arrowRotationZ = -angle;
            //showArrow = true;
            float fIndex = (angle / 360f) * pieceCount;
            tempSelectedIndex = Mathf.RoundToInt(fIndex) % pieceCount;


            if (prevSelectedIndex != tempSelectedIndex)
                ResetAllWidgetsColors();

            SetArrowRotation();
            prevSelectedIndex = tempSelectedIndex;

        }

        bool IsActive()
        {
            return (menuState == EnumMenuState.Show);
        }

        [InputRegisterButtonDown(EnumInputPriority.UI, EnumUnityEvent.Update, "IsActive", EnumGameOveralState.InGame, EnumInGameUiState.WeaponSelection, EnumButton.UiSelect)]
        public void Select()
        {
            selectedIndex = tempSelectedIndex;
            ResetAllWidgetsColors();
            if (selectedIndex >= 0)
                Notification.Notify(EnumNotifType.WeaponSwitchByMenu, new NotificationParameter(EnumNotifParam.slotIndex, allWeapons[selectedIndex].WeaponSlotIndex));
            else
                Notification.Notify(EnumNotifType.WeaponSwitchByMenu, new NotificationParameter(EnumNotifParam.slotIndex, EnumWeaponSlotIndex.None));
            Hide();
        }

        public void Hide()
        {
            StopFading();
            menuState = EnumMenuState.FadeOut;
            fadeOutTImer = appearanceDuration;
            fadeOut = StartCoroutine(CoroutineFadeOut());
            Notification.Notify(EnumNotifType.SetWeaponSelectionEnable, new NotificationParameter(EnumNotifParam.value, false));

        }

        void StopFading()
        {
            if (menuState == EnumMenuState.FadeIn)
                StopCoroutine(fadeIn);
            if (menuState == EnumMenuState.FadeOut)
                StopCoroutine(fadeOut);
        }

        public void Show()
        {
            pointer = Vector2.zero;
            SetActive(true);
            ResetAllWidgetsColors();
            UpdatWidgetsData();
            StopFading();
            fadeInTImer = 0;
            menuState = EnumMenuState.FadeIn;
            fadeIn = StartCoroutine(CoroutineFadeIn());
            Notification.Notify(EnumNotifType.SetWeaponSelectionEnable, new NotificationParameter(EnumNotifParam.value, true));
        }

        protected override void SetActive(bool val)
        {
            base.SetActive(val);
            arrow.gameObject.SetActive(val);
        }
    }
}