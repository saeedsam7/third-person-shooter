﻿
using RCPU.Agent;
using RCPU.Attributes;
using RCPU.NotificationSystem;
using UnityEngine;
using UnityEngine.UI;

namespace RCPU.UI
{
    [InjectToSystemArray(typeof(IUiThreatWidget))]
    public class UiThreatWidget : UiWidget, IUiThreatWidget
    {
        [InjectFromSystem] IUIManager UIManager { get; set; }
        public int OwnerId => owner.Id;

        [SerializeField] protected CanvasGroup canvasGroup;
        [SerializeField] protected AnimationCurve fadeOutCurve;
        [SerializeField] protected Image[] images;
        [SerializeField] public Color[] awareStateColor;

        protected static float FadeOutDuration = .3f;
        protected RectTransform trans;
        protected float createTime;
        protected IAiPublic owner;
        protected bool fadeOut;
        protected float fadeOutTime;

        public override void Init()
        {
            base.Init();
            Active = false;
            fadeOut = false;
            trans = GetComponent<RectTransform>();
        }
        protected void SetAlpha(float alpha)
        {
            canvasGroup.alpha = alpha;
        }

        public void OnStateChanged(NotificationParameter param)
        {
            if (!fadeOut)
            {
                EnumAwareState awareState = param.Get<EnumAwareState>(EnumNotifParam.newState);
                UpdateColor(owner.SuspiciousLevel);
                if (awareState != EnumAwareState.Cautious)
                {
                    FadeOut();
                }
            }
        }

        public void OnOwnerDied(NotificationParameter param)
        {
            if (!fadeOut)
            {
                UpdateColor(0); 
                FadeOut();
            }
        }

        protected void FadeOut()
        {
            fadeOutTime = TimeController.gameTime;
            fadeOut = true;
        }

        public void Bind(IAiPublic owner)
        {
            fadeOut = false;
            createTime = TimeController.gameTime;
            this.owner = owner;
            Active = true;
            SetAlpha(1);
            trans.localRotation = UIManager.WorldToCanvasRotation(this.owner.Position);
            UpdateColor(owner.SuspiciousLevel); 
            Notification.RegisterInstance(OwnerId, EnumInstanceNotifType.AwareStateChanged, Id, OnStateChanged);
            Notification.RegisterInstance(OwnerId, EnumInstanceNotifType.OwnerDied, Id, OnOwnerDied);
        }

        void UpdateColor(float value)
        {
            for (int i = 0; i < images.Length; i++)
            {
                images[i].fillAmount = Mathf.Lerp(images[i].fillAmount, value, TimeController.GetNormilizeTime(2f));
                if (value < .5f)
                    images[i].color = Color.Lerp(awareStateColor[(int)EnumAwareState.Normal], awareStateColor[(int)EnumAwareState.Cautious], value);
                else
                    images[i].color = Color.Lerp(awareStateColor[(int)EnumAwareState.Cautious], awareStateColor[(int)EnumAwareState.Offensive], value);
            }
          

        }

        private void Update()
        {
            trans.localRotation = Quaternion.Lerp(trans.localRotation, UIManager.WorldToCanvasRotation(this.owner.Position), TimeController.GetNormilizeTime(3));

            if (fadeOut)
            {
                float alpha = Mathf.Lerp(1, 0, fadeOutTime / FadeOutDuration);
                fadeOutTime += TimeController.deltaTime;
                if (alpha == 0)
                {
                    Deactivate();
                }
                else
                {
                    SetAlpha(alpha);
                }
            }
            else
            { 
                UpdateColor(owner.SuspiciousLevel); 
                if (owner.SuspiciousLevel == 0)
                {
                    Deactivate();
                }
                else
                {
                    SetAlpha(1);
                }
            }
        }

        void Deactivate()
        {
            Active = (false);
            Notification.UnRegisterInstance(OwnerId, EnumInstanceNotifType.AwareStateChanged, Id);
            Notification.UnRegisterInstance(OwnerId, EnumInstanceNotifType.OwnerDied, Id);
        }


    }
}
