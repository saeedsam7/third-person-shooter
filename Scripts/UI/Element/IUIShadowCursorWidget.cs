﻿
using UnityEngine;

namespace RCPU.UI
{
    public interface IUIShadowCrosshairWidget : IUiWidget
    {
        EnumCrosshairType Index { get; }
        Vector2 Position { get; set; }
    }
}
