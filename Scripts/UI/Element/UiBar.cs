﻿
using RCPU.Attributes;
using UnityEngine;
using UnityEngine.UI;

namespace RCPU.UI
{
    [RequireComponent(typeof(Image))]
    [InjectToSystemArray(typeof(IUiBar))]
    public class UiBar : UiElement, IUiBar
    {
        public EnumUiBarIndex Index => index;
        public override bool Active { get { return element.enabled; } set { element.enabled = value; } }
        public float Value { get => element.fillAmount; set => element.fillAmount = Mathf.Clamp(value, 0f, 1f); }

        public EnumUiBarIndex index;
        Image element;

        public override void Init()
        {
            base.Init();
            element = GetComponent<Image>();
        }


    }
}