
using RCPU.Attributes;
using UnityEngine;

namespace RCPU.UI
{
    [InitOrder(EnumInitOrder.UiElement)]
    public class UiElement : BaseObject, IUiElement
    {
        [InjectFromInstance] public IUiCanvas canvas { get; protected set; }
        public virtual bool Active { get; set; }

        public virtual void Init()
        {
            this.enabled = false;
        }

        public virtual void SetPosition(Vector3 position)
        {
        }

    }
}