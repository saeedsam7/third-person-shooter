﻿

using RCPU.Attributes;
using UnityEngine;

namespace RCPU.UI
{
    [InjectToSystemArray(typeof(IUIShadowCrosshairWidget))]
    public class UIShadowCrosshairWidget : UiWidget, IUIShadowCrosshairWidget
    {
        [SerializeField] EnumCrosshairType index;
        RectTransform trans;

        public override void Init()
        {
            base.Init();
            trans = transform as RectTransform;
            Active = false;
        }

        public EnumCrosshairType Index => index;

        public Vector2 Position { get => trans.anchoredPosition; set => trans.anchoredPosition = value; }
    }
}