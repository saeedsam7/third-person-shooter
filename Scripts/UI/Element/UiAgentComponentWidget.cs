﻿using RCPU.Attributes;
using RCPU.Component;
using RCPU.Damage;
using RCPU.NotificationSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RCPU.UI
{
    [InjectToSystemArray(typeof(IUiAiComponentWidget))]
    [RequireComponent(typeof(CanvasGroup))]
    public class UiAgentComponentWidget : UiWidget, IUiAiComponentWidget
    {

        [InjectFromSystem] IUIManager UIManager { get; set; }
        public int OwnerId => component.Id;

        protected static float FadeOutDuration = 5f;

        [SerializeField] protected RectTransform parent;
        [SerializeField] protected Image imgHealthBar;
        [SerializeField] protected Image imgShockBar;
        [SerializeField] protected Image imgLinkBar;
        [SerializeField] protected TMP_Text txtName;
        [SerializeField] protected TMP_Text txtStatus;
        [SerializeField] protected CanvasGroup canvasGroup;

        protected float createTime;
        [SerializeField] protected AnimationCurve fadeOutCurve;

        protected IDamageableComponentPublic component;
        public override void Init()
        {
            base.Init();
            Active = false;
        }

        protected void SetAlpha(float alpha)
        {
            canvasGroup.alpha = alpha;
        }


        public void Bind(IDamageableComponentPublic component)
        {
            createTime = TimeController.gameTime;
            this.component = component;
            txtName.text = component.ComponentType.ToString();
            Refresh();
            Active = true;
            Notification.RegisterInstance(OwnerId, EnumInstanceNotifType.HealthAmountHasChanged, Id, OnHealthHasChanged);

        }
        public void OnHealthHasChanged(NotificationParameter param)
        {
            Refresh();
        }

        public void Refresh()
        {
            createTime = TimeController.gameTime;
            txtStatus.text = "";
            imgShockBar.fillAmount = component.GetCurrentCapacity(EnumDamageType.Shock) / component.GetMaxCapacity(EnumDamageType.Shock);
            imgShockBar.enabled = (imgShockBar.fillAmount >= 0);
            imgHealthBar.fillAmount = component.GetCurrentCapacity(EnumDamageType.Health) / component.GetMaxCapacity(EnumDamageType.Health);
        }

        //public void OnHealthHasChanged(NotificationParameter param)
        //{
        //    txtStatus.text = owner.CurrentDecision.decision.ToString();
        //    imgShieldBar.fillAmount = param.Get<float>(EnumNotifParam.shield) / param.Get<float>(EnumNotifParam.shieldCap);
        //    imgShieldBar.enabled = (imgShieldBar.fillAmount > 0);
        //    imgHealthBar.fillAmount = param.Get<float>(EnumNotifParam.health) / param.Get<float>(EnumNotifParam.healthCap);
        //}

        private void Update()
        {
            float time = Mathf.Clamp((TimeController.gameTime - createTime) / FadeOutDuration, 0, 1f);
            float alpha = fadeOutCurve.Evaluate(time);
            //Debug.LogError(" time " + time + " alpha " + alpha);


            //if (imgHealthBar.fillAmount <= 0)
            //    alpha = 0;
            parent.anchoredPosition = UIManager.WorldToCanvasPosition(canvas.Trans, component.WidgetPostion); //WorldToCanvasPosition(can, first.GetWidgetPos(), uiCamera);  // WorldToCanvasPosition(can, can.transform as RectTransform, uiCamera, first.GetWidgetPos());  // GetCameraPos( first.GetWidgetPos())  ;  
                                                                                                              //LookAtCamera(second.parent);
            if (time == 1)
            {
                Active = (false);
                Notification.UnRegisterInstance(OwnerId, EnumInstanceNotifType.HealthAmountHasChanged, Id);
                component = null;
            }
            else
            {
                SetAlpha(alpha);
            }
        }


    }
}