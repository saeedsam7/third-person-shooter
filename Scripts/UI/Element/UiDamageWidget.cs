
using RCPU.Attributes;
using RCPU.Helper;
using UnityEngine;

namespace RCPU.UI
{
    [InjectToSystemArray(typeof(IUiDamageWidget))]
    public class UiDamageWidget : UiWidget, IUiDamageWidget
    {
        [InjectFromSystem] IUIManager UIManager { get; set; }


        //public bool IsActive => throw new System.NotImplementedException();
        [SerializeField] protected CanvasGroup canvasGroup;
        [SerializeField] protected AnimationCurve fadeOutCurve;
        //[SerializeField] protected Image image;


        protected static float FadeOutDuration = 5f;
        protected RectTransform trans;
        protected float createTime;
        protected Vector3 target;

        public override void Init()
        {
            base.Init();
            Active = false;
            trans = GetComponent<RectTransform>();
        }
        protected void SetAlpha(float alpha)
        {
            canvasGroup.alpha = alpha;
        }

        public void Refresh(Vector3 target)
        {
            createTime = TimeController.gameTime;
            this.target = target;
            SetAlpha(1);
            //trans.localRotation = UIManager.WorldToCanvasRotation(this.target);
        }

        public void Bind(Vector3 target)
        {
            createTime = TimeController.gameTime;
            this.target = target;
            Active = true;
            SetAlpha(1);
            trans.localRotation = UIManager.WorldToCanvasRotation(this.target);
        }


        private void Update()
        {
            float time = Mathf.Clamp( (TimeController.gameTime - createTime) / FadeOutDuration,0,1f);
            float alpha = fadeOutCurve.Evaluate(time);

            trans.localRotation = Quaternion.Lerp(trans.localRotation, UIManager.WorldToCanvasRotation(this.target), TimeController.GetNormilizeTime(3));

            if (time == 1)
            {
                Active = (false);
            }
            else
            {
                SetAlpha(alpha);
            }
        }

        public float Distance(Vector3 position)
        {
            return CommonHelper.Distance(position, target);
        }
    }
}