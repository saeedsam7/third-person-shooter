using RCPU.Attributes;
using UnityEngine;

namespace RCPU.UI
{
    [InjectToSystemArray(typeof(IUiGameObject))]
    public class UiGameObjectView : UiElement, IUiGameObject
    {
        public override bool Active { get { return element.activeSelf; } set { element.SetActive(value); } }
        public EnumUiGameObjectIndex Index => index;
        public EnumUiGameObjectIndex index;
        GameObject element;
        public override void Init()
        {
            base.Init();
            element = gameObject;
        }

    }
}