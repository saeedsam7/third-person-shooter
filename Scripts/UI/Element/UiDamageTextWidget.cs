﻿using RCPU.Agent;
using RCPU.Attributes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RCPU.UI
{

    [InjectToSystemArray(typeof(IUiDamageTextWidget))]
    public class UiDamageTextWidget : UiWidget, IUiDamageTextWidget
    {
        [InjectFromSystem] IUIManager UIManager { get; set; }

        [SerializeField] TMP_Text text;
        [SerializeField] Image icon;
        [SerializeField] protected AnimationCurve fadeOutCurve;
        [SerializeField] protected CanvasGroup canvasGroup;

        [SerializeField] Transform childTransform;

        RectTransform parent;

        Vector3 offset;
        protected float createTime;
        protected IAiPublic owner;
        protected float moveSpeed = 350;
        protected static float FadeOutDuration = 1.3f;
        Vector3 moveDirection;

        public override void Init()
        {
            base.Init();
            parent = GetComponent<RectTransform>();
            Active = false;
        }

        public void Bind(IAiPublic owner, Vector3 position, Sprite damageType, int damagePower, Color damageColor, int randomizeXDirection)
        {
            this.owner = owner;
            childTransform.localPosition = Vector3.zero;
            text.text = damagePower.ToString();
            text.color = damageColor;
            icon.sprite = damageType;
            offset = position - owner.Position;
            createTime = TimeController.gameTime;
            moveDirection = new Vector3(randomizeXDirection * .5f, 1, 0) * moveSpeed;
            UpdateCpomponent();
            Active = true;
        }

        private void Update()
        {
            UpdateCpomponent();
        }

        void UpdateCpomponent()
        {
            float time = Mathf.Clamp((TimeController.gameTime - createTime) / FadeOutDuration, 0, 1f);
            float alpha = fadeOutCurve.Evaluate(time);
            parent.anchoredPosition = UIManager.WorldToCanvasPosition(canvas.Trans, owner.Position + offset);
            childTransform.localPosition += TimeController.deltaTime * moveDirection;
            if (time == 1)
            {
                Active = (false);
            }
            else
            {
                SetAlpha(alpha);
            }
        }

        protected void SetAlpha(float alpha)
        {
            canvasGroup.alpha = alpha;
        }
    }
}