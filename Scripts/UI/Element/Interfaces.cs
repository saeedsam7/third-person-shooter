﻿

using RCPU.Agent;
using RCPU.Component;
using UnityEngine;

namespace RCPU.UI
{
    public interface IUiElement : IInitializable
    {
        bool Active { get; set; }
        IUiCanvas canvas { get; }
        void SetPosition(Vector3 position);
    }

    public interface IUiImage : IUiElement
    {
        EnumUiImageIndex Index { get; }
        Color Color { get; set; }
        Sprite Sprite { get; set; }


    }

    public interface IUiGameObject : IUiElement
    {
        EnumUiGameObjectIndex Index { get; }

    }

    public interface IUiText : IUiElement
    {
        EnumUiTextIndex Index { get; }
        string Text { get; set; }
        Color Color { get; set; }
    }

    public interface IUiDamageTextWidget : IUiWidget
    {
        void Bind(IAiPublic owner, Vector3 offset, Sprite damageType, int damagePower, Color damageColor, int randomizeXDirection);
    }

    public interface IUi3DText : IUiElement
    {
        EnumUi3DTextIndex Index { get; }
        string Text { get; set; }
    }

    public interface IUiSprite
    {
        EnumSpriteIndex Index { get; }
        Sprite Element { get; }
    }

    public interface IUiWidget : IUiElement
    {

    }

    public interface IUiRadialMenu : IUiElement
    {
        void Show();
        void Hide();
    }

    public interface IUiAiWidget : IUiWidget
    {
        int OwnerId { get; }
        void Bind(IAiPublic owner);
        void Refresh();
    }

    public interface IUiAiComponentWidget : IUiWidget
    {
        int OwnerId { get; }
        void Bind(IDamageableComponentPublic owner);
        void Refresh();
    }

    public interface IUiPlayerWidget : IUiWidget
    {
        void UpdateHealth(float current, float max);
        void UpdateShield(float current, float max);
        void UpdateLevel(float current);
    }

    public interface IUiDamageWidget : IUiWidget
    {
        float Distance(Vector3 position);
        void Bind(Vector3 position);
        void Refresh(Vector3 position);
    }



    public interface IUiThreatWidget : IUiWidget
    {
        int OwnerId { get; }
        void Bind(IAiPublic alive);
    }

    public interface IUICrosshairWidget : IUiWidget
    {
        EnumCrosshairType Index { get; }
        Color Color { get; set; }

        float Size { get; set; }
    }

    public interface IUiBar : IUiWidget
    {
        EnumUiBarIndex Index { get; }
        float Value { get; set; }
    }

    public interface IUiWeaponWidge : IUiWidget
    {
        string CurrentClip { set; }
        string TotalAmmo { set; }
        Sprite Icon { set; }
        bool Select { set; }

        Vector2 AnchoredPosition { get; set; }
        float Alpha { set; }
    }

}
