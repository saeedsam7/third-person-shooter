
using RCPU.Attributes;
using TMPro;
using UnityEngine;

namespace RCPU.UI
{
    [RequireComponent(typeof(TMP_Text))]
    [InjectToSystemArray(typeof(IUi3DText))]
    public class Ui3DTextView : UiElement, IUi3DText
    {
        public EnumUi3DTextIndex Index => index;
        public override bool Active { get { return element.enabled; } set { element.enabled = value; } }
        public string Text { get { return element.text; } set { element.text = value; } }

        public EnumUi3DTextIndex index;
        TMP_Text element;

        public override void Init()
        {
            base.Init();
            element = GetComponent<TMP_Text>();
        }

    }
}