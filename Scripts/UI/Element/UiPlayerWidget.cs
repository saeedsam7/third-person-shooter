﻿using RCPU.Attributes;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace RCPU.UI
{
    [InjectToSystem(typeof(IUiPlayerWidget))]
    [RequireComponent(typeof(CanvasGroup))]
    public class UiPlayerWidget : UiWidget, IUiPlayerWidget
    {
        protected static float FadeOutDuration = 10f;
        public RectTransform health;
        public Image imgHealthBar;
        public RectTransform shield;
        public Image imgShieldBar;
        public TMP_Text txtLevel;
        public TMP_Text txtHealth;
        public TMP_Text txtShield;
        [SerializeField] protected CanvasGroup canvasGroup;

        protected float updateTime;
        [SerializeField] protected AnimationCurve fadeOutCurve;

        public override void Init()
        {
            base.Init();
            Active = false;
        }

        public void SetAlpha(float alpha)
        {
            canvasGroup.alpha = alpha;
        }



        private void Update()
        {
            float time = (TimeController.gameTime - updateTime) / FadeOutDuration;
            float alpha = fadeOutCurve.Evaluate(time);

            if (alpha <= 0)
            {
                Active = (false);
            }
            else
            {
                SetAlpha(alpha);
            }
        }

     

        public void UpdateHealth(float current, float max)
        {
            txtHealth.text = current + "/<color=#8E8E8E>" + max + "</color>";
            imgHealthBar.fillAmount = current / max;
            health.sizeDelta = new Vector2(Mathf.Max(150, max), health.sizeDelta.y);
            health.anchoredPosition = new Vector2(health.sizeDelta.x / 2f + 100f, health.anchoredPosition.y);
            ForceUpdate();
        }

        public void UpdateShield(float current, float max)
        {
            txtShield.text = current + "/<color=#8E8E8E>" + max + "</color>";
            imgShieldBar.fillAmount = current / max;
            shield.sizeDelta = new Vector2(Mathf.Max(150, max), shield.sizeDelta.y);
            shield.anchoredPosition = new Vector2(shield.sizeDelta.x / 2f + 100f, shield.anchoredPosition.y);
            ForceUpdate();
        }

        public void UpdateLevel(float current)
        {
            txtLevel.text = current.ToString();
            ForceUpdate();
        }

        public void ForceUpdate()
        {
            updateTime = TimeController.gameTime;
            Active = true;
        }
    }
}