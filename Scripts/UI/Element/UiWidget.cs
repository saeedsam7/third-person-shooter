namespace RCPU.UI
{
    public class UiWidget : UiElement
    {
        protected bool isActive;
        public override bool Active { get { return isActive; } set { SetActive(value); } }

        protected virtual void SetActive(bool val)
        {
            isActive = val;
            this.enabled = val;
            gameObject.SetActive(val);
        }


    }
}