
using RCPU.Attributes;
using TMPro;
using UnityEngine;

namespace RCPU.UI
{
    [RequireComponent(typeof(TMP_Text))]
    [InjectToSystemArray(typeof(IUiText))]
    public class UiTextView : UiElement, IUiText
    {
        public EnumUiTextIndex Index => index;
        public EnumUiTextIndex index;
        TMP_Text element;
        public override bool Active { get { return element.enabled; } set { element.enabled = value; } }
        public string Text { get { return element.text; } set { element.text = value; } }

        public Color Color { get { return element.color; } set { element.color = value; } }

        public override void Init()
        {
            base.Init();
            element = GetComponent<TMP_Text>();
        }

        public override void SetPosition(Vector3 position)
        {
            element.rectTransform.anchoredPosition = position;
        }
    }
}