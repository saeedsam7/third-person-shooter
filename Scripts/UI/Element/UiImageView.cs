
using RCPU.Attributes;
using UnityEngine;
using UnityEngine.UI;

namespace RCPU.UI
{
    [RequireComponent(typeof(Image))]
    [InjectToSystemArray(typeof(IUiImage))]
    public class UiImageView : UiElement, IUiImage
    {
        public EnumUiImageIndex Index => index;
        public EnumUiImageIndex index;
        Image element;
        public override bool Active { get { return element.enabled; } set { element.enabled = value; } }
        public Color Color { get { return element.color; } set { element.color = value; } }
        public Sprite Sprite { get { return element.sprite; } set { element.sprite = value; } }



        public override void Init()
        {
            base.Init();
            element = GetComponent<Image>();
        }

        public override void SetPosition(Vector3 position)
        {
            element.rectTransform.anchoredPosition = position;
        }
    }
}