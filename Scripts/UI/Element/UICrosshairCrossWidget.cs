using UnityEngine;
using UnityEngine.UI;

namespace RCPU.UI
{
    public class UICrosshairCrossWidget : UICrosshairWidget
    {
        [SerializeField] Image[] images;
        Vector3[] defaultPos;
        //[SerializeField] float coolDown;
        [SerializeField] float max;
        [SerializeField] float scale;
        public override void Init()
        {
            base.Init();
            defaultPos = new Vector3[images.Length];
            for (int i = 0; i < images.Length; i++)
            {
                defaultPos[i] = images[i].rectTransform.anchoredPosition3D;
            }
            Active = false;
        }

        protected override void SetColor(Color c)
        {
            color = c;
            for (int i = 0; i < images.Length; i++)
            {
                images[i].color = color;
            }
        }
        protected override void SetSize(float s)
        {
            size = Mathf.Clamp(s * scale, 0, max);
            SetPos(EnumCrossCrosshairIndex.Top, Vector3.up * size);
            SetPos(EnumCrossCrosshairIndex.Bottom, Vector3.down * size);
            SetPos(EnumCrossCrosshairIndex.Right, Vector3.right * size);
            SetPos(EnumCrossCrosshairIndex.Left, Vector3.left * size);
        }

        void SetPos(EnumCrossCrosshairIndex index, Vector3 offset)
        {
            images[(int)index].rectTransform.anchoredPosition3D = defaultPos[(int)index] + offset;
        }

        //void Up date()
        //{
        //    if (size > 0)
        //    {
        //        size -= coolDown * TimeController.deltaTime;
        //        SetRS(size);
        //    }
        //}
    }
}