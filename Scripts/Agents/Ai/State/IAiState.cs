﻿

using RCPU.Component;

namespace RCPU.Agent
{
    public interface IAiState : IAgentState
    {
        void GetHit();
        void NewTargetDetect();
        void TargetAwareStateChanged(IDetectedTarget target);
    }
}