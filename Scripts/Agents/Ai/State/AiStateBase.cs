﻿using RCPU.Attributes;
using RCPU.Component;
using RCPU.Helper;
using RCPU.NotificationSystem;
using RCPU.Patrol;
using RCPU.StateMachine;
using RCPU.Weapon;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Agent
{
    [InitOrder(EnumInitOrder.AgentState)]
    [InjectToInstance(typeof(IAiState))]
    public class AiStateBase : AgentState, IAiState
    {
        [InjectFromInstance] protected IAiDecisionMaker DecisionMaker { get; set; }
        [InjectFromSystem] protected ILevelManager LevelManager { get; set; }
        [InjectFromInstance] protected IWeaponManagerComponentAuto WeaponManager { get; set; }
        protected AiStateDataModel model;
        [InjectFromInstance] protected IAiPrivate owner { get; set; }
        [InjectFromSystem] protected IGizmosHelper Gizmos { get; set; }
        [InjectFromSystem] protected IPatrol patrolManager { get; set; }
        [InjectFromInstance] protected Dictionary<EnumDecision, Action> allDecisionActions { get; set; }
        [InjectFromInstance] protected ILocomotionNavmesh locomotion { get; set; }
        protected override ILocomotionComponent locomotionComponent => locomotion;

        protected override EnumAgentState State => model.State;
        protected override EnumPosState PosState => posState;
        protected EnumPosState posState;
        protected override EnumActivityState RollState => EnumActivityState.None;
        protected override AnimationParamValue EnterStateAnimationParamValue => model.enterStateAnimationParamValue;
        protected override AnimationParamValue ExitStateAnimationParamValue => model.exitStateAnimationParamValue;
        protected override AutoExitModel AutoExitModel => model.autoExitModel;
        protected override float EnterTime { get { return model.EnterTime; } set { model.EnterTime = value; } }
        protected override float EnterFrame { get { return model.EnterFrame; } set { model.EnterFrame = value; } }
        protected bool IsAimed { set; get; }

        protected bool CanReload() { return /*this.IsEnable &&*/ !IsReloading; }
        protected IPatrolGroup patrolGroup;
        protected int ppIndex;
        protected IDetectedTarget bestTarget;
        //protected MCoverResult coverResult;
        protected float MinAttackDistance => WeaponManager.MinAttackDistance;
        protected float MaxAttackDistance => WeaponManager.MaxAttackDistance;
        protected bool IsReloading { get; set; }
        //------------------------------------------------------------------------------------------------------------------- 
        public override void EnterState(StateChangeParameter param)
        {
            model = param.Get<AiStateDataModel>(EnumStateChangeParameter.model);
            //if (!model.hasDefindedLength)
            //{
            //    StateLength = float.MaxValue;
            //}

            param.Add(EnumStateChangeParameter.isNewState, true);
            base.EnterState(param);
            //allTargets = owner.GetTargets();


            InvokeStateEvents(model.enterStateEvents);
        }

        public override void ExitState(StateChangeParameter param)
        {
            base.ExitState(param);
            InvokeStateEvents(model.exitStateEvents);
        }

        [InjectStateAction(EnumAgentStateEvent.ExitAim)]
        protected void ExitAim()
        {
            WeaponManager.Aim(false);
        }

        [InjectStateAction(EnumAgentStateEvent.EnterPatrol)]
        protected void EnterPatrol()
        {
            patrolGroup = patrolManager.GetPatrolPoints(locomotion.MovementType, owner.Position);
            ppIndex = patrolGroup.GetClosestIndex(owner.Position);
        }
        [InjectStateAction(EnumAgentStateEvent.EnterReflexMode)]
        protected void EnterReflexMode()
        {
            owner.EnterReflexMode();
        }

        [InjectStateAction(EnumAgentStateEvent.ExitReload)]
        protected void ExitReload()
        {
            if (IsReloading)
            {
                IsReloading = false;
                Notification.NotifyInstance(owner.Id, EnumInstanceNotifType.ReloadComplete, new NotificationParameter(EnumNotifParam.Successful, false));
            }
        }


        public override void UpdateState()
        {
            Logger.Error(" decision " + owner.CurrentDecision.decision, 5);
            base.UpdateState();
            if (!CommonHelper.IsNull(bestTarget))
            {
                owner.Scan(bestTarget);

                if (bestTarget.LastSeen > 1)
                {
                    if (owner.CurrentDecision.decision == EnumDecision.None)
                        UpdateDecision();
                }
                Gizmos.DrawCube(bestTarget.LastPosition + Vector3.up, bestTarget.LastRotation, new Vector3(.2f, 2, .2f), CommonHelper.Color(Color.cyan, .25f));
                Gizmos.DrawCube(bestTarget.Target.PredictPosition(.5f) + Vector3.up, bestTarget.LastRotation, new Vector3(.2f, 2, .2f), CommonHelper.Color(Color.red, .25f));
            }

            if (owner.CurrentDecision.decision != EnumDecision.None && owner.IsAlive && !owner.IsShocked)
            {
                allDecisionActions[owner.CurrentDecision.decision]();
            }
        }

        public override void ChangeStateIf(EnumAgentState next)
        {
            owner.ChangeStateIf(State, next);
        }
        //-------------------------------------------------------------------------------------------------------------------
        public virtual void NewTargetDetect()
        {
            if (owner.IsAlive && !owner.IsShocked && owner.HasTarget())
            {
                bestTarget = GetBestTarget();
                UpdateDecision();
            }
        }

        public void GetHit()
        {
            if (owner.IsAlive && !owner.IsShocked && owner.HasTarget())
            {
                bestTarget = GetBestTarget();
                DecisionMaker.GetHit(bestTarget.Target);
                UpdateDecision();
            }
        }

        public void TargetAwareStateChanged(IDetectedTarget target)
        {
            UpdateDecision();
        }
        //-------------------------------------------------------------------------------------------------------------------
        protected void ApplyDecision(AiDecisionModel decisionModel)
        {
            if (/*decisionModel.decision != EnumDecision.None &&*/ decisionModel.decision != owner.CurrentDecision.decision)
            {
                Logger.Log(" ApplyDecision " + decisionModel.decision, EnumLogColor.Orange);
                owner.CurrentDecision = decisionModel;
                if (decisionModel.agentStateTarget != EnumAgentState.None && decisionModel.agentStateTarget != model.State)
                    ChangeStateIf(decisionModel.agentStateTarget);
            }
        }

        protected void UpdateDecision()
        {
            var decision = DecisionMaker.UpdateDecition(bestTarget);
            ApplyDecision(decision);
        }

        protected IDetectedTarget GetBestTarget()
        {
            return owner.GetTargets()[0];
        }

        protected void SetNextPatrolPoint()
        {
            //Logger.Log(" SetNextTarget ");
            ppIndex++;
            if (ppIndex >= patrolGroup.GetPoints(EnumPatrolPointType.Walk).Length)
                ppIndex = 0;
            locomotion.Move(patrolGroup.GetPoints(EnumPatrolPointType.Walk)[ppIndex]);
        }

        protected Coroutine coroutineReloading;
        protected IEnumerator CoroutineReload(float time)
        {
            //StateLength = time;
            IsReloading = true;

            while (time > 0)
            {
                yield return new WaitForEndOfFrame();
                time -= TimeController.deltaTime;
            }
            IsReloading = false;
            Notification.NotifyInstance(owner.Id, EnumInstanceNotifType.ReloadComplete, new NotificationParameter(EnumNotifParam.Successful, true));
            //ChangeStateIf(model.ReloadTargetState);
            UpdateDecision();
        }

        protected void TryShoot(bool keyDown)
        {
            if (WeaponManager.HasBullet && WeaponManager.TryFire(keyDown))
            {
                Shoot();
            }
            else
                TryReload();
        }

        protected void TryAttack(Vector3 targetPosition)
        {
            float distance = CommonHelper.Distance(owner.Position, targetPosition);
            if (distance < MaxAttackDistance    /* */)
            {
                owner.AimAt(targetPosition);
                //if (owner.AimAt(targetPosition))
                {
                    if (distance > MinAttackDistance)
                    {
                        TryShoot(true);
                    }
                    else
                    {
                        owner.CloseAttack();
                    }
                }
            }
        }

        //-------------------------------------------------------------------------------------------------------------------
        [InjectDecisionAction(EnumDecision.WalkAndShoot)]
        protected void WalkAndShoot()
        {
            AttackTarget(true);
        }
        [InjectDecisionAction(EnumDecision.CircleAroundAndShoot)]
        protected void CircleAroundAndShoot()
        {
            if (bestTarget.MustUpdateRing)
            {
                float distance = 5;
                float maxPoinsDistance = 3.5f;
                var points = NavMeshHelper.GetWalkablePointsOnCircle(bestTarget.LastPosition, distance, 16, locomotion.MovementNavmeshLayer);
                List<SortablePair<Vector3>> condidate = new List<SortablePair<Vector3>>();
                for (int i = 0; i < points.Count; i++)
                {
                    if (points[i].key > maxPoinsDistance)
                    {
                        break;
                    }
                    if (PhysicsHelper.Raycast(points[i].value, bestTarget.LastPosition - points[i].value, out RaycastHit hit, distance, PhysicsHelper.Layer_DamageReceiver_Environment))
                    {
                        //Debug.LogError(" Raycast " + CommonHelper.Distance(bestTarget.LastPosition, hit.point)+"  "+ hit.collider);
                        var target = LevelManager.GetDamageManager(hit.colliderInstanceID);
                        if (!CommonHelper.IsNull(owner) && owner.GetRelation(target.Family) == EnumRelation.Enemy)
                        {
                            Debug.DrawLine(bestTarget.LastPosition, points[i].value, Color.green);
                            condidate.Add(points[i]);
                        }
                        else
                            Debug.DrawLine(bestTarget.LastPosition, points[i].value, Color.red);
                    }
                    else
                    {
                        condidate.Add(points[i]);
                        Debug.DrawLine(bestTarget.LastPosition, points[i].value, Color.green);
                    }
                }
                bestTarget.Ring = condidate;
            }
            if (bestTarget.HasRing)
            {
                for (int i = 0; i < bestTarget.Ring.Count; i++)
                {
                    Debug.DrawLine(bestTarget.Ring[i].value, bestTarget.Ring[i].value + (Vector3.up * 2), Color.magenta, 1);
                }
                locomotion.Move(GetNextRingPosition(), bestTarget.LastPosition);
            }
            else
            {
                UpdateDecision();
                Debug.LogError(" GetNextRingPosition ");
            }
            //MoveShootToTarget(true, true);
        }

        Vector3 GetNextRingPosition()
        {
            List<SortablePair<Vector3>> distanceIndex = new List<SortablePair<Vector3>>();

            for (int i = 0; i < bestTarget.Ring.Count; i++)
            {
                distanceIndex.Add(new SortablePair<Vector3>(NavMeshHelper.DistanceOnNavmesh(bestTarget.Ring[i].value, owner.Position, locomotion.MovementNavmeshLayer), bestTarget.Ring[i].value));
            }
            distanceIndex.Sort();
            for (int i = 0; i < distanceIndex.Count; i++)
            {
                if (CommonHelper.SignedAngle(owner.Position, owner.Forward, distanceIndex[i].value, true) > 0 && distanceIndex[i].key > locomotion.StoppingDistance)
                    return distanceIndex[i].value;
            }
            Debug.LogError(" GetNextRingPosition ");
            return distanceIndex[0].value;
        }



        [InjectDecisionAction(EnumDecision.Reload)]
        protected void TryReload()
        {
            if (WeaponManager.CanMustReload() && CanReload())
            {
                if (WeaponManager.TryReload())
                {
                    coroutineReloading = StartCoroutine(CoroutineReload(WeaponManager.ReloadTime));
                }
            }

        }
        [InjectDecisionAction(EnumDecision.TakeCover)]
        protected void MoveToCover()
        {
            //TryAttack(bestTarget.Target.Center);

            //float dis = CommonHelper.Distance(owner.Position, coverResult.pos[0]);
            ////CLog.Log("  Cover " + dis);
            //if (dis < 1.5f)
            //{
            //    //CLog.Error("Take Cover ");
            //    locomotion.Stop();
            //}
            //else
            //{
            //    locomotion.Move(coverResult.pos[0]);
            //}
        }
        [InjectDecisionAction(EnumDecision.Shoot)]
        private void Shoot()
        {
            AttackTarget(false);
        }

        protected void AttackTarget(bool keepWalk)
        {
            if (!bestTarget.IsAlive || bestTarget.Lost)
            {
                UpdateDecision();
                return;
            }
            float dis = CommonHelper.Distance(owner.Position, bestTarget.Target.Position);
            if (bestTarget.TargetLastPositionVisited && bestTarget.LastSeen < 2)
            {
                float predictTime = .4f;
                var targetAngle = CommonHelper.Angle(transform, bestTarget.Target.Position, true);
                var predictAngle = CommonHelper.Angle(transform, bestTarget.Target.PredictPosition(predictTime), true);

                Logger.Error(" Distance " + dis.ToString("f0") + " targetAngle " + targetAngle.ToString("f0") + " predictAngle " + predictAngle.ToString("f0"), 4);

                var distancePredict = dis / 100f;
                if (targetAngle < 15)
                {
                    TryAttack(bestTarget.Target.PredictCenter(RandomHelper.Range(predictTime * (distancePredict + .0f), predictTime * (distancePredict + .5f))));
                }
                else
                    if (predictAngle < 15)
                {
                    TryAttack(bestTarget.Target.PredictCenter(RandomHelper.Range(predictTime * (distancePredict + .5f), predictTime * (distancePredict + 1.5f))));
                }

                if (predictAngle > 5)
                    locomotion.Rotate(bestTarget.Target.PredictPosition(predictTime));
            }
            else
            {
                GetCloserToTarget();
            }
            if (keepWalk)
            {
                if (dis > MaxAttackDistance)
                {
                    locomotion.Move(bestTarget.Target.Position);
                }
                else
                     if (dis < MinAttackDistance * 2f)
                {
                    locomotion.Stop();
                }
                else
                {
                    GetCloserToTarget();
                }
            }
        }

        [InjectDecisionAction(EnumDecision.BlindShoot)]
        private void BlindShoot()
        {
            BlindAttackTarget(false);
        }
        [InjectDecisionAction(EnumDecision.WalkAndBlindShoot)]
        protected void WalkAndBlindShoot()
        {
            BlindAttackTarget(true);
        }
        protected void BlindAttackTarget(bool keepWalk)
        {
            if (!bestTarget.IsAlive || bestTarget.Lost)
            {
                UpdateDecision();
                return;
            }

            float dis = CommonHelper.Distance(owner.Position, bestTarget.LastPosition);
            var targetAngle = CommonHelper.Angle(transform, bestTarget.LastPosition, true);
            if (targetAngle < 15)
            {
                TryAttack(bestTarget.LastPosition);
            }

            if (keepWalk)
            {
                if (dis < MinAttackDistance * 2f)
                {
                    locomotion.Stop();
                }
                else
                {
                    locomotion.Move(bestTarget.LastPosition, bestTarget.LastPosition);
                }
            }
            else
            {
                locomotion.Stop();
            }
        }

        [InjectDecisionAction(EnumDecision.SearchForTheTarget)]
        private void SearchForTheTarget()
        {
            float distance = 7;
            var points = NavMeshHelper.GetWalkablePointsOnCircle(bestTarget.LastPosition, distance, 16, locomotion.MovementNavmeshLayer);
            List<SortablePair<Vector3>> condidate = new List<SortablePair<Vector3>>();

            for (int i = 0; i < points.Count; i++)
            {
                if (PhysicsHelper.Raycast(points[i].value, bestTarget.LastPosition - points[i].value, out RaycastHit hit, distance, PhysicsHelper.Layer_DamageReceiver_Environment))
                {
                    //Debug.LogError(" Raycast " + CommonHelper.Distance(bestTarget.LastPosition, hit.point)+"  "+ hit.collider);
                    var target = LevelManager.GetDamageManager(hit.colliderInstanceID);
                    if (!CommonHelper.IsNull(target) && owner.GetRelation(target.Family) == EnumRelation.Enemy)
                    {
                        Debug.DrawLine(bestTarget.LastPosition, points[i].value, Color.green);
                        condidate.Add(new SortablePair<Vector3>(CommonHelper.Distance(bestTarget.Target.Position, points[i].value), points[i].value));
                    }
                    else
                        Debug.DrawLine(bestTarget.LastPosition, points[i].value, Color.red);
                }
                else
                {
                    condidate.Add(new SortablePair<Vector3>(CommonHelper.Distance(bestTarget.Target.Position, points[i].value) * 2f, points[i].value));
                    Debug.DrawLine(bestTarget.LastPosition, points[i].value, Color.green);
                }
            }
            if (condidate.Count > 0)
            {
                condidate.Sort();
                bestTarget.ReachInvestigationPosition = false;
                bestTarget.InvestigationPosition = condidate[0].value;
            }
            //    ApplyDecision(new AiDecisionModel(EnumDecision.MoveToInvestigationPosition, EnumAgentState.Suspicious));
            //}
            //else
            //{

            UpdateDecision();
            //}
        }

        [InjectDecisionAction(EnumDecision.MoveToInvestigationPosition)]
        private void MoveToInvestigationPosition()
        {
            var targetPosition = bestTarget.InvestigationPosition;
            float dis = CommonHelper.Distance(owner.Position, targetPosition);

            if (dis < locomotion.StoppingDistance)
            {
                if (!bestTarget.ReachInvestigationPosition)
                {
                    bestTarget.ReachInvestigationPosition = true;
                }
                //if (bestTarget.InvestigationTimer > 10)
                //{
                //    UpdateDecision();
                //    return;
                //}
                locomotion.Stop();
                UpdateDecision();

            }
            else
            {
                locomotion.Move(targetPosition, bestTarget.LastPosition);
                //owner.LookAt(targetPosition); 
            }
        }

        [InjectDecisionAction(EnumDecision.RotateToTarget)]
        protected void RotateToTarget()
        {
            if (!CommonHelper.IsNull(bestTarget) && bestTarget.IsAlive)
            {
                if (locomotion.Rotate(bestTarget.Target.Position) > 3)
                {
                    UpdateDecision();
                }
            }
        }

        [InjectDecisionAction(EnumDecision.Patrol)]
        protected void Patrol()
        {
            //CLog.Log(" patrolPoints " + patrolPoints);
            if (!CommonHelper.IsNull(patrolGroup) && patrolGroup.GetPoints(EnumPatrolPointType.Walk).Length > 0)
            {
                //if (TimeController.TimePassed(EnterTime + 10))
                //{
                //    Vector3 pos = patrolPoints.GetPoints(EnumPatrolPointType.IronMine)[0];

                //    if (CommonHelper.Distance(owner.Position, pos) < 2)
                //    {
                //        locomotion.Stop();
                //        posState = EnumPosState.Crawl;
                //        locomotion.UpdateAgentHeight(PosState, false);
                //    }
                //    else
                //    {
                //        locomotion.Move(pos);
                //    }
                //}
                //else
                {
                    if (CommonHelper.Distance(owner.Position, patrolGroup.GetPoints(EnumPatrolPointType.Walk)[ppIndex]) < 2)
                    {
                        SetNextPatrolPoint();
                    }
                    else
                    {
                        locomotion.Move(patrolGroup.GetPoints(EnumPatrolPointType.Walk)[ppIndex]);
                    }
                }
            }
        }

        [InjectDecisionAction(EnumDecision.GetCloserToTarget)]
        protected void GetCloserToTarget()
        {
            if (!CommonHelper.IsNull(bestTarget) && bestTarget.IsAlive)
            {
                var targetPosition = bestTarget.LastPosition;
                float dis = CommonHelper.Distance(owner.Position, targetPosition);

                float multiplier = .6f;
                if (bestTarget.Lost)
                    multiplier = .25f;

                if (dis < owner.ClearDetectDistance * multiplier || dis < MinAttackDistance * 2f)
                {
                    bestTarget.TargetLastPositionVisited = true;
                    locomotion.Stop();
                    UpdateDecision();
                }
                else
                {
                    bestTarget.TargetLastPositionVisited = false;
                    locomotion.Move(targetPosition);
                    //owner.LookAt(targetPosition); 
                }
            }
            else
            {
                locomotion.Stop();
                UpdateDecision();
            }
        }
    }
}