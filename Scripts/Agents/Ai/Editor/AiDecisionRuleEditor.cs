#if UNITY_EDITOR 
using RCPU.Agent;
using RCPU.Helper;
using UnityEditor;
using UnityEngine;

namespace RCPU.Editor
{
    [CustomEditor(typeof(AiDecisionRule_SO))]
    public class AiDecisionRule_SOEditor : UnityEditor.Editor
    {
        AiDecisionRule_SO obj = null;
        string[] awareStateFilter;
        int selectedAwareState;

        string[] startStateFilter;
        int selectedState;
        protected void OnEnable()
        {
            obj = (AiDecisionRule_SO)target;

            awareStateFilter = new string[CommonHelper.GetEnumLength(typeof(EnumAwareState)) + 1];
            awareStateFilter[0] = "All";
            var all = CommonHelper.GetEnumNames(typeof(EnumAwareState));
            for (int i = 0; i < all.Length; i++)
            {
                awareStateFilter[i + 1] = all[i];
            }

            startStateFilter = new string[CommonHelper.GetEnumLength(typeof(EnumDecision)) + 1];
            startStateFilter[0] = "All";
            all = CommonHelper.GetEnumNames(typeof(EnumDecision));
            for (int i = 0; i < all.Length; i++)
            {
                startStateFilter[i + 1] = all[i];
            }
        }

        public override void OnInspectorGUI()
        {
            selectedAwareState = EditorGUILayout.Popup(" Aware State", selectedAwareState, awareStateFilter);
            if (GUI.changed)
            {
            }

            selectedState = EditorGUILayout.Popup(" Start State", selectedState, startStateFilter);
            if (GUI.changed)
            {

            }
            EditorGUILayout.Space(100);

            //if (GUI.Button(new Rect(60, 10, 100, 30), "I am a button"))
            //{

            //}
            for (int i = 0; i < obj.rules.Length; i++)
            {
                bool awareVisible = selectedAwareState == 0 || ((EnumAwareState)(selectedAwareState - 1) == obj.rules[i].awareState);
                bool startStateVisible = selectedState == 0 || ((EnumDecision)(selectedState - 1) == obj.rules[i].startState);
                obj.rules[i].SetVisible(awareVisible && startStateVisible);
            }
            DrawDefaultInspector();
        }


    }

    [CustomPropertyDrawer(typeof(AiDecisionRule))]
    public class AiDecisionRuleEditor : PropertyDrawer
    {
        Color[] awareStateColor = { Color.green, Color.yellow, Color.red };
        //public override VisualElement CreatePropertyGUI(SerializedProperty property)
        //{
        //    // Create property container element.
        //    var container = new VisualElement();

        //    //    // Create property fields.
        //    //    SerializedProperty awareState = property.FindPropertyRelative("awareState");
        //    //    SerializedProperty startState = property.FindPropertyRelative("startState");
        //    //    SerializedProperty priority = property.FindPropertyRelative("priority");
        //    //    SerializedProperty conditions = property.FindPropertyRelative("conditions");
        //    //    SerializedProperty possibleDecision = property.FindPropertyRelative("possibleDecision");



        //    //    var awareStateField = new PropertyField(awareState);
        //    //    var startStateField = new PropertyField(startState);
        //    //    var priorityField = new PropertyField(priority);
        //    //    var conditionsField = new PropertyField(conditions);
        //    //    var possibleDecisionField = new PropertyField(possibleDecision);

        //    //    //var nameField = new PropertyField(property.FindPropertyRelative("name"), "Fancy Name");

        //    //    // Add fields to the container.

        //    //    container.Add(new Label(AddProperty(Color.white, priority.intValue) +
        //    //         " [" + AddProperty(awareStateColor[awareState.enumValueIndex], EenumString(awareState)) + "] IF ( " +
        //    //         MakeConditionsAndDecisionString(property.FindPropertyRelative("conditions"), property.FindPropertyRelative("possibleDecision"))
        //    //         ));
        //    //    //container.Add(nameField);
        //    //    container.Add(awareStateField);
        //    //    container.Add(startStateField);
        //    //    container.Add(priorityField);
        //    //    container.Add(conditionsField);
        //    //    container.Add(possibleDecisionField);

        //    return container;
        //}

        string AddProperty(Color color, object value)
        {
            return " <color=" + CommonHelper.ColorHex(color) + ">" + value.ToString() + "</color> ";
        }

        string EenumString(SerializedProperty property)
        {
            return property.enumNames[property.enumValueIndex];
        }

        string AddProperty(SerializedProperty property)
        {
            EnumAgentState value = (EnumAgentState)property.enumValueIndex;
            Color color = Color.white;
            switch (value)
            {
                case EnumAgentState.Attack: color = awareStateColor[2]; break;
                case EnumAgentState.Suspicious: color = awareStateColor[1]; break;
                case EnumAgentState.Patrol: case EnumAgentState.Normal: color = awareStateColor[0]; break;
            }

            return " <color=" + CommonHelper.ColorHex(color) + ">" + value.ToString() + "</color> ";
        }


        string MakeConditionsAndDecisionString(SerializedProperty conditions, SerializedProperty possibleDecision)
        {
            //Debug.Log("OnValidate"); 
            string ruleName = " ";
            for (int j = 0; j < conditions.arraySize; j++)
            {
                var element = conditions.GetArrayElementAtIndex(j);
                ruleName += (element.FindPropertyRelative("value").boolValue ? "" : " <color=" + CommonHelper.ColorHex(Color.red) + ">" + "! " + "</color>") + EenumString(element.FindPropertyRelative("condition"))
                    + (j == conditions.arraySize - 1 ? " )       <color=" + CommonHelper.ColorHex(Color.cyan) + "> => </color>       { " : "<color=" + CommonHelper.ColorHex(Color.gray) + "> & </color>");
            }
            for (int j = 0; j < possibleDecision.arraySize; j++)
            {
                var element = possibleDecision.GetArrayElementAtIndex(j);
                var chance = element.FindPropertyRelative("chance").floatValue;
                ruleName += EenumString(element.FindPropertyRelative("decision")) +
                    (element.FindPropertyRelative("agentStateTarget").enumValueIndex != (int)EnumAgentState.None ? " + [ State : " + AddProperty(element.FindPropertyRelative("agentStateTarget")) + " ] " : " ")
                    + (chance != 100 ? chance + "%" : "") + (j == possibleDecision.arraySize - 1 ? " } " : " | ");
            }
            return ruleName;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!property.FindPropertyRelative("visibleByFilter").boolValue)
                return;

            var labelPos = position;
            //labelPos.y = (EditorGUI.GetPropertyHeight(property) / -2f) + 10;


            GUIStyle labelStyle = new GUIStyle(GUI.skin.label);
            labelStyle.richText = true;

            var awareState = property.FindPropertyRelative("awareState");
            var startState = property.FindPropertyRelative("startState");

            string name = (AddProperty(Color.white, property.FindPropertyRelative("priority").intValue) +
                   " [" + AddProperty(awareStateColor[awareState.enumValueIndex], EenumString(awareState)) +
                   ((startState.intValue != (int)EnumDecision.None) ? "-" + AddProperty(awareStateColor[awareState.enumValueIndex], EenumString(startState)) : "") + "] IF ( " +
                   MakeConditionsAndDecisionString(property.FindPropertyRelative("conditions"), property.FindPropertyRelative("possibleDecision"))
                   );


            if (label.text.StartsWith("Element"))
                EditorGUI.LabelField(labelPos, name, labelStyle);

            label.text = "";
            EditorGUI.PropertyField(position, property, label, true);
        }


        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            //if (property.isExpanded)
            //    return EditorGUI.GetPropertyHeight(property) + 20f;
            return EditorGUI.GetPropertyHeight(property);
        }
    }
}
#endif
