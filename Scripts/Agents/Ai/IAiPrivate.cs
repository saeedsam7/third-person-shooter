﻿

using RCPU.Component;
using RCPU.Game;

namespace RCPU.Agent
{
    public interface IAiPrivate : IAgent, IAiPublic, IReflexCause
    {
        //int MovementNavmeshLayer { get; }
        float MaxDetectDistance { get; }
        float ClearDetectDistance { get; }
        //float MinChanceToSelectTarget { get; }
        float ShootAngle { get; }
        bool HasAnyDetectorComponent { get; }

        void Scan(IDetectedTarget target);
        IDetectedTarget[] GetTargets();
        bool HasTarget();
        void TargetDetect(IDetectedTarget detectedTarget, float increaseSpeed);
        void TargetAwareStateChanged(IDetectedTarget detectedTarget);
        void EnterReflexMode();
        //bool HasTarget(EnumAwareState state);
        //Vector3 GetTargetPos(EnumAwareState state);
    }
}