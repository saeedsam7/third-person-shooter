﻿using RCPU.Attributes;
using RCPU.Component;
using RCPU.Damage;
using RCPU.DependencyInjection;
using RCPU.Game;
using RCPU.Helper;
using RCPU.InputSystem;
using RCPU.NotificationSystem;
using RCPU.Pool;
using RCPU.Ragdoll;
using RCPU.StateMachine;
using RCPU.UI;
using RCPU.Weapon;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RCPU.Agent
{
    [InitOrder(EnumInitOrder.Agent)]
    [InjectToInstance(typeof(IAiPrivate))]
    [InjectToInstance(typeof(IAiPublic))]
    public class StateMachineAi : StateMachineAgent, IAiPrivate, IPoolable
    {

        protected Dictionary<EnumAgentState, AiStateDataModel> allStateModels;
        [InjectFromInstance] IWeaponManagerComponentAuto WeaponManager { get; set; }
        [InjectFromInstance] ILocomotionComponent Locomotion { get; set; }
        [InjectFromInstance] protected IAiState activeState { get; set; }
        [InjectFromInstance] protected IVisionDetector[] allDetectors { get; set; }
        [InjectFromInstance] protected IDamageManager DamageManager { get; set; }
        [InjectFromInstance] protected IRagdollRecovery RagdollRecovery { get; set; }

        [InjectFromSystem] protected IPool Pool { get; set; }

        //protected IUiAiWidget UiWidge { get; set; }
        [InjectFromSystem] protected IUIManager UIManager { get; set; }



        [Header("Model")][SerializeField] protected AiDataModel_SO model_SO;
        protected AiDataModel model;
        [SerializeField]
        public AiDecisionModel CurrentDecision
        {
            get { return currentDecision; }
            set
            {
                value.Select();
                currentDecision = debugCurrentDecision = value;
            }
        }
        [SerializeField] AiDecisionModel debugCurrentDecision;
        AiDecisionModel currentDecision;
        protected AiStateDataModel CurrentStateModel => allStateModels[CurrentState];
        public override int Level => model.Level;
        public override EnumFamily Family => model.family;
        //public override EnumMovementType MovementType => model.MovementType;
        public EnumAwareState AwareState { get; protected set; }
        //public int MovementNavmeshLayer => CommonHelper.GetNavmehsLayer(Locomotion.MovementType);
        public override bool IsInCover => currentDecision.decision == EnumDecision.TakeCover;
        public float MaxDetectDistance => maxDetectDistance[(int)AwareState];
        public float ClearDetectDistance => clearDetectDistance[(int)AwareState];
        float[] maxDetectDistance;
        float[] clearDetectDistance;
        //public float MinChanceToSelectTarget => CurrentStateModel.MinChanceToSelectTarget;

        public Vector3 WidgetPostion => Position + (Vector3.up * (Locomotion.Height + 1.5f));

        public EnumAgentType AgentType => model.AgentType;

        public float ShootAngle => model.ShootAngle;

        public bool HasAnyDetectorComponent => allDetectors.Length > 0;

        public float SuspiciousLevel { get; protected set; }

        Dictionary<int, IDetectedTarget> allDetection;
        IDetectedTarget[] sortedDetection;
        //Dictionary<int, EnumAwareState> detectedTargets;


        public override float ReflexTime => reflexTime;

        public IDamageMangerPublicData DamageMangerPublicData => DamageManager;

        public bool IsReady => CurrentState == EnumAgentState.Ready;

        protected float reflexTime;
        public void EnterReflexMode()
        {
            reflexTime = TimeController.gameTime;
            Notification.Notify(EnumNotifType.StartReflexMode, new NotificationParameter(EnumNotifParam.owner, this as IReflexCause));
        }


        protected override void OnOwnerShocked(NotificationParameter obj)
        {
            base.OnOwnerShocked(obj);
            var currentState = RagdollRecovery.GetCurrentState();
            activeRagdoll = Pool.ActivateAndGet<IRagdoll>(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.autoDestruct, EnumNotifParam.dataModel, EnumNotifParam.position, EnumNotifParam.rotation },
                                                                                            new object[] { Pool.GetEquivalentType(model.ragdollType), false, currentState, Position, Rotation }));
        }
        protected override void OnOwnerShockRestoredInitialy(NotificationParameter obj)
        {
            base.OnOwnerShockRestoredInitialy(obj);
            RagdollRecovery.Activate(activeRagdoll.GetCurrentState());
        }

        protected override void OnDamageReceived(NotificationParameter obj)
        {
            base.OnDamageReceived(obj);
            if (IsAlive)
            {
                var target = levelManager.GetAliveObject(obj.Get<int>(EnumNotifParam.ownerID));
                for (int i = 0; i < allDetectors.Length; i++)
                {
                    allDetectors[i].HitScan(target);
                }
                activeState.GetHit();
            }
        }


        public void Scan(IDetectedTarget target)
        {
            for (int i = 0; i < allDetectors.Length; i++)
            {
                allDetectors[i].Scan(target);
            }
            if (target.Visibility == 0f)
            {
                TargetLost(target.Id);
            }
            UpdateTargets();
        }

        protected override bool ChangeState(StateChangeParameter param)
        {
            EnumAgentState newState = param.Get<EnumAgentState>(EnumStateChangeParameter.newState);
            Logger.Log(" Ai " + CurrentState + " ---> " + newState + " ? " + (CurrentState != newState && allStateModels.ContainsKey(newState)) + " , currentDecision :" + currentDecision.decision, EnumLogColor.Orange);
            if (CurrentState != newState && allStateModels.ContainsKey(newState))
            {
                //Logger.Log(CurrentState + " -> " + newState, 0);
                param.Add(EnumStateChangeParameter.model, allStateModels[newState]);
                //hasCurrentState = true;
                if (CurrentState != EnumAgentState.Ready)
                {
                    activeState.ExitState(param);
                }
                CurrentState = newState;
                AwareState = allStateModels[newState].AwareState;

                Notification.NotifyInstance(Id, EnumInstanceNotifType.AwareStateChanged, new NotificationParameter(EnumNotifParam.newState, AwareState));
                if (AwareState != EnumAwareState.Normal)
                    UIManager.BindIndicatorToThreat(this);

                activeState.EnterState(param);
                return true;
            }
            else
            if (CurrentState != newState)
            {
                throw new Exception(name + " has no " + newState + " state " + " Current " + CurrentState + " allState.Count " + allStateModels.Count);
            }
            return false;
        }


        public override void Init()
        {
            base.Init();
            model = DInjector.Instantiate<AiDataModel_SO>(model_SO).model;
            allStateModels = new Dictionary<EnumAgentState, AiStateDataModel>();
            for (int i = 0; i < model_SO.states.Length; i++)
            {
                var temp = DInjector.Instantiate<AiStateDataModel_SO>(model_SO.states[i]).model;
                allStateModels.Add(temp.State, temp);
            }
            allDetection = new Dictionary<int, IDetectedTarget>();
            //detectedTargets = new Dictionary<int, EnumAwareState>();
            CurrentState = EnumAgentState.Ready;

            var len = CommonHelper.GetEnumLength(typeof(EnumAwareState));
            maxDetectDistance = new float[len];
            clearDetectDistance = new float[len];

            for (int j = 0; j < len; j++)
            {
                maxDetectDistance[j] = Mathf.NegativeInfinity;
                clearDetectDistance[j] = Mathf.NegativeInfinity;
            }
            for (int j = 0; j < len; j++)
            {
                for (int i = 0; i < allDetectors.Length; i++)
                {
                    if (maxDetectDistance[j] < allDetectors[i].GetMaxDetectDistance((EnumAwareState)j))
                        maxDetectDistance[j] = allDetectors[i].GetMaxDetectDistance((EnumAwareState)j);

                    if (clearDetectDistance[j] < allDetectors[i].GetClearDetectDistance((EnumAwareState)j))
                        clearDetectDistance[j] = allDetectors[i].GetClearDetectDistance((EnumAwareState)j);
                }
            }
        }



        protected override void Spawn()
        {
            base.Spawn();
            currentDecision = new AiDecisionModel(EnumDecision.Patrol, EnumAgentState.Patrol);
            WeaponManager.Select(EnumWeaponSlotIndex.Up);
            ChangeState(new StateChangeParameter(EnumStateChangeParameter.newState, EnumAgentState.Patrol));
            Notification.Register(EnumNotifType.PlayerDied, Id, OnPlayerDied);
        }

        private void OnPlayerDied(NotificationParameter obj)
        {
            int id = obj.Get<int>(EnumNotifParam.ownerID);
            TargetLost(id);
        }

        protected void TargetLost(int id)
        {
            if (allDetection.ContainsKey(id))
            {
                allDetection.Remove(id);
                UpdateTargets();
            }
        }

        public override void DeadVfx()
        {
            //if (agentModel.DestroyVfxType != EnumVfxType.None)
            Pool.Activate(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.dataModel, EnumNotifParam.position, EnumNotifParam.rotation },
                                                                                       new object[] { Pool.GetEquivalentType(model.DestroyVfxType), transform.position, transform.rotation }));
        }

        protected override void UpdateStateMachine()
        {
            base.UpdateStateMachine();
            debugEnterState = TimeController.gameTime - CurrentStateModel.EnterTime;
            activeState.UpdateState();
            //if (Input.GetKeyDown(KeyCode.F1))
            //{
            //    //Hit(); 
            //    //AimAt(Vector3.zero);
            //}
            //Debug.DrawLine(GetPos(), GetAimPosition(),Color.cyan);
        }

        protected override void LateUpdateStateMachine()
        {
            base.LateUpdateStateMachine();
            activeState.LateUpdateState();
        }


        //bool UpdateTarget(int targetId, EnumAwareState targetAwareState)
        //{
        //    bool update = false;
        //    if (allDetection.ContainsKey(targetId))
        //    {
        //        update = (detectedTargets[targetId] == targetAwareState);
        //        detectedTargets[targetId] = targetAwareState;
        //    }
        //    else
        //    {
        //        detectedTargets.Add(targetId, targetAwareState);
        //        update = true;
        //    }
        //    return update;
        //}
        public void TargetDetect(IDetectedTarget target, float increaseSpeed)
        {
            bool update = false;
            float lastVisibility = target.Visibility;
            if (allDetection.ContainsKey(target.Id))
            {
                lastVisibility = allDetection[target.Id].Visibility;
                allDetection[target.Id].Update(target, increaseSpeed);
            }
            else
            {
                update = true;
                allDetection.Add(target.Id, target);
            }


            if (update)
            {
                UpdateTargets();
                activeState.NewTargetDetect();
            }
            else
            {
                if ((allDetection[target.Id].Visibility >= GameSetting.OFFENSIVE_VISIBILITY && lastVisibility < GameSetting.OFFENSIVE_VISIBILITY) ||
               (allDetection[target.Id].Visibility >= GameSetting.SUSPICIOUS_VISIBILITY && lastVisibility < GameSetting.SUSPICIOUS_VISIBILITY))
                {
                    TargetAwareStateChanged(allDetection[target.Id]);
                }
            }
        }

        public void TargetAwareStateChanged(IDetectedTarget target)
        {
            UpdateTargets();
            activeState.TargetAwareStateChanged(target);
        }

        void UpdateTargets()
        {
            sortedDetection = allDetection.Values.ToArray();
            Array.Sort(sortedDetection);
            if (HasTarget())
                SuspiciousLevel = Mathf.Clamp(GetBestTarget().Visibility, 0, GameSetting.OFFENSIVE_VISIBILITY);
            else
                SuspiciousLevel = 0;


            Logger.Error(" SuspiciousLevel " + SuspiciousLevel.ToString("f2"), 6);
        }

        IDetectedTarget GetBestTarget()
        {
            if (HasTarget())
                return sortedDetection[0];
            return null;
        }

        public bool HasTarget()
        {
            return allDetection.Count > 0;
        }


        public IDetectedTarget[] GetTargets()
        {
            //List<IDetectedTarget> targets = new List<IDetectedTarget>();
            //if (detectedTargets.Count > 0)
            //{
            //    List<int> remove = new List<int>();
            //    foreach (var item in detectedTargets)
            //    {
            //        if (item.Value == state)
            //        {
            //            targets.Add(allDetection[item.Key]);
            //        }
            //    }
            //}
            return sortedDetection;
        }

        Vector3 aimTarger;
        public override void AimAt(Vector3 target)
        {
            aimTarger = target;
            if (weaponManager.HasWeapon)
                weaponManager.LookAt(target);

        }

        public override Vector3 GetAimPosition()
        {
            return aimTarger;
        }

        [InjectDecisionAction(EnumDecision.Die)]
        private void WaitForRecycle()
        {

        }

        protected override void OnDied(NotificationParameter obj)
        {
            base.OnDied(obj);
            currentDecision = new AiDecisionModel(EnumDecision.Die, EnumAgentState.Die);
            var currentState = RagdollRecovery.GetCurrentState();
            activeRagdoll = Pool.ActivateAndGet<IRagdoll>(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.autoDestruct, EnumNotifParam.dataModel, EnumNotifParam.position, EnumNotifParam.rotation },
                                                                                            new object[] { Pool.GetEquivalentType(model.ragdollType), false, currentState, Position, Rotation }));
        }

        public void Init(int index)
        {
            name += " - " + index;
            spawnByPool = true;
            //gameObject.SetActive(false);
        }

        public void Activate(NotificationParameter parameters)
        {
            //gameObject.SetActive(true);
            SetRootPosition(parameters.Get<Vector3>(EnumNotifParam.position));
            SetRotation(parameters.Get<Quaternion>(EnumNotifParam.rotation));
            Level = parameters.Get<int>(EnumNotifParam.level);
            this.Spawn();

        }
    }

    //public virtual Vector3 GetTargetPos(EnumAwareState state)
    //{
    //    bool has = false;
    //    if (detectedTargets.Count > 0)
    //    {
    //        List<int> remove = new List<int>();
    //        List<Tuple<>> remove = new List<int>();
    //        foreach (var item in detectedTargets)
    //        {
    //            if (item.Value == state)
    //            {
    //                if (allDetection[item.Key].IsAlive)
    //                {
    //                    has = true;
    //                }
    //                else
    //                {
    //                    remove.Add(item.Key);
    //                }

    //            }
    //        }
    //        for (int i = 0; i < remove.Count; i++)
    //        {
    //            detectedTargets.Remove(remove[i]);
    //            allDetection.Remove(remove[i]);
    //        }
    //    }
    //    return has;
    //}
    //public virtual bool HasTarget(EnumAwareState state)
    //{
    //    bool has = false;
    //    if (detectedTargets.Count > 0)
    //    {  
    //        List<int> remove = new List<int>();
    //        foreach (var item in detectedTargets)
    //        {
    //            if (item.Value == state)
    //            {
    //                if (allDetection[item.Key].IsAlive)
    //                {
    //                    has = true;
    //                }
    //                else
    //                {
    //                    remove.Add(item.Key);
    //                }

    //            }
    //        }
    //        for (int i = 0; i < remove.Count; i++)
    //        {
    //            detectedTargets.Remove(remove[i]);
    //            allDetection.Remove(remove[i]);
    //        } 
    //    }
    //    return has;
    //}
}
