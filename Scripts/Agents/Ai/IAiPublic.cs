﻿using RCPU.Damage;
using UnityEngine;

namespace RCPU.Agent
{

    public interface IAiPublic : IAlive
    {
        EnumAwareState AwareState { get; }
        AiDecisionModel CurrentDecision { get; set; }
        Vector3 WidgetPostion { get; }
        EnumAgentType AgentType { get; }
        float SuspiciousLevel { get; }
        IDamageMangerPublicData DamageMangerPublicData { get; }

        //void BindWidget(IUiAiWidget free);
        //void UnBindWidget();
    }
}