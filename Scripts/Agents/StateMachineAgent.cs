using RCPU.Attributes;
using RCPU.Component;
using RCPU.Helper;
using RCPU.InputSystem;
using RCPU.NotificationSystem;
using RCPU.StateMachine;
using RCPU.Trigger;
using RCPU.Vehicle;
using RCPU.Weapon;
using System;
using UnityEngine;

namespace RCPU.Agent
{
    [InitOrder(EnumInitOrder.Agent)]
    [InjectToInstance(typeof(IAgent))]
    public class StateMachineAgent : StateMachineAlive, IAgent
    {
        [InjectFromInstance] protected IAnimatorComponent anim { get; set; }
        [InjectFromInstance] protected IWeaponManagerComponentBase weaponManager { get; set; }


        [Header("Test & Debug Variables")]
        [SerializeField] protected bool drawGizmos;
        [SerializeField] protected EnumAgentState debugState;
        [SerializeField] protected float debugEnterState;


        public virtual EnumPosState PosState { get => throw new Exception(); }
        public EnumAgentState CurrentState { get { return debugState; } protected set { debugState = value; } }
        //public virtual EnumMovementType MovementType => throw new System.NotImplementedException();

        //protected ITriggerTeleportItem triggerTeleport;
        //protected ITriggerPickupItem triggerPickup;
        protected IVehicle vehicle;
        protected Coroutine coroutineCCCS;

        //float deltaY;

        protected bool spawnByPool = false;

        //public void ChangeRootOriantation(float moveShake, Quaternion rot)
        //{
        //    if (CommonHelper.IsNull(meshRoot))
        //        return;
        //    meshRoot.localPosition = new Vector3(meshRoot.localPosition.x, deltaY + (Mathf.Abs(moveShake) * .2f), meshRoot.localPosition.z);

        //    Vector3 current = meshRoot.eulerAngles;
        //    Vector3 from = new Vector3(rot.eulerAngles.x, current.y, rot.eulerAngles.z);
        //    Vector3 to = new Vector3(0, current.y, 0);

        //    current = CommonHelper.NormilizeAngle(current);
        //    from = CommonHelper.NormilizeAngle(from);
        //    to = CommonHelper.NormilizeAngle(to);

        //    meshRoot.eulerAngles = Vector3.Lerp((from + current) / 2f, to, .5f);
        //}

        protected void AutoSpawn(NotificationParameter obj)
        {
            if (!spawnByPool)
            {
                Spawn();
            }
        }

        public void GetOffVehicle()
        {
            vehicle.GetOff();
            ChangeStateIf(EnumAgentState.Riding, EnumAgentState.Normal);
            //if (HasComponent(EnumComponentType.Animator))
            anim.SetTrigger(EnumTriggerParam.GetOffVehicle);

        }
        public void GetOnVehicle(IVehicle vehicle)
        {
            this.vehicle = vehicle;
            //if (HasComponent(EnumComponentType.Animator))
            anim.SetTrigger(EnumTriggerParam.GetOnVehicle);
            ChangeStateIf(EnumAgentState.Normal, EnumAgentState.Riding);
        }


        //public MAnimationIk GetAnimationIk(string animName)
        //{
        //    return agentModel.animationIk.Find(x => x.model.animation.Equals(animName)).model;
        //}

        //public virtual void GotoCover(MTriggerCover trigger)
        //{
        //}

        //public void DisableTrigger(EnumTriggerType tType)
        //{
        //    switch (tType)
        //    {
        //        case EnumTriggerType.TeleportItem:
        //            if (!CommonHelper.IsNull(triggerTeleport))
        //                triggerTeleport.IsEnable = false;
        //            break;
        //    }
        //}

        public void EnableTrigger(EnumTriggerType tType)
        {
            switch (tType)
            {
                case EnumTriggerType.TeleportItem:
                    //if (CommonHelper.IsNull(triggerTeleport))
                    //    triggerTeleport = triggersManager.AddTelleportTrigger(Position, new MTeleportAgent(this) { });
                    //triggerTeleport.IsEnable = true;
                    break;
                case EnumTriggerType.PickupItem:
                    //if (CommonHelper.IsNull(triggerPickup))
                    //    triggerPickup = triggersManager.AddPickupItem(Position, new MPickupBullet(10, EnumSpriteIndex.BulletPistol01) { bulletType = EnumAmmoType.DamagePistol, weaponType = EnumWeaponType.Pistol });
                    break;
            }
        }

        //public bool Pickup(IPickupItem pu)
        //{
        //    switch (pu.PickupType)
        //    {
        //        case EnumPickupType.Ammo:
        //            //if (HasComponent(EnumComponentType.WeaponManager))
        //            return weaponManager.Pickup((IPickupAmmo)pu);
        //    }
        //    return false;
        //}

        public float GetSuiteNoise()
        {
            float suitNoisePower = 1;
            return (Visibility) + (anim.GetFloat(EnumFloatParam.Speed) * suitNoisePower);
        }

        //public bool CanTeleport(MTeleportItem teleportItem)
        //{
        //    return true;
        //}

        public virtual Vector3 GetAimPosition()
        {
            return Vector3.zero;
        }

        protected override void OnDied(NotificationParameter obj)
        {
            base.OnDied(obj);
            Notification.Notify(EnumNotifType.AgentDied, new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.ownerID, EnumNotifParam.agent }, new object[] { Id, this }));
            ChangeState(new StateChangeParameter(EnumStateChangeParameter.newState, EnumAgentState.Die));
        }


        //public virtual List<MDetectedTarget> UpdateSensors()
        //{
        //    return null;
        //}
        //public virtual MDetectedTarget UpdateSensorsOnTarget()
        //{
        //    return null;
        //}


        //public virtual float GetTargetDistance()
        //{
        //    return Mathf.Infinity;
        //}
        //public virtual void SetTarget(IAlive tgt, EnumComponentType detectorComponent)
        //{

        //}

        public virtual bool TryShoot(bool keyDown)
        {
            return false;
        }

        public virtual void CloseAttack()
        {

        }

        //public virtual MDetectedTarget UpdateBestTarget()
        //{
        //    return null;
        //}

        public virtual bool TryReload()
        {
            Debug.Log(" TryReload ");

            //if (HasComponent(EnumComponentType.WeaponManager))
            if (weaponManager.TryReload())
            {
                Debug.Log(" ChangeState Reload ");
                anim.SetFloat(EnumFloatParam.Multiplier_Rotation, weaponManager.ReloadSpeedMultiplier);
                //param.Add(StateChangeParameter.reloadAnimatorSpeed, model.AnimatorClipSpeedMultiplier);
                //param.Add(StateChangeParameter.reloadTime, model.ReloadTime);

                return true;
            }

            return false;
        }


        public virtual bool SwitchWeapon(EnumWeaponSlotIndex index)
        {
            //if (HasComponent(EnumComponentType.WeaponManager))
            //Logger.Log("1 SwitchWeapon "+ index);  
            var res = weaponManager.Select(index);
            if (res)
                anim.SetFloat(EnumFloatParam.Weapon, (int)weaponManager.WeaponType);
            return res;
        }


        public override void Init()
        {
            base.Init();
            Notification.Notify(EnumNotifType.AgentCreated, new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.ownerID, EnumNotifParam.agent }, new object[] { Id, this }));
            Notification.Register(EnumNotifType.GameInitializationDone, Id, AutoSpawn);
            IsEnable = false;
            gameObject.SetActive(false);
        }


        protected override void Spawn()
        {
            base.Spawn();
            gameObject.SetActive(true);
            IsEnable = true;
            //TODO Notify when Agent Spawned instead of Born()
            Notification.Notify(EnumNotifType.AgentSpawned, new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.ownerID, EnumNotifParam.agent }, new object[] { Id, this }));
            Notification.NotifyInstance(Id, EnumInstanceNotifType.OwnerSpawned, new NotificationParameter());
            //CAgentPool.AgentBorn(Id, this);
            //Health = HealthCap;
            //Emp = EmpCap;
            //Shield = ShieldCap;

            //if (!CommonHelper.IsNull(meshRoot))
            //    deltaY = meshRoot.localPosition.y;


        }


        void OnDrawGizmos()
        {
        }

        protected float SignedAngle(Vector3 target)
        {
            return CommonHelper.SignedAngle(NormalState.position, NormalState.forward, target, true);
        }

        public void LookAt(Vector3 target)
        {
            //Logger.Log(" 1 " + NormalState.eulerAngles.y + " SignedAngle(target) " + SignedAngle(target) + " target " + target); 
            CommonHelper.LookAt(NormalState, (target), true);
            //Logger.Log(" 2 " + NormalState.eulerAngles.y + " SignedAngle(target) " + SignedAngle(target) + " target " + target);
            // NormalState.LookAt(target,Vector3.up);
        }
        public void LerpLookAt(Vector3 target, float speed, float maxLimit)
        {
            //Logger.Log(" 1 " + NormalState.eulerAngles.y.ToString("f1") + " SignedAngle(target) " + SignedAngle(target).ToString("f1") + " target " + target + " speed " + (speed * TimeController.deltaTime)); 
            CommonHelper.LerpLookAt(NormalState, (target), true, Mathf.Clamp((speed * TimeController.deltaTime), 0, maxLimit));
            //Logger.Log(" 2 " + NormalState.eulerAngles.y + " SignedAngle(target) " + SignedAngle(target) + " target " + target);
            // NormalState.LookAt(target,Vector3.up);
        }

        public virtual void Aim(bool value)
        {
        }

        public virtual void AimAt(Vector3 target)
        {
            LookAt(target);

        }

        public void ChangeStateIf(EnumAgentState current, EnumAgentState next)
        {
            //Logger.Log(" ChangeStateIf currentState " + currentState + " current " + current + " next " + next);
            if (CurrentState == current)
            {
                ChangeState(new StateChangeParameter(EnumStateChangeParameter.newState, next));
            }
        }

        protected virtual bool ChangeState(StateChangeParameter param)
        {
            return false;
        }

        protected virtual void Hit()
        {
            ChangeState(new StateChangeParameter(EnumStateChangeParameter.newState, EnumAgentState.Hit));
        }


        //private void OnTriggerEnter(Collider collider)
        //{
        //    if (HasComponent(EnumComponentType.Trigger))
        //        GetComponent<ITriggerComponent>(EnumComponentType.Trigger).OnTriggerEnter(collider.tag, collider.GetInstanceID());

        //}

        //private void OnTriggerExit(Collider collider)
        //{
        //    if (HasComponent(EnumComponentType.Trigger))
        //        GetComponent<ITriggerComponent>(EnumComponentType.Trigger).OnTriggerExit(collider.tag, collider.GetInstanceID());
        //}


        //public void ExecuteEvent(string s)
        //{
        //    if (HasComponent(EnumComponentType.Animator))
        //        GetComponent<IAnimatorComponent>(EnumComponentType.Animator).ExecuteEvent(s);

        //}

        public virtual void Teleport()
        {
        }

        public virtual void DeadVfx()
        {
        }

        //protected override void UpdateStateMachine()
        //{
        //    base.UpdateStateMachine(); 
        //}

    }
}