﻿
using RCPU.StateMachine;

namespace RCPU.Agent
{
    public interface IAgentState : IState
    {
        float NormalizedTime { get; }
        //EnumAgentState State { get; }
        //float Visibility { get; }
        //EnumMoveMethod MoveMethod { get; }
        //EnumPosState PosState { get; }
        //

        //EnumAgentState ShootTargetState { get; }
        //bool IsNormalState { get; }
        //EnumAgentState ChangePosTargetState { get; }
        //bool Aimed { get; }
        //EnumActivityState AimState { get; }

        //float GetVisibility();
        //void Push(NotificationParameter p);
        //void Init(IAnimatorComponent animatorController,  State_Agent model);

    }

}
