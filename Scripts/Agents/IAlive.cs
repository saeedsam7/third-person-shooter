using RCPU.StateMachine;
using UnityEngine;

namespace RCPU.Agent
{
    public interface IAlive : IStateMachine
    {
        Vector3 Position { get; }

        Vector3 Center { get; }
        Quaternion Rotation { get; }
        Vector3 Forward { get; }
        Vector3 Right { get; }
        Vector3 EulerAngles { get; }
        bool IsAlive { get; }
        bool IsShocked { get; }
        bool IsInCover { get; }
        int Level { get; }
        float Visibility { get; }
        EnumFamily Family { get; }

        EnumRelation GetRelation(EnumFamily targetFamily);
        Vector3 PredictPosition(float time);
        Vector3 PredictCenter(float time);
    }
}