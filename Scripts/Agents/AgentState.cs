﻿
using RCPU.Attributes;
using RCPU.Component;
using RCPU.Helper;
using RCPU.StateMachine;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Agent
{
    [InitOrder(EnumInitOrder.AgentState)]

    public class AgentState : StateBase, IAgentState
    {
        [InjectFromInstance] protected IAnimatorComponent anim { get; set; }

        [InjectFromInstance] protected Dictionary<EnumAgentStateEvent, Delegate> allStateEvents { get; set; }

        protected override bool IsEnable { get { return this.enabled /*&& (EnterFrame != TimeController.frameCount)*/; } set { this.enabled = value; } }
        protected virtual EnumAgentState State { get => throw new Exception(); }
        protected virtual EnumPosState PosState { get => throw new Exception(); }
        protected virtual EnumActivityState RollState { get => throw new Exception(); }
        protected virtual AnimationParamValue EnterStateAnimationParamValue { get => throw new Exception(); }
        protected virtual AnimationParamValue ExitStateAnimationParamValue { get => throw new Exception(); }
        protected virtual ILocomotionComponent locomotionComponent { get; }

        protected virtual float EnterTime { get => throw new Exception(); set { } }
        protected virtual float EnterFrame { get => throw new Exception(); set { } }
        protected virtual AutoExitModel AutoExitModel { get => throw new Exception(); }
        protected bool HasFloatAnimParameter { get { return EnterStateAnimationParamValue.floatParamValue.Length > 0; } }
        protected bool HasFloatCurveAnimParameter { get { return EnterStateAnimationParamValue.floatCurveParamValue.Length > 0; } }
        //protected virtual float StateLength { get; set; }
        public float NormalizedTime => (TimeController.gameTime - EnterTime);



        public override void EnterState(StateChangeParameter param)
        {
            base.EnterState(param);
            //if (model.ApplyRootMotion != EnumApplyRootMotion.DontChange)
            //{
            //    anim.ApplyRootMotion(model.ApplyRootMotion == EnumApplyRootMotion.True);
            //}

            EnterTime = TimeController.gameTime;
            EnterFrame = TimeController.frameCount;
            locomotionComponent.UpdateAgentHeight(PosState, RollState == EnumActivityState.Is);
            //locomotion.InitMove(model.movementDataModel);
            if (param.Get<bool>(EnumStateChangeParameter.isNewState))
                ApplyAnimParameters(EnterStateAnimationParamValue);
        }

        public override void ExitState(StateChangeParameter param)
        {
            base.ExitState(param);
            //if (EnterFrame > 0)
            ApplyAnimParameters(ExitStateAnimationParamValue);
        }

        protected void InvokeStateEvents(EnumAgentStateEvent[] events)
        {
            for (int i = 0; i < events.Length; i++)
            {
                allStateEvents[events[i]].DynamicInvoke();
            }
        }

        protected void ApplyAnimParameters(AnimationParamValue animParamValue)
        {
            UpdateCondition(animParamValue.floatParamValue);
            for (int i = 0; i < animParamValue.floatParamValue.Length; i++)
            {
                animParamValue.floatParamValue[i].done = !animParamValue.floatParamValue[i].HasCondition;
                animParamValue.floatParamValue[i].startValue = anim.GetFloat(animParamValue.floatParamValue[i].param);
            }
            UpdateCondition(animParamValue.floatCurveParamValue);
            for (int i = 0; i < animParamValue.floatCurveParamValue.Length; i++)
            {
                animParamValue.floatCurveParamValue[i].done = !animParamValue.floatCurveParamValue[i].HasCondition;
            }


            LerpFloatParameters();
            LerpFloatCurveParameters();

            UpdateCondition(animParamValue.integerParamValue);
            for (int i = 0; i < animParamValue.integerParamValue.Length; i++)
            {
                if (animParamValue.integerParamValue[i].HasCondition)
                    anim.SetInteger(animParamValue.integerParamValue[i].param, animParamValue.integerParamValue[i].value);
            }
            //if (!CommonHelper.IsNull(animParamValue.triggerParamValue))
            UpdateCondition(animParamValue.triggerParamValue);
            for (int i = 0; i < animParamValue.triggerParamValue.Length; i++)
            {
                //Debug.LogError(" Set trigger " + animParamValue.triggerParamValue[i].param + " : " + animParamValue.triggerParamValue[i].value);
                if (animParamValue.triggerParamValue[i].HasCondition)
                {
                    if (animParamValue.triggerParamValue[i].value)
                        anim.SetTrigger(animParamValue.triggerParamValue[i].param);
                    else
                        anim.SetBool(animParamValue.triggerParamValue[i].param, false);
                }

            }
            //if (!CommonHelper.IsNull(animParamValue.booleanParamValue))
            UpdateCondition(animParamValue.booleanParamValue);
            for (int i = 0; i < animParamValue.booleanParamValue.Length; i++)
            {
                if (animParamValue.booleanParamValue[i].HasCondition)
                    anim.SetBool(animParamValue.booleanParamValue[i].param, animParamValue.booleanParamValue[i].value);
            }
        }

        void LerpFloatParameters()
        {
            if (HasFloatAnimParameter)
            {
                for (int i = 0; i < EnterStateAnimationParamValue.floatParamValue.Length; i++)
                {
                    var paramVal = EnterStateAnimationParamValue.floatParamValue[i];
                    if (!paramVal.done)
                    {
                        if (paramVal.time <= 0)
                            anim.SetFloat(paramVal.param, paramVal.value);
                        else
                        {
                            anim.SetFloat(paramVal.param, paramVal.startValue, paramVal.value, Mathf.Clamp01((TimeController.gameTime - EnterTime) / paramVal.time));
                            //if (paramVal.param == EnumFloatParam.Aim)
                            //    Debug.Log("state " + State + " value " + paramVal.value);
                        }

                        paramVal.done = (TimeController.gameTime >= EnterTime + paramVal.time);
                    }
                }
            }
        }

        void LerpFloatCurveParameters()
        {
            if (HasFloatCurveAnimParameter)
            {
                for (int i = 0; i < EnterStateAnimationParamValue.floatCurveParamValue.Length; i++)
                {
                    var paramVal = EnterStateAnimationParamValue.floatCurveParamValue[i];
                    if (!paramVal.done)
                    {
                        if (paramVal.time <= 0)
                            anim.SetFloat(paramVal.param, paramVal.curve.Evaluate(1));
                        else
                        {
                            anim.SetFloat(paramVal.param, paramVal.curve.Evaluate(Mathf.Clamp01((TimeController.gameTime - EnterTime) / paramVal.time)));
                            //if (paramVal.param == EnumFloatParam.Aim)
                            //Debug.Log("state " + State + " param " + paramVal.param + " value " + paramVal.curve.Evaluate(Mathf.Clamp01((TimeController.gameTime - EnterTime) / paramVal.time)).ToString("f2") + " t " + Mathf.Clamp01((TimeController.gameTime - EnterTime) / paramVal.time).ToString("f2"));
                        }
                        paramVal.done = (TimeController.gameTime >= EnterTime + paramVal.time);
                    }
                }
            }
        }

        public override void UpdateState()
        {
            base.UpdateState();
            LerpFloatParameters();
            LerpFloatCurveParameters();

            //if (State == EnumAgentState.Rotate)
            //{
            //    Debug.Log("UpdateState  TimePassed " + TimeController.TimePassed(EnterTime + model.autoExitModel.AutoExitMinWaitTime) + " TimePassed2 " + TimeController.TimePassed(EnterTime + model.autoExitModel.AnimExitMinWaitTime));
            //}

            if (TimeController.TimePassed(EnterTime + AutoExitModel.AutoExitMinWaitTime))
            {
                if (AutoExitModel.AutoExitMaxWaitTime > 0 && TimeController.TimePassed(EnterTime + AutoExitModel.AutoExitMaxWaitTime))
                {
                    ChangeStateIf(AutoExitModel.AutoExitState);
                }
            }
            if (!CommonHelper.IsNull(AutoExitModel.AnimTagCompare) && TimeController.TimePassed(EnterTime + AutoExitModel.AnimExitMinWaitTime))
            {
                for (int i = 0; i < AutoExitModel.AnimTagCompare.Length; i++)
                {
                    var next = AutoExitModel.AnimTagCompare[i];
                    if (IsConditionCorrect(next))
                    {
                        Logger.Log(" Is tag " + next.tag + " layer " + next.animLayer + " State " + next.State + "  WaitTime " + AutoExitModel.AnimExitMinWaitTime, EnumLogColor.GreenLight);
                        ChangeStateIf(next.State);
                    }
                }
            }
        }

        protected bool IsConditionCorrect(AnimTagCondition animTagCompare)
        {
            var isTag = anim.IsAnimTag(animTagCompare.tag, animTagCompare.acceptNextState, animTagCompare.animLayer);
            //if(isTag)
            //    Debug.Log(" IsConditionCorrect tag " + animTagCompare.tag  );
            switch (animTagCompare.CompareOperator)
            {
                case EnumCompareOperator.Equal:
                    return (isTag);
                case EnumCompareOperator.NotEqual:
                    return (!isTag);
            }
            return false;
        }

        protected void UpdateCondition(AnimationBaseParamValue[] animationBaseParamValue)
        {
            for (int i = 0; i < animationBaseParamValue.Length; i++)
            {
                animationBaseParamValue[i].HasCondition = true;
                for (int j = 0; j < animationBaseParamValue[i].AnimTagCompare.Length; j++)
                {
                    var next = animationBaseParamValue[i].AnimTagCompare[j];
                    if (!IsConditionCorrect(next))
                    {
                        //Debug.LogError("HasCondition = false ");
                        animationBaseParamValue[i].HasCondition = false;
                        break;
                    }
                }
            }
        }

        public virtual void ChangeStateIf(EnumAgentState next)
        {
        }
        //public virtual void ChangeStateIf(EnumAgentState next, StateChangeParameter param)
        //{
        //} 

    }
}
