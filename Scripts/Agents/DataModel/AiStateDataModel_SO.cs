﻿

using UnityEngine;

namespace RCPU.Agent
{
    [CreateAssetMenu(fileName = "AiState", menuName = "RCPU/DataModel/Agent/AiStateDataModel", order = 1)]
    public class AiStateDataModel_SO : ScriptableObject
    {
        public AiStateDataModel model; 
    }
}