﻿

using RCPU.CameraFSM;
using RCPU.Component;
using System;
using UnityEngine;

namespace RCPU.Agent
{
    [Serializable]
    public class PlayerStateDataModel : AgentStateDataModel
    {
        [Space(20)]

        //public float aimIkWeight;
        [Header("Locomotion")]
        public EnumMoveRotateAbility MoveRotateAbility;
        public EnumAgentState RotateTargetState;

        [Header("Pos  ")]
        public EnumPosState currentPosState;
        public EnumAgentState ChangePosTargetState;

        [Header("Aim  ")]
        public EnumActivityState AimState;
        public EnumAgentState AimTargetState;

        [Header("Shoot  ")]
        public EnumActivityState ShootState;
        public EnumAgentState ShootTargetState;
        public EnumShootMode ShootMode;

        [Header("Roll  ")]
        public EnumActivityState RollState;

        [Header("Sprint  ")]
        public EnumActivityState SprintState;
        public EnumAgentState SprintTargetState;

        [Header("Reload   ")]
        public EnumActivityState ReloadState;
        public EnumAgentState ReloadTargetState;

        [Header("Cover   ")]
        public EnumActivityState CoverState;
        public EnumAgentState CoverTargetState;

        [Header("Switch Weapon  ")]
        public EnumActivityState SwitchWeaponState;
        public EnumAgentState SwitchWeaponTargetState;

        [Header("Ik  ")]
        public IkWeight[] IkWeights;
        //public AnimationCurve IkWeight;
        //public float IkSwitchSpeed;
        //public EnumIkState IkState;
        //public EnumIkPart IkParts;

        [Header("Camera  ")]
        public bool ChangeCameraState;
        public EnumCameraState CameraState;
    }
}
