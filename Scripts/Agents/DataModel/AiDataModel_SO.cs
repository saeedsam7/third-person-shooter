﻿

using UnityEngine;

namespace RCPU.Agent
{
    [CreateAssetMenu(fileName = "Ai", menuName = "RCPU/DataModel/Agent/AiDataModel", order = 1)]
    public class AiDataModel_SO : ScriptableObject
    {
        public AiDataModel model;
        public AiStateDataModel_SO[] states;
    }
}