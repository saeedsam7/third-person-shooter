﻿
using UnityEngine;

namespace RCPU.Agent
{
    [CreateAssetMenu(fileName = "PlayerState", menuName = "RCPU/DataModel/Agent/PlayerStateDataModel", order = 1)]
    public class PlayerStateDataModel_SO : ScriptableObject
    {
        public PlayerStateDataModel model;

    }
}