
using UnityEngine;

namespace RCPU.Agent
{
    [CreateAssetMenu(fileName = "AgentState", menuName = "RCPU/DataModel/Agent/AgentStateDataModel", order = 1)]
    public class AgentStateDataModel_SO : ScriptableObject
    {
        public AgentStateDataModel model;
    }
}