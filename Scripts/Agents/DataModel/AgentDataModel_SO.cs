﻿
using UnityEngine;

namespace RCPU.Agent
{
    [CreateAssetMenu(fileName = "Agent", menuName = "RCPU/DataModel/Agent/AgentDataModel", order = 1)]
    public class AgentDataModel_SO : ScriptableObject
    {
        public AgentDataModel model;
        public AgentStateDataModel_SO[] states;
    }
}