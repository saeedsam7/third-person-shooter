﻿using RCPU.Component;
using System;
using UnityEngine;

namespace RCPU.Agent
{
    [Serializable]
    public class AgentStateDataModel
    {
        [HideInInspector]
        public float EnterTime;
        [HideInInspector]
        public float EnterFrame;

        public EnumAgentState State;
        //public bool hasDefindedLength;

        


        [Space(10)]
        public float Visibility;

        [Header("Enter/Exit Animation Parameters")]
        public AnimationParamValue enterStateAnimationParamValue;
        public AnimationParamValue exitStateAnimationParamValue;
        public EnumAgentStateEvent[] enterStateEvents;
        public EnumAgentStateEvent[] exitStateEvents;

        [Header("Auto Exit Conditions")]
        public AutoExitModel autoExitModel;






        //public List<EnumIk> activeIk;
    }

    [Serializable]
    public class IkWeight
    {
        public AnimationCurve value;
        public float speed;
        //public EnumIkState state;
        public EnumIkPart part;
    }
}
