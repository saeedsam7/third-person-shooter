﻿

using RCPU.Component;
using RCPU.Vfx;
using RCPU.Weapon;
using System;

namespace RCPU.Agent
{
    [Serializable]
    public class AiDataModel : AgentDataModel
    {
        public EnumAgentType AgentType;
        public EnumVfxType DestroyVfxType;
        public EnumMovementType MovementType;
        public EnumRagdollType ragdollType;
        public float ShootAngle;
    }

}