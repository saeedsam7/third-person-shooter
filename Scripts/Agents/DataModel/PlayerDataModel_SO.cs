﻿
using UnityEngine;

namespace RCPU.Agent
{
    [CreateAssetMenu(fileName = "Player", menuName = "RCPU/DataModel/Agent/PlayerDataModel", order = 1)]
    public class PlayerDataModel_SO : ScriptableObject
    {
        public PlayerDataModel model;
        public PlayerStateDataModel_SO[] states;
    }
}