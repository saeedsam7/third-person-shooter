﻿
using System;
using UnityEngine;

namespace RCPU.Agent
{
    [Serializable]
    public class AgentDataModel
    {
        public int Level;
        public EnumFamily family;


        [Header("Height")]
        public float CrawlHeight;
        public float CrawlTime;
        public float CrouchHeight;
        public float CrouchTime;
        public float DefaultHeight;
        public float DefaultTime;
        public float centerOffset;

        [Header("Crosshair")]
        public float NoAimCrosshairSize;
        public float MoveCrosshairSize;
        //public bool addHalfHeightAsOffset;


        //public List<MInternalComponent_Base_SO> internalComponent;

        //public MBase_SO movementComponent;

        //public List<MComponent_Detector_Base_SO> components;
        //public List<MState_Agent_SO> statesModel;
        //public List<MWeapon_Base_SO> availableWeapon;
        //public List<MAnimationIk_SO> animationIk;

        //public TextAsset attackBehaviour;
        //public EnumVfxType DestroyVfxType;



        //public MDamage closeAttakDamage;
    }


}
