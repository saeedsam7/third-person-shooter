using RCPU.Attributes;
using RCPU.Helper;
using RCPU.NotificationSystem;
using RCPU.Ragdoll;
using RCPU.StateMachine;
using UnityEngine;

namespace RCPU.Agent
{
    [InjectToInstance(typeof(IAlive))]
    public class StateMachineAlive : StateMachineBase, IAlive
    {
        [InjectFromSystem] protected ILevelManager levelManager { set; get; }
        [InjectFromSystem] protected IRelation relation { get; set; }

        //[InjectFromInstance] protected IDamageReceiver[] injectedDamageReceiver { set; get; }

        [SerializeField] protected Transform NormalState;
        //[SerializeField] protected Transform meshRoot;

        public virtual EnumFamily Family => EnumFamily.None;
        //protected Dictionary<int, IDamageReceiver> damageReceiver { set; get; }
        public virtual float ReflexTime => 0;
        public Vector3 Right => NormalState.right;
        public virtual int Level { get; protected set; }
        public virtual float Visibility => 0;
        public Vector3 Position => NormalState.position;
        public virtual Vector3 Center => Position;
        public Vector3 Forward => NormalState.forward;
        public Vector3 EulerAngles => NormalState.eulerAngles;
        public Quaternion Rotation => NormalState.rotation;
        public bool IsAlive { get; protected set; }
        public bool IsShocked { get; protected set; }
        public virtual bool IsInCover => throw new System.NotImplementedException();
        protected IRagdoll activeRagdoll;

        public override void Init()
        {
            base.Init();
            levelManager.RegisterAgent(this.Id, this);
            Notification.RegisterInstance(Id, EnumInstanceNotifType.OwnerDied, Id, OnDied);
            Notification.RegisterInstance(Id, EnumInstanceNotifType.DamageReceived, Id, OnDamageReceived);
            Notification.RegisterInstance(Id, EnumInstanceNotifType.OwnerShocked, Id, OnOwnerShocked);
            Notification.RegisterInstance(Id, EnumInstanceNotifType.OwnerShockRestoredInitialy, Id, OnOwnerShockRestoredInitialy);
            Notification.RegisterInstance(Id, EnumInstanceNotifType.OwnerShockRestoredInitialy, Id, OnOwnerShockRestoredCompletly);
        }

        protected override void Spawn()
        {
            base.Spawn();
            IsAlive = true;
            IsShocked = false;
        }

        protected virtual void OnDied(NotificationParameter obj)
        {
            IsAlive = false;
        }
        protected virtual void OnDamageReceived(NotificationParameter obj)
        {
        }

        protected virtual void OnOwnerShocked(NotificationParameter obj)
        {
            IsShocked = true;
        }
        protected virtual void OnOwnerShockRestoredInitialy(NotificationParameter obj)
        {
        }

        protected virtual void OnOwnerShockRestoredCompletly(NotificationParameter obj)
        {
            IsShocked = false;
        }

        public void SetRotation(Quaternion rotation)
        {
            NormalState.rotation = rotation;
        }

        public void SetPosition(Vector3 position)
        {
            NormalState.position = position;
        }

        public void SetRootPosition(Vector3 position)
        {
            transform.position = position;
            if (PhysicsHelper.Raycast(position, Vector3.down, out RaycastHit hitInfo))
            {
                transform.position = hitInfo.point;
            }
        }
        public EnumRelation GetRelation(EnumFamily targetFamily)
        {
            return relation.GetRelation(Family, targetFamily);
        }


        //public int GetAimCollider()
        //{
        //    if (damageReceiver.Count > 0)
        //    {
        //        var best = damageReceiver.Find(x => x.bodyPart == MDamageReceiver.EnumBodyPart.Chest);
        //        if (!CommonHelper.IsNull(best))
        //            return best.trigger.GetInstanceID();

        //        best = damageReceiver.Find(x => x.bodyPart == MDamageReceiver.EnumBodyPart.Head);
        //        if (!CommonHelper.IsNull(best))
        //            return best.trigger.GetInstanceID();

        //        return damageReceiver[0].trigger.GetInstanceID();
        //    }
        //    else
        //        return 0;
        //}


        //public Vector3 GetDamagablePosition()
        //{
        //    if (injectedDamageReceiver.Length > 0)
        //    {
        //        return injectedDamageReceiver.First().Position;
        //    }
        //    else
        //        return Position;
        //}

        public void Rotate(Vector3 eulers, float angle)
        {
            transform.Rotate(eulers, angle);
        }

        public virtual Vector3 PredictPosition(float time)
        {
            return Position;
        }

        public virtual Vector3 PredictCenter(float time)
        {
            return Center;
        }

    }
}