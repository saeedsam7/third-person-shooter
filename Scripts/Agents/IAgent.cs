using RCPU.InputSystem;
using RCPU.Trigger;
using RCPU.Vehicle;
using UnityEngine;

namespace RCPU.Agent
{
    public interface IAgent : IAlive  /*ITeleportable,*/
    {

        EnumAgentState CurrentState { get; }
        Vector3 GetAimPosition();
        void LookAt(Vector3 target);
        void LerpLookAt(Vector3 target, float speed, float maxLimit);
        void ChangeStateIf(EnumAgentState current, EnumAgentState next);
        //void ChangeStateIf(EnumAgentState current, EnumAgentState next, StateChangeParameter param);
        //MAnimationIk GetAnimationIk(string animName);
        //void SetIksWeight(List<EnumIk> activIk);
        //bool HaveIk(EnumIk ik);
        //void SetIkTargetRotation(EnumIk ik, Quaternion rotation, float weight);
        //void SetIkTargetPosition(EnumIk ik, Vector3 position, float weight);
        //void RegisterAnimatorEvent(EnumEventReceiver receiver, Action<List<string>> action);
        //bool LostTarget(float t);
        //void ChangeRootOriantation(float moveShake, Quaternion rot);
        //void GotoCover(MTriggerCover trigger);
        //bool CanTeleport(MTeleportItem teleportItem);
        void GetOnVehicle(IVehicle vehicle);
        //bool Pickup(IPickupItem pu);
        float GetSuiteNoise();
        bool SwitchWeapon(EnumWeaponSlotIndex index);
        void SetPosition(Vector3 position);
        void SetRootPosition(Vector3 position);
        void SetRotation(Quaternion rotation);
        void Rotate(Vector3 eulers, float angle);
        void SetParent(Transform parent);
        void ResetParent();
        void EnableTrigger(EnumTriggerType tType);
        void DeadVfx();

        EnumPosState PosState { get; }
        //EnumMovementType MovementType { get; }
        //void DisableTrigger(EnumTriggerType tType);
        void AimAt(Vector3 target);
        void GetOffVehicle();
        void CloseAttack();

    }
}