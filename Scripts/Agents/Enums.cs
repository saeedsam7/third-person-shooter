

namespace RCPU.Agent
{
    public enum EnumAgentState
    {
        Ready, Spawn, Normal, Shoot, Guard,
        Reload, Cutscene, Menu, Rotate, Die,
        Knockback, Aim, Hit, Crawl, Crouch,
        Patrol, Attack, Suspicious, Knockout, Teleport,
        Riding, Cover, CoverAim, CrouchAim, Sprint,
        CrouchShoot, Roll, None, CrawlShoot, CrawlAim,
        RotateAim, CrouchReload, Slide, ReloadAim, UnequipWeapon,
        EquipWeapon, UnequipWeaponCrouch, EquipWeaponCrouch, CrouchReloadAim, BlindShoot,
        CrouchBlindShoot,
    }

    public enum EnumAgentStateEvent
    {
        None,
        EnterPatrol, EnterReload, EnterRoll, EnterSwitchWeapon,
        ExitReload, ExitAim, ExitSwitchWeapon, ExitBlindShoot, EnterBlindShoot,
        EnterReflexMode, EnterAim, EnterAimShoot
    }

    public enum EnumApplyRootMotion { DontChange, True, False }
    public enum EnumAgentType { Human, MechTwoLegs, MechFourLegs };
    public enum EnumAwareState { Normal, Cautious, Offensive }
    public enum EnumPosState
    {
        None, Stand, Crouch, Crawl,
        Jump
    }
    public enum EnumActivityState { None, Can, Is }
    public enum EnumMoveRotateAbility { None, SimpleMove, RotateOnly, CoverMove, StrafMove }
    public enum EnumCompareOperator
    {
        Equal = 0, NotEqual = 1
    }

    public enum EnumDecision
    {
        None, ActionFindCover, TakeCover, ActionFindShootPosition, Shoot,
        GetCloserToTarget, WaitForTarget, SwitchToMainWeapon, SwitchToSecondaryWeapon, UseGrenade,
        UseSpecialMove, ActionCallForBackup, WalkAndShoot, Reload, ActionFindSafePoint,
        ExitCover, StartAttackState, CircleAroundAndShoot, Patrol, Die,
        SearchForTheTarget, RotateToTarget, MoveToInvestigationPosition, GetCloserToLastPosition, BlindShoot,
        WalkAndBlindShoot
    }


    public enum EnumCondition
    {
        IsInCover, IsInShootRange, IsTooCloseToShoot, IsTooFarFromTarget, IsTargetInCover,
        XXXXHaveSpecialAction, HasGrenade, HasComunicationDevice, IsInLowHealth, HasBullet,
        IsNearGrenade, HasDetector, HavePathToTarget, GetHit, NewTargetDetected,
        HasSuspiciousTarget, TargetLost, TargetIsDead, XXXXXXXReload, XXXXXIsInShootAngle,
        VisitTargetLastPosition, IsDecisionTimeOut, RingFailed, TargetIsVisible, InvestigateTheArea,
        HasOffensiveTarget
    }
}