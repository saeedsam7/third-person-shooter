﻿using RCPU.Component;
using System;

namespace RCPU.Agent
{
    [Serializable]
    public struct AutoExitModel
    {
        public float AutoExitMaxWaitTime;
        public float AutoExitMinWaitTime;
        //public string[] AutoExitTags = new string[] { "Stand" };
        public EnumAgentState AutoExitState;
        public float AnimExitMinWaitTime;
        public AnimTagChangeState[] AnimTagCompare;

    }
}
