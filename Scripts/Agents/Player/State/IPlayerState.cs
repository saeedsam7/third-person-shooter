﻿using RCPU.Component;
using RCPU.InputSystem;

namespace RCPU.Agent
{
    public interface IPlayerState : IAgentState
    {
        //EnumCameraState CameraState { get; }
        //bool ChangeCameraState { get; }
        //EnumIkState IkState { get; }
        float GetIkWeight(EnumIkPart ikPart);
    }

    public interface IPlayerActionContainer
    {
        void TrySwitchWeapon(EnumWeaponSlotIndex index);
        void Move(bool sprint, float vertical, float horizontal, bool aim);

        void HandleSprint(float vertical, float horizontal);
        void HandleRotate(float vertical, float horizontal);
        void HandleChangePosState();
        void Roll();
        void HandleRoll(float inputV, float inputH);
        void HandleReload();
        void HandleAim(float hold);
        void DownShoot();
        void HoldShoot(float hold);

    }

}