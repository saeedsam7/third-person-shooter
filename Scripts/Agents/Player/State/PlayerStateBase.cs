
using RCPU.Attributes;
using RCPU.CameraFSM;
using RCPU.Component;
using RCPU.Game;
using RCPU.Helper;
using RCPU.InputSystem;
using RCPU.NotificationSystem;
using RCPU.StateMachine;
using RCPU.UI;
using RCPU.Weapon;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Agent
{
    [InjectToInstance(typeof(IPlayerState))]
    [InjectToInstance(typeof(IPlayerActionContainer))]
    public class PlayerStateBase : AgentState, IPlayerState, IPlayerActionContainer
    {
        [InjectFromInstance] protected IWeaponManagerComponentManual WeaponManager { get; set; }
        [InjectFromSystem] protected ICamera Cam { get; set; }
        [InjectFromSystem] protected IGameFSM GameFSM { get; set; }
        [InjectFromSystem] protected IInGameDetailFSM InGameDetailFSM { get; set; }
        [InjectFromSystem] protected IInput Input { get; set; }
        [InjectFromSystem] IUIManager UIManager { get; set; }
        [InjectFromInstance] protected IPlayer Owner { get; set; }
        [InjectFromInstance] protected ILocomotionCharacter locomotion { get; set; }
        protected override ILocomotionComponent locomotionComponent => locomotion;

        protected Dictionary<EnumIkPart, IkWeight> IkWeights { get; set; }
        protected float[] ikWeightValues;
        protected override EnumAgentState State => model.State;
        protected override EnumPosState PosState => model.currentPosState;
        protected override EnumActivityState RollState => model.RollState;
        protected override AnimationParamValue EnterStateAnimationParamValue => model.enterStateAnimationParamValue;
        protected override AnimationParamValue ExitStateAnimationParamValue => model.exitStateAnimationParamValue;
        protected override AutoExitModel AutoExitModel => model.autoExitModel;
        protected override float EnterTime { get { return model.EnterTime; } set { model.EnterTime = value; } }
        protected override float EnterFrame { get { return model.EnterFrame; } set { model.EnterFrame = value; } }
        protected bool CanReload() { return /*this.IsEnable &&*/ model.ReloadState == EnumActivityState.Can; }
        protected bool IsAimed => model.AimState == EnumActivityState.Is;

        protected PlayerStateDataModel model;
        protected EnumWeaponSlotIndex weaponSlotIndexToSelect;
        protected bool sprint;
        protected float vertical;
        protected float horizontal;
        protected bool aim;
        protected bool canChangeDirection;
        protected bool isReloading;
        protected bool isSwitchingWeapon;
        protected EnumActivityState lastReloadState = EnumActivityState.None;
        protected bool isNewState;
        protected bool ikUpdateDone;
        //protected EnumIkState previousIkState;
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        protected void SwitchWeaponByMenu(NotificationParameter param)
        {
            TrySwitchWeapon(param.Get<EnumWeaponSlotIndex>(EnumNotifParam.slotIndex));
        }

        public void TrySwitchWeapon(EnumWeaponSlotIndex index)
        {
            if (WeaponManager.HasWeaponOnSlot(index))
            {
                this.weaponSlotIndexToSelect = index;
                ChangeStateIf(model.SwitchWeaponTargetState);
            }
        }

        public void Move(bool sprint, float vertical, float horizontal, bool aim)
        {
            this.sprint = sprint;
            this.aim = aim;
            if (locomotion.Move(model.MoveRotateAbility, (model.RotateTargetState != EnumAgentState.None), horizontal, vertical, sprint, IsAimed))
            {
                ChangeStateIf(model.RotateTargetState);
            }
        }

        public void HandleSprint(float vertical, float horizontal)
        {
            if (sprint && model.SprintState == EnumActivityState.Can && (horizontal > .5f || vertical > .5f))
            {
                ChangeStateIf(model.SprintTargetState);
            }
            else
            {
                if (model.SprintState == EnumActivityState.Is && (!sprint || (horizontal < .1f && vertical < .1f)))
                {
                    ChangeStateIf(model.SprintTargetState);
                }
            }
        }

        public virtual void HandleRotate(float vertical, float horizontal)
        {
            this.vertical = vertical;
            this.horizontal = horizontal;
            if (locomotion.Rotate(horizontal, vertical))
            {
                ChangeStateIf(model.RotateTargetState);
            }
        }

        public virtual void HandleChangePosState()
        {
            if (model.ChangePosTargetState != EnumAgentState.None)
                ChangeStateIf(model.ChangePosTargetState);
        }

        public virtual void Roll()
        {
            ChangeStateIf(EnumAgentState.Roll);
        }

        public virtual void HandleRoll(float inputV, float inputH)
        {
            //Debug.Log("HandleRoll");
            if (canChangeDirection && EnterTime + .2f > TimeController.gameTime && (vertical != inputV || horizontal != inputH) && (inputH != 0 || inputV != 0))
            {
                //Debug.LogError("HandleRoll");
                canChangeDirection = false;
                Roll(inputV, inputH);
            }
        }

        public void DownShoot()
        {
            TryShoot(true);
        }

        public void HoldShoot(float hold)
        {
            if (hold > 0)
            {
                TryShoot(false);
            }
        }

        public void HandleReload()
        {
            if (WeaponManager.TryReload())
            {
                ChangeStateIf(model.ReloadTargetState);
            }
        }

        public void HandleAim(float hold)
        {
            WeaponManager.Aim(Owner.AimValue == 1);

            //base.InputButton(btns);
            if (!IsAimed && hold > 0/* && cameraAimTarget.CanAim*/ && WeaponManager.AttckType == EnumAttckType.Ranged)
            {
                ChangeStateIf(model.AimTargetState);
            }
            else
                 if ((hold == 0 /*|| !cameraAimTarget.CanAim*/) && IsAimed)
            {
                ChangeStateIf(model.AimTargetState);
                //todo check Aim need to be set
                //anim.SetFloat(EnumFloatParam.Aim, 0);
            }
        }

        protected float lastAutoReflexAim;
        [InjectStateAction(EnumAgentStateEvent.EnterAim)]
        protected void EnterAim()
        {
            if (GameFSM.CauciousState == EnumInGameCauciousState.Reflex)
            {
                if (lastAutoReflexAim + GameSetting.ReflexDuration < TimeController.gameTime && Input.GetButtonHold(EnumButton.Aim) < GameSetting.ReflexDuration)
                {
                    lastAutoReflexAim = TimeController.gameTime;
                    Owner.AimAt(InGameDetailFSM.ReflexCause.Center);
                }

            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public override void EnterState(StateChangeParameter param)
        {
            //if (!CommonHelper.IsNull(model))
            //    previousIkState = (model.IkState);
            model = param.Get<PlayerStateDataModel>(EnumStateChangeParameter.model);
            isNewState = true;
            locomotion.ResetInput();
            InvokeStateEvents(model.enterStateEvents);
            param.Add(EnumStateChangeParameter.isNewState, isNewState);
            lastReloadState = model.ReloadState;
            base.EnterState(param);
            IkWeights.Clear();
            ikUpdateDone = false;
            for (int i = 0; i < model.IkWeights.Length; i++)
            {
                var ik = model.IkWeights[i];
                IkWeights.Add(ik.part, ik);
            }
            Notification.NotifyInstance(Owner.Id, EnumInstanceNotifType.PlayerStateChanged, new NotificationParameter(EnumNotifParam.dataModel, model));
        }

        public float GetIkWeight(EnumIkPart ikPart)
        {
            return ikWeightValues[(int)ikPart];
        }

        public override void ExitState(StateChangeParameter param)
        {
            base.ExitState(param);
            //var nextModel = param.Get<PlayerStateDataModel>(EnumStateChangeParameter.model);
            InvokeStateEvents(model.exitStateEvents);
        }

        protected void UpdateIkWeight()
        {
            if (!ikUpdateDone)
            {
                for (int i = 0; i < ikWeightValues.Length; i++)
                {
                    EnumIkPart key = (EnumIkPart)i;
                    if (IkWeights.ContainsKey(key))
                    {
                        var finalValue = IkWeights[key].value.Evaluate(1);
                        if (Mathf.Abs(ikWeightValues[i] - finalValue) < .02f /*|| model.IkState != previousIkState*/)
                        {
                            ikWeightValues[i] = finalValue;
                        }
                        else
                        {
                            ikWeightValues[i] = IkWeights[key].value.Evaluate(Mathf.Clamp(NormalizedTime * IkWeights[key].speed, 0, 1));
                        }
                    }
                    else
                    {
                        ikWeightValues[i] = 0;
                    }
                }
                ikUpdateDone = NormalizedTime >= 1 /*|| (model.IkState != previousIkState)*/;
            }
        }

        public override void UpdateState()
        {
            base.UpdateState();
            UpdateIkWeight();
        }


        //------------------------------------------------------------------------------------------------------------------------------------------
        [InjectStateAction(EnumAgentStateEvent.EnterSwitchWeapon)]
        protected void EnterSwitchWeapon()
        {
            coroutineSwitchWeapon = StartCoroutine(CoroutineSwitchWeapon());
        }
        [InjectStateAction(EnumAgentStateEvent.EnterRoll)]
        protected void EnterRoll()
        {
            if (GameFSM.InGameUIState != EnumInGameUiState.HUD)
                return;
            canChangeDirection = true;
            Roll(Input.GetAxis(EnumAxis.LeftVertical), Input.GetAxis(EnumAxis.LeftHorizontal));
        }
        [InjectStateAction(EnumAgentStateEvent.EnterReload)]
        protected void EnterReload()
        {
            //Debug.LogError(" CoroutineReload lasttate "+ lasttate+" to " + model.State); 
            if (lastReloadState != EnumActivityState.Is)
            {
                coroutineReloading = StartCoroutine(CoroutineReload(WeaponManager.ReloadTime));
            }
            else
                isNewState = false;
        }
        [InjectStateAction(EnumAgentStateEvent.ExitAim)]
        protected void ExitAim()
        {
            if (IsAimed)
                WeaponManager.Aim(false);
        }
        [InjectStateAction(EnumAgentStateEvent.ExitBlindShoot)]
        protected void ExitBlindShoot()
        {
            if (isBlindShoot != EnumBlindShootState.None && model.ShootState != EnumActivityState.Is)
            {
                isBlindShoot = EnumBlindShootState.None;
                StopCoroutine(coroutineAimShoot);
            }
        }
        [InjectStateAction(EnumAgentStateEvent.ExitSwitchWeapon)]
        protected void ExitSwitchWeapon()
        {
            if (isSwitchingWeapon)
            {
                isSwitchingWeapon = false;
                StopCoroutine(coroutineSwitchWeapon);
            }
        }
        [InjectStateAction(EnumAgentStateEvent.ExitReload)]
        protected void ExitReload()
        {
            if (isReloading)
            {
                isReloading = false;
                UIManager.SetEnable(EnumUiBarIndex.Reload, false);
                StopCoroutine(coroutineReloading);
                Notification.NotifyInstance(Owner.Id, EnumInstanceNotifType.ReloadComplete, new NotificationParameter(EnumNotifParam.Successful, false));
            }
        }
        [InjectStateAction(EnumAgentStateEvent.EnterBlindShoot)]
        protected void EnterBlindShoot()
        {
            BlindShoot(true);
        }
        [InjectStateAction(EnumAgentStateEvent.EnterAimShoot)]
        protected void EnterAimShoot()
        {
            if (isInCoroutineExitAimShoot)
                StopCoroutine(coroutineExitAimShoot);
            coroutineExitAimShoot = StartCoroutine(CoroutineExitAimShoot());
        }
        Coroutine coroutineExitAimShoot;
        bool isInCoroutineExitAimShoot;
        protected IEnumerator CoroutineExitAimShoot()
        {
            isInCoroutineExitAimShoot = true;
            float time = WeaponManager.EachFireDuration;
            while (time > 0)
            {
                yield return new WaitForEndOfFrame();
                time -= TimeController.deltaTime;
            }
            if (model.ShootState == EnumActivityState.Is)
                ChangeStateIf(model.ShootTargetState);

            isInCoroutineExitAimShoot = false;
        }
        //------------------------------------------------------------------------------------------------------------------------------------------
        protected void Roll(float inputV, float inputH)
        {
            vertical = inputV;
            horizontal = inputH;
            int angle = Mathf.RoundToInt(CommonHelper.GetInputNormilizedAngle(Owner.Position, Owner.Forward, Cam.GetForward(10000, true), horizontal, vertical) / 90) * 90;

            anim.SetFloat(EnumFloatParam.RotateAngle, angle);
            //locomotion.Roll(angle);
        }
        Coroutine coroutineReloading;
        private IEnumerator CoroutineReload(float time)
        {
            float timeLength = time;
            isReloading = true;
            UIManager.SetEnable(EnumUiBarIndex.Reload, true);
            UIManager.SetBarValue(EnumUiBarIndex.Reload, 0);
            while (time > 0)
            {
                yield return new WaitForEndOfFrame();
                time -= TimeController.deltaTime;
                UIManager.SetBarValue(EnumUiBarIndex.Reload, 1f - (time / timeLength));
            }
            isReloading = false;
            UIManager.SetBarValue(EnumUiBarIndex.Reload, 1f);
            UIManager.SetEnable(EnumUiBarIndex.Reload, false);
            Notification.NotifyInstance(Owner.Id, EnumInstanceNotifType.ReloadComplete, new NotificationParameter(EnumNotifParam.Successful, true));
            ChangeStateIf(model.ReloadTargetState);
        }

        private void BlindShoot(bool keyDown)
        {
            if (WeaponManager.HasBullet)
            {
                if (model.ShootState == EnumActivityState.Is)
                {
                    if (isBlindShoot != EnumBlindShootState.AimUp)
                    {
                        if (isBlindShoot != EnumBlindShootState.None)
                            StopCoroutine(coroutineAimShoot);
                        coroutineAimShoot = StartCoroutine(CoroutineAimThenShoot(keyDown));
                    }
                }
                else
                    ChangeStateIf(model.ShootTargetState);
            }

        }

        Coroutine coroutineAimShoot;
        EnumBlindShootState isBlindShoot = 0;
        private IEnumerator CoroutineAimThenShoot(bool keyDown)
        {
            if (WeaponManager.HasBullet)
            {
                isBlindShoot = EnumBlindShootState.AimUp;
                while (Owner.AimValue < .98f)
                {
                    Owner.AimValue += TimeController.deltaTime * 2f;
                    yield return new WaitForEndOfFrame();
                }
                if (WeaponManager.TryFire(keyDown))
                {
                    anim.SetTrigger(EnumTriggerParam.Shoot);
                }

                isBlindShoot = EnumBlindShootState.AimDown;
                float time = 1;
                while (time > 0)
                {
                    Owner.AimValue += TimeController.deltaTime;
                    time -= TimeController.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
                ChangeStateIf(model.ShootTargetState);
                isBlindShoot = EnumBlindShootState.None;
            }
        }

        void AimShoot(bool keyDown)
        {
            if (WeaponManager.TryFire(keyDown))
            {
                ChangeStateIf(model.ShootTargetState);
            }
        }

        public void TryShoot(bool keyDown)
        {
            if (WeaponManager.CanMustReload())
            {
                TryReload();
            }
            else
            {
                if (model.ShootMode == EnumShootMode.Aim)
                {
                    AimShoot(keyDown);
                }
                else
                 if (model.ShootMode == EnumShootMode.Blind)
                {
                    BlindShoot(keyDown);
                }
            }
        }
        void TryReload()
        {
            if (WeaponManager.CanMustReload() && CanReload())
            {
                if (WeaponManager.CanMustReload())
                {
                    HandleReload();
                }
            }
        }

        Coroutine coroutineSwitchWeapon;
        private IEnumerator CoroutineSwitchWeapon()
        {
            isSwitchingWeapon = true;
            //anim.SetFloat(EnumFloatParam.LastWeapon, (int)WeaponManager.WeaponType);
            var time = WeaponManager.UnequipTime;
            //StateLength = time;
            do
            {
                yield return new WaitForEndOfFrame();
                time -= TimeController.deltaTime;
            } while (time > 0);
            Owner.SwitchWeapon(weaponSlotIndexToSelect);
            ChangeStateIf(model.SwitchWeaponTargetState);
            time = WeaponManager.EquipSelectTime;
            while (time > 0)
            {
                yield return new WaitForEndOfFrame();
                time -= TimeController.deltaTime;
            }
            WeaponManager.SelectActiveWeapon();
            time = WeaponManager.EquipTime;
            while (time > 0)
            {
                yield return new WaitForEndOfFrame();
                time -= TimeController.deltaTime;
            }
            isSwitchingWeapon = false;
            ChangeStateIf(model.SwitchWeaponTargetState);
        }

        public override void ChangeStateIf(EnumAgentState next)
        {
            Owner.ChangeStateIf(State, next);
        }

        public override void Init()
        {
            base.Init();
            IkWeights = new Dictionary<EnumIkPart, IkWeight>();
            ikWeightValues = new float[CommonHelper.GetEnumLength(typeof(EnumIkPart))];
            Notification.Register(EnumNotifType.WeaponSwitchByMenu, Id, SwitchWeaponByMenu);
        }
    }
}