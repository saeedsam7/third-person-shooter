using RCPU.Component;

namespace RCPU.Agent
{
    public interface IPlayer : IAgent
    {
        float GetIkWeight(EnumIkPart ikPart);
        float AimValue { get; set; }
    }
}