
using RCPU.Attributes;
using RCPU.CameraFSM;
using RCPU.Component;
using RCPU.DependencyInjection;
using RCPU.InputSystem;
using RCPU.NotificationSystem;
using RCPU.StateMachine;
using RCPU.UI;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Agent
{
    [InitOrder(EnumInitOrder.Player)]
    [InjectToSystem(typeof(ICameraTarget))]
    [InjectToInstance(typeof(IPlayer))]
    public class StateMachinePlayer : StateMachineAgent, IInitializable, ICameraTarget, IPlayer
    {
        [InjectFromSystem] protected ICamera Cam { get; set; }
        //[InjectFromSystem] protected IUIManager UiManager { get; set; }
        [InjectFromSystem] protected ICameraAimTarget CamAimTarget { get; set; }
        [InjectFromSystem] IPlayerSpawnPosition SpawnPosition { get; set; }
        [InjectFromInstance] protected IPlayerState ActiveState { get; set; }
        [InjectFromInstance] protected ILocomotionComponent Locomotion { get; set; }
        [InjectFromSystem] protected IUiPlayerWidget PlayerWidget { get; set; }


        [Header("Model")][SerializeField] protected PlayerDataModel_SO model_SO;
        protected PlayerDataModel model;
        public override int Level => model.Level;
        public override EnumFamily Family => model.family;

        protected Dictionary<EnumAgentState, PlayerStateDataModel> allState;
        protected PlayerStateDataModel CurrentStateModel => allState[CurrentState];
        //public Vector3 HeadPosition => Position + (Vector3.up * locomotion.Height);
        public override Vector3 Center => Position + (.5f * Locomotion.Height * Vector3.up);
        public Vector3 WeaponPosition => (weaponManager.WeaponPosition);
        public float AimValue { get => anim.GetFloat(EnumFloatParam.Aim); set { anim.SetFloat(EnumFloatParam.Aim, Mathf.Clamp(value, 0f, 1f)); } }
        public override EnumPosState PosState => CurrentStateModel.currentPosState;
        public override float Visibility => CurrentStateModel.Visibility;
        public override bool IsInCover => CurrentStateModel.CoverState == EnumActivityState.Is;
        //public bool CanShoot => weaponManager.AttckType ==  EnumAttckType.Ranged  && (  CurrentStateModel.ShootMode == EnumShootMode.Blind || AimValue == 1);
        protected override void Spawn()
        {
            base.Spawn();
            SetRootPosition(SpawnPosition.Position);
            Notification.Notify(EnumNotifType.SetPlayer, new NotificationParameter(EnumNotifParam.agent, this));

            UpdateLevelUI();
            ChangeState(new StateChangeParameter(EnumStateChangeParameter.newState, EnumAgentState.Normal));
            SwitchWeapon(EnumWeaponSlotIndex.None);
        }

        public float GetCrosshairSize()
        {
            float stand = anim.IsStand(true, true) ? 0f : model.MoveCrosshairSize;
            float aim = (AimValue == 1) ? 0f : model.NoAimCrosshairSize;
            float shoot = weaponManager.CrosshairSize;
            Logger.Log(" stand " + stand + " aim " + aim + " shoot " + shoot, 2);
            return stand + aim + shoot;
        }

        public float GetIkWeight(EnumIkPart ikPart)
        {
            return ActiveState.GetIkWeight(ikPart);
        }

        protected override void UpdateStateMachine()
        {
            base.UpdateStateMachine();
            debugEnterState = TimeController.gameTime - CurrentStateModel.EnterTime;
            Logger.Log(" gameTime " + TimeController.gameTime.ToString("f1") + " Enter " + CurrentStateModel.EnterTime.ToString("f1") + " NormalizedTime " + ActiveState.NormalizedTime.ToString("f1"), 1);
            ActiveState.UpdateState();
            //if (Input.GetKeyDown(KeyCode.F1))
            //{
            //    //Hit(); 
            //    //AimAt(Vector3.zero);
            //}
            //Debug.DrawLine(GetPos(), GetAimPosition(),Color.cyan);
        }

        public override void Init()
        {
            base.Init();
            model = DInjector.Instantiate<PlayerDataModel_SO>(model_SO).model;
            allState = new Dictionary<EnumAgentState, PlayerStateDataModel>();
            for (int i = 0; i < model_SO.states.Length; i++)
            {
                var temp = DInjector.Instantiate<PlayerStateDataModel_SO>(model_SO.states[i]).model;
                allState.Add(temp.State, temp);
            }
            CurrentState = EnumAgentState.Ready;
        }

        public override Vector3 GetAimPosition()
        {
            return CamAimTarget.Position;
        }

        public override void AimAt(Vector3 target)
        {
            base.AimAt(target);
            Cam.LookAt(target);

        }

        protected override bool ChangeState(StateChangeParameter param)
        {
            EnumAgentState newState = param.Get<EnumAgentState>(EnumStateChangeParameter.newState);
            Logger.Log(" Player " + CurrentState + " ---> " + newState + " ? " + (CurrentState != newState && allState.ContainsKey(newState)), EnumLogColor.GreenLight);
            if (CurrentState != newState && allState.ContainsKey(newState))
            {
                Logger.Log(CurrentState + " -> " + newState, 0);
                param.Add(EnumStateChangeParameter.model, allState[newState]);
                //hasCurrentState = true;
                if (allState.ContainsKey(CurrentState))
                {
                    ActiveState.ExitState(param);
                }
                CurrentState = newState;

                ActiveState.EnterState(param);
                NotifyStateChanged(EnumNotifType.PlayerStateChanged);
                return true;
            }
            else
            if (CurrentState != newState)
            {
                throw new Exception(name + " has no " + newState + " state " + " Current " + CurrentState + " allState.Count " + allState.Count);
            }
            return false;
        }

        void UpdateLevelUI()
        {
            PlayerWidget.UpdateLevel(Level);
        }

        protected override void OnDied(NotificationParameter obj)
        {
            base.OnDied(obj);
            Notification.Notify(EnumNotifType.PlayerDied, new NotificationParameter(EnumNotifParam.ownerID, Id));
        }


        //public override void GotoCover(MTriggerCover trigger)
        //{
        //    ChangeStateFromNormal(EnumAgentState.Cover, new MStateChangeParameter(StateChangeParameter.cover, trigger));
        //    movementController.GotoCover(trigger);
        //}

        public override bool SwitchWeapon(EnumWeaponSlotIndex index)
        {
            var res = base.SwitchWeapon(index);
            if (res)
                NotifyStateChanged(EnumNotifType.PlayerChangeWeapon);
            return res;
        }

        void NotifyStateChanged(EnumNotifType notifType)
        {
            var data = new NotificationParameter(EnumNotifParam.newState, CurrentState)
            {
                { EnumNotifParam.CameraState, CurrentStateModel.CameraState },
                { EnumNotifParam.ChangeCameraState, CurrentStateModel.ChangeCameraState }, 
                //{ EnumNotifParam.IkState, CurrentStateModel.IkState },
                { EnumNotifParam.currentWeapon, weaponManager.WeaponType }
            };
            Notification.Notify(notifType, data);
        }

        public override Vector3 PredictPosition(float time)
        {
            return Position + (Locomotion.Velocity * time);
        }

        public override Vector3 PredictCenter(float time)
        {
            return PredictPosition(time) + (.5f * Locomotion.Height * Vector3.up); ;
        }
    }
}
