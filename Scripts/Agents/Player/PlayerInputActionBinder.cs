﻿using RCPU.Agent;
using RCPU.Attributes;
using RCPU.CameraFSM;
using RCPU.Component;
using RCPU.InputSystem;
using RCPU.NotificationSystem;
using RCPU.UI;

namespace RCPU
{
    [RequireInput]
    [InitOrder(EnumInitOrder.AgentState)]
    public class PlayerInputActionBinder : ComponentBase
    {
        [InjectFromInstance] protected IAgent Owner { get; set; }
        [InjectFromInstance] protected IPlayerActionContainer Agent { get; set; }
        [InjectFromInstance] protected ITriggerComponent TriggerComponent { get; set; }
        [InjectFromSystem] IUIManager UIManager { get; set; }
        [InjectFromSystem] ICameraAimTarget CamAimTarget { get; set; }
        protected override int OwnerId => Owner.Id;
        protected PlayerStateDataModel model;
        public override EnumComponentType ComponentType => EnumComponentType.InputBinder;
        public override void Init()
        {
            base.Init();
            Notification.RegisterInstance(Owner.Id, EnumInstanceNotifType.PlayerStateChanged, Id, OnStateChanged);
        }

        private void OnStateChanged(NotificationParameter parameter)
        {
            model = parameter.Get<PlayerStateDataModel>(EnumNotifParam.dataModel);
        }

        protected override void OnOwnerDied(NotificationParameter obj)
        {
            IsEnable = false;
        }
        protected override void OnOwnerSpawned(NotificationParameter obj)
        {
            IsEnable = true;
        }

        protected bool CanMove() { return/* this.IsEnable &&*/ (model.MoveRotateAbility == EnumMoveRotateAbility.SimpleMove || model.MoveRotateAbility == EnumMoveRotateAbility.CoverMove || model.MoveRotateAbility == EnumMoveRotateAbility.StrafMove); }
        protected bool CanOnlyRotate() { return /*this.IsEnable &&*/ model.MoveRotateAbility == EnumMoveRotateAbility.RotateOnly; }
        protected bool CanSwitchWeapon() { return  /*this.IsEnable &&*/  model.SwitchWeaponState == EnumActivityState.Can; }
        protected bool CanRoll() { return  /*this.IsEnable &&*/  model.RollState == EnumActivityState.Can; }
        protected bool IsRolling() { return  /*this.IsEnable &&*/ model.RollState == EnumActivityState.Is; }
        protected bool CanHandleAim() { return  /*this.IsEnable &&*/ (model.AimState == EnumActivityState.Can || model.AimState == EnumActivityState.Is); }
        protected bool CanHandleShoot() { return /*this.IsEnable &&*/ CamAimTarget.CanShoot && (model.ShootState == EnumActivityState.Can || model.ShootState == EnumActivityState.Is); }
        protected bool CanReload() { return /*this.IsEnable &&*/ model.ReloadState == EnumActivityState.Can; }
        protected bool CanHandleSprint() { return  /*this.IsEnable &&*/  (model.SprintState == EnumActivityState.Can || model.SprintState == EnumActivityState.Is); }
        protected bool IsActive() { return IsEnable; }
        protected bool HasAimTarget() { return IsEnable && CamAimTarget.Hit; }

        //--------------------------- weapon  
        [InputRegisterButtonDown(EnumInputPriority.SwitchWeapon, EnumUnityEvent.Update, "CanSwitchWeapon", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Slot1)]
        protected void InputSwitchWeaponUp() => Agent.TrySwitchWeapon(EnumWeaponSlotIndex.Up);

        [InputRegisterButtonDown(EnumInputPriority.SwitchWeapon, EnumUnityEvent.Update, "CanSwitchWeapon", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Slot2)]
        protected void InputSwitchWeaponUpRight() => Agent.TrySwitchWeapon(EnumWeaponSlotIndex.UpRight);

        [InputRegisterButtonDown(EnumInputPriority.SwitchWeapon, EnumUnityEvent.Update, "CanSwitchWeapon", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Slot3)]
        protected void InputSwitchWeaponRight() => Agent.TrySwitchWeapon(EnumWeaponSlotIndex.Right);

        [InputRegisterButtonDown(EnumInputPriority.SwitchWeapon, EnumUnityEvent.Update, "CanSwitchWeapon", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Slot4)]
        protected void InputSwitchWeaponDownRight() => Agent.TrySwitchWeapon(EnumWeaponSlotIndex.DownRight);

        [InputRegisterButtonDown(EnumInputPriority.SwitchWeapon, EnumUnityEvent.Update, "CanSwitchWeapon", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Slot5)]
        protected void InputSwitchWeaponDown() => Agent.TrySwitchWeapon(EnumWeaponSlotIndex.Down);

        [InputRegisterButtonDown(EnumInputPriority.SwitchWeapon, EnumUnityEvent.Update, "CanSwitchWeapon", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Slot6)]
        protected void InputSwitchWeaponDownLeft() => Agent.TrySwitchWeapon(EnumWeaponSlotIndex.DownLeft);

        [InputRegisterButtonDown(EnumInputPriority.SwitchWeapon, EnumUnityEvent.Update, "CanSwitchWeapon", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Slot7)]
        protected void InputSwitchWeaponLeft() => Agent.TrySwitchWeapon(EnumWeaponSlotIndex.Left);

        [InputRegisterButtonDown(EnumInputPriority.SwitchWeapon, EnumUnityEvent.Update, "CanSwitchWeapon", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Slot8)]
        protected void InputSwitchWeaponUpLeft() => Agent.TrySwitchWeapon(EnumWeaponSlotIndex.UpLeft);
        //--------------------------- MENU

        [InputRegisterButtonDown(EnumInputPriority.SwitchWeapon, EnumUnityEvent.Update, "CanSwitchWeapon", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.WeaponMenu)]
        protected void InputShowWeaponMenu() => UIManager.SetWeaponMenuEnable(true);

        [InputRegisterButtonUp(EnumInputPriority.SwitchWeapon, EnumUnityEvent.Update, "CanSwitchWeapon", EnumGameOveralState.InGame, EnumInGameUiState.WeaponSelection, EnumButton.WeaponMenu)]
        protected void InputHideWeaponMenu(float time) => UIManager.SetWeaponMenuEnable(false);

        [InputRegisterButtonHold(EnumInputPriority.UI, EnumUnityEvent.Update, "HasAimTarget", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Scan)]
        protected void Scan(float hold) => UIManager.Scan(hold, CamAimTarget.HitId);

        //---------------------------   locomotion

        [InputRegisterAxis(EnumInputPriority.Move, EnumUnityEvent.Update, "CanMove", EnumGameOveralState.InGame, EnumInGameUiState.HUD)]
        protected virtual void InputHandleMove(float[] axis, bool[] buttons) => Agent.Move(buttons[(int)EnumButton.Sprint], axis[(int)EnumAxis.LeftVertical], axis[(int)EnumAxis.LeftHorizontal], buttons[(int)EnumButton.Aim]);

        [InputRegisterAxis(EnumInputPriority.Move, EnumUnityEvent.Update, "CanMove", EnumGameOveralState.InGame, EnumInGameUiState.WeaponSelection)]
        protected virtual void InputStopMove(float[] axis, bool[] buttons) => Agent.Move(false, 0, 0, false);

        [InputRegisterAxis(EnumInputPriority.Move, EnumUnityEvent.Update, "CanHandleSprint", EnumGameOveralState.InGame, EnumInGameUiState.HUD)]
        protected void HandleSprint(float[] axis, bool[] btn) => Agent.HandleSprint(axis[(int)EnumAxis.LeftVertical], axis[(int)EnumAxis.LeftHorizontal]);

        [InputRegisterAxis(EnumInputPriority.Rotate, EnumUnityEvent.Update, "CanOnlyRotate", EnumGameOveralState.InGame, EnumInGameUiState.HUD)]
        protected virtual void InputRotate(float[] axis, bool[] buttons) => Agent.HandleRotate(axis[(int)EnumAxis.LeftVertical], axis[(int)EnumAxis.LeftHorizontal]);

        [InputRegisterButtonDown(EnumInputPriority.ChangePos, EnumUnityEvent.Update, "IsActive", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Crouch)]
        protected virtual void InputChangePosState() => Agent.HandleChangePosState();

        [InputRegisterButtonDown(EnumInputPriority.Roll, EnumUnityEvent.FixedUpdate, "CanRoll", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Roll)]
        protected virtual void InputRoll() => Agent.Roll();

        [InputRegisterAxis(EnumInputPriority.Roll, EnumUnityEvent.Update, "IsRolling", EnumGameOveralState.InGame, EnumInGameUiState.HUD)]
        protected virtual void HandleRoll(float[] axis, bool[] buttons) => Agent.HandleRoll(axis[(int)EnumAxis.LeftVertical], axis[(int)EnumAxis.LeftHorizontal]);


        //--------------------------- shoot aim reaload input

        [InputRegisterButtonDown(EnumInputPriority.Shoot, EnumUnityEvent.Update, "CanHandleShoot", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Shoot)]
        protected void InputDownShoot() => Agent.DownShoot();

        [InputRegisterButtonHold(EnumInputPriority.Shoot, EnumUnityEvent.Update, "CanHandleShoot", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Shoot)]
        protected void InputShoot(float hold) => Agent.HoldShoot(hold);

        [InputRegisterButtonDown(EnumInputPriority.Reload, EnumUnityEvent.Update, "CanReload", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Reload)]
        protected void InputReload() => Agent.HandleReload();

        [InputRegisterButtonHold(EnumInputPriority.Aim, EnumUnityEvent.Update, "CanHandleAim", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Aim)]
        protected void HandleAim(float hold) => Agent.HandleAim(hold);

        //--------------------------- Trigger interact
        [InputRegisterButtonDown(EnumInputPriority.Pickup, EnumUnityEvent.Update, "IsActive", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.Pickup)]
        protected void Pickup() => TriggerComponent.Interact();

    }
}