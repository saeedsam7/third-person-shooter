﻿
using System;
using System.Collections.Generic;

namespace RCPU.NotificationSystem
{
    public class NotificationAction : Dictionary<long, Action<NotificationParameter>>
    {

    }
    public class NotificationGroup : Dictionary<EnumNotifType, NotificationAction>
    {

    }
    public class InstanceNotificationGroup : Dictionary<EnumInstanceNotifType, NotificationAction>
    {

    }
}
