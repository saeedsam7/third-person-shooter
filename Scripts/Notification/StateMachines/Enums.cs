﻿

namespace RCPU.StateMachine
{
    public enum EnumStateChangeParameter
    {
        currentWeapon,
        shoot,
        angleH,
        angleV,
        owner,
        cover,
        newState,
        force,
        reloadAnimatorSpeed,
        reloadTime,
        model,
        isNewState,
        targets,
        stateDetail,
        inGameStateUi
    }
}
