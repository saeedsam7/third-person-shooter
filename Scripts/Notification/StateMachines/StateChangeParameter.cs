﻿using System;
using System.Collections.Generic;

namespace RCPU.StateMachine
{
    public class StateChangeParameter : Dictionary<EnumStateChangeParameter, object>
    {
        public StateChangeParameter()
        {
        }
        public StateChangeParameter(EnumStateChangeParameter key, object val)
        {
            Add(key, val);
        }
        //public StateChangeParameter(EnumStateChangeParameter key1, object val1, EnumStateChangeParameter key2, object val2)
        //{
        //    Add(key1, val1);
        //    Add(key2, val2);
        //}
        public StateChangeParameter(EnumStateChangeParameter[] key, object[] val)
        {
            if (key.Length != val.Length)
                throw new Exception("key value count not match");
         
            for (int i = 0; i < key.Length; i++)
            {
                Add(key[i], val[i]);
            }
        }

        public T Get<T>(EnumStateChangeParameter name)
        {
            return (T)this[name];
        }

        public bool Has(EnumStateChangeParameter name)
        {
            return this.ContainsKey(name);
        }
    }

}