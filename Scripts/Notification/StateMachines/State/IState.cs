﻿
namespace RCPU.StateMachine
{

    public interface IState : IInitializable
    {
        void UpdateState();
        void LateUpdateState();
        void ExitState(StateChangeParameter mStateChangeParameter);
        void EnterState(StateChangeParameter param);
    }
}
