
using RCPU.NotificationSystem;

namespace RCPU.StateMachine
{

    public class StateBase : BaseObject
    {
        public virtual void Init()
        {
            this.IsEnable = false;
        }

        public bool IsActive()
        {
            return IsEnable;
        }

        public virtual void Push(NotificationParameter param)
        {
        }
        public virtual void EnterState(StateChangeParameter param)
        {
            this.IsEnable = true;
        }

        //private void Update()
        //{
        //    UpdateState();
        //}

        private void LateUpdate()
        {
            LateUpdateState();
        }

        public virtual void UpdateState()
        {
        }

        public virtual void LateUpdateState()
        {
        }

        public virtual void ExitState(StateChangeParameter param)
        {
            this.IsEnable = false;
        }
    }
}
