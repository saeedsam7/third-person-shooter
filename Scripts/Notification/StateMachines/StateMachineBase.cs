using RCPU.NotificationSystem;
using System;
using UnityEngine;

namespace RCPU.StateMachine
{
    public class StateMachineBase : BaseObject, IStateMachine
    {

        public string Name => name;
        //protected bool hasCurrentState;
        Transform parent;
        bool inited;

        public virtual void Init()
        {
            inited = true;
        }
        protected virtual void Spawn()
        {
            parent = transform.parent;
            SetEnable(true);
        }
        void Update()
        {
            if (inited)
                UpdateStateMachine();
        }
        void FixedUpdate()
        {
            if (inited)
                FixedUpdateStateMachine();
        }
        void LateUpdate()
        {
            if (inited)
                LateUpdateStateMachine();
        }

        public virtual bool FrameChanged()
        {
            return true;
        }

        protected virtual void UpdateStateMachine()
        {

        }
        protected virtual void FixedUpdateStateMachine()
        {
        }

        protected virtual void LateUpdateStateMachine()
        {
        }

        public void Register(EnumNotifType notifType, Action<NotificationParameter> action)
        {
            Notification.Register(notifType, Id, action);
        }

        public void UnRegister(EnumNotifType notifType)
        {
            Notification.UnRegister(notifType, Id);
        }

        public bool IsActive()
        {
            return IsEnable;
        }


        public void SetParent(Transform tr)
        {
            transform.SetParent(tr);
        }

        public void ResetParent()
        {
            transform.SetParent(parent);
        }

        public virtual void SetEnable(bool value)
        {
            if (!value)
                StopAllCoroutines();
            gameObject.SetActive(value);
        }



        //public Coroutine RunCoroutine(IEnumerator routine)
        //{
        //    return StartCoroutine(routine);
        //}
    }
}