﻿namespace RCPU.StateMachine
{
    public interface IStateMachine : IInitializable
    {
        string Name { get; }
        bool FrameChanged();
        //Coroutine RunCoroutine(IEnumerator routine);
        void SetEnable(bool value);
    }
}
