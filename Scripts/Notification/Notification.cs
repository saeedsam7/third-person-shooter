﻿
using RCPU.Attributes;
using System;
using System.Collections.Generic;

namespace RCPU.NotificationSystem
{
    [InitOrder(EnumInitOrder.Notification)] 
    public class Notification : BaseObject,/* INotification, */IInitializable
    {
        protected static NotificationGroup generalGroup;
        protected static Dictionary<long, InstanceNotificationGroup> instanceGroups;



        static void UnRegister(NotificationGroup group, EnumNotifType notifType, long id)
        {
            if (!group.ContainsKey(notifType))
            {
                return;
            }
            if (group[notifType].ContainsKey(id))
                group[notifType].Remove(id);
        }

        static void UnRegister(InstanceNotificationGroup group, EnumInstanceNotifType notifType, long id)
        {
            if (!group.ContainsKey(notifType))
            {
                return;
            }
            if (group[notifType].ContainsKey(id))
                group[notifType].Remove(id);
        }
        static void Notify(NotificationGroup group, EnumNotifType notifType, NotificationParameter hashtable)
        {
            if (!group.ContainsKey(notifType))
            {
                return;
            }
            //Debug.LogError("Notify " + notifType);
            foreach (var item in group[notifType])
            {
                item.Value(hashtable);
            }
        }

        static void Notify(InstanceNotificationGroup group, EnumInstanceNotifType notifType, NotificationParameter hashtable)
        {
            if (!group.ContainsKey(notifType))
            {
                return;
            }
            //Debug.LogError("Notify " + notifType);
            foreach (var item in group[notifType])
            {
                item.Value(hashtable);
            }
        }
        static void Register(NotificationGroup group, EnumNotifType notifType, long id, Action<NotificationParameter> action)
        {
            if (!group.ContainsKey(notifType))
            {
                group.Add(notifType, new NotificationAction());
            }
            if (!group[notifType].ContainsKey(id))
                group[notifType].Add(id, action);
        }

        static void Register(InstanceNotificationGroup group, EnumInstanceNotifType notifType, long id, Action<NotificationParameter> action)
        {
            if (!group.ContainsKey(notifType))
            {
                group.Add(notifType, new NotificationAction());
            }
            if (!group[notifType].ContainsKey(id))
                group[notifType].Add(id, action);
        }


        public void Init()
        {
            generalGroup = new NotificationGroup();
            instanceGroups = new Dictionary<long, InstanceNotificationGroup>();
        }

        public static void Register(EnumNotifType notifType, long id, Action<NotificationParameter> action)
        {
            Register(generalGroup, notifType, id, action);
        }

        public static void UnRegister(EnumNotifType notifType, long id)
        {
            UnRegister(generalGroup, notifType, id);
        }

        public static void Notify(EnumNotifType notifType, NotificationParameter hashtable)
        {
            Notify(generalGroup, notifType, hashtable);
        }

        public static void RegisterInstance(long ownerID, EnumInstanceNotifType notifType, long id, Action<NotificationParameter> action)
        {
            if (!instanceGroups.ContainsKey(ownerID))
            {
                instanceGroups.Add(ownerID, new InstanceNotificationGroup());
            }
            Register(instanceGroups[ownerID], notifType, id, action);
        }

        public static void UnRegisterInstance(long ownerID, EnumInstanceNotifType notifType, long id)
        {
            if (!instanceGroups.ContainsKey(ownerID))
            {
                return;
            }
            UnRegister(instanceGroups[ownerID], notifType, id);
        }

        public static void NotifyInstance(long ownerID, EnumInstanceNotifType notifType, NotificationParameter hashtable)
        {
            if (!instanceGroups.ContainsKey(ownerID))
            {
                return;
            }
            Notify(instanceGroups[ownerID], notifType, hashtable);
        }
    }
}