﻿using System;

namespace RCPU.NotificationSystem
{
    public class ActionModel
    {
        public Action action;
    }

    public class ActionModel<T>
    {
        public Action<T> action;
    }

    public class ActionAndParameter<T>
    {
        public Action<T, NotificationParameter> action;
        public NotificationParameter parameters;
    }

    public class ActionModel<T, K>
    {
        public Action<T, K> action;
    }



    public class FuncModel<T>
    {
        public Func<T> action;
    }

    public class FuncModel<T, K>
    {
        public Func<T, K> action;
    }


}