﻿
using System;
using System.Collections.Generic;

namespace RCPU.NotificationSystem
{
    public class NotificationParameter : Dictionary<EnumNotifParam, object>
    {
        public NotificationParameter()
        {
        }

        public NotificationParameter(EnumNotifParam key, object val)
        {
            Add(key, val);
        }


        //public NotificationParameter(EnumNotifParam key1, object val1, EnumNotifParam key2, object val2)
        //{
        //    Add(key1, val1);
        //    Add(key2, val2);
        //}
        //public NotificationParameter(EnumNotifParam key1, object val1, EnumNotifParam key2, object val2, EnumNotifParam key3, object val3)
        //{
        //    Add(key1, val1);
        //    Add(key2, val2);
        //    Add(key3, val3);
        //}
        //public NotificationParameter(EnumNotifParam key1, object val1, EnumNotifParam key2, object val2,
        //   EnumNotifParam key3, object val3, EnumNotifParam key4, object val4)
        //{
        //    Add(key1, val1);
        //    Add(key2, val2);
        //    Add(key3, val3);
        //    Add(key4, val4);
        //}

        //public NotificationParameter(EnumNotifParam key1, object val1, EnumNotifParam key2, object val2,
        //  EnumNotifParam key3, object val3, EnumNotifParam key4, object val4, EnumNotifParam key5, object val5)
        //{
        //    Add(key1, val1);
        //    Add(key2, val2);
        //    Add(key3, val3);
        //    Add(key4, val4);
        //    Add(key5, val5);
        //}

        public NotificationParameter(EnumNotifParam[] key, object[] val)
        {
            if (key.Length != val.Length)
                throw new Exception("key value count not match");
            for (int i = 0; i < key.Length; i++)
            {
                Add(key[i], val[i]);
            }
        }

        public T Get<T>(EnumNotifParam name)
        {
            return (T)this[name];
        }
    }
}
