﻿using System.Collections;
using System.Reflection;

namespace RCPU.DependencyInjection
{
    public class DInjectionRequest
    {
        public BaseObject instance;
        public PropertyInfo propertyInfo;

        public DInjectionRequest(BaseObject instance, PropertyInfo propertyInfo)
        {
            this.instance = instance;
            this.propertyInfo = propertyInfo;
        }

        public object GetValue()
        {
            return propertyInfo.GetValue(instance, null);
        }

        public void SetValue(IList array)
        {
            propertyInfo.SetValue(instance, array);
        }
        public void SetValue(object obj)
        {
            propertyInfo.SetValue(instance, obj);
        }
    }
}