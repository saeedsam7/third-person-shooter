
using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Helper;
using RCPU.InputSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace RCPU.DependencyInjection
{
    public class DInjector : BaseObject, IInitializable
    {

        static Dictionary<string, BaseObject> injectObjects;
        static Dictionary<string, List<DInjectionRequest>> injectRequest;

        static Dictionary<string, List<BaseObject>> injectArrays;
        static Dictionary<string, List<DInjectionRequest>> injectArrayRequest;

        static List<BaseObject> inputRequest;
        static Dictionary<string, DInjectionRequestStatus> notFinishedInstanceRequest;

        [InjectFromSystem] static IInput InputController { get; set; }
        public void Init()
        {

        }
        public void Init(bool debug)
        {
            DEBUG = debug;
            notFinishedInstanceRequest = new Dictionary<string, DInjectionRequestStatus>();
            inputRequest = new List<BaseObject>();
            injectObjects = new Dictionary<string, BaseObject>();
            injectRequest = new Dictionary<string, List<DInjectionRequest>>();
            injectArrays = new Dictionary<string, List<BaseObject>>();
            injectArrayRequest = new Dictionary<string, List<DInjectionRequest>>();
        }

        static void AddInjectObject(Type interfaceName, BaseObject obj)
        {
            if (injectObjects.ContainsKey(interfaceName.FullName))
                throw new Exception("more than one object inject for type > " + interfaceName.FullName);

            injectObjects.Add(interfaceName.FullName, obj);

        }

        static void AddInjectArray(Type interfaceName, BaseObject obj)
        {
            if (!injectArrays.ContainsKey(interfaceName.FullName))
                injectArrays.Add(interfaceName.FullName, new List<BaseObject>());
            injectArrays[interfaceName.FullName].Add(obj);
        }

        static List<BaseObject> GetInjectArray(string type)
        {
            if (injectArrays.ContainsKey(type))
                return injectArrays[type];
            return null;
            //throw new Exception("the type > "+ type+" not provided for injection");
        }

        static object GetInjectObject(string type)
        {
            if (injectObjects.ContainsKey(type))
                return injectObjects[type];
            return null;
            //throw new Exception("the type > "+ type+" not provided for injection");
        }

        public void Instantiate(Transform gameRoot, GameObject[] instantiatableObject/*, MScriptArray initializableObject*/)
        {
            List<KeyValuePair<int, IInitializable>> initializables = new List<KeyValuePair<int, IInitializable>>();
            if (DEBUG)
            {
                Debug.Log("Instantiate 10");
            }
            //Objects in scene
            foreach (var item in FindObjectsOfType<BaseObject>())
            {
                if (DEBUG)
                {
                    Debug.Log("Instantiate 11");
                }
                if (item is IInitializable && item.GetType() != typeof(DInjector))
                    initializables.Add(new KeyValuePair<int, IInitializable>(MethodBaseAttribute.GetAttributeOrder(item.GetType()), item as IInitializable));
                if (DEBUG)
                {
                    Debug.Log("Instantiate 12");
                }
                CheckForInstanceInjection(item, item.transform.root);
                if (DEBUG)
                {
                    Debug.Log("Instantiate 13");
                }
                CheckForInjectionAndInput(item, DEBUG);
            }
            if (DEBUG)
            {
                Debug.Log("Instantiate 20");
            }
            //prefabs
            foreach (var item in instantiatableObject)
            {
                var instance = Instantiate(item, gameRoot);
                foreach (var mono in instance.GetComponentsInChildren<BaseObject>())
                {
                    CheckForInstanceInjection(mono, instance.transform);
                    CheckForInjectionAndInput(mono, DEBUG);
                }
                foreach (var mono in instance.GetComponentsInChildren<IInitializable>())
                {
                    initializables.Add(new KeyValuePair<int, IInitializable>(MethodBaseAttribute.GetAttributeOrder(mono.GetType()), mono));
                }
            }

            ////classes
            //foreach (var item in initializableObject.scripts)
            //{
            //    var instance = componentMaker.CreateClass(item);
            //    initializables.Add(new KeyValuePair<int, IInitializable>(Attribute_Method_Base.GetAttributeOrder(instance.GetType()), instance as IInitializable));
            //    CheckForInjection(instance);
            //}

            if (DEBUG)
            {
                Debug.Log("Instantiate 30");
            }
            //Inject all 
            InjectAll();
            if (DEBUG)
            {
                Debug.Log("Instantiate 40");
            }
            //log all unfinished inject from instance
            foreach (var item in notFinishedInstanceRequest.Values)
            {
                if (item.status == false)
                    Debug.LogError(" inject from instance request not completed. \r gameobject : " + item.instance.name + " , class : " + item.instance.GetType().Name + " , property : " + item.property);
            }
            if (DEBUG)
            {
                Debug.Log("Instantiate 50");
            }
            if (injectRequest.Count > 0)
            {
                foreach (var item in injectRequest)
                {
                    Debug.LogError(" unfinished inject request injectRequest " + item.Key + " - " + item.Value[0]);
                }
                //throw new Exception("unfinished inject request exist " + injectRequest.Count);
            }
            if (DEBUG)
            {
                Debug.Log("Instantiate 60");
            }
            //Init all initializables
            initializables = initializables.OrderBy(x => x.Key).ToList();
            foreach (var item in initializables)
            {
                item.Value.Init();
            }
            if (DEBUG)
            {
                Debug.Log("Instantiate 70");
            }
            RegisterInputs();
            if (DEBUG)
            {
                Debug.Log("Instantiate 80");
            }
        }
        static void InjectAll()
        {
            InjectAllObjects();
            InjectAllArrays();
        }
        static void InjectAllArrays()
        {
            List<string> finished = null;
            foreach (var item in injectArrayRequest)
            {
                var target = GetInjectArray(item.Key);
                if (!CommonHelper.IsNull(target))
                {
                    for (int i = 0; i < item.Value.Count; i++)
                    {
                        var array = (IList)item.Value[i].GetValue();
                        Type t = Type.GetType(item.Key);
                        if (CommonHelper.IsNull(array))
                        {
                            array = Array.CreateInstance(t, target.Count);
                            for (int index = 0; index < target.Count; index++)
                            {
                                array[index] = target[index];
                            }
                            item.Value[i].SetValue(array);
                        }
                        else
                        {
                            var copy = Array.CreateInstance(t, array.Count + target.Count);
                            for (int index = 0; index < array.Count; index++)
                            {
                                copy.SetValue(array[index], index);
                            }
                            for (int index = 0; index < target.Count; index++)
                            {
                                copy.SetValue(target[index], array.Count + index);
                            }
                            item.Value[i].SetValue(copy);
                        }

                        //item.Value[i].Item2.SetValue(item.Value[i].Item1, target);


                    }
                    if (finished == null)
                        finished = new List<string>();
                    finished.Add(item.Key);
                }
            }
            if (finished != null)
                for (int i = 0; i < finished.Count; i++)
                {
                    injectArrayRequest.Remove(finished[i]);
                }
        }

        static void InjectAllObjects()
        {
            List<string> finished = null;
            foreach (var item in injectRequest)
            {
                var target = GetInjectObject(item.Key);
                if (!CommonHelper.IsNull(target))
                {
                    for (int i = 0; i < item.Value.Count; i++)
                    {
                        item.Value[i].SetValue(target);
                    }
                    if (finished == null)
                        finished = new List<string>();
                    finished.Add(item.Key);
                }
            }
            if (finished != null)
                for (int i = 0; i < finished.Count; i++)
                {
                    injectRequest.Remove(finished[i]);
                }
        }

        static void RegisterInputs()
        {
            for (int i = 0; i < inputRequest.Count; i++)
            {
                InputController.RegisterInputs(inputRequest[i]);
            }
            InputController.SortAllInputByPriority();
            inputRequest.Clear();
        }

        private static void CheckForInstanceInjection(BaseObject item, Transform root)
        {
            // make a dictionary of all InjectToInstanceAttribute
            var injectsToInstance = item.GetType().GetCustomAttributes(typeof(InjectToInstanceAttribute)) as IEnumerable<InjectToInstanceAttribute>;
            Dictionary<string, object> injectProperities = new Dictionary<string, object>();
            if (!CommonHelper.IsNull(injectsToInstance) && injectsToInstance.Count() > 0)
            {
                foreach (var owner in injectsToInstance)
                {
                    var name = owner.interfaceType.FullName;
                    if (injectProperities.ContainsKey(name))
                    {
                        throw new InvalidOperationException("the object contains more than one Attribute_InjectOwner " + injectProperities[name] + " " + item);
                    }
                    injectProperities.Add(name, item);
                }
            }

            // make a dictionary of all BindToDecisionAttribute
            var objectMethods = item.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            Dictionary<EnumDecision, Action> injectDecision = new Dictionary<EnumDecision, Action>();
            Dictionary<EnumAgentStateEvent, Delegate> injectStateEvent = new Dictionary<EnumAgentStateEvent, Delegate>();
            for (int i = 0; i < objectMethods.Length; i++)
            {
                var attribute1 = objectMethods[i].GetCustomAttribute(typeof(InjectStateActionAttribute)) as InjectStateActionAttribute;
                if (!CommonHelper.IsNull(attribute1))
                {
                    var Key = attribute1.state;
                    if (injectStateEvent.ContainsKey(Key))
                    {
                        throw new InvalidOperationException("the object contains more than one method for the  decision " + Key + " " + item);
                    }
                    injectStateEvent.Add(Key, attribute1.Value(item, objectMethods[i]));
                }

                var attribute2 = objectMethods[i].GetCustomAttribute(typeof(InjectDecisionActionAttribute)) as InjectDecisionActionAttribute;
                if (!CommonHelper.IsNull(attribute2))
                {
                    var Key = attribute2.decision;
                    if (injectDecision.ContainsKey(Key))
                    {
                        throw new InvalidOperationException("the object contains more than one method for the  decision " + Key + " " + item);
                    }
                    injectDecision.Add(Key, attribute2.Value(item, objectMethods[i]));
                }
            }

            //inject all requested InjectToInstanceAttribute and BindToDecisionAttribute
            if (injectProperities.Count > 0 || injectDecision.Count > 0 || injectStateEvent.Count > 0)
            {
                foreach (var child in root.GetComponentsInChildren(typeof(BaseObject), true))
                {
                    int id = child.GetInstanceID();
                    //if (id == item.Id) continue;
                    var objectAllFields = child.GetType().GetRuntimeProperties().ToArray();
                    for (int i = 0; i < objectAllFields.Length; i++)
                    {
                        InjectFromInstance(objectAllFields[i], child, injectProperities, injectDecision, injectStateEvent, id);
                    }
                }
            }

        }


        private static void InjectFromInstance(PropertyInfo propertyInfo, UnityEngine.Component instance, Dictionary<string, object> injectProperities, Dictionary<EnumDecision, Action> injectDecision, Dictionary<EnumAgentStateEvent, Delegate> injectStateEvent, int id)
        {
            var arrayRequest = propertyInfo.GetCustomAttribute(typeof(InjectFromInstanceAttribute)) as InjectFromInstanceAttribute;
            if (!CommonHelper.IsNull(arrayRequest))
            {
                if (propertyInfo.PropertyType.IsArray)
                {
                    string name = propertyInfo.PropertyType.GetElementType().FullName;
                    if (!notFinishedInstanceRequest.ContainsKey(name + id))
                    {
                        notFinishedInstanceRequest.Add(name + id, new DInjectionRequestStatus(instance, false, name));
                    }
                    if (injectProperities.ContainsKey(name))
                    {
                        var array = (IList)propertyInfo.GetValue(instance, null);
                        Type t = Type.GetType(name);
                        if (CommonHelper.IsNull(array))
                        {
                            array = Array.CreateInstance(t, 1);
                            array[0] = (injectProperities[name]);
                            propertyInfo.SetValue(instance, array);
                        }
                        else
                        {
                            var copy = Array.CreateInstance(t, array.Count + 1);
                            for (int index = 0; index < array.Count; index++)
                            {
                                copy.SetValue(array[index], index);
                            }
                            copy.SetValue(injectProperities[name], array.Count);
                            propertyInfo.SetValue(instance, copy);
                        }
                        notFinishedInstanceRequest[name + id] = new DInjectionRequestStatus(instance, true, name);
                    }
                }
                else
              if (propertyInfo.PropertyType.IsGenericType)
                {
                    Type[] arguments = injectDecision.GetType().GetGenericArguments();
                    Type keyType = arguments[0];
                    Type valueType = arguments[1];
                    if (propertyInfo.PropertyType.GetGenericArguments()[0] == keyType && propertyInfo.PropertyType.GetGenericArguments()[1] == valueType)
                    {
                        //string name = typeof(Tkey).FullName;
                        //if (!notFinishedInstanceRequest.ContainsKey(name + id))
                        //{
                        //    notFinishedInstanceRequest.Add(name + id, (Tuple.Create(instance, false, name)));
                        //}
                        var array = (IDictionary)propertyInfo.GetValue(instance, null);
                        if (CommonHelper.IsNull(array))
                        {
                            array = (IDictionary)Activator.CreateInstance(propertyInfo.PropertyType);
                        }
                        foreach (var item in injectDecision)
                        {
                            array.Add(item.Key, item.Value);
                        }
                        propertyInfo.SetValue(instance, array);
                        //notFinishedInstanceRequest[name + id] = Tuple.Create(instance, true, name);
                    }
                    else
                    {
                        arguments = injectStateEvent.GetType().GetGenericArguments();
                        keyType = arguments[0];
                        valueType = arguments[1];
                        if (propertyInfo.PropertyType.GetGenericArguments()[0] == keyType && propertyInfo.PropertyType.GetGenericArguments()[1] == valueType)
                        {
                            //string name = typeof(Tkey).FullName;
                            //if (!notFinishedInstanceRequest.ContainsKey(name + id))
                            //{
                            //    notFinishedInstanceRequest.Add(name + id, (Tuple.Create(instance, false, name)));
                            //}
                            var array = (IDictionary)propertyInfo.GetValue(instance, null);
                            if (CommonHelper.IsNull(array))
                            {
                                array = (IDictionary)Activator.CreateInstance(propertyInfo.PropertyType);
                            }
                            foreach (var item in injectStateEvent)
                            {
                                array.Add(item.Key, item.Value);
                            }
                            propertyInfo.SetValue(instance, array);
                            //notFinishedInstanceRequest[name + id] = Tuple.Create(instance, true, name);
                        }
                    }
                }
                else
                {
                    string name = propertyInfo.PropertyType.FullName;
                    if (!notFinishedInstanceRequest.ContainsKey(name + id))
                    {
                        notFinishedInstanceRequest.Add(name + id, new DInjectionRequestStatus(instance, false, name));
                    }
                    if (injectProperities.ContainsKey(name))
                    {
                        propertyInfo.SetValue(instance, injectProperities[name]);
                        notFinishedInstanceRequest[name + id] = new DInjectionRequestStatus(instance, true, name);
                    }
                    //throw new Exception("the " + instance.GetType() + " object the " + propertyInfo.Name + " has InjectArrayFromInstance attribute but it's not an array");
                }
            }
        }

        private static void CheckForInjectionAndInput(BaseObject item, bool debug)
        {
            if (debug)
            {
                Debug.Log("Instantiate 130");
            }
            var injectToSystem = item.GetType().GetCustomAttributes(typeof(InjectToSystemAttribute)) as IEnumerable<InjectToSystemAttribute>;
            if (!CommonHelper.IsNull(injectToSystem) && injectToSystem.Count() > 0)
            {
                foreach (var inject in injectToSystem)
                    AddInjectObject(inject.interfaceType, item);
            }
            if (debug)
            {
                Debug.Log("Instantiate 131");
            }
            var injectToSystemArray = item.GetType().GetCustomAttributes(typeof(InjectToSystemArrayAttribute)) as IEnumerable<InjectToSystemArrayAttribute>;
            if (!CommonHelper.IsNull(injectToSystemArray) && injectToSystemArray.Count() > 0)
            {
                foreach (var inject in injectToSystemArray)
                    AddInjectArray(inject.interfaceType, item);
            }
            if (debug)
            {
                Debug.Log("Instantiate 132");
            }
            var requireInput = item.GetType().GetCustomAttribute(typeof(RequireInputAttribute)) as RequireInputAttribute;
            if (!CommonHelper.IsNull(requireInput))
            {
                inputRequest.Add(item);
            }
            if (debug)
            {
                Debug.Log("Instantiate 133");
            }
            var objectFields = item.GetType().GetRuntimeProperties().ToArray();
            for (int i = 0; i < objectFields.Length; i++)
            {
                var field = objectFields[i].GetCustomAttribute(typeof(InjectFromSystemAttribute)) as InjectFromSystemAttribute;
                if (!CommonHelper.IsNull(field))
                {
                    InjectFromSystem(item, objectFields[i]);
                }
            }
            if (debug)
            {
                Debug.Log("Instantiate 134");
            }
        }

        private static void InjectFromSystem(BaseObject instance, PropertyInfo propertyInfo)
        {
            if (propertyInfo.PropertyType.IsArray)
            {
                string name = propertyInfo.PropertyType.GetElementType().FullName;
                if (!injectArrayRequest.ContainsKey(name))
                    injectArrayRequest.Add(name, new List<DInjectionRequest>());
                injectArrayRequest[name].Add(new DInjectionRequest(instance, propertyInfo));

            }
            else
             if (propertyInfo.PropertyType.IsGenericType)
            {
                throw new Exception("the " + instance.GetType() + " object the " + propertyInfo.Name + " has InjectArrayFromInstance attribute but it's not an array");
            }
            else
            {
                string name = propertyInfo.PropertyType.FullName;
                if (!injectRequest.ContainsKey(name))
                    injectRequest.Add(name, new List<DInjectionRequest>());
                injectRequest[name].Add(new DInjectionRequest(instance, propertyInfo));
                //throw new Exception("the " + item.GetType() + " object the " + arrayFields[i].Name + " has InjectArrayFromInstance attribute but it's not an array");
            }

        }

        public static GameObject Instantiate(GameObject go, Transform parent)
        {
            if (CommonHelper.IsNull(parent))
                return Instantiate(go);
            return Instantiate(go, parent.position, parent.rotation, parent);
        }

        public static new T Instantiate<T>(T go, Transform parent) where T : UnityEngine.Object
        {
            if (CommonHelper.IsNull(parent))
                return Instantiate(go) as T;
            return Instantiate(go, parent.position, parent.rotation, parent) as T;
        }


        public static GameObject InstantiateAndInject(GameObject prefab, Transform parent, bool debug)
        {
            GameObject instance = Instantiate(prefab, parent);


            foreach (var item in instance.GetComponentsInChildren<BaseObject>())
            {
                CheckForInstanceInjection(item, instance.transform);
                CheckForInjectionAndInput(item, debug);
            }
            InjectAll();
            RegisterInputs();

            List<KeyValuePair<int, IInitializable>> initializables = new List<KeyValuePair<int, IInitializable>>();
            foreach (var mono in instance.GetComponentsInChildren<IInitializable>())
            {
                initializables.Add(new KeyValuePair<int, IInitializable>(MethodBaseAttribute.GetAttributeOrder(mono.GetType()), mono));
            }
            initializables = initializables.OrderBy(x => x.Key).ToList();
            foreach (var item in initializables)
            {
                item.Value.Init();
            }

            return instance;
        }

        //public static T InstantiateAndInject<T>(T go, Transform parent, bool debug) where T : UnityEngine.Object
        //{
        //    return InstantiateAndInject(go, parent, debug) as T; ;
        //}


    }
}
