﻿

namespace RCPU.DependencyInjection
{
    public class DInjectionRequestStatus
    {
        public UnityEngine.Component instance;
        public bool status;
        public string property;


        public DInjectionRequestStatus(UnityEngine.Component instance, bool status, string property)
        {
            this.instance = instance;
            this.status = status;
            this.property = property;
        }
    }
}