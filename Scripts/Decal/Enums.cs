﻿

namespace RCPU.Decal
{
    public enum EnumDecalType
    {
        Wood, Concrete, Glass, Metal, Brick, Dirt, Plaster, Rock
    };
}