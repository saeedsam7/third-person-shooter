
using RCPU.Attributes;
using RCPU.UI;
using UnityEngine;

namespace RCPU
{
    public class TestScript : BaseObject
    {
        [InjectFromSystem] IUIManager UI { get; set; }
        public Transform damageTransform;
        void Start()
        {

        }

        [ContextMenu("Damage")]
        public void Damage()
        {
            UI.BindIndicatorToDamage(damageTransform.position);
        }

        void Update()
        {

        }
    }
}
