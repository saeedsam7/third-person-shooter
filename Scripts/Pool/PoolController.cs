using RCPU.Agent;
using RCPU.Attributes;
using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.NotificationSystem;
using RCPU.Vfx;
using RCPU.Weapon;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Pool
{
    [InitOrder(EnumInitOrder.Pool)]
    [InjectToSystem(typeof(IPool))]
    public class PoolController : BaseObject, IPool
    {
        Dictionary<EnumPoolable, PoolStack<IPoolable>> instances;
        [SerializeField] private PoolObjectTypeCount[] poolItems;
        [Space(50)]
        [Header("Binders")]
        [SerializeField] private PoolTypeBinders<EnumAmmoType>[] ammoBinder;
        [SerializeField] private PoolTypeBinders<EnumVfxType>[] vfxBinder;
        [SerializeField] private PoolTypeBinders<EnumMaterialType>[] matrialBinder;
        [SerializeField] private PoolTypeBinders<EnumShellType>[] shellBinder;
        [SerializeField] private PoolTypeBinders<EnumRagdollType>[] ragdollBinder;
        [SerializeField] private PoolTypeBinders<EnumAgentType>[] agentBinder;

        Dictionary<EnumPoolGroup, Dictionary<int, EnumPoolable>> allTypeBinder;

        //Dictionary<string, EnumMaterialType> allMatrialTags;
        public void Init()
        {
            Id = (int)EnumStaticIds.Pool;
            instances = new Dictionary<EnumPoolable, PoolStack<IPoolable>>();
            allTypeBinder = new Dictionary<EnumPoolGroup, Dictionary<int, EnumPoolable>>();
            for (int i = 0; i < CommonHelper.GetEnumLength(typeof(EnumPoolGroup)); i++)
            {
                allTypeBinder.Add(((EnumPoolGroup)i), new Dictionary<int, EnumPoolable>());
            }

            //Notification.Register(EnumNotifType.GameInitializationDone, Id, GeneratePoolItems);
            for (int i = 0; i < poolItems.Length; i++)
            {
                instances.Add(poolItems[i].poolObjectType, Generate<IPoolable>(poolItems[i]));
            }

            for (int i = 0; i < ammoBinder.Length; i++)
            {
                allTypeBinder[EnumPoolGroup.Ammo].Add((int)ammoBinder[i].itemType, ammoBinder[i].poolObjectType);
            }
            for (int i = 0; i < vfxBinder.Length; i++)
            {
                allTypeBinder[EnumPoolGroup.Vfx].Add((int)vfxBinder[i].itemType, vfxBinder[i].poolObjectType);
            }
            for (int i = 0; i < matrialBinder.Length; i++)
            {
                allTypeBinder[EnumPoolGroup.Impact].Add((int)matrialBinder[i].itemType, matrialBinder[i].poolObjectType);
            }
            for (int i = 0; i < shellBinder.Length; i++)
            {
                allTypeBinder[EnumPoolGroup.Shell].Add((int)shellBinder[i].itemType, shellBinder[i].poolObjectType);
            }
            for (int i = 0; i < ragdollBinder.Length; i++)
            {
                allTypeBinder[EnumPoolGroup.Ragdoll].Add((int)ragdollBinder[i].itemType, ragdollBinder[i].poolObjectType);
            }
            for (int i = 0; i < agentBinder.Length; i++)
            {
                allTypeBinder[EnumPoolGroup.Agent].Add((int)agentBinder[i].itemType, agentBinder[i].poolObjectType);
            }
        }

        private void GeneratePoolItems()
        {

        }

        private void AddBinder(EnumPoolGroup group, PoolTypeBinders<int>[] binder)
        {
            for (int i = 0; i < binder.Length; i++)
            {
                allTypeBinder[group].Add((int)binder[i].itemType, binder[i].poolObjectType);
            }
        }


        PoolStack<IPoolable> Generate<T>(PoolObjectTypeCount typeCount)
        {
            var instances = new IPoolable[typeCount.count];
            int prefabIndex = 0;
            //var pref = typeCount.prefab;
            //Debug.Log(" Generate " + typeCount.poolObjectType);
            for (int k = 0; k < instances.Length; k++)
            {
                var pref = typeCount.prefabs[prefabIndex++];
                if (prefabIndex >= typeCount.prefabs.Length) prefabIndex = 0;

                instances[k] = DInjector.InstantiateAndInject(pref, typeCount.parent, DEBUG).GetComponent<IPoolable>();
                instances[k].Init(k);
                //instances[k].SetEnable(false);
            }
            return new PoolStack<IPoolable>(instances);
        }


        public void Activate(NotificationParameter param)
        {
            EnumPoolable pool = param.Get<EnumPoolable>(EnumNotifParam.poolGroup);
            if (!instances.ContainsKey(pool) || CommonHelper.IsNull(instances[pool]) || instances[pool].Count == 0)
                throw new System.Exception("pool object type not found " + pool);

            instances[pool].Get().Activate(param);

            //param.Get<ActionModel<IPoolable>>(EnumNotifParam.actionCallBack).action(instances[pool].Get()); 
        }

        public T ActivateAndGet<T>(NotificationParameter param) where T : IPoolable
        {
            EnumPoolable pool = param.Get<EnumPoolable>(EnumNotifParam.poolGroup);
            if (!instances.ContainsKey(pool) || CommonHelper.IsNull(instances[pool]) || instances[pool].Count == 0)
                throw new System.Exception("pool object type not found " + pool);

            var target = instances[pool].Get();
            target.Activate(param);
            return (T)target;
            //param.Get<ActionModel<IPoolable>>(EnumNotifParam.actionCallBack).action(instances[pool].Get()); 
        }



        EnumPoolable GetEquivalentType(EnumPoolGroup itemGroup, int itemType)
        {
            if (allTypeBinder[itemGroup].ContainsKey(itemType))
            {
                return allTypeBinder[itemGroup][itemType];
            }
            throw new System.Exception("EnumAmmoType not detected in pool " + itemType);
        }

        public EnumPoolable GetEquivalentType(EnumAmmoType itemType)
        {
            return GetEquivalentType(EnumPoolGroup.Ammo, (int)itemType);
        }

        public EnumPoolable GetEquivalentType(EnumShellType itemType)
        {
            return GetEquivalentType(EnumPoolGroup.Shell, (int)itemType);
        }

        public EnumPoolable GetEquivalentType(EnumVfxType itemType)
        {
            return GetEquivalentType(EnumPoolGroup.Vfx, (int)itemType);
        }

        public EnumPoolable GetEquivalentType(EnumMaterialType materialType)
        {
            return GetEquivalentType(EnumPoolGroup.Impact, (int)materialType);
        }

        public EnumPoolable GetEquivalentType(EnumRagdollType ragdollType)
        {
            return GetEquivalentType(EnumPoolGroup.Ragdoll, (int)ragdollType);
        }

        public EnumPoolable GetEquivalentType(EnumAgentType agentType)
        {
            return GetEquivalentType(EnumPoolGroup.Agent, (int)agentType);
        }
    }
}