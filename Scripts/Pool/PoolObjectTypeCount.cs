
using RCPU.Vfx;
using RCPU.Weapon;
using System;
using UnityEngine;

namespace RCPU.Pool
{


    [Serializable]
    public class PoolObjectTypeCount
    {
        public EnumPoolable poolObjectType;
        public int count;
        public Transform parent;
        //public GameObject prefab;
        public GameObject[] prefabs;
    }

    [Serializable]
    public class PoolTypeBinder
    {
        public EnumPoolable poolObjectType;
    }

    [Serializable]
    public class PoolTypeBinders<T>
    {
        public EnumPoolable poolObjectType;
        public T itemType;
    }

    [Serializable]
    public class PoolAmmoTypeBinder : PoolTypeBinder
    {
        public EnumAmmoType ammoType;
    }
    [Serializable]
    public class PoolVfxTypeBinder : PoolTypeBinder
    {
        public EnumVfxType vfxType;
    }
    [Serializable]
    public class PoolMatrialTypeBinder : PoolTypeBinder
    {
        public EnumMaterialType materialType;
    }
    [Serializable]
    public class PoolShellTypeBinder : PoolTypeBinder
    {
        public EnumShellType shellType;
    }
}