﻿using RCPU.Agent;
using RCPU.NotificationSystem;
using RCPU.Vfx;
using RCPU.Weapon;

namespace RCPU.Pool
{
    public interface IPool : IInitializable
    {
        void Activate(NotificationParameter param);
        T ActivateAndGet<T>(NotificationParameter param) where T : IPoolable;

        //EnumPoolable GetEquivalentType(EnumPoolGroup itemGroup, object itemType);
        EnumPoolable GetEquivalentType(EnumAgentType agentType);
        EnumPoolable GetEquivalentType(EnumAmmoType itemType);
        EnumPoolable GetEquivalentType(EnumShellType itemType);
        EnumPoolable GetEquivalentType(EnumVfxType itemType);
        EnumPoolable GetEquivalentType(EnumMaterialType materialType);
        EnumPoolable GetEquivalentType(EnumRagdollType ragdollType);
    }
}
