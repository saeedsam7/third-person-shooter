namespace RCPU
{
    public enum EnumPoolGroup
    {
        Ammo, Impact, Vfx, Agent, Decal, Shell, Ragdoll
    }
    public enum EnumPoolable
    {
        BulletPistolDamage = 0,
        BulletPistolEmp,
        BulletPistolDetacher,
        BulletRifleDamage,
        BulletRifleEmp,
        BulletRifleDetacher,
        BulletSniperDamage,
        BulletSniperEmp,
        BulletSniperDetacher,
        BulletShotgunDamage,
        BulletShotgunEmp,
        BulletShotgunDetacher,
        ProjectileGrenadeLauncher,

        VfxImpactBrick = 100,
        VfxImpactConcrete,
        VfxImpactDirt,
        VfxImpactFoliage,
        VfxImpactGlass,
        VfxImpactMetal,
        VfxImpactPlastic,
        VfxImpactRock,
        VfxImpactWater,
        VfxImpactWood,


        VfxExplosionSmall,
        VfxExplosionMedium,
        VfxExplosionBig,
        VfxExplosionNuke,
        VfxImpactBlood,

        AgentE01 = 200,
        AgentE02,

        Decal = 300,

        ShellPistol = 400,
        ShellAssaultRifle,
        ShellShotgun,
        ShellSniper,
        ShellRocketLauncher,
        ShellGrenadeLauncher,

        RagdollE01 = 500,
        RagdollE02,
        RagdollE03,
    }
}