namespace RCPU.Pool
{
    public class PoolStack<T> where T : IPoolable
    {
        private T[] instances;
        private int index;
        public int Count { get; }

        public PoolStack(T[] instances)
        {
            index = 0;
            this.instances = instances;
            Count = instances.Length;
        }
        public T Get()
        {

            int safeCounter = 0;
            while (!instances[index].IsReady)
            {
                index++;
                safeCounter++;
                if (index == Count)
                    index = 0;
                if (safeCounter > Count)
                    throw new System.Exception("ready object not found " + typeof(T));
            }
            //instances[index].SetEnable(true);
            return instances[index];
        }
    }
}