
using RCPU.NotificationSystem;

namespace RCPU.Pool
{
    public interface IPoolable
    {
        //void SetEnable(bool val);
        void Init(int index);
        bool IsReady { get; }
        void Activate(NotificationParameter parameters);
    }
}