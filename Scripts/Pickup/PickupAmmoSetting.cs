﻿using RCPU.Weapon;
using System;
using UnityEngine;

namespace RCPU.Pickup
{
    [Serializable]
    public class PickupAmmoSetting : PickupSetting, IPickupAmmoSetting
    {
        [field: SerializeField] public EnumAmmoType AmmoType { get; private set; }
    }
}

