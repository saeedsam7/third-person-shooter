﻿

using RCPU.Weapon;
using System;
using UnityEngine;

namespace RCPU.Pickup
{
    [Serializable]
    public class PickupWeaponSetting : PickupSetting, IPickupWeaponSetting
    {

        [field: SerializeField] public EnumWeaponType WeaponType { get; private set; }
    }
}

