﻿using System;
using UnityEngine;

namespace RCPU.Pickup
{
    [Serializable]
    public class PickupResourceSetting : PickupSetting, IPickupResourceSetting
    {
        [field: SerializeField] public EnumResourceType ResourceType { get; private set; }
    }
}

