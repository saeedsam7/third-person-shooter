﻿using RCPU.Attributes;
using RCPU.Pickup;
using RCPU.Weapon;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Inventory
{
    [InjectToSystem(typeof(IPickupManager))]
    [InitOrder(EnumInitOrder.PickupManager)]
    public class PickupManager : BaseObject, IPickupManager
    {

        Dictionary<EnumPickupType, IPickupItemSetting> allSettings;

        Dictionary<EnumPickupType, IPickupAmmoSetting> allAmmoSettings;
        Dictionary<EnumPickupType, IPickupWeaponSetting> allWeaponSettings;
        Dictionary<EnumPickupType, IPickupResourceSetting> allResourceSettings;


        [SerializeField] private PickupAmmoSetting[] ammoSettings;
        [Space(30)]
        [SerializeField] private PickupWeaponSetting[] weaponSettings;
        [Space(30)]
        [SerializeField] private PickupResourceSetting[] resourceSettings;
        [Space(40)]
        [Header("Binder")]
        [SerializeField] PairMap<EnumPickupType, EnumAmmoType> ammoBinder;
        [Space(30)]
        [SerializeField] PairMap<EnumPickupType, EnumWeaponType> weaponBinder;
        [Space(30)]
        [SerializeField] PairMap<EnumPickupType, EnumResourceType> resourceBinder;

        public IPickupItemSetting Get(EnumPickupType pickupType)
        {
            if (allSettings.ContainsKey(pickupType))
                return allSettings[pickupType];

            throw new System.NotImplementedException();
        }


        public EnumAmmoType GetAmmo(EnumPickupType pickupType)
        {
            return ammoBinder.Get(pickupType);
        }
        public EnumWeaponType GetWeapon(EnumPickupType pickupType)
        {
            return weaponBinder.Get(pickupType);
        }
        public EnumResourceType GetResource(EnumPickupType pickupType)
        {
            return resourceBinder.Get(pickupType);
        }


        public EnumPickupType GetPickupType(EnumWeaponType weaponType)
        {
            return weaponBinder.Get(weaponType);
        }
        public EnumPickupType GetPickupType(EnumAmmoType ammoType)
        {
            return ammoBinder.Get(ammoType);
        }
        public EnumPickupType GetPickupType(EnumResourceType resourceType)
        {
            return resourceBinder.Get(resourceType);
        }

        public IPickupWeaponSetting GetWeaponSetting(EnumPickupType pickupType)
        {
            if (allWeaponSettings.ContainsKey(pickupType))
                return allWeaponSettings[pickupType];

            throw new System.NotImplementedException();
        }

        public IPickupAmmoSetting GetAmmoSetting(EnumPickupType pickupType)
        {
            if (allAmmoSettings.ContainsKey(pickupType))
                return allAmmoSettings[pickupType];

            throw new System.NotImplementedException();
        }

        public IPickupResourceSetting GetResourceSetting(EnumPickupType pickupType)
        {
            if (allResourceSettings.ContainsKey(pickupType))
                return allResourceSettings[pickupType];

            throw new System.NotImplementedException();
        }

        public void Init()
        {
            allSettings = new Dictionary<EnumPickupType, IPickupItemSetting>();
            allAmmoSettings = new Dictionary<EnumPickupType, IPickupAmmoSetting>();
            allWeaponSettings = new Dictionary<EnumPickupType, IPickupWeaponSetting>();
            allResourceSettings = new Dictionary<EnumPickupType, IPickupResourceSetting>();


            for (int i = 0; i < ammoSettings.Length; i++)
            {
                allSettings.Add(ammoSettings[i].PickupType, ammoSettings[i]);
                allAmmoSettings.Add(ammoSettings[i].PickupType, ammoSettings[i]);
            }

            for (int i = 0; i < weaponSettings.Length; i++)
            {
                allSettings.Add(weaponSettings[i].PickupType, weaponSettings[i]);
                allWeaponSettings.Add(weaponSettings[i].PickupType, weaponSettings[i]);
            }

            for (int i = 0; i < resourceSettings.Length; i++)
            {
                allSettings.Add(resourceSettings[i].PickupType, resourceSettings[i]);
                allResourceSettings.Add(resourceSettings[i].PickupType, resourceSettings[i]);
            }

            ammoBinder.Refresh();
            weaponBinder.Refresh();
            resourceBinder.Refresh();
        }
    }
}