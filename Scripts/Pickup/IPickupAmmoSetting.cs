﻿using RCPU.Weapon;

namespace RCPU
{



    public interface IPickupAmmoSetting : IPickupItemSetting
    {
        EnumAmmoType AmmoType { get; }
    }

}