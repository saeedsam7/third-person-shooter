﻿namespace RCPU.Pickup
{
    public enum EnumPickupIndicatorSkin { White, Gold, Red, Green, Blue }

    public enum EnumPickupType { AmmoDamagePistol, AmmoEmpPistol, AmmoDetacherPistol, AmmoExplosiveGrenadeLauncher, AmmoDamageAssultRifle, AmmoEmpAssultRifle, AmmoDetacherAssultRifle, AmmoDamageSMG, AmmoEmpSMG, AmmoDetacherSMG, HealthPack100 = 100, HealthPack200, HealthPack500, HealthPack1000, Pistol01 = 200, Pistol02, GrenadeLauncher, AssaultRifle, Metal = 300 }
    public enum EnumPickupGroup { Ammo, HealthPack, Weapon, Resource }

    public enum EnumResourceType { Metal }
}