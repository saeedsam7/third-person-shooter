﻿
using RCPU.Pickup;

namespace RCPU
{

    public interface IPickupResourceSetting : IPickupItemSetting
    {
        EnumResourceType ResourceType { get; }
    }

}