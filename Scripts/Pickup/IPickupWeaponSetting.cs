﻿using RCPU.Weapon;

namespace RCPU
{
    public interface IPickupWeaponSetting : IPickupItemSetting
    {
        EnumWeaponType WeaponType { get; }
    }
}