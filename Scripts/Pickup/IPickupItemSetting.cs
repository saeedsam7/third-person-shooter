﻿
using RCPU.Pickup;
using RCPU.UI;

namespace RCPU
{

    public interface IPickupItemSetting
    {
        int Width { get; }
        int Height { get; }
        int MaxCount { get; }
        EnumPickupType PickupType { get; }
        EnumPickupGroup PickupGroup { get; }
        EnumSpriteIndex Icon { get; }
        EnumPickupIndicatorSkin Skin { get; }
    }


}