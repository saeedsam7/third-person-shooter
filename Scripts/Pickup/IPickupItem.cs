
using RCPU.Pickup;

namespace RCPU
{
    public interface IPickupItem
    {
        EnumPickupType PickupType { get; }
        int Count { get; set; }
    }


    public class PickupItem : IPickupItem
    {
        public PickupItem(EnumPickupType pickupType, int count)
        {
            this.PickupType = pickupType;
            this.Count = count;
        }
        public EnumPickupType PickupType { get; protected set; }
        public int Count { get; set; }
    }
}