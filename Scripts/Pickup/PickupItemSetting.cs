﻿using RCPU.UI;
using System;
using UnityEngine;

namespace RCPU.Pickup
{
    //public class PickupItemSetting : BaseObject, IPickupItemSetting
    //{
    //    [field: SerializeField] public int Width { get; private set; }

    //    [field: SerializeField] public int Height { get; private set; }

    //    [field: SerializeField] public int MaxCount { get; private set; }

    //    [field: SerializeField] public EnumPickupGroup PickupGroup { get; private set; }

    //    [field: SerializeField] public EnumPickupType PickupType { get; private set; }

    //    [field: SerializeField] public EnumSpriteIndex Icon { get; private set; }

    //    [field: SerializeField] public EnumPickupIndicatorSkin Skin { get; private set; }
    //}

    [Serializable]
    public class PickupSetting : IPickupItemSetting
    {
        [field: SerializeField] public int Width { get; private set; }

        [field: SerializeField] public int Height { get; private set; }

        [field: SerializeField] public int MaxCount { get; private set; }

        [field: SerializeField] public EnumPickupGroup PickupGroup { get; private set; }

        [field: SerializeField] public EnumPickupType PickupType { get; private set; }

        [field: SerializeField] public EnumSpriteIndex Icon { get; private set; }

        [field: SerializeField] public EnumPickupIndicatorSkin Skin { get; private set; }


    }
}