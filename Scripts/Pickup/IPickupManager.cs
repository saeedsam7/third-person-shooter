﻿
using RCPU.Pickup;
using RCPU.Weapon;

namespace RCPU.Inventory
{
    public interface IPickupManager : IInitializable
    {
        IPickupItemSetting Get(EnumPickupType pickupType);

        EnumAmmoType GetAmmo(EnumPickupType pickupType);
        EnumWeaponType GetWeapon(EnumPickupType pickupType);
        EnumResourceType GetResource(EnumPickupType pickupType);

        EnumPickupType GetPickupType(EnumWeaponType weaponType);
        EnumPickupType GetPickupType(EnumAmmoType ammoType);
        EnumPickupType GetPickupType(EnumResourceType resourceType);

        IPickupWeaponSetting GetWeaponSetting(EnumPickupType pickupType);
        IPickupAmmoSetting GetAmmoSetting(EnumPickupType pickupType);
        IPickupResourceSetting GetResourceSetting(EnumPickupType pickupType);

    }
}