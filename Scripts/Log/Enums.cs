

namespace RCPU
{
    public enum EnumLogPriority { Light, Normal, Dark, Extremly }
    public enum EnumLogType { General, Enemy, Player }

    public enum EnumLogColor
    {
        Red, RedLight, RedDark,
        Orange, OrangeLight, OrangeDark,
        Yellow, YellowLight, YellowDark,
        Green, GreenLight, GreenDark,
        Gray, GrayLight, GrayDark,
        Blue, BlueLight, BlueDark,
        Cyan, CyanLight, CyanDark,
        Purple, PurpleLight, PurpleDark,
    }
}