using RCPU.Attributes;
using RCPU.UI;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU
{
    [InitOrder(EnumInitOrder.Logger)]
    public class Logger : BaseObject, IInitializable
    {
        [InjectFromSystem] IUIManager uiManager { get; set; }
        static Logger instance;
        [SerializeField] Color[] colors;
        static Dictionary<EnumLogColor, Color> allColors;

        //public static Color[] Color_Priority = { new Color(.5f, .5f, .5f, 1), new Color(.8f, .8f, .8f, 1), new Color(1f, 1f, 0f, 1), new Color(1f, .5f, 0f, 1) };

        //static Logger _instance;
        //[SerializeField]
        //private bool[] LogType;
        //[SerializeField]
        //private bool[] WarningType;
        //[SerializeField]
        //private bool[] ErrorType;

        //static Logger instance
        //{
        //    get
        //    {
        //        return _instance;
        //    }
        //}
        public void Init()
        {
            allColors = new Dictionary<EnumLogColor, Color>();
            for (int i = 0; i < colors.Length; i++)
            {
                allColors.Add((EnumLogColor)i, colors[i]);
            }
            instance = this;
        }

        public static void Log(string txt, int index)
        {
            instance.uiManager.SetColor((EnumUiTextIndex)EnumUiTextIndex.Debug01 + index, EnumUiColor.UiElementActive);
            instance.uiManager.SetText((EnumUiTextIndex)EnumUiTextIndex.Debug01 + index, txt);
        }

        public static void Error(string txt, int index)
        {
            instance.uiManager.SetColor((EnumUiTextIndex)EnumUiTextIndex.Debug01 + index, EnumUiColor.UiElementError);
            instance.uiManager.SetText((EnumUiTextIndex)EnumUiTextIndex.Debug01 + index, txt);
        }

        public static void Warning(string txt, int index)
        {
            instance.uiManager.SetColor((EnumUiTextIndex)EnumUiTextIndex.Debug01 + index, EnumUiColor.UiElementWarning);
            instance.uiManager.SetText((EnumUiTextIndex)EnumUiTextIndex.Debug01 + index, txt);
        }
        public static void Log(string txt, EnumLogColor color)
        {
            Log(txt, allColors[color]);
        }
        public static void Log(string txt, Color color)
        {
            Debug.Log(string.Format("<color=#{0:X2}{1:X2}{2:X2}>{3}</color>", (byte)(color.r * 255f), (byte)(color.g * 255f), (byte)(color.b * 255f), txt));
        }

        //public static void Log(string txt, Color color, EnumLogType tpy = EnumLogType.General)
        //{
        //    _Log(string.Format("<color=#{0:X2}{1:X2}{2:X2}>{3}</color>", (byte)(color.r * 255f), (byte)(color.g * 255f), (byte)(color.b * 255f), txt), tpy);
        //}

        //public static void Log(string txt, EnumLogType tpy = EnumLogType.General)
        //{
        //    Log(txt, EnumLogPriority.Low, tpy);
        //}


        //public static void Log(string txt, EnumLogPriority priority, EnumLogType tpy = EnumLogType.General)
        //{
        //    Log(txt, Color_Priority[(int)priority], tpy);
        //}

        //static void _Log(string txt, EnumLogType tpy = EnumLogType.General)
        //{
        //    Debug.Log(TimeController.gameTime.ToString("f2") + "] " + tpy + " -> " + txt);
        //}

        ////public static void Warning(string txt, EnumType tpy = EnumType.General)
        ////{
        ////    if (instance.WarningType[(int)tpy])
        ////    {
        ////        Debug.LogWarning("[" + CTime.gameTime.ToString("f2") + "] " + tpy + " -> " + txt);
        ////    }
        ////}

        //public static void Error(string txt, EnumLogType tpy = EnumLogType.General)
        //{
        //    Debug.LogError("[" + TimeController.gameTime.ToString("f2") + "] " + tpy + " -> " + txt);
        //}

        //public static void Break(string txt = "")
        //{
        //    if (!string.IsNullOrEmpty(txt))
        //        Debug.LogError("[" + TimeController.gameTime.ToString("f2") + "]" + " Break -> " + txt);
        //    Debug.Break();
        //}


    }
}
