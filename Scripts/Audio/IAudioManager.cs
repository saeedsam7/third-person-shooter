﻿using UnityEngine;

namespace RCPU.Audio
{
    public interface IAudioManager : IInitializable
    {
        //void PlayOneShot(EnumAudioClip sfx, Vector3 position);
        //void PlayOneShot(EnumAudioClip ambient);

        void PlaySfx(EnumAudioSFX sfx, Vector3 position);
        void PlaySfx(EnumAudioSFX sfx);
        void ChangeAmbient(EnumAudioAmbient ambient);
        void ChangeMusic(EnumAudioMusic music);
    }
}
