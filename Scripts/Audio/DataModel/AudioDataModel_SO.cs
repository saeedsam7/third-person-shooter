﻿

using UnityEngine;

namespace RCPU.Audio
{
    [CreateAssetMenu(fileName = "Audio", menuName = "RCPU/DataModel/Audio/AudioDataModel", order = 1)]
    public class AudioDataModel_SO : ScriptableObject
    {
        public AudioDataModel model;
    }
}