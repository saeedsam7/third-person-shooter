
using System;

namespace RCPU.Audio
{
    [Serializable]
    public struct AudioDataModel
    {
        public AudioSFX[] audioSfxs;
        public AudioAmbient[] audioAmbients;
        public AudioMusic[] audioMusics;
    }
}
