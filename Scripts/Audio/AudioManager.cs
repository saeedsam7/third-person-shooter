﻿using FMODUnity;
using RCPU.Attributes;
using RCPU.DependencyInjection;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Audio
{
    [InjectToSystem(typeof(IAudioManager))]
    [InitOrder(EnumInitOrder.Audio)]
    public class AudioManager : BaseObject, IAudioManager
    {

        [SerializeField] protected AudioDataModel_SO model_SO;
        protected AudioDataModel model;
        //Dictionary<EnumAudioClip, EventReference> clips;

        Dictionary<EnumAudioAmbient, EventReference> ambients;
        Dictionary<EnumAudioSFX, EventReference> sfxs;
        Dictionary<EnumAudioMusic, EventReference> musics;
        public void Init()
        {
            model = DInjector.Instantiate<AudioDataModel_SO>(model_SO).model;



            ambients = new Dictionary<EnumAudioAmbient, EventReference>();
            for (int i = 0; i < model.audioAmbients.Length; i++)
            {
                ambients.Add(model.audioAmbients[i].index, model.audioAmbients[i].clip);
            }

            sfxs = new Dictionary<EnumAudioSFX, EventReference>();
            for (int i = 0; i < model.audioSfxs.Length; i++)
            {
                sfxs.Add(model.audioSfxs[i].index, model.audioSfxs[i].clip);
            }

            musics = new Dictionary<EnumAudioMusic, EventReference>();
            for (int i = 0; i < model.audioMusics.Length; i++)
            {
                musics.Add(model.audioMusics[i].index, model.audioMusics[i].clip);
            }
        }
        public void PlaySfx(EnumAudioSFX sfx, Vector3 position)
        {
            if (sfx != EnumAudioSFX.None)
                RuntimeManager.PlayOneShot(sfxs[sfx], position);
        }

        public void PlaySfx(EnumAudioSFX sfx)
        {
            if (sfx != EnumAudioSFX.None)
                RuntimeManager.PlayOneShot(sfxs[sfx]);
        }

        public void ChangeAmbient(EnumAudioAmbient ambient)
        {
            if (ambient != EnumAudioAmbient.None)
                RuntimeManager.PlayOneShot(ambients[ambient]);
        }

        public void ChangeMusic(EnumAudioMusic music)
        {
            if (music != EnumAudioMusic.None)
                RuntimeManager.PlayOneShot(musics[music]);
        }
    }
}
