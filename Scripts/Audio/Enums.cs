namespace RCPU.Audio
{
    //public enum EnumAudioClip
    //{
    //    None,
    //    SfxPistolShoot = 1, SfxPistolReload, SfxPistolEmpty,


    //    AmbientLevel01 = 1000,


    //    MusicLevel01 = 2000,

    //}

    public enum EnumAudioSFX
    {
        None,
        PistolShoot = 1, PistolReload, PistolEmpty, PistolShellDrop,



        FootStepHumanMetal = 1000, FootStepHumanWood, FootStepHumanConcerete,
        FootStepMechMetal = 1100, FootStepMechWood, FootStepMechConcerete,
        MechLegUp = 1200
    }

    public enum EnumAudioAmbient
    {
        None,
        AmbientLevel01,

    }

    public enum EnumAudioMusic
    {
        None,
        MusicLevel01,

    }
}