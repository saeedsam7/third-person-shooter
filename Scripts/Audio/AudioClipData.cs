﻿

using FMODUnity;
using System;

namespace RCPU.Audio
{

    [Serializable]
    public class AudioData<T>
    {
        public T index;
        public EventReference clip;
    }
    [Serializable]
    public class AudioSFX : AudioData<EnumAudioSFX>
    {
    }
    [Serializable]
    public class AudioAmbient : AudioData<EnumAudioAmbient>
    {
    }
    [Serializable]
    public class AudioMusic : AudioData<EnumAudioMusic>
    {
    }
}
