﻿using RCPU.Agent;
using RCPU.Helper;
using RCPU.Models;
using System;
using UnityEngine;

namespace RCPU.Spawn
{
    [Serializable]
    public class SpawnGroupDataModel
    {
        public SpawnPositionData[] spawnPositionData;
        public SpawnAgentnData[] spawnAgentnData;
        public float startDelay;
        public float eachDelay;
        public EnumSpawnTrigger triggerToStart;
        //public EnumSpawnTriggerDone dispatchOnDone;
    }


    [Serializable]
    public class SpawnPositionData
    {
        public Transform target;

        public Vector3 positionOffset;
        public float rotationYOffset;

        public Vector3 GetRandomPosition => target.position + RandomHelper.Range(positionOffset);
        public Quaternion GetRandomRotation => target.rotation * Quaternion.AngleAxis(RandomHelper.Range(rotationYOffset), Vector3.up);

    }

    [Serializable]
    public class SpawnAgentnData
    {
        public EnumAgentType agentType;
        public RangeInteger count;
        public RangeInteger level;

    }
}