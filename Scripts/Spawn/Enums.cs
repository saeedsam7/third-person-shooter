﻿namespace RCPU.Spawn
{
    public enum EnumSpawnTrigger
    {
        None,
        SpawnTrigger01, SpawnTrigger02, SpawnTrigger03, SpawnTrigger04, SpawnTrigger05,
        SpawnTrigger06, SpawnTrigger07, SpawnTrigger08, SpawnTrigger09, SpawnTrigger10,
        SpawnTrigger11, SpawnTrigger12, SpawnTrigger13, SpawnTrigger14, SpawnTrigger15,
        SpawnTrigger16, SpawnTrigger17, SpawnTrigger18, SpawnTrigger19, SpawnTrigger20,
        SpawnTrigger21, SpawnTrigger22, SpawnTrigger23, SpawnTrigger24, SpawnTrigger25,
        SpawnTrigger26, SpawnTrigger27, SpawnTrigger28, SpawnTrigger29, SpawnTrigger30,
        SpawnTrigger31, SpawnTrigger32, SpawnTrigger33, SpawnTrigger34, SpawnTrigger35,
        SpawnTrigger36, SpawnTrigger37, SpawnTrigger38, SpawnTrigger39, SpawnTrigger40,
        SpawnTrigger41, SpawnTrigger42, SpawnTrigger43, SpawnTrigger44, SpawnTrigger45,
        SpawnTrigger46, SpawnTrigger47, SpawnTrigger48, SpawnTrigger49, SpawnTrigger50,
        SpawnTrigger51, SpawnTrigger52, SpawnTrigger53, SpawnTrigger54, SpawnTrigger55,
        SpawnTrigger56, SpawnTrigger57, SpawnTrigger58, SpawnTrigger59, SpawnTrigger60,
        SpawnTrigger61, SpawnTrigger62, SpawnTrigger63, SpawnTrigger64, SpawnTrigger65,
        SpawnTrigger66, SpawnTrigger67, SpawnTrigger68, SpawnTrigger69, SpawnTrigger70,
        SpawnTrigger71, SpawnTrigger72, SpawnTrigger73, SpawnTrigger74, SpawnTrigger75,
        SpawnTrigger76, SpawnTrigger77, SpawnTrigger78, SpawnTrigger79, SpawnTrigger80,
        SpawnTrigger81, SpawnTrigger82, SpawnTrigger83, SpawnTrigger84, SpawnTrigger85,
        SpawnTrigger86, SpawnTrigger87, SpawnTrigger88, SpawnTrigger89, SpawnTrigger90,
        SpawnTrigger91, SpawnTrigger92, SpawnTrigger93, SpawnTrigger94, SpawnTrigger95,
        SpawnTrigger96, SpawnTrigger97, SpawnTrigger98, SpawnTrigger99, SpawnTrigger100

    }

    //public enum EnumSpawnTriggerDone
    //{
    //    None,
    //    SpawnTrigger01_Done, SpawnTrigger02_Done, SpawnTrigger03_Done, SpawnTrigger04_Done, SpawnTrigger05_Done,
    //    SpawnTrigger06_Done, SpawnTrigger07_Done, SpawnTrigger08_Done, SpawnTrigger09_Done, SpawnTrigger10_Done,
    //    SpawnTrigger11_Done, SpawnTrigger12_Done, SpawnTrigger13_Done, SpawnTrigger14_Done, SpawnTrigger15_Done,
    //    SpawnTrigger16_Done, SpawnTrigger17_Done, SpawnTrigger18_Done, SpawnTrigger19_Done, SpawnTrigger20_Done,
    //    SpawnTrigger21_Done, SpawnTrigger22_Done, SpawnTrigger23_Done, SpawnTrigger24_Done, SpawnTrigger25_Done,
    //    SpawnTrigger26_Done, SpawnTrigger27_Done, SpawnTrigger28_Done, SpawnTrigger29_Done, SpawnTrigger30_Done,
    //    SpawnTrigger31_Done, SpawnTrigger32_Done, SpawnTrigger33_Done, SpawnTrigger34_Done, SpawnTrigger35_Done,
    //    SpawnTrigger36_Done, SpawnTrigger37_Done, SpawnTrigger38_Done, SpawnTrigger39_Done, SpawnTrigger40_Done,
    //    SpawnTrigger41_Done, SpawnTrigger42_Done, SpawnTrigger43_Done, SpawnTrigger44_Done, SpawnTrigger45_Done,
    //    SpawnTrigger46_Done, SpawnTrigger47_Done, SpawnTrigger48_Done, SpawnTrigger49_Done, SpawnTrigger50_Done,
    //    SpawnTrigger51_Done, SpawnTrigger52_Done, SpawnTrigger53_Done, SpawnTrigger54_Done, SpawnTrigger55_Done,
    //    SpawnTrigger56_Done, SpawnTrigger57_Done, SpawnTrigger58_Done, SpawnTrigger59_Done, SpawnTrigger60_Done,
    //    SpawnTrigger61_Done, SpawnTrigger62_Done, SpawnTrigger63_Done, SpawnTrigger64_Done, SpawnTrigger65_Done,
    //    SpawnTrigger66_Done, SpawnTrigger67_Done, SpawnTrigger68_Done, SpawnTrigger69_Done, SpawnTrigger70_Done,
    //    SpawnTrigger71_Done, SpawnTrigger72_Done, SpawnTrigger73_Done, SpawnTrigger74_Done, SpawnTrigger75_Done,
    //    SpawnTrigger76_Done, SpawnTrigger77_Done, SpawnTrigger78_Done, SpawnTrigger79_Done, SpawnTrigger80_Done,
    //    SpawnTrigger81_Done, SpawnTrigger82_Done, SpawnTrigger83_Done, SpawnTrigger84_Done, SpawnTrigger85_Done,
    //    SpawnTrigger86_Done, SpawnTrigger87_Done, SpawnTrigger88_Done, SpawnTrigger89_Done, SpawnTrigger90_Done,
    //    SpawnTrigger91_Done, SpawnTrigger92_Done, SpawnTrigger93_Done, SpawnTrigger94_Done, SpawnTrigger95_Done,
    //    SpawnTrigger96_Done, SpawnTrigger97_Done, SpawnTrigger98_Done, SpawnTrigger99_Done, SpawnTrigger100

    //}
}