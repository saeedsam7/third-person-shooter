﻿using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Helper;
using RCPU.NotificationSystem;
using RCPU.Pool;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Spawn
{
    //[InjectToSystem(typeof(ISpawnManager))]
    [InitOrder(EnumInitOrder.Spawn)]
    public class SpawnManager : BaseObject, ISpawnManager
    {
        [InjectFromSystem] protected IPool Pool { get; set; }
        //[Header("Model")][SerializeField] protected SpawnGroupDataModel_SO model_SO;
        [SerializeField] protected SpawnGroupDataModel model;
        public void Init()
        {
            if (model.spawnPositionData.Length == 0)
            {
                model.spawnPositionData = new SpawnPositionData[] { new SpawnPositionData() { target = transform } };
            }
            if (model.spawnAgentnData.Length == 0)
            {
                throw new Exception("no spawn Agentn Data");
            }
            //model = DInjector.Instantiate<SpawnGroupDataModel_SO>(model_SO).model;
            Notification.Register(EnumNotifType.SpawnRequest, Id, OnSpawnRequest);
        }

        private void OnSpawnRequest(NotificationParameter parameter)
        {
            if (parameter.Get<EnumSpawnTrigger>(EnumNotifParam.Id) == model.triggerToStart)
                StartCoroutine(CoroutineSpawn());
        }

        private IEnumerator CoroutineSpawn()
        {
            yield return new WaitForSeconds(model.startDelay);
            List<EnumAgentType> allAgent = new List<EnumAgentType>();
            for (int i = 0; i < model.spawnAgentnData.Length; i++)
            {
                int count = RandomHelper.Range(model.spawnAgentnData[i].count);

                for (int j = 0; j < count; j++)
                    allAgent.Add(model.spawnAgentnData[i].agentType);
            }
            //randomize list
            for (int i = 0; i < allAgent.Count; i++)
            {
                int rand = RandomHelper.Range(model.spawnAgentnData.Length);

                Pool.Activate(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.position, EnumNotifParam.rotation, EnumNotifParam.level },
                                                                new object[] { Pool.GetEquivalentType(allAgent[i]), model.spawnPositionData[rand].GetRandomPosition, model.spawnPositionData[rand].GetRandomRotation, RandomHelper.Range(model.spawnAgentnData[rand].level) }));

                if (model.eachDelay > 0)
                    yield return new WaitForSeconds(model.eachDelay);
            }

            Notification.Notify(EnumNotifType.SpawnCompleted, new NotificationParameter(EnumNotifParam.Id, model.triggerToStart));
            Notification.UnRegister(EnumNotifType.SpawnRequest, Id);
        }
    }
}