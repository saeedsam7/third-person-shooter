using RCPU.NotificationSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Ragdoll
{
    public class RagdollController : BaseObject, IRagdoll
    {
        [SerializeField] protected TransformDataModel[] rootBones;
        protected Dictionary<string, TransformDataModel> allBones;

        public bool IsReady => !isActive;
        bool isActive;
        public void Activate(NotificationParameter parameters)
        {
            transform.position = parameters.Get<Vector3>(EnumNotifParam.position);
            transform.rotation = parameters.Get<Quaternion>(EnumNotifParam.rotation);

            var newBonesState = parameters.Get<Dictionary<string, TransformDataModel>>(EnumNotifParam.dataModel);


            Debug.Log(gameObject.name + " --  " + allBones.Count + "  count " + newBonesState.Count);

            foreach (var bone in allBones)
            {
                if (newBonesState.ContainsKey(bone.Key))
                {
                    bone.Value.Update(newBonesState[bone.Key]);
                    //Debug.Log(bone.Key + "  exist " + CommonHelper.Distance(bone.Value.transform.position, newBonesState[bone.Key].transform.position));
                }
                //else
                //{
                //    Debug.LogError(bone.Key + " doesn't exist ");
                //}
            }


            if (parameters.Get<bool>(EnumNotifParam.autoDestruct))
            {
                Deactivate();
            }
            isActive = true;
            gameObject.SetActive(isActive);
            //Debug.Break();
        }

        public void Init(int index)
        {
            gameObject.name += " " + index;
            allBones = new Dictionary<string, TransformDataModel>();
            foreach (var bone in rootBones)
            {
                bone.Init();
                allBones.Add(bone.Name, bone);
            }

            isActive = false;
            gameObject.SetActive(isActive);
        }

        public Dictionary<string, TransformDataModel> GetCurrentState()
        {
            isActive = false;
            gameObject.SetActive(isActive);
            return allBones;
        }

        public void Deactivate()
        {
            StartCoroutine(CoroutineDeactivate());
        }

        public IEnumerator CoroutineDeactivate()
        {
            float t = GameSetting.DEACTIVATE_RAGDOLL;
            while (t > 0)
            {
                yield return new WaitForEndOfFrame();
                t -= TimeController.deltaTime;
            }
            ResetBones();
            isActive = false;
            gameObject.SetActive(isActive);
        }

        protected void ResetBones()
        {
            foreach (var bone in allBones)
            {
                bone.Value.Reset();
            }
        }
    }


}