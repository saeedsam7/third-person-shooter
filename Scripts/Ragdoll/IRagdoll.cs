using RCPU.Pool;
using System.Collections.Generic;

namespace RCPU.Ragdoll
{
    public interface IRagdoll : IPoolable
    {
        public Dictionary<string, TransformDataModel> GetCurrentState();
    }
}