﻿
using RCPU.Attributes;
using System;
using System.Collections.Generic;
using UnityEngine;
namespace RCPU.Helper
{
    [InitOrder(EnumInitOrder.Logger)]
    public class TagsHelper : BaseObject, IInitializable
    {
        protected static Dictionary<string, EnumMaterialType> materialType;

        public static Transform GetChildWithTag(Transform stepper, string footPole)
        {
            foreach (var item in stepper.GetComponentsInChildren<Transform>())
            {
                if (item.CompareTag(footPole))
                    return item;
            }
            Debug.LogError(" parent " + stepper + " tag " + footPole);
            return null;
        }

        public static List<Transform> GetChildrenWithTag(Transform parent, string footPole)
        {
            List<Transform> res = new List<Transform>();
            foreach (var item in parent.GetComponentsInChildren<Transform>())
            {
                if (item.CompareTag(footPole))
                    res.Add(item);
            }
            return res;
        }

        internal static Transform GetChildWithTag(Transform root, object rightHandGrip)
        {
            throw new NotImplementedException();
        }

        public void Init()
        {
            materialType = new Dictionary<string, EnumMaterialType>();
            for (int i = 0; i < CommonHelper.GetEnumLength(typeof(EnumMaterialType)); i++)
            {
                materialType.Add(((EnumMaterialType)i).ToString(), ((EnumMaterialType)i));
            }
        }

        public static EnumMaterialType GetMaterialType(string tag)
        {
            if (materialType.ContainsKey(tag))
            {
                return materialType[tag];
            }
            return EnumMaterialType.None;
            //throw new System.Exception("tag not " + tag);
        }
    }


}