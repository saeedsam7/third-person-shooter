﻿
using System.Linq;
using UnityEngine; 

namespace RCPU.Helper
{
    public static class PhysicsHelper
    {
        public static string Layer_Agent_Name = ("Agent");
        public static string Layer_Trigger_Name = ("Trigger");
        public static string Layer_Environment_Name = ("Environment");
        public static string Layer_DamageReceiver_Name = ("DamageReceiver");
        public static string Layer_Damage_Name = ("Damage");

        public static LayerMask Layer_Agent = LayerMask.GetMask(Layer_Agent_Name);
        public static LayerMask Layer_Trigger = LayerMask.GetMask(Layer_Trigger_Name);
        public static LayerMask Layer_Agent_Environment = LayerMask.GetMask(Layer_Agent_Name, Layer_Environment_Name);
        public static LayerMask Layer_Environment = LayerMask.GetMask(Layer_Environment_Name);
        public static LayerMask Layer_DamageReceiver = LayerMask.GetMask(Layer_DamageReceiver_Name);
        public static LayerMask Layer_DamageReceiver_Environment = LayerMask.GetMask(Layer_DamageReceiver_Name, Layer_Environment_Name);
        public static LayerMask Layer_DamageReceiver_Environment_Trigger = LayerMask.GetMask(Layer_DamageReceiver_Name, Layer_Environment_Name, Layer_Trigger_Name);


        public static bool Linecast(Vector3 start, Vector3 end, out RaycastHit hitInfo, LayerMask layerMask)
        {
            return Physics.Linecast(start, end, out hitInfo, layerMask);
        }

        public static RaycastHit[] RaycastAll(Vector3 origin, Vector3 direction, float maxDistance, LayerMask layerMask)
        {
            return Physics.RaycastAll(origin, direction, maxDistance, layerMask);
        }

        public static bool Raycast(Vector3 origin, Vector3 direction, out RaycastHit hitInfo)
        {
            return Physics.Raycast(origin, direction, out hitInfo);
        }

        public static bool Raycast(Vector3 origin, Vector3 direction, float maxDistance, Collider collider, LayerMask layer, out RaycastHit hit)
        {
            foreach (var item in Physics.RaycastAll(origin, direction, maxDistance, layer))
            {
                if (item.collider == collider)
                {
                    hit = item;
                    return true;
                }
            }
            hit = default(RaycastHit);
            return false;
        }

        public static bool Raycast(Vector3 origin, Vector3 direction, out RaycastHit hitInfo, float maxDistance)
        {
            return Physics.Raycast(origin, direction, out hitInfo, maxDistance);
        }
        public static bool Raycast(Ray ray, out RaycastHit hitInfo)
        {
            return Physics.Raycast(ray, out hitInfo);
        }
        public static bool SphereCast(Ray ray, float radius, float maxDistance, int layerMask)
        {
            return Physics.SphereCast(ray, radius, maxDistance, layerMask);
        }



        public static bool Raycast(Vector3 origin, Vector3 direction, out RaycastHit hitInfo, float maxDistance, int layerMask)
        {
            return Physics.Raycast(origin, direction, out hitInfo, maxDistance, layerMask);
        }

        public static IOrderedEnumerable<RaycastHit> RaycastOrderAll(Vector3 origin, Vector3 direction, float maxDistance, int layerMask)
        {
            return Physics.RaycastAll(origin, direction, maxDistance, layerMask).OrderBy(x => x.distance);
        }

        public static Vector3 Gravity { get { return Physics.gravity; } }
        //public static bool Raycast(Vector3 origin, Vector3 direction, out RaycastHit hitInfo, float maxDistance, int layerMask, ITarget ignore)
        //{
        //    foreach (var item in Physics.RaycastAll(origin, direction, maxDistance, layerMask).OrderBy(x => x.distance))
        //    {
        //        int colId = item.collider.GetInstanceID();
        //        if (CommonHelper.IsNull(CGame.Instance.GetAliveObject(colId)) || !CommonHelper.IsNull(CGame.Instance.GetAliveObjectExcept(colId, ignore)))
        //        {
        //            hitInfo = item;
        //            return true;
        //        }
        //    }
        //    hitInfo = new RaycastHit() { };
        //    return false;
        //}

        public static bool ShouldIgnoreCollision(GameObject g1, GameObject g2)
        {
            //TODO
            //unity problem in testing version maybe fix in new versions
            return Physics.GetIgnoreLayerCollision(g1.layer, g2.layer);
        }

        public static bool Raycast(Vector3 origin, Vector3 direction, out RaycastHit hitInfo, float maxDistance, int layerMask, QueryTriggerInteraction queryTriggerInteraction)
        {
            return Physics.Raycast(origin, direction, out hitInfo, maxDistance, layerMask, queryTriggerInteraction);
        }

        public static bool Raycast(Ray ray, out RaycastHit hitInfo, float maxDistance, int layerMask)
        {
            return Physics.Raycast(ray, out hitInfo, maxDistance, layerMask );
        }

        public static Collider[] OverlapSphere(Vector3 position, float radius, int layerMask)
        {
            return Physics.OverlapSphere(position, radius, layerMask);
        }

        //public static Dictionary<ITarget, int> OverlapSphere(Vector3 position, float radius, int layerMask, bool confirmByRaycast, int layerMaskConfirm)
        //{
        //    Dictionary<ITarget, int> allAlive = new Dictionary<ITarget, int>();
        //    RaycastHit hit;
        //    foreach (var item in Physics.OverlapSphere(position, radius, layerMask))
        //    {
        //        int colId = item.GetInstanceID();
        //        var alive = CGame.Instance.GetAliveObject(colId);
        //        //LogController.Error(" OnTriggerEnter damage " + (damage != null) + "  other " + other);
        //        if (!CommonHelper.IsNull(alive) && !allAlive.ContainsKey(alive))
        //        {
        //            if (confirmByRaycast)
        //            {
        //                Vector3 pos = item.transform.position;
        //                if (Raycast(position, (pos - position), out hit, CommonHelper.Distance(pos, position), layerMaskConfirm))
        //                {
        //                    alive = CGame.Instance.GetAliveObject(hit.collider.GetInstanceID());
        //                    if (!CommonHelper.IsNull(alive) && !allAlive.ContainsKey(alive))
        //                    {
        //                        allAlive.Add(alive, hit.collider.GetInstanceID());
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                allAlive.Add(alive, colId);
        //            }
        //        }
        //    }
        //    return allAlive;
        //}


    }
}
