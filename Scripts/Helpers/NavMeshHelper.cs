﻿using RCPU.Component;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace RCPU.Helper
{
    public static class NavMeshHelper
    {
        public static int Navmeh_Area_All => NavMesh.AllAreas;
        public static int Navmeh_Area_Walkable => NavMesh.GetAreaFromName("Walkable");
        public static int Navmeh_Area_Flight = NavMesh.GetAreaFromName("Flight");
        public static int Navmeh_Area_Jump => NavMesh.GetAreaFromName("Jump");


        public static bool SamplePosition(Vector3 sourcePosition, out NavMeshHit hit, float maxDistance, int areaMask)
        {
            return NavMesh.SamplePosition(sourcePosition, out hit, maxDistance, areaMask);
        }

        public static bool SamplePosition(Vector3 sourcePosition, float maxDistance, int areaMask)
        {
            NavMeshHit temp;
            return NavMesh.SamplePosition(sourcePosition, out temp, maxDistance, areaMask);
        }

        public static List<SortablePair<Vector3>> GetWalkablePointsOnCircle(Vector3 center, float radius, int count, int navmeshLayer)
        {
            float deltaDegree = (360f / count) * Mathf.Deg2Rad;
            List<SortablePair<Vector3>> point = new List<SortablePair<Vector3>>();
            Vector3 lastPos = Vector3.zero;
            for (int i = 0; i < count; i++)
            {
                Vector3 pos = center + (radius * new Vector3(Mathf.Sin(i * deltaDegree), 0, Mathf.Cos(i * deltaDegree)));
                if (SamplePosition(pos, out NavMeshHit temp, 1, navmeshLayer))
                {
                    pos = temp.position;
                    point.Add(new SortablePair<Vector3>(DistanceOnNavmesh(lastPos, pos, navmeshLayer), pos));
                    lastPos = pos;
                }
                Debug.DrawLine(pos, pos + Vector3.up, UnityEngine.Color.red, 1);
            }
            if (point.Count > 1)
            {
                point[0].key = point[point.Count - 1].key = DistanceOnNavmesh(point[0].value, point[point.Count - 1].value, navmeshLayer);
            }
            return point;
            //NavMeshHit temp;
            //NavMesh.SamplePosition(center, out temp, maxDistance, areaMask);
        }

        public static float DistanceOnNavmesh(Vector3 target1, Vector3 target2, int navmeshLayer)
        {
            float distance = 0;
            NavMeshPath path = new NavMeshPath();
            if (NavMesh.CalculatePath(target1, target2, navmeshLayer, path))
            {
                for (int i = 0; i < path.corners.Length - 1; i++)
                    distance += CommonHelper.Distance(path.corners[i], path.corners[i + 1]);
            }
            return distance;
        }

        public static int GetNavmehsLayer(EnumMovementType movementType)
        {
            return (movementType == EnumMovementType.Ground ? Navmeh_Area_Walkable : Navmeh_Area_Flight);
        }
    }
}
