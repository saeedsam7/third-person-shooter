﻿using RCPU.CameraFSM;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace RCPU.Helper
{
    public static class CommonHelper
    {

        public static float Distance(Vector3 target1, Vector3 target2)
        {
            return Vector3.Distance(target1, target2);
            //return  (target1 - target2).sqrMagnitude;
        }



        public static float DistanceIgnoreY(Vector3 target1, Vector3 target2)
        {
            target1.y = target2.y;
            return Vector3.Distance(target1, target2);
            //return  (target1 - target2).sqrMagnitude;
        }





        public static float GetCloserSpeed(float spd, Vector2[] speeds)
        {
            if (speeds.Length == 0)
                return 0;
            if (spd < speeds[0].x)
                return speeds[0].y;
            for (int i = 0; i < speeds.Length - 1; i++)
            {
                if (IsBetween(spd, speeds[i].x, speeds[i + 1].x))
                {
                    float totalX = speeds[i + 1].x - speeds[i].x;
                    float totalY = speeds[i + 1].y - speeds[i].y;
                    float multi = (spd - speeds[i].x) / totalX;
                    return speeds[i].y + (totalY * multi);
                }
            }
            return speeds[speeds.Length - 1].y;
        }

        public static int GetEnumLength(Type type)
        {
            return Enum.GetNames(type).Length;
        }
        public static int GetEnumLength<T>()
        {
            return Enum.GetNames(typeof(T)).Length;
        }
        public static string[] GetEnumNames(Type type)
        {
            return Enum.GetNames(type);
        }
        public static string[] GetEnumNames<T>()
        {
            return Enum.GetNames(typeof(T));
        }

        public static T[] GetEnumMembers<T>()
        {
            return Enum.GetValues(typeof(T)) as T[];
        }

        public static T GetEnumMember<T>(int index)
        {
            return (T)Enum.Parse(typeof(T), CommonHelper.GetEnumNames(typeof(T))[index]);
        }

        public static bool IsBetween(float f, float min, float max)
        {
            return (f >= min && f <= max);
        }

        public static float Linear(float start, float end, float t)
        {
            return (start * t) + (end * (1 - t));
        }

        public static Vector3 Linear(Vector3 start, Vector3 end, float t)
        {
            return (start * t) + (end * (1 - t));
        }

        public static Quaternion Average(Quaternion a, Quaternion b)
        {
            Vector3 result = a.ToAngularVelocity() + b.ToAngularVelocity();
            return (result / 2).FromAngularVelocity();
        }
        public static Quaternion Average(Quaternion[] source)
        {
            Vector3 result = new Vector3();
            foreach (var q in source)
            {
                result += q.ToAngularVelocity();
            }
            return (result / source.Length).FromAngularVelocity();
        }
        static Vector3 ToAngularVelocity(this Quaternion q)
        {
            if (Mathf.Abs(q.w) > 1023.5f / 1024.0f)
                return new Vector3();
            var angle = Mathf.Acos(Mathf.Abs(q.w));
            var gain = Mathf.Sign(q.w) * 2.0f * angle / Mathf.Sin(angle);

            return new Vector3(q.x * gain, q.y * gain, q.z * gain);
        }
        static Quaternion FromAngularVelocity(this Vector3 w)
        {
            var mag = w.magnitude;
            if (mag <= 0)
                return Quaternion.identity;
            var cs = Mathf.Cos(mag * 0.5f);
            var siGain = Mathf.Sin(mag * 0.5f) / mag;
            return new Quaternion(w.x * siGain, w.y * siGain, w.z * siGain, cs);
        }

        public static Vector3 GetCloserSpeed(Vector3 move, Vector3[] speeds)
        {
            return move * 4;
            //if (speeds.Length == 0)
            //    return 0;
            //if (spd < speeds[0].x)
            //    return speeds[0].y;
            //for (int i = 0; i < speeds.Length - 1; i++)
            //{
            //    if (IsBetween(spd, speeds[i].x, speeds[i + 1].x))
            //    {
            //        float totalX = speeds[i + 1].x - speeds[i].x;
            //        float totalY = speeds[i + 1].y - speeds[i].y;
            //        float multi = (spd - speeds[i].x) / totalX;
            //        return speeds[i].y + (totalY * multi);
            //    }
            //}
            //return speeds[speeds.Length - 1].y;
        }

        public static bool IsNull(System.Object obj)
        {
            bool res = (obj == null);
            return res;
        }

        public static bool IsNull(UnityEngine.Object obj)
        {
            bool res = (obj == null);
            return res;
        }

        public static Color Color(Color c, float a)
        {
            return new Color(c.r, c.g, c.b, a);
        }

        public static GUIStyle GetLabelStyle(Color color)
        {
            return new GUIStyle() { fontStyle = FontStyle.Bold, alignment = TextAnchor.MiddleCenter, normal = new GUIStyleState() { textColor = color } };
        }

        public static float Angle(Transform trans, Vector3 target, bool ignoreY)
        {
            Vector3 dir = target - trans.position;
            Vector3 fwd = trans.forward;
            if (ignoreY)
            {
                dir.y = 0;
                fwd.y = 0;
            }
            return Vector3.Angle(dir, fwd);
        }

        public static float SignedAngle(Transform trans, Vector3 target, bool ignoreY)
        {
            Vector3 dir = target - trans.position;
            Vector3 fwd = trans.forward;
            if (ignoreY)
            {
                dir.y = 0;
                fwd.y = 0;
            }
            return Vector3.SignedAngle(dir, fwd, Vector3.up);
        }

        public static Transform FindCloser(List<Transform> array, Vector3 position, bool removeResult)
        {
            if (array.Count == 0)
                return null;

            Transform result = null;
            float closer = Mathf.Infinity;
            int closerIndex = 0;
            for (int i = 0; i < array.Count; i++)
            {
                var dis = Distance(array[i].position, position);
                if (dis < closer)
                {
                    closer = dis;
                    closerIndex = i;
                }
            }
            result = array[closerIndex];
            if (removeResult)
            {
                array.RemoveAt(closerIndex);
            }
            return result;
        }

        public static float SignedAngle(Vector3 pos, Vector3 forward, Vector3 target, bool ignoreY)
        {
            Vector3 dir = target - pos;
            Vector3 fwd = forward;
            if (ignoreY)
            {
                dir.y = 0;
                fwd.y = 0;
            }
            return Vector3.SignedAngle(dir, fwd, Vector3.up);
        }



        public static float GetAngleFromAxis(float horizental, float vertical)
        {
            if (horizental != 0 || vertical != 0)
            {
                float v = (Mathf.Acos(vertical) * Mathf.Rad2Deg);
                float h = (Mathf.Asin(horizental) * Mathf.Rad2Deg);
                float total = Mathf.Abs(horizental) + Mathf.Abs(vertical);
                float vMulti = vertical / total;
                float hMulti = horizental / total;
                float H = (h * Mathf.Abs(horizental * hMulti));
                float V = (v * Mathf.Abs(vertical * vMulti)) * Mathf.Sign(H);
                return (V + H);
            }
            return 0;
        }


        public static float GetInputNormilizedAngle(Vector3 targetPos, Vector3 targetForward, Vector3 camForward, float horizontal, float vertical)
        {
            float angle = GetAngleFromAxis(horizontal, vertical);
            Vector3 forward = camForward;
            forward.y = 0;
            Vector3 ownerForward = targetForward;
            ownerForward.y = 0;
            Vector3 ownerPosition = targetPos;
            ownerPosition.y = 0;

            float Fangle = angle - SignedAngle(ownerPosition, ownerForward, forward, true);
            //string log = " angle " + angle + " Fangle " + Fangle;
            return NormilizeAngle(Fangle);
        }



        public static Quaternion Lerp(Quaternion a, Quaternion b, float t)
        {
            if (t < .001f)
                return a;
            if (t > 1f)
                t = 1f;
            return Quaternion.Lerp(a, b, t);
        }

        public static Vector3 Lerp(Vector3 a, Vector3 b, float t)
        {
            if (t < .001f)
                return a;
            if (t > 1f)
                t = 1f;
            return Vector3.Lerp(a, b, t);
        }
        public static Vector2 Lerp(Vector2 a, Vector2 b, float t)
        {
            if (t < .001f)
                return a;
            if (t > 1f)
                t = 1f;
            return Vector2.Lerp(a, b, t);
        }


        public static float Lerp(float a, float b, float t)
        {
            if (t < .001f)
                return a;
            if (t > 1f)
                t = 1f;
            return Mathf.Lerp(a, b, t);
        }



        public static void LookAt(Transform transform, Vector3 target, bool justY)
        {
            if (justY)
            {
                transform.LookAt(new Vector3(target.x, transform.position.y, target.z));
            }
            else
                transform.LookAt(target);
        }

        public static void LerpLookAt(Transform transform, Vector3 target, bool justY, float t)
        {
            Quaternion defRot = transform.rotation;
            LookAt(transform, target, justY);
            Quaternion finalRot = transform.rotation;
            transform.rotation = Lerp(defRot, finalRot, t);
        }

        public static void LinearLookAt(Transform transform, Vector3 target, float speed, bool justY)
        {
            //Debug.LogError(" SignedAngle " + SignedAngle(transform, target, true));
            Quaternion defRot = transform.rotation;
            LookAt(transform, target, justY);
            Quaternion finalRot = transform.rotation;
            transform.rotation = Quaternion.Slerp(defRot, finalRot, TimeController.GetNormilizeTime(speed));
        }



        public static float NormilizeAngle(float P)
        {
            if (P > 180)
                P -= 360;
            if (P < -180)
                P += 360;
            return P;
        }
        public static Vector3 NormilizeAngle(Vector3 P)
        {
            P.x = NormilizeAngle(P.x);
            P.y = NormilizeAngle(P.y);
            P.z = NormilizeAngle(P.z);
            return P;
        }



        public static Vector3 NearestPointOnLine(Vector3 vA, Vector3 vB, Vector3 vPoint)
        {
            var vVector1 = vPoint - vA;
            var vVector2 = (vB - vA).normalized;

            var d = Vector3.Distance(vA, vB);
            var t = Vector3.Dot(vVector2, vVector1);

            if (t <= 0)
                return vA;

            if (t >= d)
                return vB;

            var vVector3 = vVector2 * t;

            var vClosestPoint = vA + vVector3;

            return vClosestPoint;
        }


        //public static Dictionary<string, object> GetDictionary()
        //{
        //	return new Dictionary<string, object>();
        //}

        //public static Dictionary<string, object> GetDictionary(string key,object value)
        //{
        //	var d= new Dictionary<string, object>();
        //	d.Add(key,value);
        //	return d;
        //}

        public static float Power(float value, EnumPower pow)
        {
            switch (pow)
            {
                case EnumPower.Linear:
                    return value;
                case EnumPower.Quadratic:
                    return value * value;
                case EnumPower.Cubic:
                    return value * value * value;
                case EnumPower.Quadric:
                    return value * value * value * value;
                default:
                    return value;
            }
        }

        /// <summary>
        /// Returns multiplier for the strength of a shake, based on source and camera positions.
        /// </summary>
        public static float Strength(StrengthAttenuationParams pars, Vector3 sourcePosition, Vector3 cameraPosition)
        {
            Vector3 vec = cameraPosition - sourcePosition;
            float distance = Vector3.Scale(pars.axesMultiplier, vec).magnitude;
            float strength = Mathf.Clamp01(1 - (distance - pars.clippingDistance) / pars.falloffScale);

            return Power(strength, pars.falloffDegree);
        }

        /// <summary>
        /// Returns displacement, opposite to the direction to the source in camera's local space.
        /// </summary>
        public static Displacement Direction(Vector3 sourcePosition, Vector3 cameraPosition, Quaternion cameraRotation)
        {
            Displacement direction = Displacement.Zero;
            direction.position = (cameraPosition - sourcePosition).normalized;
            direction.position = Quaternion.Inverse(cameraRotation) * direction.position;

            direction.eulerAngles.x = direction.position.z;
            direction.eulerAngles.y = direction.position.x;
            direction.eulerAngles.z = -direction.position.x;

            return direction;
        }



        public static T GetCopyOf<T>(UnityEngine.Component comp, T other) where T : UnityEngine.Component
        {
            Type type = comp.GetType();
            if (type != other.GetType()) return null; // type mis-match
            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
            PropertyInfo[] pinfos = type.GetProperties(flags);
            foreach (var pinfo in pinfos)
            {
                if (pinfo.CanWrite)
                {
                    try
                    {
                        pinfo.SetValue(comp, pinfo.GetValue(other, null), null);
                    }
                    catch { } // In case of NotImplementedException being thrown. For some reason specifying that exception didn't seem to catch it, so I didn't catch anything specific.
                }
            }
            FieldInfo[] finfos = type.GetFields(flags);
            foreach (var finfo in finfos)
            {
                finfo.SetValue(comp, finfo.GetValue(other));
            }
            return comp as T;
        }

        public static string ColorHex(Color color)
        {
            return (string.Format("#{0:X2}{1:X2}{2:X2}", (byte)(color.r * 255f), (byte)(color.g * 255f), (byte)(color.b * 255f)));
        }

        public static Vector2 GetPolarCoord(float radius, float angle)
        {
            angle = ClampAngle(angle);
            if (radius == 0f && angle == 0f)
                return Vector2.zero;

            angle = CovertAngle(angle);
            return new Vector2(radius * Cos(angle), radius * Sin(angle));
        }

        public static float GetAngleFromPolarCoord(Vector2 coord)
        {
            if (coord == Vector2.zero)
                return 0;

            float angle = Mathf.Atan2(coord.y, coord.x) * Mathf.Rad2Deg;
            return ClampAngle(CovertAngle(angle));
        }


        private static float ClampAngle(float angle)
        {
            angle %= 360f;
            if (angle < 0f)
                angle += 360f;
            return angle;
        }
        private static float CovertAngle(float angle) => 90f - angle;
        private static float Sin(float angle) => Mathf.Sin(angle * Mathf.Deg2Rad);

        private static float Cos(float angle) => Mathf.Cos(angle * Mathf.Deg2Rad);

        internal static void Swap<T>(ref T first, ref T second)
        {
            T temp = first;
            first = second;
            second = temp;
        }
    }
}
