﻿#if UNITY_EDITOR 
#endif
//using UnityEditor;
using RCPU.Attributes;
using TMPro;
using UnityEngine;

namespace RCPU.Helper
{
    public interface IGizmosHelper : IInitializable
    {
        void DrawCube(Vector3 position, Quaternion rotation, Vector3 size, Color color);
        void DrawCone(Vector3 position, Quaternion rotation, Vector3 size, Color color);
        void DrawSphere(Vector3 position, Quaternion rotation, Vector3 size, Color color);
        void Label(Vector3 position, Quaternion rotation, float size, string text, Color color);

    }

    [InitOrder(EnumInitOrder.Logger)]
    [InjectToSystem(typeof(IGizmosHelper))]
    public class GizmosHelper : BaseObject, IGizmosHelper
    {
        [SerializeField] protected Mesh CubeMesh;

        [SerializeField] protected Mesh SphereMesh;

        [SerializeField] protected Mesh ConeMesh;

        [SerializeField] protected Mesh CylinderMesh;

        [SerializeField] protected Mesh QuadMesh;

        [SerializeField] protected Material mat;
        [SerializeField] protected TMP_FontAsset font;

        protected Material[] mats;
        int frameIndex = -1;
        int materialIndex = -1;
        public void Init()
        {
            mats = new Material[10];
            for (int i = 0; i < mats.Length; i++)
            {
                mats[i] = Instantiate(mat);
            }

        }

        protected Material GetMaterial(Color color)
        {
            if (frameIndex != TimeController.frameCount)
            {
                frameIndex = TimeController.frameCount;
                materialIndex = -1;
            }
            materialIndex++;
            mats[materialIndex].color = color;
            return mats[materialIndex];
        }



        public void DrawSphere(Vector3 position, Quaternion rotation, Vector3 size, Color color)
        {
            Graphics.DrawMesh(SphereMesh, Matrix4x4.TRS(position, rotation, size), GetMaterial(color), 0);

            //Graphics.DrawMeshNow(s_CubeMesh, mat);
            //Handles.CubeHandleCap(id, position, rotation, size, EventType.Repaint);
        }

        public void DrawCube(Vector3 position, Quaternion rotation, Vector3 size, Color color)
        {
            Graphics.DrawMesh(CubeMesh, Matrix4x4.TRS(position, rotation, size), GetMaterial(color), 0);

            //Graphics.DrawMeshNow(s_CubeMesh, mat);
            //Handles.CubeHandleCap(id, position, rotation, size, EventType.Repaint);
        }

        public static void DrawLines(Vector3[] points, bool loop, Color color)
        {
            for (int i = 0; i < points.Length - 1; i++)
            {
                Debug.DrawLine(points[i], points[i + 1], color);
            }
            if (loop)
            {
                Debug.DrawLine(points[points.Length - 1], points[0], color);
            }
        }

        //public static void DrawSphere(SphereCollider collider)
        //{
        //    Handles.SphereHandleCap(collider.GetInstanceID(), collider.bounds.center, Quaternion.identity, collider.radius * 2 * collider.transform.lossyScale.magnitude, EventType.Repaint);
        //}

        //public static void DrawSphere(int id, Vector3 pos, float radius)
        //{
        //    Handles.SphereHandleCap(id, pos, Quaternion.identity, radius, EventType.Repaint);
        //}
        public void Label(Vector3 position, Quaternion rotation, float size, string text, Color color)
        {
            TextDrawer.DrawText(text, size, color, Matrix4x4.TRS(position, rotation, Vector3.one), font);
            //Graphics.DrawMesh(GetTextMesh(text), position, rotation, GetTextMaterial(color), 0);



            //Handles.Label(position, text);
        }
        //public static void SetHandlesColor(Color c, float a)
        //{
        //    //Handles.color = CommonHelper.Color(c, a);
        //}

        //public static void SetHandlesColor(Color c)
        //{
        //    //Handles.color = c;
        //}

        //public static void SetGuiColor(Color c, float a)
        //{
        //    GUI.color = CommonHelper.Color(c, a);
        //}

        //public static void SetGuiColor(Color c)
        //{
        //    GUI.color = c;
        //}

        public void DrawCone(Vector3 position, Quaternion rotation, Vector3 size, Color color)
        {
            Graphics.DrawMesh(ConeMesh, Matrix4x4.TRS(position, rotation * Quaternion.Euler(0, -90, 0), size), GetMaterial(color), 0);
            //Handles.DrawSolidArc(center, normal, from, angle, radius);
        }

        public static void DrawArrow(Vector3 origin, Vector3 direction, float length, Color color, float duration)
        {
            float size = length * .1f;
            Vector3 end = origin + (direction.normalized * length);
            Vector3 endArrow = origin + (direction.normalized * (length + size));


            Debug.DrawLine(origin + Vector3.up * size, origin + Vector3.down * size, color, duration);
            Debug.DrawLine(origin, end, color, duration);
            Debug.DrawLine(end + Vector3.up * size, end + Vector3.down * size, color, duration);

            Debug.DrawLine(end + Vector3.up * size, endArrow, color, duration);
            Debug.DrawLine(end + Vector3.down * size, endArrow, color, duration);
        }

        public static void DrawArrow(Vector3 origin, Vector3 direction, float length, Color color)
        {
            float size = length * .1f;
            Vector3 end = origin + (direction.normalized * length);
            Vector3 endArrow = origin + (direction.normalized * (length + size));


            Debug.DrawLine(origin + Vector3.up * size, origin + Vector3.down * size, color);
            Debug.DrawLine(origin, end, color);
            Debug.DrawLine(end + Vector3.up * size, end + Vector3.down * size, color);

            Debug.DrawLine(end + Vector3.up * size, endArrow, color);
            Debug.DrawLine(end + Vector3.down * size, endArrow, color);
        }

        //public static void CircleHandleCap(Vector3 position, Quaternion rotaion, float size)
        //{
        //    Handles.CircleHandleCap(1, position, rotaion, size, EventType.Repaint);
        //}

        //public static void ConeHandleCap(Vector3 position, Quaternion rotaion, float size)
        //{
        //    Handles.ConeHandleCap(1, position, rotaion, size, EventType.Repaint);
        //}

        //public static void CylinderHandleCap(Vector3 position, Quaternion rotaion, float size)
        //{
        //    Handles.CylinderHandleCap(1, position, rotaion, size, EventType.Repaint);
        //}

        //public static void DrawPolygon(Vector3[] points)
        //{
        //    Handles.DrawAAConvexPolygon(points);
        //}

        //public static void DrawArrow(int controlID, Vector3 position, Quaternion rotation, float size)
        //{
        //    //Handles.ArrowHandleCap(controlID, position, rotation, size, EventType.Repaint);
        //}


    }


}