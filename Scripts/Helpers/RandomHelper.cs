﻿
using RCPU.Models;
using UnityEngine;

namespace RCPU.Helper
{
    public static class RandomHelper
    {
        public static Vector3 insideUnitSphere => Random.insideUnitSphere;
        public static Vector2 insideUnitCircle => Random.insideUnitCircle;



        public static Vector3 Range(Vector3 max)
        {
            return new Vector3(Range(0, max.x), Range(0, max.y), Range(0, max.z));
        }
        public static float Range(float max)
        {
            return Range(0, max);
        }
        public static float Range(float min, float max)
        {
            return Random.Range(min, max);
        }

        public static float Range(RangeFloat range)
        {
            return Random.Range(range.min, range.max);
        }

        public static int Range(RangeInteger range)
        {
            return Random.Range(range.min, range.max);
        }

        public static int Range(int min, int max)
        {
            return Random.Range(min, max);
        }

        public static int Range(int max)
        {
            return Random.Range(0, max);
        }

        public static int Sign()
        {
            return Random.Range(0, 2) == 0 ? 1 : -1;
        }

        public static bool HaveChance(int chance)
        {
            if (chance <= 0)
                return false;
            return Random.Range(0, 101) <= chance;
        }
        public static bool HaveChance(float chance)
        {
            if (chance <= 0)
                return false;
            return Random.Range(0f, 100f) <= chance;
        }

        public static Color RandomColor()
        {
            return new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f), 1);
        }
    }
}
