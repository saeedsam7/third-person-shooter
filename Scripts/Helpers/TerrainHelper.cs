﻿using UnityEngine;

namespace RCPU.Helper
{
    public static class TerrainHelper
    {
        public static Mesh TerrainSectionToMesh(Terrain terrain, Vector3 startPoint, Vector2 scale, int vertexCount, float offset)
        {
            // First get the step sizes according to vertexCount 
            //startPoint-=new Vector3(radius/2f, 0, radius/2f);   
            var stepX = scale.x / vertexCount;
            var stepZ = scale.y / vertexCount;

            // iterate the size in X and Z direction to get the target vertices
            // This crates a vertex grid with given terrain heights on according positions
            var vertices = new Vector3[(vertexCount + 1) * (vertexCount + 1)];
            var uvs = new Vector2[vertices.Length];
            var tangents = new Vector4[vertices.Length];
            var tangent = new Vector4(1f, 0f, 0f, -1f);
            int i = 0;
            for (int z = 0; z <= vertexCount; z++)
            {
                for (var x = 0; x <= vertexCount; x++, i++)
                {
                    var position = (Vector3.forward * stepZ * (z - vertexCount / 2f)) + (Vector3.right * stepX * (x - vertexCount / 2f));

                    position.y = terrain.SampleHeight(startPoint + position) + terrain.transform.position.y - startPoint.y;
                    vertices[i] = position;

                    uvs[i] = (new Vector2((float)x / vertexCount, (float)z / vertexCount));
                    tangents[i] = (tangent);
                }
            }

            // Procedural grid generation taken from https://catlikecoding.com/unity/tutorials/procedural-grid
            // This generates the triangles for the given vertex grid
            int[] triangles = new int[vertices.Length * 6];
            for (int ti = 0, vi = 0, y = 0; y < vertexCount; y++, vi++)
            {
                for (int x = 0; x < vertexCount; x++, ti += 6, vi++)
                {
                    triangles[ti] = vi;
                    triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                    triangles[ti + 4] = triangles[ti + 1] = vi + vertexCount + 1;
                    triangles[ti + 5] = vi + vertexCount + 2;
                }
            }

            // Finally create the mesh and fill in the data
            Mesh mesh = new Mesh();
            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.SetUVs(0, uvs);
            mesh.tangents = tangents;
            mesh.RecalculateNormals();
            for (int j = 0; j < vertices.Length; j++)
            {
                vertices[j] += mesh.normals[j] * offset;
            }
            mesh.vertices = vertices;
            mesh.Optimize();
            return mesh;
        }
    }
}
