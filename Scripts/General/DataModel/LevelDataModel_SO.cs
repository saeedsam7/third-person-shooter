﻿
using UnityEngine;

namespace RCPU
{
    [CreateAssetMenu(fileName = "Level", menuName = "RCPU/DataModel/Level/LevelDataModel", order = 2)]
    public class LevelDataModel_SO : ScriptableObject
    {
        public LevelDataModel model;
    }
}
