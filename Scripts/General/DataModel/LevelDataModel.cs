﻿
using RCPU.Audio;
using System;
using UnityEngine;

namespace RCPU
{
    [Serializable]

    public struct LevelDataModel
    {
        [Header("Sound")]
        public EnumAudioAmbient ambient;
        public EnumAudioMusic music;

    }
}
