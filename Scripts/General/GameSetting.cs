﻿namespace RCPU
{
    public class GameSetting
    {
        public static readonly float ReflexDuration = 1.0f;


        public static readonly float MAX_VISIBILITY = 2f;
        public static readonly float OFFENSIVE_VISIBILITY = 1f;
        public static readonly float SUSPICIOUS_VISIBILITY = .5f;

        public static readonly float DEACTIVATE_RAGDOLL = .5f;
    }
}