
using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Audio;
using RCPU.Damage;
using RCPU.DependencyInjection;
using RCPU.Helper;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RCPU
{
    [InitOrder(EnumInitOrder.Level)]
    [InjectToSystem(typeof(ILevelManager))]
    public class LevelManager : ControllerBase, ILevelManager
    {
        [SerializeField] public LevelDataModel_SO model_so;
        protected LevelDataModel model;


        [InjectFromSystem] IAudioManager AudioManager { get; set; }
        //public static IGameController Instance { get; private set; }
        protected Dictionary<int, IAlive> allAliveObject;
        protected Dictionary<int, IDamageManager> allDamageManager;

        void ResetGameSetting()
        {
            Cursor.visible = false;
            Application.targetFrameRate = 60;
            //Debug.LogError(" Application.targetFrameRate " + Application.targetFrameRate);
        }

        public void Init()
        {
            this.model = DInjector.Instantiate<LevelDataModel_SO>(model_so).model;
            //Instance = this;
            //Debug.Log(" Helper.Layer_Agent " + Helper.Layer_Agent.value+ " Helper.Layer_Agent2 " + Helper.Layer_Agent2.value + " Helper.Layer_Agent_Environment " + Helper.Layer_Agent_Environment.value);
            ResetGameSetting();
            allAliveObject = new Dictionary<int, IAlive>();
            allDamageManager = new Dictionary<int, IDamageManager>();

            AudioManager.ChangeAmbient(model.ambient);
            AudioManager.ChangeMusic(model.music);
        }

        public void RegisterDamageManager(int id, IDamageManager damageManager)
        {
            if (!allDamageManager.ContainsKey(id))
            {
                allDamageManager.Add(id, damageManager);
            }
        }

        public void RegisterAgent(int id, IAlive agent)
        {
            if (!allAliveObject.ContainsKey(id))
            {
                allAliveObject.Add(id, agent);
            }
        }
        public IDamageManager GetDamageManager(int id)
        {
            if (allDamageManager.ContainsKey(id))
            {
                return allDamageManager[id];
            }
            return null;
        }

        public IDamageManager GetDamageManagerExcept(int id, int ownerID)
        {
            var tgt = GetDamageManager(id);
            if (!CommonHelper.IsNull(tgt) && tgt.GetOwner().Id != ownerID)
            {
                return tgt;
            }
            return null;
        }

        public IAlive GetAliveObject(int id)
        {
            if (allAliveObject.ContainsKey(id))
            {
                return allAliveObject[id];
            }
            return null;
        }
        public IAlive GetAliveObjectExcept(int id, IAlive owner)
        {
            var tgt = GetAliveObject(id);
            if (tgt != owner)
            {
                return tgt;
            }
            return null;
        }



        public bool Raycast(Vector3 origin, Vector3 direction, out RaycastHit hitInfo, float maxDistance, int layerMask, IAlive ignore)
        {
            foreach (var item in PhysicsHelper.RaycastOrderAll(origin, direction, maxDistance, layerMask))
            {
                int colId = item.colliderInstanceID;
                if (CommonHelper.IsNull(GetDamageManager(colId)) || !CommonHelper.IsNull(GetDamageManagerExcept(colId, ignore.Id)))
                {
                    hitInfo = item;
                    return true;
                }
            }
            hitInfo = new RaycastHit() { };
            return false;
        }

        //public Dictionary<IDamageManager, int> GetAgentsInArea(Vector3 position, float radius, int layerMask, bool confirmByRaycast, int layerMaskConfirm)
        //{
        //    Dictionary<IDamageManager, int> allAlive = new Dictionary<IDamageManager, int>();

        //    foreach (var item in PhysicsHelper.OverlapSphere(position, radius, layerMask))
        //    {
        //        int colId = item.GetInstanceID();
        //        var alive = GetDamageManager(colId);
        //        //CLog.Error(" OnTriggerEnter damage " + (damage != null) + "  other " + other);
        //        if (!CommonHelper.IsNull(alive) && !allAlive.ContainsKey(alive))
        //        {
        //            if (confirmByRaycast)
        //            {
        //                Vector3 pos = item.transform.position;
        //                if (PhysicsHelper.Raycast(position, (pos - position), out RaycastHit hit, CommonHelper.Distance(pos, position), layerMaskConfirm))
        //                {
        //                    alive = GetDamageManager(hit.colliderInstanceID);
        //                    if (!CommonHelper.IsNull(alive) && !allAlive.ContainsKey(alive))
        //                    {
        //                        allAlive.Add(alive, hit.colliderInstanceID);
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                allAlive.Add(alive, colId);
        //            }
        //        }
        //    }
        //    return allAlive;
        //}

        public IEnumerable<IAlive> GetNearAgents(Vector3 position, float distance)
        {
            return allAliveObject.Values.Select(z => z).Where(x => CommonHelper.Distance(position, x.Position) < distance);
        }

        public bool IsNearGrenade(Vector3 position)
        {
            return false;
        }

        public Dictionary<IDamageManager, int> GetAgentsInArea(Vector3 position, float radius)
        {
            Dictionary<IDamageManager, int> allAlive = new Dictionary<IDamageManager, int>();
            foreach (var item in PhysicsHelper.OverlapSphere(position, radius, PhysicsHelper.Layer_DamageReceiver))
            {
                int colId = item.GetInstanceID();
                var alive = GetDamageManager(colId);
                //CLog.Error(" OnTriggerEnter damage " + (damage != null) + "  other " + other);
                if (!CommonHelper.IsNull(alive) && !allAlive.ContainsKey(alive))
                {
                    allAlive.Add(alive, colId);
                }
            }
            return allAlive;
        }

        public Dictionary<IDamageManager, int> GetAgentsInAreaRaycast(Vector3 position, float radius, int layerMaskConfirm)
        {
            Dictionary<IDamageManager, int> allAlive = new Dictionary<IDamageManager, int>();

            foreach (var item in PhysicsHelper.OverlapSphere(position, radius, PhysicsHelper.Layer_DamageReceiver))
            {
                int colId = item.GetInstanceID();
                var alive = GetDamageManager(colId);
                //CLog.Error(" OnTriggerEnter damage " + (damage != null) + "  other " + other);
                if (!CommonHelper.IsNull(alive) && !allAlive.ContainsKey(alive))
                {
                    Vector3 pos = item.transform.position;
                    if (PhysicsHelper.Raycast(position, (pos - position), out RaycastHit hit, CommonHelper.Distance(pos, position), layerMaskConfirm))
                    {
                        alive = GetDamageManager(hit.collider.GetInstanceID());
                        if (!CommonHelper.IsNull(alive) && !allAlive.ContainsKey(alive))
                        {
                            allAlive.Add(alive, hit.collider.GetInstanceID());
                        }
                    }

                }
            }
            return allAlive;
        }
    }
}