﻿
using RCPU.Attributes;
using RCPU.Helper;
using UnityEngine;

namespace RCPU
{
    [InjectToSystem(typeof(IPlayerSpawnPosition))]
    public class PlayerSpawnPosition : BaseObject, IPlayerSpawnPosition
    {
        public Vector3 Position => transform.position;

        void OnDrawGizmos()
        {
#if UNITY_EDITOR
            //Vector3 pos = transform.position + Vector3.up * 5;
            //GizmosHelper.SetHandlesColor(Color.green, .8f);
            Debug.DrawLine(transform.position, transform.position + Vector3.up * 5, CommonHelper.Color(Color.blue, .8f));
            Debug.DrawLine(transform.position, transform.position + Vector3.up + Vector3.right, CommonHelper.Color(Color.blue, .8f));
            Debug.DrawLine(transform.position, transform.position + Vector3.up + Vector3.left, CommonHelper.Color(Color.blue, .8f));
            Debug.DrawLine(transform.position, transform.position + Vector3.up + Vector3.forward, CommonHelper.Color(Color.blue, .8f));
            Debug.DrawLine(transform.position, transform.position + Vector3.up + Vector3.back, CommonHelper.Color(Color.blue, .8f));
#endif
        }
    }
}
