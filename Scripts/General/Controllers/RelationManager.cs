using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Damage;
using RCPU.NotificationSystem;

namespace RCPU
{

    [InitOrder(EnumInitOrder.Relation)]
    [InjectToSystem(typeof(IRelation))]
    public class RelationManager : ControllerBase, IRelation
    {

        RelationData model;
        public void Init()
        {
            Notification.Notify(EnumNotifType.LoadData, new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.type, EnumNotifParam.actionCallBack }, new object[] {
                typeof(RelationData), new ActionModel<object> { action = LoadComplete }}));

            LoadComplete(null);
        }

        void LoadComplete(object data)
        {
            //TODO LOAD FROM FILE
            //model = data as RelationData;
            //if (CommonHelper.IsNull(model))
            {
                model = new RelationData() { table = new float[10, 10] };
                for (int i = 0; i < 10; i++)
                {
                    for (int j = 0; j < 10; j++)
                    {
                        model.table[i, j] = 0;
                    }
                }
                SetRelation(EnumFamily.Player, EnumFamily.Player, -100f);
                SetRelation(EnumFamily.Player, EnumFamily.Buddy, -100f);
                SetRelation(EnumFamily.Player, EnumFamily.World_01_Neutral, 0f);
                SetRelation(EnumFamily.Player, EnumFamily.World_02_Neutral, 0f);
                SetRelation(EnumFamily.Player, EnumFamily.World_03_Neutral, 0f);
                SetRelation(EnumFamily.Buddy, EnumFamily.World_01_Neutral, 0f);
                SetRelation(EnumFamily.Buddy, EnumFamily.World_02_Neutral, 0f);
                SetRelation(EnumFamily.Buddy, EnumFamily.World_03_Neutral, 0f);

                SetRelation(EnumFamily.World_01_Neutral, EnumFamily.World_02_Neutral, 0f);
                SetRelation(EnumFamily.World_01_Neutral, EnumFamily.World_03_Neutral, 0f);
                SetRelation(EnumFamily.World_02_Neutral, EnumFamily.World_03_Neutral, 0f);


                SetRelation(EnumFamily.World_01_Ai, EnumFamily.World_02_Ai, -100f);
                SetRelation(EnumFamily.World_01_Ai, EnumFamily.World_03_Ai, -100f);
                SetRelation(EnumFamily.World_02_Ai, EnumFamily.World_03_Ai, -100f);


                SetRelation(EnumFamily.World_01_Ai, EnumFamily.Player, 100f);
                SetRelation(EnumFamily.World_01_Ai, EnumFamily.Buddy, 100f);
                SetRelation(EnumFamily.World_02_Ai, EnumFamily.Player, 100f);
                SetRelation(EnumFamily.World_02_Ai, EnumFamily.Buddy, 100f);
                SetRelation(EnumFamily.World_03_Ai, EnumFamily.Player, 100f);
                SetRelation(EnumFamily.World_03_Ai, EnumFamily.Buddy, 100f);

                SetRelation(EnumFamily.World_01_Ai, EnumFamily.World_01_Neutral, 0f);
                SetRelation(EnumFamily.World_02_Ai, EnumFamily.World_02_Neutral, 0f);
                SetRelation(EnumFamily.World_03_Ai, EnumFamily.World_03_Neutral, 0f);


                Notification.Notify(EnumNotifType.SaveData, new NotificationParameter(EnumNotifParam.dataToSave, model));
                //CDataBase.ins tance.SaveData(model);
            }
        }

        public void SetRelation(EnumFamily source, EnumFamily destination, EnumRelation val)
        {
            SetRelation(source, destination, (int)val);
        }
        public void SetRelation(EnumFamily source, EnumFamily destination, float val)
        {
            model.table[(int)source, (int)destination] = val;
            model.table[(int)destination, (int)source] = val;
        }

        public float GetRelationValue(EnumFamily source, EnumFamily destination)
        {
            if (source == destination)
                return -100;
            return model.table[(int)source, (int)destination];
        }

        public EnumRelation GetRelation(EnumFamily source, EnumFamily destination)
        {
            if (source == destination)
                return EnumRelation.Friend;

            float f = GetRelationValue(source, destination);
            if (f <= (int)EnumRelation.Friend)
                return EnumRelation.Friend;
            if (f < (int)EnumRelation.PotentialEnemy)
                return EnumRelation.Neutral;
            if (f < (int)EnumRelation.Enemy)
                return EnumRelation.PotentialEnemy;

            return EnumRelation.Enemy;
        }

        //public IRelation Cast()
        //{
        //    return this;
        //}

        public bool IsEnemy(EnumRelation enumRelation)
        {
            return (enumRelation == EnumRelation.Enemy || enumRelation == EnumRelation.PotentialEnemy);
        }

        public bool IsEnemy(IAlive agent, IAlive target)
        {
            return IsEnemy(agent.GetRelation(target.Family));
        }

        public bool IsEnemy(IAlive agent, IDamageManager target)
        {
            return IsEnemy(agent.GetRelation(target.Family));
        }

        public bool IsFriend(EnumRelation enumRelation)
        {
            return (enumRelation == EnumRelation.Friend || enumRelation == EnumRelation.Neutral);
        }

        public bool IsFriend(IAlive agent, IAlive target)
        {
            return IsFriend(agent.GetRelation(target.Family));
        }
    }
}