using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.NotificationSystem;
using System;
using UnityEngine;

namespace RCPU
{
    public class GameInitializer : BaseObject
    {
        [SerializeField] protected GameObject[] initializableObjects;
        bool isInitialized;
        DInjector DI;
        void Awake()
        {
            if (!TryGetComponent<DInjector>(out DI))
            {
                DI = gameObject.AddComponent<DInjector>();
            }
            DI.Init(DEBUG);

            if (!CommonHelper.IsNull(initializableObjects) && initializableObjects.Length > 0)
            {
                Initialize();
            }
            else
            {
                throw new Exception(" Game Initializer Init  " + name);
            }
        }

        private void Initialize()
        {
            if (!isInitialized)
            {
                if (DEBUG)
                {
                    Debug.Log("Initialize started ");
                }
                isInitialized = true;
                DI.Instantiate(transform, initializableObjects);
                Notification.Notify(EnumNotifType.GameInitializationDone, new NotificationParameter());
                if (DEBUG)
                {
                    Debug.Log("Initialized ended");
                }
            }
        }


    }
}