﻿
//using RCPU.Attributes;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using UnityEngine;
//namespace RCPU  
//{
//    [Attribute_InitOrder(1)]
//    [Attribute_InjectToSystem(typeof(ITriggersManager))]
//    public class TriggersManager : MonoBehaviour, ITriggersManager
//    {
//        //public static ITriggersManager Instance { get; private set; }
//        [SerializeField]
//        private List<MTriggerDayCycle> dayCycles;
//        //[SerializeField]
//        //List<MTriggerPickupItem> pickups;
//        //[SerializeField]
//        //List<MTriggerTeleportItem> teleports;
//        [SerializeField]
//        private List<MTriggerDoor> doors;
//        //[SerializeField]
//        //private List<MTriggerInteractable> interactables;
//        [SerializeField]
//        private List<MTriggerCover> covers;
//        //private List<Vector3> getCoverResult;
//        //[SerializeField]
//        //private List<MTrigger> covers;

//        [SerializeField]
//        private List<MTriggerSpawn> spawn;


//        private Dictionary<int, ITrigger> allTriggers;





//        public void Remove(int triggerId)
//        {
//            if (allTriggers.ContainsKey(triggerId))
//            {
//                allTriggers.Remove(triggerId);
//                //dayCycles.Remove(tr);
//            }
//        }


//        //public enum TriggerType { DayCycle, Elevator }
//        //public TriggerType regionType; 
//        //public bool IsDayCycle { get { return regionType == TriggerType.DayCycle; } }
//        //[ShowIf("IsDayCycle")]
//        //public MDayCycle dayCycle;

//        public void Init()
//        {
//            //if (!CommonHelper.IsNull(Instance))
//            //{
//            //    return;
//            //}

//            //Instance = this;
//            allTriggers = new Dictionary<int, ITrigger>();
//            //getCoverResult = new List<Vector3>();

//            for (int i = 0; i < dayCycles.Count; i++)
//            {
//                allTriggers.Add(dayCycles[i].Id, dayCycles[i]);
//            }

//            //for (int i = 0; i < pickups.Count; i++)
//            //{
//            //    allTriggers.Add(pickups[i].Id, pickups[i]);
//            //}
//            for (int i = 0; i < doors.Count; i++)
//            {
//                allTriggers.Add(doors[i].Id, doors[i]);
//            }
//            //for (int i = 0; i < interactables.Count; i++)
//            //{
//            //    allTriggers.Add(interactables[i].Id, interactables[i]);
//            //}
//            UpdateSceneCover();
//            for (int i = 0; i < covers.Count; i++)
//            {
//                allTriggers.Add(covers[i].Id, covers[i]);
//            }

//            for (int i = 0; i < spawn.Count; i++)
//            {
//                allTriggers.Add(spawn[i].Id, spawn[i]);
//            }

//        }



//        public bool GetCover(ITarget caller, List<ITarget> treats, MCoverResult coverResult)
//        {
//            //getCoverResult.Clear();
//            Vector3 agentPosition = caller.Position;
//            foreach (var item in covers.Select(z => z).Where(x => CommonHelper.Distance(agentPosition, x.Position) < 50).OrderBy(x => CommonHelper.Distance(agentPosition, x.Position)))
//            {
//                if (item.HitCheck(caller, treats, coverResult))
//                {
//                    return true;
//                }
//                //continue;
//            }
//            return false;
//        }

//        [ContextMenu("UpdateSceneCover")]
//        public void UpdateSceneCover()
//        {

//            covers = new List<MTriggerCover>();
//            foreach (var item in FindObjectsOfType<BoxCollider>())
//            {
//                if (item.CompareTag(TagsHelper.Cover))
//                {
//                    var cover = new MTriggerCover(item);
//                    covers.Add(cover);
//                }
//            }
//            //connection = new List<MCoverConnection>();
//            for (int i = 0; i < covers.Count; i++)
//            {
//                for (int k = i + 1; k < covers.Count; k++)
//                {
//                    covers[i].MakeConnection(covers[k]);
//                }
//            }
//            for (int i = 0; i < covers.Count; i++)
//            {
//                covers[i].RemoveUnusedConnections();
//            }

//            //foreach (var item in connection)
//            //{
//            //    Debug.DrawLine(item.start, item.end, Color.yellow, 120);
//            //}
//        }
//        //public List<MCoverConnection> connection;


//        Collider CreateGameObject(Vector3 pos, string name, float radius)
//        {
//            var go = new GameObject(name);
//            var rb = go.AddComponent<Rigidbody>();
//            rb.isKinematic = true;
//            SphereCollider c = go.AddComponent<SphereCollider>();
//            c.isTrigger = true;
//            c.transform.SetParent(transform);
//            c.transform.position = pos;
//            c.radius = radius;
//            c.tag = TagsHelper.Trigger;
//            c.gameObject.layer = LayerMask.NameToLayer(PhysicsHelper.Layer_Trigger_Name);
//            return c;
//        }

//        //public MTriggerPickupItem AddPickupItem(Vector3 pos, MBullet_Base.EnumBulletType bulletType,  CStateMachine_Weapon_Base.EnumWeaponType weaponType, int amount)
//        //{
//        //    Collider c = CreateGameObject(pos);
//        //    MTriggerPickupItem trig = new MTriggerPickupItem() { trigger = c, pickupItem = new MPickupBullet(amount) { bulletType = bulletType, weaponType = weaponType } };
//        //    pickups.Add(trig);
//        //    allTriggers.Add(trig.Id, trig);
//        //    return trig;
//        //}

//        public MTriggerPickupItem AddPickupItem(Vector3 pos, IPickupItem pickupItem)
//        {
//            Collider c = CreateGameObject(pos, "Pickup type " + pickupItem.PickupType, 2);
//            MTriggerPickupItem trig = CInjection.CreateClass<MTriggerPickupItem>(new object[] { c, pickupItem });

//            //interactables.Add(trig);
//            allTriggers.Add(trig.Id, trig);
//            return trig;
//        }



//        public MTriggerVehicle AddVehicleTrigger(Vector3 pos, CVehicle_Base vehicleController)
//        {
//            Collider c = CreateGameObject(pos, "Trigger Interactable Trigger Vehicle ", 2);
//            MTriggerVehicle trig = CInjection.CreateClass<MTriggerVehicle>(new object[] { c, vehicleController });
//            //interactables.Add(trig);
//            allTriggers.Add(trig.Id, trig);
//            return trig;
//        }


//        public MTriggerTeleportItem AddTelleportTrigger(Vector3 pos, MTeleportItem teleportItem)
//        {
//            Collider c = CreateGameObject(pos, "Telleport type " + teleportItem.teleportType, 2);
//            MTriggerTeleportItem trig = CInjection.CreateClass<MTriggerTeleportItem>(new object[] { c, teleportItem });
//            //interactables.Add(trig);
//            allTriggers.Add(trig.Id, trig);
//            return trig;
//        }



//        public ITrigger GetTriggerModel(int id)
//        {
//            if (allTriggers.ContainsKey(id))
//                return allTriggers[id];
//            return null;
//        }

//#if UNITY_EDITOR
//        void DrawGizmos(ITrigger reg)
//        {
//            Collider c = reg.Trigger;
//            GizmosHelper.Label(c.bounds.center, " Trigger : " + reg.TriggerType + " - " + c.name);
//            if (c.GetType() == typeof(BoxCollider))
//            {
//                GizmosHelper.DrawCube((BoxCollider)c);
//            }
//            if (c.GetType() == typeof(SphereCollider))
//            {
//                GizmosHelper.DrawSphere((SphereCollider)c);
//            }
//        }
//        void OnDrawGizmos()
//        {
//            GizmosHelper.SetHandlesColor(Color.cyan, .1f);

//            if (allTriggers != null)
//                foreach (var item in allTriggers)
//                {
//                    DrawGizmos(item.Value);
//                }
//        }
//#endif
//    }

//}