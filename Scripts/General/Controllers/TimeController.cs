using UnityEngine;

namespace RCPU
{
    public class TimeController : BaseObject
    {
        static int _frameCount;
        static float _gameTime;
        [SerializeField]
        float DebugGameTime;
        public void Init()
        {
            _gameTime = 0;
        }
        void Update()
        {
            _frameCount = Time.frameCount;
            _gameTime += Time.deltaTime;
            DebugGameTime = _gameTime;
        }
        void LateUpdate()
        {
            _frameCount = Time.frameCount;
        }
        public static int frameCount
        {
            get
            {
                //Logger.Log(" _frameCount " + _frameCount);
                return _frameCount;
            }
        }
        public static float realtimeSinceStartup { get { return Time.realtimeSinceStartup; } }

        public static int renderedFrameCount { get { return Time.renderedFrameCount; } }

        public static float timeScale { get { return Time.timeScale; } set { Time.timeScale = value; } }

        public static float maximumParticleDeltaTime { get { return Time.maximumParticleDeltaTime; } set { Time.maximumParticleDeltaTime = value; } }

        public static float smoothDeltaTime { get { return Time.smoothDeltaTime; } }

        public static float maximumDeltaTime { get { return Time.maximumDeltaTime; } set { Time.maximumDeltaTime = value; } }
        public static int captureFramerate { get { return Time.captureFramerate; } set { Time.captureFramerate = value; } }

        public static float fixedDeltaTime { get { return Time.fixedDeltaTime; } set { Time.fixedDeltaTime = value; } }

        public static float unscaledDeltaTime { get { return Time.unscaledDeltaTime; } }

        public static float fixedUnscaledTime { get { return Time.fixedUnscaledTime; } }

        public static float unscaledTime { get { return Time.unscaledTime; } }

        public static float fixedTime { get { return Time.fixedTime; } }

        public static float deltaTime { get { return Time.deltaTime; } }
        public static float GetNormilizeTime(float speed)
        {
            if (deltaTime < 0.001f)
                return 0;
            return Mathf.Clamp(deltaTime * speed, 0, 1);
            //      var x = deltaTime * speed;
            //return (x > 1 ? 1 : x);
        }
        public static float NormilizeDeltaTime => Mathf.Clamp(deltaTime, 0, 1);


        public static float timeSinceLevelLoad { get { return Time.timeSinceLevelLoad; } }

        public static float time { get { return Time.time; } }

        public static float fixedUnscaledDeltaTime { get { return Time.fixedUnscaledDeltaTime; } }

        public static bool inFixedTimeStep { get { return Time.inFixedTimeStep; } }

        public static float gameTime { get { return _gameTime; } }

        public static bool TimePassed(float t)
        {
            return (t < gameTime);
        }
    }
}
