

namespace RCPU
{
    public enum EnumUnityEvent
    {
        Update, FixedUpdate
    }

    public enum EnumGameOveralState
    {
        Menu, InGame, Pause, CutScene, GameOver, Debug
    }

    public enum EnumInGameCauciousState
    {
        None, Normal, Reflex, Alert
    }

    public enum EnumInGameUiState
    {
        None, HUD, WeaponSelection, Inventory, Scan
    }

    public enum EnumFamily
    {
        Player,
        World_01_Ai, World_02_Ai, World_03_Ai,
        World_01_Neutral, World_02_Neutral, World_03_Neutral,
        HackedEnemy, Buddy,
        None
    }
    public enum EnumRelation
    {
        Friend = -30, Neutral = 0, PotentialEnemy = 30, Enemy = 50
    }

    public enum EnumStaticIds
    {
        DataManager = 2, AgentPool = 4, Pool = 8, CommunicationCenter = 16,
        Vfx = 32,
        Spawn = 64,
        Decal = 128
    }

    public enum EnumNotifType
    {
        PlayerChangeWeapon,
        PlayerStateChanged,
        PlayerDied,
        GameStateChanged,
        PlayerEnterInteractableTrigger,
        PlayerExitInteractableTrigger,
        PressDoorButton,
        DoorStateHasChanged,
        NoiseCreated,
        StartReflexMode,
        PressCoverButton,
        SaveData,
        LoadData,
        DeleteData,
        AgentSpawned,
        AgentCreated,
        AgentDied,
        SetPlayer,
        //GetPool,   
        //GetVfx, 
        DayCycle,
        GetDecal,
        Recoil,
        GameInitializationDone,
        InGameStateChanged,
        WeaponSwitchByMenu,
        SetMenuEnable,
        SetInventoryEnable,
        SetWeaponSelectionEnable,
        InGameUiStateChanged,
        PlayerWeaponsUpdated,
        SetScanUiEnable,
        SpawnRequest,
        EventTrigger,
        SpawnCompleted,
    }

    public enum EnumInstanceNotifType
    {
        ReloadComplete,
        AwareStateChanged,
        OwnerDied,
        OwnerSpawned,
        DamageReceived,
        OwnerShocked,
        OwnerStateChanged,
        PlayerStateChanged,
        JumpStarted,
        JumpEnded,
        GetRadyForJump,
        ReadyForJump,
        DamageReceiverBroken,
        OwnerShockRestoredCompletly,
        OwnerShockRestoredInitialy,
        HealthAmountHasChanged,
        UpdateBulletUi
    }

    public enum EnumNotifParam
    {
        newState,
        currentWeapon,
        oldState,
        patrolPoints,
        doorIndex,
        floorIndex,
        pickup,
        levelIndex,
        doorState,
        message,
        ownerID,
        noisePower,
        position,
        owner,
        icon,
        agentUiUpdater,
        color,
        uiIndex,
        dataToSave,
        type,
        actionCallBack,
        agent,
        gameState,
        funcCheck,
        button,
        poolGroup,
        shakePower,
        curveData,
        target,
        interactType,
        dataModel,
        rotation,
        isTerrain,
        value,
        updateEvent,
        CameraState,
        AimState,
        Successful,
        //IkWeight,
        targetPosition,
        ChangeCameraState,
        //IkParts,
        IkState,
        health,
        healthCap,
        shockCap,
        shock,
        linkCap,
        link,
        velocity,
        slotIndex,
        Id,
        damageType,
        autoDestruct,
        level,
    }



    public enum EnumInitOrder
    {
        Logger,
        UiCamera,
        Input,
        Audio,
        Level,
        Notification,
        UiElement,
        GameState,
        GameFSM,
        Spawn,
        UiState,
        UiManager,
        Pool,
        PickupManager,
        Noise,
        Relation,
        Component,
        CameraShake,
        CameraState,
        CameraAim,
        CameraFSM,
        DamageReceiver,
        TriggerComponent,
        InvontoryComponent,
        Animator,
        Ragdoll,
        Vfx,
        MuzzleFlash,
        Weapon,
        WeaponManager,
        Leg,
        Locomotion,
        Detector,
        Patrol,
        AgentState,
        Ik,
        Player,
        Agent,
        Trigger,

    }

    public enum EnumPower { Linear, Quadratic, Cubic, Quadric }

    public enum EnumMaterialType
    {
        Wood, Concrete, Glass, Metal, Brick,
        Dirt, Plaster, Rock, Water, Foliage,
        Flesh, None
    }


}
