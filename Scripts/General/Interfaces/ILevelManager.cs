
using RCPU.Agent;
using RCPU.Damage;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU
{
    public interface ILevelManager : IInitializable
    {
        //Vector3 PlayerSpawnPosition { get; }

        IAlive GetAliveObject(int id);
        IAlive GetAliveObjectExcept(int id, IAlive owner);
        IDamageManager GetDamageManager(int id);
        IDamageManager GetDamageManagerExcept(int id, int ownerID);
        void RegisterAgent(int id, IAlive agent);
        void RegisterDamageManager(int id, IDamageManager agent);
        IEnumerable<IAlive> GetNearAgents(Vector3 center, float magnitude);

        //void UnRegisterAgent(int id, ITarget agent);
        //Dictionary<IDamageManager, int> GetAgentsInArea(Vector3 position, float radius, int layerMask, bool confirmByRaycast, int layerMaskConfirm);
        bool IsNearGrenade(Vector3 position);
        Dictionary<IDamageManager, int> GetAgentsInArea(Vector3 position, float radius);
        Dictionary<IDamageManager, int> GetAgentsInAreaRaycast(Vector3 position, float radius, int layerMaskConfirm);
    }
}