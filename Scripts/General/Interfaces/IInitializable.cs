

namespace RCPU
{
    public interface IInitializable
    {
        int Id { get; }
        void Init();
    }
}