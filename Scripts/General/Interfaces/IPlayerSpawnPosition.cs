﻿
using UnityEngine;

namespace RCPU
{
    public interface IPlayerSpawnPosition
    {
        Vector3 Position { get; }
    }
}
