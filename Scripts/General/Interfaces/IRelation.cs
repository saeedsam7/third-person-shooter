
using RCPU.Agent;
using RCPU.Damage;

namespace RCPU
{
    public interface IRelation : IInitializable
    {
        EnumRelation GetRelation(EnumFamily family, EnumFamily targetFamily);

        bool IsEnemy(EnumRelation enumRelation);

        bool IsEnemy(IAlive agent, IAlive target);
        bool IsEnemy(IAlive agent, IDamageManager target);

        bool IsFriend(EnumRelation enumRelation);

        bool IsFriend(IAlive agent, IAlive target);
    }
}