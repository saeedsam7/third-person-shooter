﻿using System;

namespace RCPU.Models
{
    [Serializable]
    public struct RangeFloat
    {
        public float min;
        public float max;

        public RangeFloat(float min, float max)
        {
            this.min = min;
            this.max = max;
        }
    }

    [Serializable]
    public struct RangeInteger
    {
        public int min;
        public int max;

        public RangeInteger(int min, int max)
        {
            this.min = min;
            this.max = max;
        }
    }
}
