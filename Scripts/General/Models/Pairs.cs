﻿using System;
using System.Collections.Generic;

namespace RCPU
{
    [Serializable]
    public class Pair<KEY, VALUE>
    {
        public KEY key;
        public VALUE value;

        public Pair() { }
        public Pair(KEY key, VALUE value) { this.key = key; this.value = value; }
    }

    [Serializable]
    public class PairArray<KEY, VALUE>
    {
        public Pair<KEY, VALUE>[] data;
        public Dictionary<KEY, VALUE> ToDictionary()
        {
            Dictionary<KEY, VALUE> keyValuePairs = new Dictionary<KEY, VALUE>();
            for (int i = 0; i < data.Length; i++)
            {
                keyValuePairs.Add(data[i].key, data[i].value);
            }
            return keyValuePairs;
        }
    }

    [Serializable]
    public class PairMap<KEY, VALUE>
    {
        public Pair<KEY, VALUE>[] data;
        Dictionary<KEY, VALUE> forwardMap = new Dictionary<KEY, VALUE>();
        Dictionary<VALUE, KEY> backwardMap = new Dictionary<VALUE, KEY>();
        public KEY Get(VALUE val)
        {
            return backwardMap[val];
        }
        public VALUE Get(KEY val)
        {
            return forwardMap[val];
        }

        public void Refresh()
        {
            for (int i = 0; i < data.Length; i++)
            {
                forwardMap.Add(data[i].key, data[i].value);
                backwardMap.Add(data[i].value, data[i].key);
            }
        }
    }
}