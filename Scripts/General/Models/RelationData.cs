﻿

using RCPU.Agent;

namespace RCPU
{
    public class RelationData : MDBObject
    {
        public float[,] table;

        public static bool IsEnemy(EnumRelation enumRelation)
        {
            return (enumRelation == EnumRelation.Enemy || enumRelation == EnumRelation.PotentialEnemy);
        }

        public static bool IsEnemy(IAlive agent, IAlive target)
        {
            return IsEnemy(agent.GetRelation(target.Family));
        }

        public static bool IsFriend(EnumRelation enumRelation)
        {
            return (enumRelation == EnumRelation.Friend || enumRelation == EnumRelation.Neutral);
        }

        public static bool IsFriend(IAlive agent, IAlive target)
        {
            return IsFriend(agent.GetRelation(target.Family));
        }
    }
}
