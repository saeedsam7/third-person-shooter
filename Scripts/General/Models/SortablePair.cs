﻿using RCPU.Helper;
using System;

namespace RCPU
{
    public class SortablePair<T> : IComparable<SortablePair<T>>
    {
        public float key;
        public T value;

        public SortablePair(float key, T value)
        {
            this.key = key;
            this.value = value;
        }

        public int CompareTo(SortablePair<T> other)
        {
            if (CommonHelper.IsNull(other)) return 1;
            if (key > other.key)
                return 1;
            if (key < other.key)
                return -1;
            else
                return 0;
        }
    }
}

