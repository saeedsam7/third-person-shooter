﻿using System;
using UnityEngine;

namespace RCPU
{
    [Serializable]
    public class MaterialInstance
    {
        public Renderer renderer;
        public int materialIndex;

        public Material GetMaterial()
        {
            return renderer.materials[materialIndex];
        }
    }
}