﻿
using System;
using UnityEngine;

namespace RCPU
{
    public interface IMaterialModifier
    {
        void Init(Material[] mat);
        void Apply();
    }

    [Serializable]
    public class MaterialModifier : IMaterialModifier
    {
        public string properyName;
        protected int properyID;
        protected Material[] mat;
        public int ProperyID
        {
            get
            {
                if (properyID == 0)
                    properyID = Shader.PropertyToID(properyName);
                return properyID;
            }
        }

        public virtual void Apply()
        {
        }

        public virtual void Init(Material[] mat)
        {
            this.mat = mat;
        }
    }

    [Serializable]
    public class MaterialColorModifier : MaterialModifier
    {
        public Color color;

        public override void Apply()
        {
            for (int i = 0; i < mat.Length; i++)
            {
                mat[i].SetColor(ProperyID, color);
            }
        }
    }

    [Serializable]
    public class MaterialHdrColorModifier : MaterialModifier
    {
        [ColorUsage(true, true)]
        public Color color;

        public override void Apply()
        {
            for (int i = 0; i < mat.Length; i++)
            {
                mat[i].SetColor(ProperyID, color);
            }
        }
    }

    [Serializable]
    public class MaterialTextureOffsetModifier : MaterialModifier
    {
        public Vector2 speed;
        Vector2 scroll;
        public override void Apply()
        {
            scroll += speed * Time.deltaTime;
            for (int i = 0; i < mat.Length; i++)
            {
                mat[i].SetTextureOffset(ProperyID, scroll);
            }
        }

        public override void Init(Material[] mat)
        {
            base.Init(mat);
            for (int i = 0; i < mat.Length; i++)
            {
                scroll = mat[i].GetTextureOffset(ProperyID);
            }
        }
    }

    public interface IMaterialModifierCollection
    {
        void Apply();
        void Init(Material[] mat);
    }

    [Serializable]
    public class MaterialModifierCollection : IMaterialModifierCollection
    {
        [SerializeField] protected MaterialColorModifier[] colorModifiers;
        [SerializeField] protected MaterialHdrColorModifier[] hdrColorModifiers;
        [SerializeField] protected MaterialTextureOffsetModifier[] textureOffsetModifier;

        public void Apply()
        {
            for (int i = 0; i < colorModifiers.Length; i++)
            {
                colorModifiers[i].Apply();
            }
            for (int i = 0; i < hdrColorModifiers.Length; i++)
            {
                hdrColorModifiers[i].Apply();
            }
            for (int i = 0; i < textureOffsetModifier.Length; i++)
            {
                textureOffsetModifier[i].Apply();
            }
        }

        public void Init(Material[] mat)
        {
           
                for (int i = 0; i < colorModifiers.Length; i++)
                {
                    colorModifiers[i].Init(mat );
                }
                for (int i = 0; i < hdrColorModifiers.Length; i++)
                {
                    hdrColorModifiers[i].Init(mat );
                }
                for (int i = 0; i < textureOffsetModifier.Length; i++)
                {
                    textureOffsetModifier[i].Init(mat );
                } 

        }
    }
}
