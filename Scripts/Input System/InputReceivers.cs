﻿
using System;
namespace RCPU.InputSystem
{
    [Serializable]
    public class InputReceiver
    {
        public EnumInputPriority priority;
        public EnumUnityEvent unityEvent;
        public EnumInGameUiState inGameUIState;
        public Func<bool> activeCheck;
        public InputReceiver(EnumInputPriority priority, EnumUnityEvent unityEvent, EnumInGameUiState inGameUIState, Func<bool> activeCheck)
        {
            this.priority = priority;
            this.unityEvent = unityEvent;
            this.inGameUIState = inGameUIState;
            this.activeCheck = activeCheck;
        }
    }
    [Serializable]
    public class InputReceiverAxis : InputReceiver
    {
        public Action<float[], bool[]> act;
        public InputReceiverAxis(Action<float[], bool[]> act, EnumInputPriority priority, EnumUnityEvent unityEvent, EnumInGameUiState inGameUIState, Func<bool> activeCheck) : base(priority, unityEvent, inGameUIState, activeCheck)
        {
            this.act = act;
        }
    }
    [Serializable]
    public class InputReceiverButton : InputReceiver
    {
        public Action<bool[]> act;
        public InputReceiverButton(Action<bool[]> act, EnumInputPriority priority, EnumUnityEvent unityEvent, EnumInGameUiState inGameUIState, Func<bool> activeCheck) : base(priority, unityEvent, inGameUIState, activeCheck)
        {
            this.act = act;
        }
    }
    [Serializable]
    public class InputReceiverButtonDown : InputReceiver
    {
        public Action act;
        public InputReceiverButtonDown(Action act, EnumInputPriority priority, EnumUnityEvent unityEvent, EnumInGameUiState inGameUIState, Func<bool> activeCheck) : base(priority, unityEvent, inGameUIState, activeCheck)
        {
            this.act = act;
        }
    }
    [Serializable]
    public class InputReceiverButtonUp : InputReceiver
    {
        public Action<float> act;
        public InputReceiverButtonUp(Action<float> act, EnumInputPriority priority, EnumUnityEvent unityEvent, EnumInGameUiState inGameUIState, Func<bool> activeCheck) : base(priority, unityEvent, inGameUIState, activeCheck)
        {
            this.act = act;
        }
    }
    [Serializable]
    public class InputReceiverButtonHold : InputReceiver
    {
        public Action<float> act;
        public InputReceiverButtonHold(Action<float> act, EnumInputPriority priority, EnumUnityEvent unityEvent, EnumInGameUiState inGameUIState, Func<bool> activeCheck) : base(priority, unityEvent, inGameUIState, activeCheck)
        {
            this.act = act;
        }
    }

}