﻿
using RCPU.Attributes;
using RCPU.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace RCPU.InputSystem
{
    [InitOrder(EnumInitOrder.Input)]
    public abstract class InputControllerBase : BaseObject, IInitializable, IInput
    {
        protected GameStateInputs[] gameStateInputs;
        protected int axisCount;
        protected int buttonCount;
        protected float[] buttonsDownTime;


        public virtual void Init()
        {
            gameStateInputs = new GameStateInputs[10];
            for (int i = 0; i < gameStateInputs.Length; i++)
            {
                gameStateInputs[i] = new GameStateInputs();
            }
            axisCount = CommonHelper.GetEnumLength(typeof(EnumAxis));
            buttonCount = CommonHelper.GetEnumLength(typeof(EnumButton));
            buttonsDownTime = new float[buttonCount];

        }


        public abstract float ReadAxis(int axis);
        public abstract bool ReadButton(int button);
        public abstract bool ReadButtonDown(int button);
        public abstract bool ReadButtonUp(int button);


        //the activeCheck parameter should be after the act parameter to let it generate 
        public virtual void RegisterInputButtonDown(EnumInputPriority priority, EnumUnityEvent unityEvent, EnumInGameUiState inGameUIState, EnumGameOveralState gameState, EnumButton btn, Action act, Func<bool> activeCheck)
        {
            if (!gameStateInputs[(int)gameState].inputButtonDownAction.ContainsKey((int)btn))
                gameStateInputs[(int)gameState].inputButtonDownAction.Add((int)btn, new List<InputReceiverButtonDown>());
            gameStateInputs[(int)gameState].inputButtonDownAction[(int)btn].Add(new InputReceiverButtonDown(act, priority, unityEvent, inGameUIState, activeCheck));
        }
        //the activeCheck parameter should be after the act parameter to let it generate 
        public virtual void RegisterInputButtonUp(EnumInputPriority priority, EnumUnityEvent unityEvent, EnumInGameUiState inGameUIState, EnumGameOveralState gameState, EnumButton btn, Action<float> act, Func<bool> activeCheck)
        {
            if (!gameStateInputs[(int)gameState].inputButtonUpAction.ContainsKey((int)btn))
                gameStateInputs[(int)gameState].inputButtonUpAction.Add((int)btn, new List<InputReceiverButtonUp>());
            gameStateInputs[(int)gameState].inputButtonUpAction[(int)btn].Add(new InputReceiverButtonUp(act, priority, unityEvent, inGameUIState, activeCheck));
        }
        //the activeCheck parameter should be after the act parameter to let it generate 
        public virtual void RegisterInputButtonHold(EnumInputPriority priority, EnumUnityEvent unityEvent, EnumInGameUiState inGameUIState, EnumGameOveralState gameState, EnumButton btn, Action<float> act, Func<bool> activeCheck)
        {
            if (!gameStateInputs[(int)gameState].inputButtonHoldAction.ContainsKey((int)btn))
                gameStateInputs[(int)gameState].inputButtonHoldAction.Add((int)btn, new List<InputReceiverButtonHold>());
            gameStateInputs[(int)gameState].inputButtonHoldAction[(int)btn].Add(new InputReceiverButtonHold(act, priority, unityEvent, inGameUIState, activeCheck));
        }

        public virtual void RegisterInputButton(EnumInputPriority priority, Func<bool> activeCheck, EnumUnityEvent unityEvent, EnumInGameUiState inGameUIState, EnumGameOveralState gameState, Action<bool[]> act)
        {
            gameStateInputs[(int)gameState].inputButton.Add(new InputReceiverButton(act, priority, unityEvent, inGameUIState, activeCheck));
        }

        //the activeCheck parameter should be after the act parameter to let it generate 
        public virtual void RegisterInputAxis(EnumInputPriority priority, EnumUnityEvent unityEvent, EnumInGameUiState inGameUIState, EnumGameOveralState gameState, Action<float[], bool[]> act, Func<bool> activeCheck)
        {
            //Logger.Error(" RegisterInputAxis " + activeCheck + " action " + act);
            gameStateInputs[(int)gameState].inputAxis.Add(new InputReceiverAxis(act, priority, unityEvent, inGameUIState, activeCheck));

        }

        public void SortAllInputByPriority()
        {
            for (int i = 0; i < gameStateInputs.Length; i++)
            {
                var state = gameStateInputs[i];
                state.inputAxis = state.inputAxis.OrderBy(x => x.priority).ToList();
                state.inputButton = state.inputButton.OrderBy(x => x.priority).ToList();

                var keys = state.inputButtonHoldAction.Keys.ToList();
                for (int j = 0; j < keys.Count; j++)
                {
                    gameStateInputs[i].inputButtonHoldAction[keys[j]] = state.inputButtonHoldAction[keys[j]].OrderBy(x => x.priority).ToList();
                }
                keys = state.inputButtonUpAction.Keys.ToList();
                for (int j = 0; j < keys.Count; j++)
                {
                    gameStateInputs[i].inputButtonUpAction[keys[j]] = state.inputButtonUpAction[keys[j]].OrderBy(x => x.priority).ToList();
                }
                keys = state.inputButtonDownAction.Keys.ToList();
                for (int j = 0; j < keys.Count; j++)
                {
                    gameStateInputs[i].inputButtonDownAction[keys[j]] = state.inputButtonDownAction[keys[j]].OrderBy(x => x.priority).ToList();
                }
                keys = state.inputAxisHoldAction.Keys.ToList();
                for (int j = 0; j < keys.Count; j++)
                {
                    gameStateInputs[i].inputAxisHoldAction[keys[j]] = state.inputAxisHoldAction[keys[j]].OrderBy(x => x.priority).ToList();
                }

            }
        }

        public virtual float GetAxis(EnumAxis axis)
        {
            throw new NotImplementedException();
        }

        public virtual bool GetButton(EnumButton button)
        {
            throw new NotImplementedException();
        }
        public virtual float GetButtonHold(EnumButton index)
        {
            throw new NotImplementedException();
        }

        public virtual void RegisterInputs(object listener)
        {
            Type monoType = listener.GetType();
            var objectMethods = monoType.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            for (int i = 0; i < objectMethods.Length; i++)
            {
                var attribute1 = objectMethods[i].GetCustomAttribute(typeof(InputRegisterButtonDownAttribute)) as InputRegisterButtonDownAttribute;
                if (!CommonHelper.IsNull(attribute1))
                {
                    for (int iindex = 0; iindex < attribute1.gameState.Length; iindex++)
                        RegisterInputButtonDown(attribute1.priority, attribute1.unityEvent, attribute1.inGameUIState, attribute1.gameState[iindex], attribute1.btn, attribute1.GetDelegate(listener, objectMethods[i]), attribute1.checkFunction);
                }
                else
                {
                    var attribute2 = objectMethods[i].GetCustomAttribute(typeof(InputRegisterButtonUpAttribute)) as InputRegisterButtonUpAttribute;
                    if (!CommonHelper.IsNull(attribute2))
                    {
                        for (int iindex = 0; iindex < attribute2.gameState.Length; iindex++)
                            RegisterInputButtonUp(attribute2.priority, attribute2.unityEvent, attribute2.inGameUIState, attribute2.gameState[iindex], attribute2.btn, attribute2.GetDelegate<float>(listener, objectMethods[i]), attribute2.checkFunction);
                    }
                    else
                    {
                        var attribute3 = objectMethods[i].GetCustomAttribute(typeof(InputRegisterAxisAttribute)) as InputRegisterAxisAttribute;
                        if (!CommonHelper.IsNull(attribute3))
                        {
                            for (int iindex = 0; iindex < attribute3.gameState.Length; iindex++)
                                RegisterInputAxis(attribute3.priority, attribute3.unityEvent, attribute3.inGameUIState, attribute3.gameState[iindex], attribute3.GetDelegate<float[], bool[]>(listener, objectMethods[i]), attribute3.checkFunction);
                        }
                        else
                        {
                            var attribute4 = objectMethods[i].GetCustomAttribute(typeof(InputRegisterButtonHoldAttribute)) as InputRegisterButtonHoldAttribute;
                            if (!CommonHelper.IsNull(attribute4))
                            {
                                for (int iindex = 0; iindex < attribute4.gameState.Length; iindex++)
                                    RegisterInputButtonHold(attribute4.priority, attribute4.unityEvent, attribute4.inGameUIState, attribute4.gameState[iindex], attribute4.btn, attribute4.GetDelegate<float>(listener, objectMethods[i]), attribute4.checkFunction);
                            }
                        }
                    }
                }
            }
        }
    }





}