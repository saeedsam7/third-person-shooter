﻿namespace RCPU.InputSystem
{
    public interface IInput
    {
        void RegisterInputs(object listener);
        float GetAxis(EnumAxis axis);
        bool GetButton(EnumButton button);
        float GetButtonHold(EnumButton index);
        void SortAllInputByPriority();
        //float GetButtonHold(EnumButton button);


        //public void RegisterInputButtonDown(FuncModel<bool> activeCheck, EnumUnityEvent unityEvent, EnumGameOveralState gameState, EnumButton btn, ActionModel act);

        //public void RegisterInputButtonUp(FuncModel<bool> activeCheck, EnumUnityEvent unityEvent, EnumGameOveralState gameState, EnumButton btn, ActionModel<float> act);

        //public void RegisterInputButtonHold(FuncModel<bool> activeCheck, EnumUnityEvent unityEvent, EnumGameOveralState gameState, EnumButton btn, ActionModel<float> act);

        //public void RegisterInputButton(FuncModel<bool> activeCheck, EnumUnityEvent unityEvent, EnumGameOveralState gameState, ActionModel<bool[]> act);
        //void RegisterInputAxis(FuncModel<bool> activeCheck, EnumUnityEvent unityEvent, EnumGameOveralState gameState, ActionModel<float[], bool[]> act);
    }
}
