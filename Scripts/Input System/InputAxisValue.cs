﻿using UnityEngine;

namespace RCPU.InputSystem
{
    public class InputAxisValue
    {
        public int[] allData;
        float min;

        float max;
        float step;
        int size;
        public InputAxisValue(float step, float min, float max)
        {
            this.min = min;
            this.max = max;
            this.step = step;
            float distance = (max - min);
            size = Mathf.CeilToInt(distance / step);
            allData = new int[size + 1];
            Reset();
        }
        public float Value { get { return GetValue(); } set { SetValue(value); } }
        void SetValue(float val)
        {
            for (int i = 0; i <= size; i++)
            {
                if (val <= min + (step * i))
                {
                    allData[i]++;
                    return;
                }
            }
        }

        float GetValue()
        {
            int indexResult = -1;
            int result = 0;
            for (int i = 0; i <= size; i++)
            {
                if (allData[i] >= result)
                {
                    result = allData[i];
                    indexResult = i;
                }
            }
            Reset();
            if (result > 0)
            {
                //indexResult += (indexResult >= (size / 2) - 1) ? 1 : 0;
                return min + (indexResult * step);
            }

            return min + ((max - min) / 2f);
        }

        public void Reset()
        {
            for (int i = 0; i <= size; i++)
            {
                allData[i] = 0;
            }
        }

        public override string ToString()
        {
            var s = "";
            for (int i = 0; i <= size; i++)
            {
                s += i + ":" + allData[i] + ", ";
            }
            return s;
        }
    }
}
