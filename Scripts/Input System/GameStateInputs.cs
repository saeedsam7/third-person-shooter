﻿using System;
using System.Collections.Generic;
namespace RCPU.InputSystem
{
    [Serializable]
    public class GameStateInputs
    {
        public Dictionary<int, List<InputReceiverButtonHold>> inputButtonHoldAction;
        public Dictionary<int, List<InputReceiverButtonUp>> inputButtonUpAction;
        public Dictionary<int, List<InputReceiverButtonDown>> inputButtonDownAction;
        public List<InputReceiverButton> inputButton;
        public List<InputReceiverAxis> inputAxis;
        public Dictionary<int, List<InputReceiverAxis>> inputAxisHoldAction;
        public GameStateInputs()
        {
            inputButton = new List<InputReceiverButton>();
            inputAxis = new List<InputReceiverAxis>();
            inputButtonDownAction = new Dictionary<int, List<InputReceiverButtonDown>>();
            inputButtonUpAction = new Dictionary<int, List<InputReceiverButtonUp>>();
            inputButtonHoldAction = new Dictionary<int, List<InputReceiverButtonHold>>();
            inputAxisHoldAction = new Dictionary<int, List<InputReceiverAxis>>();
        }
    }

}