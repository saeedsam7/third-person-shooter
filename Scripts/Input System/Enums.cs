namespace RCPU.InputSystem
{
    public enum EnumAxis { LeftHorizontal, LeftVertical, RightHorizontal, RightVertical, AimX, AimY }
    public enum EnumButton
    {
        Action, Aim, Shoot, Run, Inventory,
        Reload, Sprint, Slot1, Slot2, Slot3,
        Slot4, Slot5, Slot6, Slot7, Slot8,
        Crouch, Escape, Pickup, Cover, Jump,
        Up, Down, Left, Right, ChangeShoulder,
        Roll, WeaponMenu, UiSelect, Scan
    }

    public enum EnumWeaponSlotIndex { None, Up, UpRight, Right, DownRight, Down, DownLeft, Left, UpLeft }

    public enum EnumAttckType { None, Melee, Ranged };

    public enum EnumInputPriority
    {
        Roll,
        Move,
        Rotate,
        Shoot,
        ChangePos,
        Aim,
        Reload,
        Camera,
        SwitchWeapon,
        Escape,
        ChangeShoulder,
        UI,
        Pickup
    }
}