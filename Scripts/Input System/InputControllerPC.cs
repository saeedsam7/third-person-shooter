﻿
using RCPU.Attributes;
using RCPU.Game;
using System;
using UnityEngine;

namespace RCPU.InputSystem
{
    [InitOrder(EnumInitOrder.Input)]
    [InjectToSystem(typeof(IInput))]
    public class InputControllerPC : InputControllerBase, IInitializable
    {
        private bool[] btns;
        private float[] btnsHold;
        private float[] axis;
        private string[] btnsName;
        private string[] axisName;

        [InjectFromSystem] protected IGameFSM gameFSM { get; set; }
        public override void Init()
        {
            base.Init();
            btns = new bool[buttonCount];
            axis = new float[axisCount];
            btnsHold = new float[buttonCount];
            btnsName = new string[buttonCount];
            axisName = new string[axisCount];
            for (int i = 0; i < btnsName.Length; i++)
            {
                btnsName[i] = ((EnumButton)i).ToString();
            }
            for (int i = 0; i < axisName.Length; i++)
            {
                axisName[i] = ((EnumAxis)i).ToString();
            }
            //CNotification.Register(CNotification.NotifType.GameStateChanged, Id, GameStateChanged);
            //Logger.Error(" CComponent_Mono_Input_Pc "+ CComponent_Mono_Input_Base.intance);
        }

        public override float ReadAxis(int axis)
        {
            return Input.GetAxis(axisName[axis]);
        }

        public override bool ReadButton(int button)
        {
            return Input.GetButton(btnsName[button]);
        }

        public override bool ReadButtonDown(int button)
        {
            return Input.GetButtonDown(btnsName[button]);
        }

        public override bool ReadButtonUp(int button)
        {
            return Input.GetButtonUp(btnsName[button]);
        }

        int UpdateGameStateIndex()
        {
            return (int)gameFSM.State;
        }

        EnumInGameUiState UpdateinGameUIStateIndex()
        {
            return gameFSM.InGameUIState;
        }


        void Update()
        {
            UpdateInputs(EnumUnityEvent.Update);
        }

        void FixedUpdate()
        {
            UpdateInputs(EnumUnityEvent.FixedUpdate);
        }

        int gameStateIndex;
        EnumInGameUiState inGameUIStateIndex;
        void UpdateInputs(EnumUnityEvent unityEvent)
        {
            UpdateState();


            for (int i = 0; i < buttonCount; i++)
            {
                btns[i] = ReadButton(i);
                if (btns[i])
                    btnsHold[i] += TimeController.deltaTime;
                else
                    btnsHold[i] = 0;
            }
            //Debug.Log(" input Button  CInput_Base.Button.Aim" + btns[(int)CInput_Base.Button.Aim]);
            for (int i = 0; i < buttonCount; i++)
            {
                if (ReadButtonDown(i))
                {
                    if (gameStateInputs[gameStateIndex].inputButtonDownAction.ContainsKey(i))
                        foreach (var item in gameStateInputs[gameStateIndex].inputButtonDownAction[i])
                        {
                            if (CheckAndExecute(item, unityEvent, item.act))
                                break;
                        }
                    buttonsDownTime[i] = TimeController.gameTime;
                }
                if (ReadButtonUp(i))
                {
                    if (gameStateInputs[gameStateIndex].inputButtonUpAction.ContainsKey(i))
                        foreach (var item in gameStateInputs[gameStateIndex].inputButtonUpAction[i])
                        {
                            if (CheckAndExecute(item, unityEvent, item.act, buttonsDownTime[i]))
                                break;
                        }
                    buttonsDownTime[i] = 0;
                }
                if (gameStateInputs[gameStateIndex].inputButtonHoldAction.ContainsKey(i))
                    foreach (var item in gameStateInputs[gameStateIndex].inputButtonHoldAction[i])
                    {
                        if (CheckAndExecute(item, unityEvent, item.act, btnsHold[i]))
                            break;
                    }
            }
            for (int i = 0; i < axisCount; i++)
            {
                axis[i] = ReadAxis(i);
            }
            foreach (var item in gameStateInputs[gameStateIndex].inputAxis)
            {
                CheckAndExecute(item, unityEvent, item.act, axis, btns);
            }
            foreach (var item in gameStateInputs[gameStateIndex].inputButton)
            {
                CheckAndExecute(item, unityEvent, item.act, btns);
            }
        }
        protected void UpdateState()
        {
            gameStateIndex = UpdateGameStateIndex();
            inGameUIStateIndex = UpdateinGameUIStateIndex();
        }
        protected bool Check(InputReceiver item, EnumUnityEvent unityEvent)
        {
            return (item.unityEvent == unityEvent && (item.inGameUIState == EnumInGameUiState.None || item.inGameUIState == inGameUIStateIndex) && item.activeCheck());
        }
        protected bool CheckAndExecute(InputReceiver item, EnumUnityEvent unityEvent, Action action)
        {
            if (Check(item, unityEvent))
            {
                action();
                UpdateState();
                return true;
            }
            return false;
        }
        protected bool CheckAndExecute<T>(InputReceiver item, EnumUnityEvent unityEvent, Action<T> action, T param)
        {
            if (Check(item, unityEvent))
            {
                action(param);
                UpdateState();
                return true;
            }
            return false;
        }
        protected bool CheckAndExecute<T, K>(InputReceiver item, EnumUnityEvent unityEvent, Action<T, K> action, T paramT, K paramK)
        {
            if (Check(item, unityEvent))
            {
                action(paramT, paramK);
                UpdateState();
                return true;
            }
            return false;
        }
        public override float GetAxis(EnumAxis index)
        {
            return axis[(int)index];
        }

        public override bool GetButton(EnumButton index)
        {
            return btns[(int)index];
        }

        public override float GetButtonHold(EnumButton index)
        {
            return btnsHold[(int)index];
        }


    }
}