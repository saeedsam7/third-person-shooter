
namespace RCPU.Vehicle
{
    public interface IVehicle : IInitializable
    {
        void GetOff();
    }
}