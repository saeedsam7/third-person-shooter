using RCPU.Attributes;
using RCPU.Audio;
using RCPU.Helper;
using RCPU.NotificationSystem;
using UnityEngine;

namespace RCPU.Shell
{
    [RequireComponent(typeof(Rigidbody))]
    public class ShellController : BaseObject, IShell
    {
        [InjectFromSystem] protected IAudioManager AudioManager { set; get; }

        [SerializeField] protected float physicTime;
        [SerializeField] protected float renderTime;
        [SerializeField] protected EnumAudioSFX sfxBounce;
        [SerializeField] protected Rigidbody rigid;
        [SerializeField] protected Vector3 velocityMin;
        [SerializeField] protected Vector3 velocityMax;
        public bool IsReady => !isAlive;
        protected bool isAlive;
        protected float renderLife;
        protected float physicLife;
        public virtual void Init(int index)
        {
            name += " " + index;
            SetEnable(false);
        }

        protected virtual void SetEnable(bool val)
        {
            //Debug.Log(name + " SetEnable " + val);
            gameObject.SetActive(val);
            isAlive = val;
            rigid.isKinematic = !val;
            if (val)
            {
                rigid.velocity = Vector3.zero;
                renderLife = renderTime <= 0 ? Mathf.Infinity : TimeController.gameTime + renderTime;
                physicLife = physicTime <= 0 ? Mathf.Infinity : TimeController.gameTime + physicTime;
            }
        }

        protected virtual void Activate(Vector3 pos, Quaternion rot)
        {
            transform.position = pos;
            transform.rotation = rot;
            SetEnable(true);
            rigid.AddRelativeForce(Vector3.Lerp(velocityMin, velocityMax, RandomHelper.Range(1f)));
            //SetEnable(true);

        }


        private void Update()
        {
            if (physicLife < TimeController.gameTime)
                rigid.isKinematic = true;
            if (renderLife < TimeController.gameTime)
                Die();
        }

        private void Die()
        {
            SetEnable(false);
        }

        public void Activate(NotificationParameter parameters)
        {
            Activate(parameters.Get<Vector3>(EnumNotifParam.position), parameters.Get<Quaternion>(EnumNotifParam.rotation));
        }

        private void OnCollisionEnter(Collision collision)
        {
            AudioManager.PlaySfx(sfxBounce, transform.position);
        }
    }
}