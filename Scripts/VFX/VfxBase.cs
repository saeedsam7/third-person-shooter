﻿using RCPU.Attributes;
using RCPU.NotificationSystem;
using RCPU.VFX;
using UnityEngine;

namespace RCPU.Vfx
{
    [InitOrder(EnumInitOrder.Vfx)]
    public class VfxBase : BaseObject, IVfx
    {
        [SerializeField] float LifeTime;
        public bool IsReady => !isAlive;
        protected bool isAlive;
        protected float life;
        public virtual void Init(int index)
        {
            name += " " + index;
            SetEnable(false);
        }

        protected virtual void SetEnable(bool val)
        {
            //Debug.Log(name + " SetEnable " + val);
            gameObject.SetActive(val);
            isAlive = val;
            if (val)
                life = TimeController.gameTime + LifeTime;
        }

        protected virtual void Activate(Vector3 pos, Quaternion rot)
        {
            transform.position = pos;
            transform.rotation = rot;
            //SetEnable(true);
            SetEnable(true);
        }


        private void Update()
        {
            if (life < TimeController.gameTime)
                Die();
        }

        private void Die()
        {
            SetEnable(false);
        }

        public void Activate(NotificationParameter parameters)
        {
            Activate(parameters.Get<Vector3>(EnumNotifParam.position), parameters.Get<Quaternion>(EnumNotifParam.rotation));
        }
    }
}
