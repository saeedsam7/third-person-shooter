
using RCPU.Attributes;
using RCPU.VFX;
using UnityEngine;

namespace RCPU.Vfx
{
    //[InjectToInstance(typeof(IMuzzleFlash))]
    [InitOrder(EnumInitOrder.MuzzleFlash)]
    public class MuzzleFlash : BaseObject, IMuzzleFlash
    {
        [SerializeField] float LifeTime;
        protected float life;
        public void Activate()
        {
            SetEnable(true);
        }

        public void Init()
        {
            SetEnable(false);
        }


        protected void SetEnable(bool val)
        {
            gameObject.SetActive(val);
            if (val)
                life = TimeController.gameTime + LifeTime;
        }



        private void Update()
        {
            if (life < TimeController.gameTime)
                Die();
        }

        protected void Die()
        {
            SetEnable(false);
        }
    }
}
