﻿

namespace RCPU.VFX
{
    public interface IMuzzleFlash
    {
        void Activate();
    }
}
