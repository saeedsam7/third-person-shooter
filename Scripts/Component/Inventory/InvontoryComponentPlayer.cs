﻿using RCPU.NotificationSystem;
using RCPU.Pickup;
using RCPU.Weapon;

namespace RCPU.Inventory
{
    public class InvontoryComponentPlayer : InvontoryComponentBase
    {

        public override void RemoveAmmo(EnumAmmoType ammoType, int removeCount)
        {
            base.RemoveAmmo(ammoType, removeCount);
            Notification.NotifyInstance(OwnerId, EnumInstanceNotifType.UpdateBulletUi, new NotificationParameter());
        }

        protected override void Add(IPickupItemSetting setting, int count)
        {
            base.Add(setting, count);
            switch (setting.PickupGroup)
            {
                case EnumPickupGroup.Weapon:
                    break;
                case EnumPickupGroup.Ammo:
                    Notification.NotifyInstance(OwnerId, EnumInstanceNotifType.UpdateBulletUi, new NotificationParameter());
                    break;
                case EnumPickupGroup.Resource:
                    break;
            }
        }

    }
}