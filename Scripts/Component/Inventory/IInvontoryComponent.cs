﻿using RCPU.Weapon;

namespace RCPU.Inventory
{
    public interface IInvontoryComponent : IInitializable
    {
        int Add(IPickupItem pickupItem);
        int AmmoCount(EnumAmmoType weaponType);
        void RemoveAmmo(EnumAmmoType weaponType, int ammoInClip);
    }
}