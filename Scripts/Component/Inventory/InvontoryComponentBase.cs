﻿using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Component;
using RCPU.Pickup;
using RCPU.Weapon;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Inventory
{
    [InjectToInstance(typeof(IInvontoryComponent))]
    [InitOrder(EnumInitOrder.InvontoryComponent)]
    public class InvontoryComponentBase : ComponentBase, IInvontoryComponent
    {
        [InjectFromSystem] protected IPickupManager PickupManager { get; set; }

        [SerializeField] protected int capacityWidth;
        [SerializeField] protected int capacityHeight;

        protected int[,] inventorySpace;
        List<Pair<EnumPickupType, int>> inventoryItems;

        Dictionary<EnumAmmoType, int> ammo;
        Dictionary<EnumWeaponType, int> weapon;
        Dictionary<EnumResourceType, int> resource;

        [InjectFromInstance] protected IAgent owner { get; set; }
        protected override int OwnerId => owner.Id;

        public override void Init()
        {
            base.Init();
            ammo = new Dictionary<EnumAmmoType, int>();
            weapon = new Dictionary<EnumWeaponType, int>();
            resource = new Dictionary<EnumResourceType, int>();

            inventoryItems = new List<Pair<EnumPickupType, int>>();
            inventorySpace = new int[capacityWidth, capacityHeight];
        }

        public int Add(IPickupItem pickupItem)
        {
            IPickupItemSetting setting = PickupManager.Get(pickupItem.PickupType);

            for (int i = 0; i < inventoryItems.Count; i++)
            {
                if (inventoryItems[i].key == pickupItem.PickupType && inventoryItems[i].value < setting.MaxCount)
                {
                    inventoryItems[i].value += pickupItem.Count;
                    if (inventoryItems[i].value > setting.MaxCount)
                    {
                        var extra = (inventoryItems[i].value - setting.MaxCount);
                        Add(setting, pickupItem.Count - (extra));
                        pickupItem.Count = extra;
                        inventoryItems[i].value = setting.MaxCount;
                    }
                    else
                    {
                        Add(setting, pickupItem.Count);
                        pickupItem.Count = 0;
                    }
                    if (pickupItem.Count == 0)
                        break;
                }
            }
            while (pickupItem.Count > 0)
            {
                if (!AddNew(pickupItem, setting))
                    break;
            }
            if (DEBUG)
                Print();
            return pickupItem.Count;
        }

        void Print()
        {
            string s = "capacity :  \n";
            for (int i = 0; i < capacityWidth; i++)
            {
                s += "[ ";
                for (int j = 0; j < capacityHeight; j++)
                {
                    s += inventorySpace[i, j] + " ,";
                }
                s += "] \n";
            }
            Debug.Log(s);
        }

        protected bool AddNew(IPickupItem pickupItem, IPickupItemSetting setting)
        {
            if (HasEmptySpace(setting.Width, setting.Height, out int x, out int y, true))
            {
                if (pickupItem.Count <= setting.MaxCount)
                {
                    Add(setting, pickupItem.Count);
                    inventoryItems.Add(new Pair<EnumPickupType, int>(pickupItem.PickupType, pickupItem.Count));
                    pickupItem.Count = 0;
                }
                else
                {
                    Add(setting, setting.MaxCount);
                    inventoryItems.Add(new Pair<EnumPickupType, int>(pickupItem.PickupType, setting.MaxCount));
                    pickupItem.Count -= setting.MaxCount;
                }
                for (int i = x; i < x + setting.Width; i++)
                {
                    for (int j = y; j < y + setting.Height; j++)
                    {
                        inventorySpace[i, j] = inventoryItems.Count;
                    }
                }
                if (DEBUG)
                    Print();
                return true;
            }
            if (DEBUG)
                Print();
            return false;
        }



        protected bool HasEmptySpace(int width, int height, out int x, out int y, bool tryRotate)
        {
            for (int i = 0; i < capacityWidth; i++)
            {
                for (int j = 0; j < capacityHeight; j++)
                {
                    if (inventorySpace[i, j] == 0 && i + width <= capacityWidth && j + height <= capacityHeight)
                    {
                        if (HasEmptySpace(i, j, width, height))
                        {
                            x = i;
                            y = j;
                            return true;
                        }
                    }
                }
            }
            if (tryRotate)
            {
                return HasEmptySpace(height, width, out x, out y, false);
            }
            x = 0;
            y = 0;
            return false;
        }

        protected bool HasEmptySpace(int x, int y, int width, int height)
        {
            for (int i = x; i < x + width; i++)
            {
                for (int j = y; j < y + height; j++)
                {
                    if (inventorySpace[i, j] != 0)
                        return false;
                }
            }
            return true;
        }

        protected virtual void Add(IPickupItemSetting setting, int count)
        {
            switch (setting.PickupGroup)
            {
                case EnumPickupGroup.Weapon:
                    var weapontype = PickupManager.GetWeapon(setting.PickupType);
                    if (!weapon.ContainsKey(weapontype))
                        weapon.Add(weapontype, 0);
                    weapon[weapontype] += count;
                    break;
                case EnumPickupGroup.Ammo:
                    var ammoType = PickupManager.GetAmmo(setting.PickupType);
                    if (!ammo.ContainsKey(ammoType))
                        ammo.Add(ammoType, 0);
                    ammo[ammoType] += count;
                    break;
                case EnumPickupGroup.Resource:
                    var resourceType = PickupManager.GetResource(setting.PickupType);
                    if (!resource.ContainsKey(resourceType))
                        resource.Add(resourceType, 0);
                    resource[resourceType] += count;
                    break;
            }
        }

        public int AmmoCount(EnumAmmoType ammoType)
        {
            if (!ammo.ContainsKey(ammoType))
                return 0;
            return ammo[ammoType];
        }
        public void RemoveItem(EnumPickupType pickupType, int removeCount)
        {
            List<Pair<int, int>> temp = new List<Pair<int, int>>();
            for (int i = 0; i < inventoryItems.Count; i++)
            {
                if (inventoryItems[i].key == pickupType)
                {
                    temp.Add(new Pair<int, int>(i, inventoryItems[i].value));
                }
            }

            while (removeCount > 0)
            {
                int smaller = 100000;
                int smallerIndex = -1;
                for (int i = 0; i < temp.Count; i++)
                {
                    if (temp[i].value < smaller)
                    {
                        smaller = temp[i].value;
                        smallerIndex = temp[i].key;
                    }
                }
                if (inventoryItems[smallerIndex].value > removeCount)
                {
                    inventoryItems[smallerIndex].value -= removeCount;
                    removeCount = 0;
                }
                else
                {
                    removeCount -= inventoryItems[smallerIndex].value;
                    inventoryItems[smallerIndex].value = 0;
                    for (int i = 0; i < capacityWidth; i++)
                    {
                        for (int j = 0; j < capacityHeight; i++)
                        {
                            if (inventorySpace[i, j] == smallerIndex)
                            {
                                inventorySpace[i, j] = 0;
                            }
                        }
                    }
                }
            }
        }
        public virtual void RemoveAmmo(EnumAmmoType ammoType, int removeCount)
        {
            ammo[ammoType] -= removeCount;
            var pickupType = PickupManager.GetPickupType(ammoType);
            RemoveItem(pickupType, removeCount);
        }
    }
}