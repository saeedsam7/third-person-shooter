﻿

using RCPU.Agent;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Component
{
    public interface IDetectedTarget
    {
        int Id { get; }
        float Visibility { get; }
        bool IsAlive { get; }
        IAlive Target { get; }
        float LastSeen { get; }
        Vector3 LastPosition { get; }
        Quaternion LastRotation { get; }
        bool TargetLastPositionVisited { get; set; }
        Vector3 InvestigationPosition { get; set; }
        bool ReachInvestigationPosition { get; set; }
        float InvestigationTimer { get; set; }
        List<SortablePair<Vector3>> Ring { get; set; }
        bool MustUpdateRing { get; }
        bool HasRing { get; }
        bool RingFailed { get; }
        bool Lost { get; }

        void NotSeen(float speed, float min);
        void Seen(float speed, float newVisibility);
        void Update(IDetectedTarget target, float increaseSpeed);
    }
}
