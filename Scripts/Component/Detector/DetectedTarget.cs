﻿
using RCPU.Agent;
using RCPU.Helper;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Component
{
    public class DetectedTarget : IDetectedTarget, IComparable<DetectedTarget>
    {
        public IAlive target;
        public float time;
        public int detectorId;

        public int Id => target.Id;
        public float Visibility { get; set; }

        public bool IsAlive => target.IsAlive;
        public IAlive Target => target;

        public float LastSeen { get { return TimeController.gameTime - time; } private set { time = value; } }
        public Vector3 LastPosition { get; set; }
        public Quaternion LastRotation { get; set; }
        public bool TargetLastPositionVisited { get; set; }
        public Vector3 InvestigationPosition { get; set; }
        public bool ReachInvestigationPosition
        {
            get => reachInvestigationPosition;
            set
            {
                reachInvestigationPosition = value;
                if (value)
                    InvestigationTimer = TimeController.gameTime;
            }
        }
        bool reachInvestigationPosition;
        public float InvestigationTimer { get => (TimeController.gameTime - investigationTimer); set { investigationTimer = value; } }

        public List<SortablePair<Vector3>> Ring { get => ring; set { ring = value; ringUpdateTime = TimeController.gameTime; ringUpdatePosition = LastPosition; } }
        List<SortablePair<Vector3>> ring;
        public bool MustUpdateRing => CommonHelper.Distance(ringUpdatePosition, LastPosition) > 5 || TimeController.gameTime - ringUpdateTime > 5f;

        public bool HasRing => ring.Count > 2;

        public bool RingFailed => !MustUpdateRing && !HasRing;

        public bool Lost => LastSeen > 6;

        float ringUpdateTime;
        Vector3 ringUpdatePosition;


        float investigationTimer;
        public DetectedTarget(IAlive target, float visibility, int detectorId)
        {
            this.target = target;
            this.Visibility = visibility;
            this.time = TimeController.gameTime;
            this.LastPosition = target.Position;
            this.LastRotation = target.Rotation;
            this.detectorId = detectorId;
            ring = new List<SortablePair<Vector3>>();
        }

        public void Seen(float speed, float newVisibility)
        {
            if (this.Visibility < GameSetting.MAX_VISIBILITY)
            {
                float increament = TimeController.GetNormilizeTime(speed);
                this.Visibility = Mathf.Clamp(this.Visibility + increament, 0, GameSetting.MAX_VISIBILITY);
            }

            LastSeen = TimeController.gameTime;
            LastPosition = target.Position;
            LastRotation = target.Rotation;
        }

        public void NotSeen(float speed, float min)
        {
            if (this.Visibility > min)
            {
                this.Visibility -= TimeController.GetNormilizeTime(speed);
                this.Visibility = Mathf.Clamp(this.Visibility, min, GameSetting.MAX_VISIBILITY);
            }
        }

        public void Update(IDetectedTarget obj, float increaseSpeed)
        {
            if (this.Visibility < obj.Visibility)
            {
                this.Visibility = obj.Visibility;
            }
            else
            {
                float increament = TimeController.GetNormilizeTime(increaseSpeed);
                this.Visibility = Mathf.Clamp(this.Visibility + increament, 0, GameSetting.MAX_VISIBILITY);
            }

            this.Visibility = Mathf.Clamp(this.Visibility, 0, GameSetting.MAX_VISIBILITY);
            LastSeen = TimeController.gameTime;
            LastPosition = target.Position;
            LastRotation = target.Rotation;
        }

        public int CompareTo(DetectedTarget obj)
        {
            if (CommonHelper.IsNull(obj)) return 1;
            if (Visibility > obj.Visibility)
                return 1;
            if (Visibility < obj.Visibility)
                return -1;
            else
                return 0;
        }
    }
}
