﻿using RCPU.Agent;
using UnityEngine;

namespace RCPU.Component
{
    public interface INoiseDetector : IDetector
    {
        Vector3 Position { get; }
        float Radius { get; }

        void NoiseDetected(Vector3 position, IAlive target);
    }
}
