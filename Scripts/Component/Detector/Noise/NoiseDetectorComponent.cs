﻿
using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Damage;
using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.Noise;
using RCPU.NotificationSystem;
using UnityEngine;

namespace RCPU.Component
{
    [InitOrder(EnumInitOrder.Detector)]
    [InjectToInstance(typeof(INoiseDetector))]
    public class NoiseDetectorComponent : DamageableComponent, INoiseDetector
    {
        NoiseDetectorDataModel model;
        [SerializeField] NoiseDetectorDataModel_SO model_SO;
        [InjectFromInstance] IAiPrivate Owner { get; set; }
        [InjectFromSystem] protected IGizmosHelper Gizmos { get; set; }
        [InjectFromSystem] protected ILevelManager levelManager { get; set; }
        [InjectFromSystem] protected INoiseManager noiseManager { get; set; }

        [InjectFromSystem] protected IRelation relation { get; set; }
        protected override int OwnerId => Owner.Id;
        public override EnumComponentType ComponentType => EnumComponentType.NoiseDetector;


        public Vector3 Position => transform.position;

        public float Radius
        {
            get
            {
                return model.radiusPerAwareness[(int)Owner.AwareState];
            }
        }
        float castRadius = .1f;
        public override void Init()
        {
            base.Init();
            model = DInjector.Instantiate<NoiseDetectorDataModel_SO>(model_SO).model;
            IsEnable = false;
        }


        protected override void OnOwnerDied(NotificationParameter obj)
        {
            IsEnable = false;
            noiseManager.UnregisterNoiseDetector(this);
        }
        protected override void OnOwnerSpawned(NotificationParameter obj)
        {
            IsEnable = true;
            noiseManager.RegisterNoiseDetector(this);
        }





        protected virtual float GetVisibility(IAlive target)
        {

            //CLog.Log(" GetVisibility " + (target.GetVisibility() * CDayCycle.instance.GetVisibility()));
            return target.Visibility /** dayCycle.GetVisibility()*/;
        }


        //DetectedTarget tempBestDetectedTarget;

        void Update()
        {

            DrawGizmos();
        }








#if UNITY_EDITOR

        //void SetGizmosColor(Color color, float alpha)
        //{
        //    GizmosHelper.SetHandlesColor(color, alpha);
        //}



        public void DrawGizmos(float distance, float angle, Color color, float alpha)
        {
            if (DEBUG)
            {
                //float size = distance / Mathf.Cos(angle * .5f * Mathf.Deg2Rad);
                float size1 = distance * Mathf.Cos(angle * Mathf.Deg2Rad);
                float size2 = distance * Mathf.Sin(angle * Mathf.Deg2Rad) * 2f;
                Gizmos.DrawCone(transform.position + (transform.forward * size1 * .5f), transform.rotation * Quaternion.Euler(-90, 0, 0), new Vector3(size2, size1, size2), CommonHelper.Color(color, alpha));
                //GizmosHelper.DrawSolidArc(transform.position, Vector3.up, owner.Forward, angle / -2f, distance);

                Gizmos.DrawSphere(transform.position + (transform.forward * size1), transform.rotation * Quaternion.Euler(-90, 0, 0), new Vector3(size2, size1, size2), CommonHelper.Color(color, alpha));

            }
        }
#endif
        public void DrawGizmos()
        {
#if UNITY_EDITOR
            Debug.DrawLine(transform.position, transform.position + (transform.forward * 3f), Color.red);
            if (Application.isPlaying)
            {
                Color color = (Owner.AwareState == EnumAwareState.Cautious ? Color.yellow : Owner.AwareState == EnumAwareState.Offensive ? Color.red : Color.green);

            }
#endif
        }

        public void NoiseDetected(Vector3 position, IAlive target)
        {
            float visibility = Radius / CommonHelper.Distance(position, Position);
            var detectedTarget = new DetectedTarget(target, visibility, Owner.Id);
            Owner.TargetDetect(detectedTarget, 1);
        }
    }
}