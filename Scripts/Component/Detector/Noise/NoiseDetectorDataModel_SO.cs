﻿

using UnityEngine;

namespace RCPU.Component
{
    [CreateAssetMenu(fileName = "NoiseDetector", menuName = "RCPU/DataModel/Component/Detector/NoiseDetector", order = 1)]
    public class NoiseDetectorDataModel_SO : ScriptableObject
    {
        public NoiseDetectorDataModel model;
    }
}