﻿

using RCPU.Damage;
using System;

namespace RCPU.Component
{
    [Serializable]
    public class NoiseDetectorDataModel : DamageableComponentDataModel
    {
        public int level;
        public float accuracy;
        public float[] radiusPerAwareness;
    }
}