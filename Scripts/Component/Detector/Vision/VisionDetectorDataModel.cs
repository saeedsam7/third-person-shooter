﻿
using RCPU.Damage;
using System;
using UnityEngine;

namespace RCPU.Component
{
    [Serializable]
    public class VisionDetectorDataModel : DamageableComponentDataModel
    {
        public int level;
        public float accuracy;
        public LayerMask WatchLayer;

        public DetectorDataPerAware[] dataPerAware;

        public DistanceAngle hitDistanceAngle;

        [HideInInspector] public float lastIteration;
    }


    [Serializable]
    public class DistanceAngle
    {
        public float MaxDistance;
        public float ClearDistance;
        public float CloseAngle;
        public float FarAngle;
        public float CloseDetectionChance;
        public float FarDetectionChance;
    }

    [Serializable]
    public class DetectorDataPerAware
    {
        public float iteration;
        public DistanceAngle distanceAngle;
        public MaterialModifierCollection materialModifier;
        public float increaseSpeed;
        public float decreaseSpeed;
        public float minPossible;
    }
}
