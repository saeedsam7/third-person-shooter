using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Damage;
using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.NotificationSystem;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Component
{
    [InitOrder(EnumInitOrder.Detector)]
    [InjectToInstance(typeof(IVisionDetector))]
    public class VisionDetectorComponent : DamageableComponent, IVisionDetector
    {
        VisionDetectorDataModel model;
        [SerializeField] VisionDetectorDataModel_SO model_SO;
        [InjectFromInstance] IAiPrivate Owner { get; set; }
        [InjectFromSystem] protected IGizmosHelper Gizmos { get; set; }
        [InjectFromSystem] protected ILevelManager levelManager { get; set; }
        [InjectFromSystem] protected IRelation relation { get; set; }
        [SerializeField] protected MaterialInstance[] visualiser;
        protected override int OwnerId => Owner.Id;
        public override EnumComponentType ComponentType => EnumComponentType.VisionDetector;
        protected DetectorDataPerAware Data
        {
            get
            {
                return model.dataPerAware[(int)Owner.AwareState];
            }
        }

        protected int materialPropertyID;
        protected Vector2 materialScroll;
        IMaterialModifierCollection materialModifierCollection;
        float castRadius = .1f;
        public override void Init()
        {
            base.Init();
            model = DInjector.Instantiate<VisionDetectorDataModel_SO>(model_SO).model;
            Notification.RegisterInstance(Owner.Id, EnumInstanceNotifType.AwareStateChanged, Id, AwareStateChanged);
            IsEnable = false;
        }


        protected override void OnOwnerDied(NotificationParameter obj)
        {
            IsEnable = false;
            for (int i = 0; i < visualiser.Length; i++)
            {
                visualiser[i].renderer.enabled = false;
            }
        }
        protected override void OnOwnerSpawned(NotificationParameter obj)
        {
            for (int i = 0; i < visualiser.Length; i++)
            {
                visualiser[i].renderer.enabled = true;
            }
            UpdateMaterial();
            IsEnable = true;
        }

        protected virtual void AwareStateChanged(NotificationParameter param)
        {
            UpdateMaterial();
        }

        void UpdateMaterial()
        {
            materialModifierCollection = Data.materialModifier;
            List<Material> materials = new List<Material>();
            for (int i = 0; i < visualiser.Length; i++)
            {
                materials.Add(visualiser[i].GetMaterial());
            }
            materialModifierCollection.Init(materials.ToArray());
        }

        private void Scan(IDetectedTarget detectedTarget, DistanceAngle distanceAngle)
        {
            Vector3 targetPos = detectedTarget.Target.Position;
            Vector3 myPos = transform.position;
            float targetDisance = CommonHelper.Distance(myPos, targetPos);
            bool detected = false;
            float startVisibility = detectedTarget.Visibility;

            if (targetDisance < distanceAngle.ClearDistance)
            {
                if (CommonHelper.Angle(transform, targetPos, true) < distanceAngle.CloseAngle)
                {
                    Ray ray = new Ray(myPos, targetPos - myPos);
                    if (DEBUG)
                    {
                        Debug.DrawRay(ray.origin, ray.direction, Color.red, 3);
                    }
                    if (!PhysicsHelper.SphereCast(ray, castRadius, targetDisance, model.WatchLayer))
                    {
                        if (DEBUG)
                        {
                            Debug.DrawLine(myPos + (Vector3.up * castRadius), targetPos, Color.cyan, 3);
                            Debug.DrawLine(myPos - (Vector3.up * castRadius), targetPos, Color.cyan, 3);
                            Debug.DrawLine(myPos + (Vector3.right * castRadius), targetPos, Color.cyan, 3);
                            Debug.DrawLine(myPos - (Vector3.right * castRadius), targetPos, Color.cyan, 3);
                        }
                        detectedTarget.Seen(Data.increaseSpeed, GetVisibility(detectedTarget.Target) * distanceAngle.CloseDetectionChance);
                        detected = true;
                    }
                }
            }
            else
            if (targetDisance < distanceAngle.MaxDistance)
            {
                if (CommonHelper.Angle(transform, targetPos, true) < distanceAngle.FarAngle)
                {
                    Ray ray = new Ray(myPos, myPos - targetPos);
                    if (DEBUG)
                    {
                        Debug.DrawRay(ray.origin, ray.direction, Color.red, 3);
                    }
                    if (!PhysicsHelper.SphereCast(ray, castRadius, targetDisance, model.WatchLayer))
                    {
                        if (DEBUG)
                        {
                            Debug.DrawLine(myPos + (Vector3.up * castRadius), targetPos, Color.cyan, 3);
                            Debug.DrawLine(myPos - (Vector3.up * castRadius), targetPos, Color.cyan, 3);
                            Debug.DrawLine(myPos + (Vector3.right * castRadius), targetPos, Color.cyan, 3);
                            Debug.DrawLine(myPos - (Vector3.right * castRadius), targetPos, Color.cyan, 3);
                        }
                        detectedTarget.Seen(Data.increaseSpeed, GetVisibility(detectedTarget.Target) * distanceAngle.FarDetectionChance);
                        detected = true;
                    }
                }
            }
            if (!detected)
            {
                detectedTarget.NotSeen(Data.decreaseSpeed, Data.minPossible);
            }
            else
            if ((detectedTarget.Visibility >= GameSetting.OFFENSIVE_VISIBILITY && startVisibility < GameSetting.OFFENSIVE_VISIBILITY) ||
                (detectedTarget.Visibility >= GameSetting.SUSPICIOUS_VISIBILITY && startVisibility < GameSetting.SUSPICIOUS_VISIBILITY))
            {
                Owner.TargetAwareStateChanged(detectedTarget);
            }
        }

        public void Scan(IDetectedTarget detectedTarget)
        {
            if (!IsEnable || ownerShocked || detectedTarget.LastSeen < Data.iteration)
                return;
            Scan(detectedTarget, Data.distanceAngle);
        }

        public bool HitScan(IAlive target)
        {
            if (!IsEnable || ownerShocked)
                return false;
            float targetDis = CommonHelper.Distance(transform.position, target.Position);
            if (targetDis < model.hitDistanceAngle.MaxDistance)
            {
                return InternalScan(target, model.hitDistanceAngle);
            }
            return false;
        }

        public bool Scan(IAlive target)
        {
            if (!IsEnable || ownerShocked)
                return false;
            float targetDis = CommonHelper.Distance(transform.position, target.Position);
            if (targetDis < Data.distanceAngle.ClearDistance)
            {
                return InternalScan(target, Data.distanceAngle);
            }
            else
             if (targetDis < Data.distanceAngle.MaxDistance)
            {
                return InternalScan(target, Data.distanceAngle);
            }
            return false;
        }

        protected virtual float GetVisibility(IAlive target)
        {

            //CLog.Log(" GetVisibility " + (target.GetVisibility() * CDayCycle.instance.GetVisibility()));
            return target.Visibility /** dayCycle.GetVisibility()*/;
        }


        //DetectedTarget tempBestDetectedTarget;

        void Update()
        {
            if ((model.lastIteration + Data.iteration) < TimeController.gameTime)
            {
                Scan();
            }
            DrawGizmos();
            materialModifierCollection.Apply();
            RenderVisionArea();
        }

        public void ForceScan()
        {
            if (!IsEnable || ownerShocked)
                return;
            Scan();
        }

        private bool InternalScan(IAlive target, DistanceAngle distanceAngle)
        {
            if (!target.IsAlive || relation.IsFriend(Owner, target))
                return false;

            Vector3 myPos = transform.position;
            Vector3 targetPos = target.Center;
            float targetDisance = CommonHelper.Distance(myPos, targetPos);
            if (targetDisance > distanceAngle.MaxDistance)
                return false;
            float acceptedAngle = distanceAngle.CloseAngle;

            if (targetDisance > distanceAngle.ClearDistance)
            {
                acceptedAngle = distanceAngle.CloseAngle - ((targetDisance - distanceAngle.ClearDistance) * (distanceAngle.CloseAngle - distanceAngle.FarAngle) / (distanceAngle.MaxDistance - distanceAngle.ClearDistance));
            }

            if (CommonHelper.Angle(transform, targetPos, true) < acceptedAngle)
            {
                if (DEBUG)
                {
                    Debug.DrawRay(myPos, targetPos - myPos, Color.red, 3);
                }
                bool hitResult = PhysicsHelper.Raycast(myPos, targetPos - myPos, out RaycastHit hit, CommonHelper.Distance(myPos, targetPos), PhysicsHelper.Layer_DamageReceiver_Environment);
                var hitTarget = levelManager.GetDamageManager(hit.colliderInstanceID);
                if (!hitResult || (!CommonHelper.IsNull(hitTarget) && (hitTarget.GetOwner().Id == OwnerId || hitTarget.GetOwner().Id == target.Id))) // Physics.Linecast(pos, tPos, WatchLayer))
                {
                    float visibility = GetVisibility(target) * (targetDisance <= distanceAngle.ClearDistance ? distanceAngle.CloseDetectionChance : distanceAngle.FarDetectionChance);
                    var detectedTarget = new DetectedTarget(target, visibility, Owner.Id);
                    //if (CommonHelper.IsNull(tempBestDetectedTarget) || tempBestDetectedTarget.Chance < detectedTarget.Chance)
                    //{
                    //    tempBestDetectedTarget = detectedTarget;
                    //}
                    if (DEBUG)
                    {
                        Debug.DrawLine(myPos + (Vector3.up * castRadius), hit.point, Color.cyan, 3);
                        Debug.DrawLine(myPos - (Vector3.up * castRadius), hit.point, Color.cyan, 3);
                        Debug.DrawLine(myPos + (Vector3.right * castRadius), hit.point, Color.cyan, 3);
                        Debug.DrawLine(myPos - (Vector3.right * castRadius), hit.point, Color.cyan, 3);
                        if (Owner.AwareState == EnumAwareState.Normal)
                            Debug.LogError(" Angle " + CommonHelper.Angle(transform, targetPos, true) + " Disance " + targetDisance);
                    }
                    Owner.TargetDetect(detectedTarget, Data.increaseSpeed);
                    return true;
                }
            }
            return false;
        }

        [SerializeField] protected LineRenderer[] visionRenderer;
        float visionRotation;
        private void RenderVisionArea(int start, int count, float radius, float offset, Color color)
        {
            for (int i = start; i < start + count; i++)
            {
                float angle = ((360f / count) * i) + offset;
                visionRenderer[i].material.color = color;
                visionRenderer[i].SetPosition(0, transform.position);

                Vector3 center = transform.position + (transform.forward * Data.distanceAngle.ClearDistance);
                float len = Mathf.Tan(Data.distanceAngle.CloseAngle * radius * Mathf.Deg2Rad) * Data.distanceAngle.ClearDistance;
                Vector3 closeTarget = center + (transform.up * Mathf.Cos(angle * Mathf.Deg2Rad) * len) + (transform.right * Mathf.Sin(angle * Mathf.Deg2Rad) * len);
                Vector3 farTarget = closeTarget + (transform.forward * (Data.distanceAngle.MaxDistance - Data.distanceAngle.ClearDistance));

                if (PhysicsHelper.Raycast(transform.position, farTarget - transform.position, out RaycastHit hitInfo, CommonHelper.Distance(transform.position, farTarget), PhysicsHelper.Layer_DamageReceiver_Environment))
                {
                    visionRenderer[i].SetPosition(1, hitInfo.point);
                }
                else
                {
                    visionRenderer[i].SetPosition(1, farTarget);
                }

            }
        }
        private void RenderVisionArea()
        {
            Color color = (Owner.AwareState == EnumAwareState.Cautious ? Color.yellow : Owner.AwareState == EnumAwareState.Offensive ? Color.red : Color.green);
            visionRotation += TimeController.deltaTime * 30;
            RenderVisionArea(0, 10, .5f, visionRotation, color);
            RenderVisionArea(10, 7, .3f, -visionRotation, color);
            RenderVisionArea(17, 4, .17f, visionRotation, color);


            //for (int i = 0; i < count; i++)
            //{
            //    float angle = (360f / count) * i;
            //    visionRenderer[i].material.color = color;
            //    visionRenderer[i].SetPosition(0, transform.position);

            //    Vector3 center = transform.position + (transform.forward * Data.distanceAngle.ClearDistance);
            //    float len = Mathf.Tan(Data.distanceAngle.CloseAngle * .5f * Mathf.Deg2Rad) * Data.distanceAngle.ClearDistance;
            //    Vector3 closeTarget = center + (transform.up * Mathf.Cos(angle * Mathf.Deg2Rad) * len) + (transform.right * Mathf.Sin(angle * Mathf.Deg2Rad) * len);
            //   visionRenderer[i].SetPosition(1, closeTarget);
            //    Vector3 farTarget = closeTarget + (transform.forward * (Data.distanceAngle.MaxDistance - Data.distanceAngle.ClearDistance)); 
            //    visionRenderer[i].SetPosition(2, farTarget);
            //}
        }


        private void Scan()
        {
            //tempBestDetectedTarget = null;
            model.lastIteration = TimeController.gameTime;
            Vector3 myPos = transform.position;
            foreach (var alive in levelManager.GetNearAgents(myPos, Data.distanceAngle.MaxDistance))
            {
                InternalScan(alive, Data.distanceAngle);
            }
        }

        public float GetMaxDetectDistance(EnumAwareState awareState)
        {
            return model.dataPerAware[(int)awareState].distanceAngle.MaxDistance;
        }

        public float GetClearDetectDistance(EnumAwareState awareState)
        {
            return model.dataPerAware[(int)awareState].distanceAngle.ClearDistance;
        }

#if UNITY_EDITOR 

        //void SetGizmosColor(Color color, float alpha)
        //{
        //    GizmosHelper.SetHandlesColor(color, alpha);
        //}



        public void DrawGizmos(float distance, float angle, Color color, float alpha)
        {
            if (DEBUG)
            {
                //float size = distance / Mathf.Cos(angle * .5f * Mathf.Deg2Rad);
                float size1 = distance * Mathf.Cos(angle * Mathf.Deg2Rad);
                float size2 = distance * Mathf.Sin(angle * Mathf.Deg2Rad) * 2f;
                Gizmos.DrawCone(transform.position + (transform.forward * size1 * .5f), transform.rotation * Quaternion.Euler(-90, 0, 0), new Vector3(size2, size1, size2), CommonHelper.Color(color, alpha));
                //GizmosHelper.DrawSolidArc(transform.position, Vector3.up, owner.Forward, angle / -2f, distance);

                Gizmos.DrawSphere(transform.position + (transform.forward * size1), transform.rotation * Quaternion.Euler(-90, 0, 0), new Vector3(size2, size1, size2), CommonHelper.Color(color, alpha));

                //float c = (MaxDistance - distance) * Mathf.Sin(LongDistanceAngle / 2f * Mathf.Deg2Rad) / Mathf.Sin(90 - (LongDistanceAngle / 2f) * Mathf.Deg2Rad);
                //Debug.LogError( " c " +c);

                //Vector3 forward = transform.forward;
                //forward.y = 0;
                //Vector3 pos = transform.position + (forward * distance * .88f);
                //forward *= (MaxDistance - distance);
                //Vector3 right = transform.right * (c);
                //right.y = 0;
                //SetGizmosColor(Color.blue, alpha);
                //GizmosHelper.DrawPolygon(new Vector3[] {
                //      pos +right ,
                //      pos + right+ forward,
                //      pos  - right + forward,
                //       pos  -right
                //       });
            }
        }
#endif
        public void DrawGizmos()
        {
#if UNITY_EDITOR
            Debug.DrawLine(transform.position, transform.position + (transform.forward * 3f), Color.red);
            if (Application.isPlaying)
            {
                Color color = (Owner.AwareState == EnumAwareState.Cautious ? Color.yellow : Owner.AwareState == EnumAwareState.Offensive ? Color.red : Color.green);
                DrawGizmos(Data.distanceAngle.ClearDistance, Data.distanceAngle.CloseAngle, color, .2f);
                DrawGizmos(Data.distanceAngle.MaxDistance, Data.distanceAngle.FarAngle, color, .2f);
            }
#endif
        }




    }
}