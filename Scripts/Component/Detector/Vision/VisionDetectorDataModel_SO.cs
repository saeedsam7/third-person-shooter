﻿
using UnityEngine;

namespace RCPU.Component
{
    [CreateAssetMenu(fileName = "VisionDetector", menuName = "RCPU/DataModel/Component/Detector/VisionDetector", order = 1)]
    public class VisionDetectorDataModel_SO : ScriptableObject
    {
        public VisionDetectorDataModel model;
    }
}