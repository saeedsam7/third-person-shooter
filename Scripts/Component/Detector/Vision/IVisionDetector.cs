﻿using RCPU.Agent;

namespace RCPU.Component
{
    public interface IVisionDetector : IDetector
    {
        bool Scan(IAlive target);
        bool HitScan(IAlive target);
        void ForceScan();
        void Scan(IDetectedTarget detectedTarget);
        float GetMaxDetectDistance(EnumAwareState j);
        float GetClearDetectDistance(EnumAwareState j);
    }
}
