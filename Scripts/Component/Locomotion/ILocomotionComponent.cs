
using RCPU.Agent;
using UnityEngine;

namespace RCPU.Component
{
    public interface ILocomotionComponent : IComponent
    {
        bool Enable { get; set; }
        float Speed { get; set; }
        float Height { get; set; }
        Vector3 Velocity { get; set; }
        float Center { get; set; }
        int MovementNavmeshLayer { get; }
        EnumMovementType MovementType { get; }
        EnumMoveRotateAbility LastMoveType { get; }
        void Roll(float dir);
        void UpdateAgentHeight(EnumPosState posState, bool isRolling);
    }

    public interface ILocomotionCharacter : ILocomotionComponent
    {
        void ResetInput();
        bool Rotate(float horizontal, float vertical);
        bool Move(EnumMoveRotateAbility moveMethod, bool hasRotateState, float horizontal, float vertical, bool sprint, bool isAimed);
    }

    public interface ILocomotionNavmesh : ILocomotionComponent
    {
        //bool ShouldStop(Vector3 position);
        Vector3 SteeringTarget { get; }
        float StoppingDistance { get; }
        Vector3 Destination { get; }
        void SetLag(bool value);
        void Stop();
        void ForceMove(Vector3 destination);
        bool Move(Vector3 destination);
        bool Move(Vector3 moveDestination, Vector3 lookDestination);
        float Rotate(Vector3 targetPosition);
    }

}