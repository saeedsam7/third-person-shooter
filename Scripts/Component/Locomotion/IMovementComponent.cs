
using UnityEngine;

namespace RCPU.Component
{
    public interface IMovementComponent : IInitializable, IComponent
    {
        int MovementNavmeshLayer { get; }
        bool Enabled { get; set; }
        EnumMovementType MovementType { get; }
        bool ShouldStop(Vector3 position);
        void Update(float horizontal, float vertical, bool sprint);
        bool SetTarget(Vector3 destination);
        void Stop();
        //void Init(MComponent_Movement_Base mMovement);
        void Spawn();
        void LateUpdate();
        void Update();
        void FixedUpdate();
        //void SetDataModel(MovementDataModel movementDataModel);
        //void GotoCover(MTriggerCover trigger);
        void OnDrawGizmos();
        void SetRollDirection(Vector3 forward);
    }
}