﻿using UnityEngine;

namespace RCPU.Component
{

    [CreateAssetMenu(fileName = "LocomotionCC", menuName = "RCPU/DataModel/Component/LocomotionCC", order = 4)]
    public class LocomotionCharacterControllerDataModel_SO : ScriptableObject
    {
        public LocomotionCharacterControllerDataModel model;
    }

}
