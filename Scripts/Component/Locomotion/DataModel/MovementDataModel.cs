﻿using System;
using UnityEngine;

namespace RCPU.Component
{
    [Serializable]
    public class MovementDataModel
    {
        public bool StopOnEnterState;
        public Vector2[] normalSpeedBind;
        public Vector3[] aimSpeedBind;
        public float RotateSpeed;

    }
}
