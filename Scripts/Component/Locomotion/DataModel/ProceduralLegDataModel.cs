﻿
using RCPU.Audio;
using System;
using UnityEngine;

namespace RCPU.Component
{
    [Serializable]
    public class ProceduralLegDataModel
    {

        public AnimationCurve SpeedCurve;
        public float MinStepDistance;
        public float StepLength;
        public float StepHeight;
        public AnimationCurve MoveCurveVertical;
        public AnimationCurve MoveCurveHorizental;

        public EnumAudioSFX legSfx;

        public float MinAngleToRotate;
    }
}
