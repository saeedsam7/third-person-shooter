﻿
using UnityEngine;

namespace RCPU.Component
{
    [CreateAssetMenu(fileName = "ProceduralLeg", menuName = "RCPU/DataModel/Component/ProceduralLeg", order = 3)]
    public class ProceduralLegDataModel_SO : ScriptableObject
    {
        public FootstepDataModel footstepData;
        public ProceduralLegDataModel simpleMoveModel;
        public ProceduralLegDataModel strafMoveModel;
        public ProceduralLegDataModel rotateOnlyModel;
    }
}
