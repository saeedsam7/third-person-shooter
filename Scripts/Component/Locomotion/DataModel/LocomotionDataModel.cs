﻿using System;
using UnityEngine;

namespace RCPU.Component
{
    [Serializable]
    public class LocomotionDataModel
    {
        public EnumMovementType movementType;
        public float CrawlHeight;
        public float CrawlTime;

        public float CrouchHeight;
        public float CrouchTime;

        public float StandHeight;
        public float StandTime;

        public float RollHeight;
        public float RollTime;

        public float JumpHeight;
        public float JumpTime;

        public bool AddHalfHeightAsOffset;
        public float CenterOffset;


    }

    [Serializable]
    public class LocomotionCharacterControllerDataModel : LocomotionDataModel
    {
        public float RotateSpeed;
        public float MaxVelocity;
    }

    [Serializable]
    public class LocomotionNavmeshAgentDataModel : LocomotionDataModel
    {
        public float[] RotateSpeedPerAwareness;
        public float[] WalkSpeedPerAwareness;
        public float[] RunSpeedPerAwareness;
        public float[] MinDistanceToRunPerAwareness;
        public float MinDistanceToMove;
        public float StopDistance;

        public float CrawlOffset;
        public float CrouchOffset;
        public float StandOffset;
        public float JumpOffset;

        public AnimationCurve JumpCurve;

        public bool canJump;
        public float minHeightToJump;

        public bool hasStrafMove;
    }
}
