﻿using UnityEngine;

namespace RCPU.Component
{

    [CreateAssetMenu(fileName = "LocomotionNavmesh", menuName = "RCPU/DataModel/Component/LocomotionNavmehs", order = 5)]
    public class LocomotionNavmeshAgentDataModel_SO : ScriptableObject
    {
        public LocomotionNavmeshAgentDataModel model;
    }
}
