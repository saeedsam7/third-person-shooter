﻿using UnityEngine;

namespace RCPU.Component
{
    public interface IProceduralLeg : IInitializable
    {
        int Group { get; }
        bool IsMoving { get; }
        EnumLegIndex LegIndex { get; }
        float TargetOffset { get; }

        void UpdateLeg(bool canMove, bool hasLag);
        void Jump();
        void PutOnGround();
        void PutOnGround(float lerp, Vector3 target);
    }
}
