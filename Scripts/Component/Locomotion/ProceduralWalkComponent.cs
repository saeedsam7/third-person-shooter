﻿
using RCPU.Agent;
using RCPU.Attributes;
using RCPU.NotificationSystem;
using RCPU.Ragdoll;
using System.Collections.Generic;

namespace RCPU.Component
{
    [InjectToInstance(typeof(IProceduralWalk))]
    [InitOrder(EnumInitOrder.Locomotion)]
    public class ProceduralWalkComponent : ComponentBase, IProceduralWalk
    {
        [InjectFromInstance] IProceduralLeg[] legs { get; set; }
        [InjectFromInstance] private IAlive Owner { get; set; }
        [InjectFromInstance] private ILocomotionNavmesh Locomotion { get; set; }
        protected override int OwnerId => Owner.Id;
        public override EnumComponentType ComponentType => EnumComponentType.Walk;
        Dictionary<int, List<int>> groups;
        int currentGroup;
        bool isJumping;
        bool hasLag;
        bool prepairForJump;
        public override void Init()
        {
            base.Init();
            Notification.RegisterInstance(OwnerId, EnumInstanceNotifType.GetRadyForJump, Id, OnGetRadyForJump);
            Notification.RegisterInstance(OwnerId, EnumInstanceNotifType.JumpEnded, Id, OnJumpEnded);
            groups = new Dictionary<int, List<int>>();
            currentGroup = -1;
            for (int i = 0; i < legs.Length; i++)
            {
                int g = legs[i].Group;
                if (currentGroup == -1)
                    currentGroup = g;
                if (!groups.ContainsKey(g))
                    groups.Add(legs[i].Group, new List<int>());
                groups[g].Add(i);
            }
            IsEnable = false;
        }
        protected void OnGetRadyForJump(NotificationParameter obj)
        {
            prepairForJump = true;
            isJumping = false;
        }
        protected void OnJumpEnded(NotificationParameter obj)
        {
            prepairForJump = false;
            isJumping = false;
        }
        protected override void OnOwnerDied(NotificationParameter obj)
        {
            IsEnable = false;
        }
        protected override void OnOwnerSpawned(NotificationParameter obj)
        {
            IsEnable = true;
            hasLag = false;
        }
        void Update()
        {
            if (isJumping)
            {
                for (int i = 0; i < legs.Length; i++)
                {
                    legs[i].Jump();
                }
            }
            else
            {

                if (ownerShocked)
                    return;

                float biggestTargetOffset = 0;
                int biggestTargetGroup = -1;
                for (int i = 0; i < legs.Length; i++)
                {
                    legs[i].UpdateLeg(legs[i].Group == currentGroup, hasLag);
                    if (biggestTargetOffset < legs[i].TargetOffset)
                    {
                        biggestTargetOffset = legs[i].TargetOffset;
                        biggestTargetGroup = legs[i].Group;
                    }
                }
                if (biggestTargetOffset > .15f)
                {
                    if (!hasLag)
                    {
                        hasLag = true;
                        //Debug.LogError(" Freeze " + biggestTargetOffset.ToString("f2"));
                        Locomotion.SetLag(true);
                    }
                }
                else
                if (hasLag)
                {
                    //Debug.Log(" unfreeze " + biggestTargetOffset);
                    hasLag = false;
                    Locomotion.SetLag(false);
                }

                var group = groups[currentGroup];
                int isMoving = group.Count;
                for (int i = 0; i < group.Count; i++)
                {
                    if (!legs[group[i]].IsMoving)
                    {
                        isMoving--;
                    }
                }
                if (isMoving == 0)
                {
                    if (prepairForJump)
                    {
                        Notification.NotifyInstance(OwnerId, EnumInstanceNotifType.ReadyForJump, new NotificationParameter());
                        isJumping = true;
                    }
                    if (legs.Length > 2 && biggestTargetGroup != -1)
                    {
                        currentGroup = biggestTargetGroup;
                    }
                    else
                    {
                        currentGroup++;
                        if (currentGroup >= groups.Count)
                            currentGroup = 0;
                    }

                }
            }
        }

        public void PutAllLegsOnGround()
        {
            for (int i = 0; i < legs.Length; i++)
            {
                legs[i].PutOnGround();
            }
        }

        public void PutAllLegsOnGround(float lerp, Dictionary<EnumLegIndex, TransformDataModel> target)
        {
            for (int i = 0; i < legs.Length; i++)
            {
                legs[i].PutOnGround(lerp, target[legs[i].LegIndex].Position);
            }
        }
    }
}
