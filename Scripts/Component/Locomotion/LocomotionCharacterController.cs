﻿using RCPU.Agent;
using RCPU.Attributes;
using RCPU.CameraFSM;
using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.InputSystem;
using RCPU.NotificationSystem;
using System;
using System.Collections;
using UnityEngine;

namespace RCPU.Component
{
    [RequireComponent(typeof(CharacterController))]
    [InjectToInstance(typeof(ILocomotionCharacter))]
    [InjectToInstance(typeof(ILocomotionComponent))]
    [InitOrder(EnumInitOrder.Locomotion)]
    public class LocomotionCharacterController : ComponentBase, ILocomotionCharacter
    {
        [InjectFromInstance] IAnimatorComponent anim { get; set; }
        [InjectFromInstance] private IAgent owner { get; set; }
        [InjectFromSystem] private ICamera cam { get; set; }
        protected Vector3 lastPosition;
        protected Vector3 position;
        protected override int OwnerId => owner.Id;
        public override EnumComponentType ComponentType => EnumComponentType.Locomotion;

        protected LocomotionCharacterControllerDataModel model;
        [SerializeField] protected LocomotionCharacterControllerDataModel_SO model_SO;
        private CharacterController characterController;
        protected Coroutine coroutineCCCS;
        bool canUpdateStopSpeed;
        bool changeAgentHeight;
        float speed;
        float direction;
        float targetSpeed;
        float targetAngle;
        InputAxisValue inputV;
        InputAxisValue inputH;
        float horizontalAverage = 0;
        float verticalAverage = 0;
        float inputVertical;
        float inputHorizental;
        float inputTimer;


        public override void Init()
        {
            base.Init();
            this.characterController = GetComponent<CharacterController>();
            this.model = DInjector.Instantiate<LocomotionCharacterControllerDataModel_SO>(model_SO).model;

            inputV = new InputAxisValue(.1f, -1, 1);
            inputH = new InputAxisValue(.1f, -1, 1);
            ChangeAgentHeight(model.StandHeight, 0);
            IsEnable = false;
        }

        protected override void OnOwnerDied(NotificationParameter obj)
        {
            IsEnable = false;
        }
        protected override void OnOwnerSpawned(NotificationParameter obj)
        {
            IsEnable = true;
        }

        public bool Enable { get => this.characterController.enabled; set => this.characterController.enabled = value; }
        public float Speed { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

        public float Height { get => this.characterController.height; set => this.characterController.height = value; }

        public Vector3 Velocity { get => (position - lastPosition) / TimeController.deltaTime; set { } }

        public float Center { get => characterController.center.y; set => characterController.center = new Vector3(characterController.center.x, value, characterController.center.z); }



        public int MovementNavmeshLayer
        {
            get
            {
                switch (MovementType)
                {
                    case EnumMovementType.Ground:
                        return NavMeshHelper.Navmeh_Area_Walkable;
                    case EnumMovementType.Flight:
                        return NavMeshHelper.Navmeh_Area_Flight;
                    default:
                        return NavMeshHelper.Navmeh_Area_Walkable;
                }
            }
        }

        public EnumMovementType MovementType => model.movementType;

        public EnumMoveRotateAbility LastMoveType { get; protected set; }

        public Vector3 Destination => owner.Position + (Velocity * TimeController.deltaTime);

        public bool Move(Vector3 motion)
        {
            return this.characterController.Move(motion) == CollisionFlags.CollidedBelow;
        }

        public void ForceMove(Vector3 motion)
        {
            if (!IsEnable || ownerShocked)
                return;
            this.characterController.Move(motion);
        }

        public void Update()
        {
            lastPosition = position;
            position = transform.position;
        }

        public void Roll(float dir)
        {
            if (!IsEnable || ownerShocked)
                return;
            //Debug.LogError(" Roll " + dir);
            owner.Rotate(Vector3.up, dir);
        }

        public void UpdateAgentHeight(EnumPosState posState, bool isRolling)
        {
            if (!IsEnable || ownerShocked)
                return;
            if (isRolling)
            {
                ChangeAgentHeight(model.RollHeight, model.RollTime);
            }
            else
            {
                switch (posState)
                {
                    case EnumPosState.Crawl:
                        ChangeAgentHeight(model.CrawlHeight, model.CrawlTime);
                        break;
                    case EnumPosState.Crouch:
                        ChangeAgentHeight(model.CrouchHeight, model.CrouchTime);
                        break;
                    default:
                        ChangeAgentHeight(model.StandHeight, model.StandTime);
                        break;
                }
            }
        }

        void ChangeAgentHeight(float height, float time)
        {
            if (time <= 0)
            {
                Height = height;
                Center = (model.AddHalfHeightAsOffset ? height / 2 : 0) + model.CenterOffset;
            }
            else
            {
                if (changeAgentHeight)
                {
                    changeAgentHeight = false;
                    StopCoroutine(coroutineCCCS);
                }
                coroutineCCCS = StartCoroutine(Coroutine_ChangeAgentHeight(height, time));
            }
        }

        IEnumerator Coroutine_ChangeAgentHeight(float height, float time)
        {
            changeAgentHeight = true;
            float startSize = Height;
            if (startSize != height)
            {
                while (time > 0)
                {
                    ChangeAgentHeight(height - ((height - startSize) * TimeController.deltaTime), 0);
                    yield return new WaitForEndOfFrame();
                    time -= TimeController.deltaTime;
                }
                ChangeAgentHeight(height, 0);
            }
            changeAgentHeight = false;
        }

        public void ResetInput()
        {
            inputV.Reset();
            inputH.Reset();
            inputTimer = 1;
        }

        private bool SimpleMove(bool hasRotateState, bool sprint)
        {
            LastMoveType = EnumMoveRotateAbility.SimpleMove;
            inputVertical = MathF.Max(Math.Abs(horizontalAverage), Math.Abs(verticalAverage));
            //Logger.Log(" SimpleMove  verticalAverage " + verticalAverage.ToString("f1") + " inputVertical " + inputVertical.ToString("f1"), 8);
            if (inputVertical < .01)
                inputVertical = 0;
            targetAngle = CommonHelper.GetInputNormilizedAngle(owner.Position, owner.Forward, cam.GetForward(10000, true), horizontalAverage, verticalAverage);


            //Debug.Log(" State " + owner.CurrentState + " input " + input.ToString("f2") + " V " + verticalAverage.ToString("f2") + " H " + horizontalAverage.ToString("f2") + " Angle " + targetAngle.ToString("f1"));

            if (inputVertical == 0 && speed > 0 && canUpdateStopSpeed)
            {
                canUpdateStopSpeed = false;
                anim.SetFloat(EnumFloatParam.StopSpeed, speed);
            }
            else
            {
                if (inputVertical != 0 && !canUpdateStopSpeed)
                {
                    canUpdateStopSpeed = true;
                }
            }

            if (sprint && inputVertical > 0.5f)
            {
                targetSpeed = 2;
            }
            else
            {
                targetSpeed = inputVertical;
            }
            speed = CommonHelper.Lerp(speed, targetSpeed, TimeController.GetNormilizeTime(3));
            if (speed < .01)
                speed = 0;

            //Logger.Log(" SimpleMove inputVertical " + inputVertical.ToString("f1") + " Speed " + speed.ToString("f1"), 9);
            anim.SetFloat(EnumFloatParam.InputDirection, 0);
            anim.SetFloat(EnumFloatParam.Direction, 0);
            anim.SetFloat(EnumFloatParam.InputSpeed, inputVertical);
            anim.SetFloat(EnumFloatParam.Speed, speed);
            //anim.SetFloat(EnumFloatParam.Direction, horizontal);
            if (inputVertical != 0 && MathF.Abs(targetAngle) > 5)
            {
                return (CheckForRotation(hasRotateState));
            }
            return false;
        }

        private bool StrafMove(bool isAimed)
        {
            LastMoveType = EnumMoveRotateAbility.StrafMove;
            if (verticalAverage == 0 && horizontalAverage == 0 && speed > 0 && canUpdateStopSpeed)
            {
                canUpdateStopSpeed = false;
                anim.SetFloat(EnumFloatParam.StopSpeed, speed);
            }
            else
            {
                if ((verticalAverage != 0 || horizontalAverage != 0) && !canUpdateStopSpeed)
                {
                    canUpdateStopSpeed = true;
                }
            }

            inputVertical = verticalAverage * .5f;
            inputHorizental = horizontalAverage * .5f;

            speed = CommonHelper.Lerp(speed, inputVertical, TimeController.GetNormilizeTime(3));
            if (Mathf.Abs(speed) < .01)
                speed = 0;

            direction = CommonHelper.Lerp(direction, inputHorizental, TimeController.GetNormilizeTime(3));
            if (Mathf.Abs(direction) < .01)
                direction = 0;


            anim.SetFloat(EnumFloatParam.InputDirection, inputHorizental);
            anim.SetFloat(EnumFloatParam.InputSpeed, inputVertical);
            anim.SetFloat(EnumFloatParam.Speed, speed);
            anim.SetFloat(EnumFloatParam.Direction, direction);

            return StandRotate(isAimed);
        }

        public bool Move(EnumMoveRotateAbility moveMethod, bool hasRotateState, float horizontal, float vertical, bool sprint, bool isAimed)
        {

            if (!IsEnable)
                return false;
            inputV.Value = vertical;
            inputH.Value = horizontal;
            inputTimer += TimeController.deltaTime;

            if (inputTimer > .15f)
            {
                //Logger.Log("inputV " + inputV.ToString(), 7);
                inputTimer = 0;
                verticalAverage = inputV.Value;
                horizontalAverage = inputH.Value;

                if (Mathf.Abs(verticalAverage) < .01)
                    verticalAverage = 0;
                if (Mathf.Abs(horizontalAverage) < .01)
                    horizontalAverage = 0;

                //Logger.Log(" Move  vertical " + vertical.ToString("f1") + " verticalAverage " + verticalAverage.ToString("f1"), 8);
            }

            switch (moveMethod)
            {
                case EnumMoveRotateAbility.SimpleMove:
                    return SimpleMove(hasRotateState, sprint);
                case EnumMoveRotateAbility.StrafMove:
                    return StrafMove(isAimed);
            }
            return false;
        }
        bool StandRotate(bool isAimed)
        {

            Vector3 forward = cam.GetForward(100, true);
            targetAngle = CommonHelper.SignedAngle(owner.Position, owner.Forward, forward, true) * -1;
            Debug.DrawRay(owner.Position, owner.Forward * 10, Color.red);
            Debug.DrawRay(owner.Position, forward, Color.green);
            //Debug.Log(" targetAngle " + targetAngle);
            bool stand = anim.IsStand(true, false);
            float abs = Math.Abs(targetAngle);
            if (abs > 30 && stand)
            {
                //Debug.LogError(" StandRotate targetAngle " + targetAngle);
                anim.SetFloat(EnumFloatParam.Multiplier_Rotation, isAimed ? 2 : 1.25f);
                anim.SetFloat(EnumFloatParam.RotateSpeed, (Velocity.magnitude / model.MaxVelocity));
                anim.SetFloat(EnumFloatParam.RotateAngle, targetAngle);
                anim.SetTrigger(EnumTriggerParam.Rotate);
                return true;
            }

            if (abs > 1 /*&& !stand*/)
            {
                //Debug.LogError("StandRotate  " + forward);
                owner.LerpLookAt(forward, model.RotateSpeed, .9f);
            }
            //owner.LookAt(forward, false);
            return false;
        }


        public bool Rotate(float horizontal, float vertical)
        {
            if (anim.IsRotate(true, true))
            {
                inputV.Value = vertical;
                inputH.Value = horizontal;

                inputTimer += TimeController.deltaTime;
                if (inputTimer > .15f)
                {
                    inputTimer = 0;

                    float inputVvalue = inputV.Value;
                    if (Math.Abs(inputVvalue) < .01f)
                        inputVvalue = 0;
                    float inputHvalue = inputH.Value;
                    if (Math.Abs(inputHvalue) < .01f)
                        inputHvalue = 0;


                    inputVertical = MathF.Max(Math.Abs(inputHvalue), Math.Abs(inputVvalue));
                    if (inputVertical != 0)
                    {
                        targetSpeed = inputVertical;
                        speed = CommonHelper.Lerp(speed, targetSpeed, TimeController.GetNormilizeTime(3));
                        if (speed < .01)
                            speed = 0;
                        anim.SetFloat(EnumFloatParam.Speed, speed);
                    }



                    if ((Math.Sign(verticalAverage) != Math.Sign(inputVvalue) && Math.Abs(verticalAverage - inputVvalue) > .5f) ||
                        (Math.Sign(horizontalAverage) != Math.Sign(inputHvalue) && Math.Abs(horizontalAverage - inputHvalue) > .5f))
                    {
                        inputVertical = MathF.Max(Math.Abs(inputVvalue), Math.Abs(inputHvalue));
                        if (inputVertical < .01f)
                            inputVertical = 0;

                        if (inputVertical != 0)
                        {
                            anim.SetTrigger(EnumTriggerParam.Rotate);
                            return true;
                        }
                    }

                }


                //inputVertical = verticalAverage  ;
                //inputHorizental = horizontalAverage ;

                //speed = CommonHelper.Lerp(speed, inputVertical, TimeController.deltaTime * 3);
                //if (Mathf.Abs(speed) < .01)
                //    speed = 0;

                //direction = CommonHelper.Lerp(direction, inputHorizental, TimeController.deltaTime * 3);
                //if (Mathf.Abs(direction) < .01)
                //    direction = 0;


                anim.SetFloat(EnumFloatParam.InputDirection, inputHorizental);
                anim.SetFloat(EnumFloatParam.InputSpeed, inputVertical);
                //anim.SetFloat(EnumFloatParam.Speed, speed);
                //anim.SetFloat(EnumFloatParam.Direction, direction);

            }
            return false;
        }

        bool CheckForRotation(bool hasRotateState)
        {
            if (hasRotateState)
            {
                //Debug.Log(" IsStand " + anim.IsStand(true, false) + " IsMove " + anim.IsMove(true, false) + " targetAngle " + targetAngle);
                if (anim.IsStand(true, false) && (Mathf.Abs(targetAngle) > 70))
                {
                    anim.SetFloat(EnumFloatParam.Multiplier_Rotation, 1f);
                    anim.SetFloat(EnumFloatParam.RotateSpeed, 0);
                    anim.SetFloat(EnumFloatParam.RotateAngle, targetAngle);
                    anim.SetTrigger(EnumTriggerParam.Rotate);
                    return true;
                }
                else
                     if (anim.IsMove(true, false) && (Mathf.Abs(targetAngle) > 135))
                {
                    anim.SetFloat(EnumFloatParam.Multiplier_Rotation, 1f);
                    //Debug.LogError("input " + input + " targetAngle " + targetAngle + " magnitude " + Velocity.magnitude);
                    anim.SetFloat(EnumFloatParam.RotateSpeed, (Velocity.magnitude / model.MaxVelocity));
                    anim.SetFloat(EnumFloatParam.RotateAngle, targetAngle);
                    anim.SetTrigger(EnumTriggerParam.Rotate);
                    return true;
                }
            }
            //Debug.LogError("FREE ROTATE  " + targetAngle + "  Angle " + (targetAngle * TimeController.deltaTime * model.RotateSpeed));
            owner.Rotate(Vector3.up, Mathf.Clamp(targetAngle * TimeController.deltaTime * model.RotateSpeed, -5, 5));
            return false;
        }

    }
}
