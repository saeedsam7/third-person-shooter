﻿using RCPU.Ragdoll;
using System.Collections.Generic;

namespace RCPU.Component
{
    public interface IProceduralWalk : IComponent
    {
        void PutAllLegsOnGround();
        void PutAllLegsOnGround(float lerp, Dictionary<EnumLegIndex, TransformDataModel> target);
    }
}