﻿using RCPU.Agent;
using RCPU.Attributes;
using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.NotificationSystem;
using System;
using System.Collections;
using Unity.AI.Navigation;
using UnityEngine;
using UnityEngine.AI;

namespace RCPU.Component
{
    [RequireComponent(typeof(NavMeshAgent))]
    [InjectToInstance(typeof(ILocomotionNavmesh))]
    [InjectToInstance(typeof(ILocomotionComponent))]
    [InitOrder(EnumInitOrder.Locomotion)]
    public class LocomotionNavmesh : ComponentBase, ILocomotionNavmesh
    {
        protected override int OwnerId => Owner.Id;
        public override EnumComponentType ComponentType => EnumComponentType.Locomotion;
        [InjectFromInstance] private IAiPrivate Owner { get; set; }
        [InjectFromInstance] private IIkHeightModifier HeightModifier { get; set; }


        protected LocomotionNavmeshAgentDataModel model;
        [SerializeField] protected LocomotionNavmeshAgentDataModel_SO model_SO;

        private NavMeshAgent navMeshAgent;
        protected Coroutine coroutineCCCS;
        bool changeAgentHeight;
        float targetAngle;
        EnumLocomotionState locomotionState;
        bool canJump;
        bool hasLag;
        public EnumMoveRotateAbility LastMoveType { get; protected set; }

        public override void Init()
        {
            base.Init();
            this.navMeshAgent = GetComponent<NavMeshAgent>();
            this.model = DInjector.Instantiate<LocomotionNavmeshAgentDataModel_SO>(model_SO).model;
            this.navMeshAgent.autoTraverseOffMeshLink = !model.canJump;
            Notification.RegisterInstance(OwnerId, EnumInstanceNotifType.ReadyForJump, Id, OnRadyForJump);
        }

        private void OnRadyForJump(NotificationParameter parameter)
        {
            canJump = true;
        }

        protected override void OnOwnerDied(NotificationParameter obj)
        {
            base.OnOwnerDied(obj);
            IsEnable = false;
            navMeshAgent.velocity = Vector3.zero;
            Stop();
        }

        protected override void OnOwnerShocked(NotificationParameter obj)
        {
            base.OnOwnerShocked(obj);
            hasLag = false;
            navMeshAgent.velocity = Vector3.zero;
            Stop();
        }

        protected override void OnOwnerSpawned(NotificationParameter obj)
        {
            IsEnable = true;
            ChangeAgentHeight(model.StandHeight, model.StandOffset, 0);
            LastMoveType = EnumMoveRotateAbility.SimpleMove;
        }

        public bool Enable { get => this.navMeshAgent.enabled; set => this.navMeshAgent.enabled = value; }
        public float Speed { get => navMeshAgent.speed; set => navMeshAgent.speed = value; }

        public float Height { get => this.navMeshAgent.height; set => this.navMeshAgent.height = value; }

        public Vector3 Velocity { get => this.navMeshAgent.velocity; set => this.navMeshAgent.velocity = value; }
        public Vector3 Destination { get => this.navMeshAgent.nextPosition; }
        public Vector3 SteeringTarget => throw new System.NotImplementedException();

        public float StoppingDistance { get => model.StopDistance; }
        public float Center { get => navMeshAgent.baseOffset; set => navMeshAgent.baseOffset = value; }

        public int MovementNavmeshLayer
        {
            get
            {
                switch (MovementType)
                {
                    case EnumMovementType.Ground:
                        return NavMeshHelper.Navmeh_Area_Walkable;
                    case EnumMovementType.Flight:
                        return NavMeshHelper.Navmeh_Area_Flight;
                    default:
                        return NavMeshHelper.Navmeh_Area_Walkable;
                }
            }
        }
        protected float RotateSpeed { get => model.RotateSpeedPerAwareness[(int)Owner.AwareState]; }
        protected float WalkSpeed { get => model.WalkSpeedPerAwareness[(int)Owner.AwareState]; }
        protected float RunSpeed { get => model.RunSpeedPerAwareness[(int)Owner.AwareState]; }
        protected float MinDistanceToRun { get => model.MinDistanceToRunPerAwareness[(int)Owner.AwareState]; }

        public EnumMovementType MovementType => model.movementType;

        public void ForceMove(Vector3 motion)
        {
            if (!IsEnable)
                return;
            this.navMeshAgent.Move(motion);
        }
        Vector3 destination;
        public bool Move(Vector3 moveDestination, Vector3 lookDestination)
        {
            if (!IsEnable || ownerShocked)
                return false;

            if (!model.hasStrafMove)
            {
                return Move(moveDestination);
            }

            LastMoveType = EnumMoveRotateAbility.StrafMove;
            navMeshAgent.isStopped = false;
            navMeshAgent.updateRotation = false;
            float distance = CommonHelper.Distance(Owner.Position, moveDestination);
            targetAngle = (CommonHelper.SignedAngle(Owner.Position, Owner.Forward, lookDestination, true));

            Speed = (hasLag ? 0.5f : 1f) * (distance > MinDistanceToRun ? RunSpeed : WalkSpeed);
            if (distance > model.StopDistance)
            {
                if (destination != moveDestination)
                {
                    destination = moveDestination;
                    navMeshAgent.SetDestination(moveDestination);
                }
            }
            else
            {
                Stop();
            }
            if (MathF.Abs(targetAngle) > 3f)
            {
                if (DEBUG)
                    Debug.Log("Rotate " + targetAngle.ToString("f0") + " Clamp " + Mathf.Clamp(targetAngle * TimeController.GetNormilizeTime(RotateSpeed), -5, 5).ToString("f0"));
                Owner.Rotate(Vector3.up, Mathf.Clamp(-targetAngle * TimeController.GetNormilizeTime(RotateSpeed), -5, 5));
            }
            return false;
        }

        public bool Move(Vector3 destination)
        {
            if (!IsEnable || ownerShocked)
                return false;

            if (locomotionState == EnumLocomotionState.Jump)
            {
                return false;
            }
            if (navMeshAgent.isOnOffMeshLink && model.canJump)
            {
                if (CheckForJump())
                    return false;
            }
            float distance = CommonHelper.Distance(Owner.Position, destination);
            targetAngle = Mathf.Abs(CommonHelper.SignedAngle(Owner.Position, Owner.Forward, destination, true));
            navMeshAgent.isStopped = false;
            if (!model.hasStrafMove && targetAngle > 45)
            {
                navMeshAgent.updateRotation = false;
                Rotate(destination);
            }
            else
            {
                LastMoveType = EnumMoveRotateAbility.SimpleMove;
                navMeshAgent.updateRotation = true;
                Speed = (hasLag ? 0.5f : 1f) * (distance > MinDistanceToRun ? RunSpeed : WalkSpeed);
                if (distance > StoppingDistance)
                {

                    navMeshAgent.SetDestination(destination);
                }
                else
                {
                    Stop();
                }
            }
            return false;
        }

        public bool CheckForJump()
        {
            var link = navMeshAgent.navMeshOwner as NavMeshLink;
            if (!CommonHelper.IsNull(link) && link.area == NavMeshHelper.Navmeh_Area_Jump)
            {
                var startPos = navMeshAgent.currentOffMeshLinkData.startPos;
                var endPos = navMeshAgent.currentOffMeshLinkData.endPos;
                var distToStart = CommonHelper.Distance(Owner.Position, startPos);
                var disToEnd = CommonHelper.Distance(Owner.Position, endPos);

                //if (distToStart < 1 || disToEnd < 1)
                {
                    if (distToStart > disToEnd)
                    {
                        CommonHelper.Swap(ref startPos, ref endPos);
                    }
                    if ((Mathf.Abs(startPos.y - endPos.y) > model.minHeightToJump) && CommonHelper.Distance(navMeshAgent.destination, startPos) > CommonHelper.Distance(navMeshAgent.destination, endPos))
                    {
                        StartCoroutine(CoroutineJump(Owner.Position, endPos));
                        return true;
                    }
                }
            }
            return false;
        }

        IEnumerator CoroutineJump(Vector3 start, Vector3 end)
        {
            canJump = false;
            float stateChageTime = .35f;
            locomotionState = EnumLocomotionState.Jump;
            Stop();
            navMeshAgent.updateRotation = false;

            float time = 1;
            while (time > 0)
            {
                yield return new WaitForEndOfFrame();
                time -= TimeController.deltaTime;
            }
            if (ownerShocked)
                yield break;
            while (Rotate(end) > 5)
            {
                yield return new WaitForEndOfFrame();
                time -= TimeController.deltaTime;
            }
            if (ownerShocked)
                yield break;
            Notification.NotifyInstance(OwnerId, EnumInstanceNotifType.GetRadyForJump, new NotificationParameter());
            while (!canJump)
            {
                yield return new WaitForEndOfFrame();
            }
            Notification.NotifyInstance(OwnerId, EnumInstanceNotifType.JumpStarted, new NotificationParameter());

            time = stateChageTime;
            ChangeAgentHeight(model.CrawlHeight, model.CrawlOffset, time);
            while (time > 0)
            {
                yield return new WaitForEndOfFrame();
                time -= TimeController.deltaTime;
            }
            time = stateChageTime;
            ChangeAgentHeight(model.JumpHeight, model.JumpOffset, time);
            while (time > 0)
            {
                yield return new WaitForEndOfFrame();
                time -= (TimeController.deltaTime * 2);
            }
            AnimationCurve jumpCurve = new AnimationCurve();
            int i = 0;
            for (i = 0; i < model.JumpCurve.keys.Length - 1; i++)
            {
                jumpCurve.AddKey(model.JumpCurve.keys[i].time, model.JumpCurve.keys[i].value + start.y);
            }
            jumpCurve.AddKey(model.JumpCurve.keys[i].time, model.JumpCurve.keys[i].value + end.y);

            time = model.JumpTime;
            while (time > 0)
            {
                yield return new WaitForEndOfFrame();
                var t = 1 - (time / model.JumpTime);
                float yOffset = jumpCurve.Evaluate(t);
                var pos = Vector3.Lerp(start, end, t);
                pos.y = yOffset;
                Owner.SetPosition(pos);
                time -= TimeController.deltaTime;
            }

            time = stateChageTime;
            ChangeAgentHeight(model.StandHeight, model.StandOffset, time);
            while (time > 0)
            {
                yield return new WaitForEndOfFrame();
                time -= TimeController.deltaTime;
            }

            navMeshAgent.CompleteOffMeshLink();
            Notification.NotifyInstance(OwnerId, EnumInstanceNotifType.JumpEnded, new NotificationParameter());
            locomotionState = EnumLocomotionState.Walk;
        }

        public void Stop()
        {
            navMeshAgent.isStopped = true;
        }

        public void Roll(float dir)
        {
            //Debug.LogError(" Roll " + dir);
            //owner.Rotate(Vector3.up, dir);
        }

        public void UpdateAgentHeight(EnumPosState posState, bool isRolling)
        {
            if (isRolling)
            {
                ChangeAgentHeight(model.RollHeight, model.StandOffset, model.RollTime);
            }
            else
            {
                switch (posState)
                {
                    case EnumPosState.Crawl:
                        ChangeAgentHeight(model.CrawlHeight, model.CrawlOffset, model.CrawlTime);
                        break;
                    case EnumPosState.Crouch:
                        ChangeAgentHeight(model.CrouchHeight, model.CrouchOffset, model.CrouchTime);
                        break;
                    case EnumPosState.Jump:
                        ChangeAgentHeight(model.JumpHeight, model.JumpOffset, model.JumpTime);
                        break;
                    default:
                        ChangeAgentHeight(model.StandHeight, model.StandOffset, model.StandTime);
                        break;
                }
            }
        }

        void ChangeAgentHeight(float height, float offset, float time)
        {
            if (time <= 0)
            {
                Height = height;
                HeightModifier.Offset = offset;
                Center = (model.AddHalfHeightAsOffset ? height / 2 : 0) + model.CenterOffset;
            }
            else
            {
                if (changeAgentHeight)
                {
                    changeAgentHeight = false;
                    StopCoroutine(coroutineCCCS);
                }
                coroutineCCCS = StartCoroutine(CoroutineChangeAgentHeight(height, offset, time));
            }
        }

        IEnumerator CoroutineChangeAgentHeight(float height, float offset, float time)
        {
            changeAgentHeight = true;
            float startSize = Height;
            float startOffset = HeightModifier.Offset;
            float t = time;
            if (startSize != height || startOffset != offset)
            {
                while (t > 0)
                {
                    ChangeAgentHeight(Mathf.Lerp(startSize, height, 1f - (t / time)), Mathf.Lerp(startOffset, offset, 1f - (t / time)), 0);
                    yield return new WaitForEndOfFrame();
                    t -= TimeController.deltaTime;
                }
                ChangeAgentHeight(height, offset, 0);
            }
            changeAgentHeight = false;
        }

        public float Rotate(Vector3 destination)
        {
            if (DEBUG)
                Debug.Log("*Rotate * " + (CommonHelper.SignedAngle(Owner.Position, Owner.Forward, destination, true)));
            if (!IsEnable)
                return Mathf.Infinity;

            LastMoveType = EnumMoveRotateAbility.RotateOnly;
            Owner.LerpLookAt(destination, RotateSpeed, .95f);
            return Mathf.Abs(CommonHelper.SignedAngle(Owner.Position, Owner.Forward, destination, true));

        }

        public void SetLag(bool value)
        {
            hasLag = value;
            if (hasLag)
            {
                Velocity /= 2f;
            }
        }

    }
}
