﻿

using System;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Agent
{
    [Serializable]
    public class AiDecisionRule
    {
        //[HideInInspector] public string name;
        public EnumAwareState awareState;
        public EnumDecision startState;
        public int priority;
        public List<AiCondicion> conditions;
        public List<AiDecisionModel> possibleDecision;

#if UNITY_EDITOR
        [HideInInspector] public bool visibleByFilter;
#endif
        public void SetVisible(bool value)
        {
#if UNITY_EDITOR
            visibleByFilter = value;
#endif
        }
    }

    [Serializable]
    public class AiCondicion
    {
        public EnumCondition condition;
        public bool value;
    }

    [Serializable]
    public class AiDecisionModel
    {
        public static readonly AiDecisionModel Empty = new AiDecisionModel() { decision = EnumDecision.None, chance = 0, startTime = Mathf.NegativeInfinity, agentStateTarget = EnumAgentState.None };
        public EnumDecision decision;
        [Range(0f, 100f)]
        public float chance;
        float startTime;
        public float TimeOut;
        public bool IsDecisionTimeOut => TimeOut > 0 && (TimeController.gameTime - startTime) > TimeOut;
        //public EnumAgentState changeStateTarget;

        //public bool changeAwareState;
        public EnumAgentState agentStateTarget;
        AiDecisionModel()
        {
        }
        public AiDecisionModel(AiDecisionModel item)
        {
            this.decision = item.decision;
            this.chance = item.chance;
            this.agentStateTarget = item.agentStateTarget;
            this.TimeOut = item.TimeOut;
        }

        public AiDecisionModel(EnumDecision decision, EnumAgentState agentStateTarget)
        {
            this.decision = decision;
            this.chance = 100;
            this.agentStateTarget = agentStateTarget;
            this.TimeOut = 0;
        }

        public void Select()
        {
            this.startTime = TimeController.gameTime;
        }
    }
}
