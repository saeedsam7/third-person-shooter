﻿

using RCPU.Attributes;
using RCPU.Component;
using RCPU.Damage;
using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.NotificationSystem;
using RCPU.Weapon;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using UnityEngine;

namespace RCPU.Agent
{
    [InjectToInstance(typeof(IAiDecisionMaker))]
    [InitOrder(EnumInitOrder.AgentState)]
    public class AiDecisionMaker : ComponentBase, IAiDecisionMaker
    {
        [InjectFromInstance] IWeaponManagerComponentAuto WeaponManager { get; set; }
        [InjectFromInstance] ILocomotionComponent Locomotion { get; set; }
        [InjectFromSystem] ILevelManager LevelManager { get; set; }
        [InjectFromInstance] IAiPrivate Owner { get; set; }
        [SerializeField] protected AiDecisionRule_SO DecisionRule_SO;
        protected Dictionary<EnumAwareState, AiDecisionRule[]> rules;
        [InjectFromInstance] protected IDamageManager damageManager { get; set; }
        protected override int OwnerId => Owner.Id;
        public override EnumComponentType ComponentType => EnumComponentType.DecisionMaker;
        protected Dictionary<EnumCondition, bool> allCondition;
        bool ownerAlive;

        [SerializeField] protected List<string> debugText;
        //protected List<EnumDecision> ressult;

        public Dictionary<EnumCondition, bool> UpdateCondition(IDetectedTarget detectedTarget)
        {
            IAlive target = detectedTarget.Target;
            float distance = CommonHelper.Distance(Owner.Position, target.Position);
            float angle = Mathf.Abs(CommonHelper.SignedAngle(Owner.Position, Owner.Forward, target.Position, true));
            float MinAttackDistance = WeaponManager.MinAttackDistance;
            float MaxAttackDistance = WeaponManager.MaxAttackDistance;
            //TODO update with real data  
            allCondition[EnumCondition.IsInCover] = Owner.IsInCover;
            allCondition[EnumCondition.IsInShootRange] = (distance > MinAttackDistance && distance < MaxAttackDistance);
            allCondition[EnumCondition.IsTooCloseToShoot] = distance < MinAttackDistance;
            allCondition[EnumCondition.IsTooFarFromTarget] = distance > Owner.MaxDetectDistance;
            allCondition[EnumCondition.IsTargetInCover] = target.IsInCover;
            allCondition[EnumCondition.XXXXHaveSpecialAction] = false;
            allCondition[EnumCondition.HasGrenade] = WeaponManager.HasGrenade();
            allCondition[EnumCondition.HasComunicationDevice] = false;
            allCondition[EnumCondition.IsInLowHealth] = (damageManager.GetCurrentCapacity(EnumDamageType.Health) < damageManager.GetMaxCapacity(EnumDamageType.Health) % .5f);
            allCondition[EnumCondition.HasBullet] = WeaponManager.HasBullet;
            allCondition[EnumCondition.IsNearGrenade] = LevelManager.IsNearGrenade(Owner.Position);
            allCondition[EnumCondition.HasDetector] = Owner.HasAnyDetectorComponent;
            allCondition[EnumCondition.HavePathToTarget] = NavMeshHelper.SamplePosition(Owner.Position, 10, Locomotion.MovementNavmeshLayer);
            allCondition[EnumCondition.GetHit] = GetLastHit(target) == 0;
            allCondition[EnumCondition.NewTargetDetected] = detectedTarget.LastSeen == 0;
            allCondition[EnumCondition.HasSuspiciousTarget] = Owner.SuspiciousLevel >= .5f;
            allCondition[EnumCondition.TargetLost] = detectedTarget.Lost;
            allCondition[EnumCondition.TargetIsDead] = !detectedTarget.Target.IsAlive;
            allCondition[EnumCondition.XXXXXXXReload] = WeaponManager.CanMustReload() && !WeaponManager.IsReloading;
            allCondition[EnumCondition.XXXXXIsInShootAngle] = angle < Owner.ShootAngle;
            allCondition[EnumCondition.VisitTargetLastPosition] = detectedTarget.TargetLastPositionVisited;
            allCondition[EnumCondition.IsDecisionTimeOut] = Owner.CurrentDecision.IsDecisionTimeOut;
            allCondition[EnumCondition.RingFailed] = detectedTarget.RingFailed;
            allCondition[EnumCondition.TargetIsVisible] = detectedTarget.LastSeen < 2;
            allCondition[EnumCondition.InvestigateTheArea] = detectedTarget.ReachInvestigationPosition && detectedTarget.InvestigationTimer > 5;
            allCondition[EnumCondition.HasOffensiveTarget] = Owner.SuspiciousLevel == 1;

            if (DEBUG)
            {
                debugText.Clear();
                for (int i = 0; i < allCondition.Count; i++)
                {
                    EnumCondition index = (EnumCondition)i;
                    debugText.Add(index.ToString() + " - " + allCondition[index]);
                }
            }

            return allCondition;
        }

        Dictionary<int, float> lastHit;
        public void GetHit(IAlive target)
        {
            if (!lastHit.ContainsKey(target.Id))
                lastHit.Add(target.Id, TimeController.gameTime);
            else
                lastHit[target.Id] = TimeController.gameTime;
        }

        float GetLastHit(IAlive target)
        {
            if (!lastHit.ContainsKey(target.Id))
                return Mathf.NegativeInfinity;
            return lastHit[target.Id] - TimeController.gameTime;
        }

        public AiDecisionRule[] GetPossibleDecisions(EnumDecision start)
        {
            return Array.FindAll(rules[Owner.AwareState], x => x.startState == start || x.startState == EnumDecision.None);
        }

        public override void Init()
        {
            base.Init();
            allCondition = new Dictionary<EnumCondition, bool>();
            for (int i = 0; i < CommonHelper.GetEnumLength(typeof(EnumCondition)); i++)
            {
                allCondition.Add((EnumCondition)i, false);
            }
            lastHit = new Dictionary<int, float>();
            var allRules = DInjector.Instantiate<AiDecisionRule_SO>(DecisionRule_SO).rules;
            rules = new Dictionary<EnumAwareState, AiDecisionRule[]>();
            for (int i = 0; i < CommonHelper.GetEnumLength(typeof(EnumAwareState)); i++)
            {
                var awareState = (EnumAwareState)i;
                rules.Add(awareState, Array.FindAll(allRules, x => x.awareState == awareState).OrderBy(x => x.priority).ToArray());
            }
            IsEnable = false;
            //ressult = new List<EnumDecision>();
        }

        protected override void OnOwnerDied(NotificationParameter obj)
        {
            ownerAlive = false;
        }
        protected override void OnOwnerSpawned(NotificationParameter obj)
        {
            ownerAlive = true;
        }

        private bool HaveConditions(AiDecisionRule rule, Dictionary<EnumCondition, bool> current)
        {
            foreach (var item in rule.conditions)
            {
                if (!current.ContainsKey(item.condition) || current[item.condition] != item.value)
                    return false;
            }
            return true;
        }

        private List<AiDecisionModel> GetAllPriorityCandinate(Dictionary<EnumCondition, bool> conditions, IEnumerable<AiDecisionRule> decitions)
        {
            if (decitions.Count() == 0)
                return null;
            Dictionary<EnumDecision, AiDecisionModel> condinates = new Dictionary<EnumDecision, AiDecisionModel>();
            foreach (var decide in decitions)
            {
                if (HaveConditions(decide, conditions))
                {
                    foreach (var item in decide.possibleDecision)
                    {
                        if (condinates.ContainsKey(item.decision))
                        {
                            condinates[item.decision].chance = (condinates[item.decision].chance + item.chance) / 2f;
                        }
                        else
                            condinates.Add(item.decision, new AiDecisionModel(item));
                    }
                }
            }
            return condinates.Values.ToList();
        }

        public AiDecisionModel UpdateDecition(IDetectedTarget target)
        {
            if (ownerAlive && !ownerShocked)
            {
                Dictionary<EnumCondition, bool> conditions = UpdateCondition(target);
                AiDecisionRule[] possibleDecisions = GetPossibleDecisions(Owner.CurrentDecision.decision);

                List<int> allPriorities = new List<int>();
                foreach (var decide in possibleDecisions)
                {
                    if (!allPriorities.Contains(decide.priority))
                        allPriorities.Add(decide.priority);
                }
                allPriorities.Sort();

                //ressult.Clear();

                for (int i = 0; i < allPriorities.Count; i++)
                {
                    var condinates = GetAllPriorityCandinate(conditions, possibleDecisions.Where(x => x.priority == allPriorities[i]));
                    if (CommonHelper.IsNull(condinates))
                        continue;
                    while (condinates.Count > 0)
                    {
                        float total = 0;
                        for (int j = 0; j < condinates.Count; j++)
                        {
                            total += condinates[j].chance;
                        }
                        float rand = RandomHelper.Range(total);
                        float currentChance = 0;
                        for (int j = 0; j < condinates.Count; j++)
                        {
                            currentChance += condinates[j].chance;
                            if (rand <= currentChance)
                            {
                                return condinates[j];
                                //ressult.Add(condinates[j].decision);
                                //condinates.RemoveAt(j);
                                //j--;
                                //break;
                            }
                        }
                    }
                }
                //CLog.Error(" condinates " + condinates.Count); 
                Debug.LogWarning(" EnumDecision.None ");
            }
            return Owner.CurrentDecision;
            //CLog.Log(" allAgentBehaviourTransition " + owner.GetAgentBehaviourTransition().Count);
        }



    }
}
