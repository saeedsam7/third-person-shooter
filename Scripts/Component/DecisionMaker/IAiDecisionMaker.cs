﻿

using RCPU.Component;

namespace RCPU.Agent
{
    public interface IAiDecisionMaker : IInitializable
    {
        void GetHit(IAlive target);
        AiDecisionModel UpdateDecition(IDetectedTarget target  );
       
    }
}
