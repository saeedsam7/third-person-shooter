﻿using UnityEngine;

namespace RCPU.Agent
{
    [CreateAssetMenu(fileName = "DecisionRule", menuName = "RCPU/DataModel/Agent/DecisionRule", order = 1)]
    public class AiDecisionRule_SO : ScriptableObject
    {
        public AiDecisionRule[] rules;

        //void OnValidate()
        //{
        //    //Debug.Log("OnValidate");
        //    for (int i = 0; i < rules.Length; i++)
        //    {
        //        string ruleName = rules[i].priority + " [" + rules[i].awareState + "] IF ( ";
        //        for (int j = 0; j < rules[i].conditions.Count; j++)
        //        {
        //            ruleName += (rules[i].conditions[j].value ? "" : "! ") + rules[i].conditions[j].condition + (j == rules[i].conditions.Count - 1 ? " ) => { " : " & ");
        //        }
        //        for (int j = 0; j < rules[i].possibleDecision.Count; j++)
        //        {
        //            ruleName += rules[i].possibleDecision[j].decision + (rules[i].possibleDecision[j].agentStateTarget != EnumAgentState.None ? " + [ State : " + rules[i].possibleDecision[j].agentStateTarget + " ] " : " ") + rules[i].possibleDecision[j].chance + "%" + (j == rules[i].possibleDecision.Count - 1 ? " } " : " | ");
        //        }
        //        //rules[i].name = ruleName;
        //    }
        //}
    }
}