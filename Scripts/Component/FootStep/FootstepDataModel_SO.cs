﻿
using UnityEngine;

namespace RCPU.Component
{

    [CreateAssetMenu(fileName = "Footstep", menuName = "RCPU/DataModel/Component/FootstepDataModel", order = 5)]
    public class FootstepDataModel_SO : ScriptableObject
    {
        public FootstepDataModel model;
    }
}
