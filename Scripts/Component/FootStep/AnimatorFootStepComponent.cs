﻿
using RCPU.Agent;
using RCPU.Attributes;
using RCPU.DependencyInjection;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Component
{
    [InitOrder(EnumInitOrder.Leg)]
    public class AnimatorFootStepComponent : FootStepComponent
    {
        [InjectFromInstance] protected IAnimatorComponent anim { get; set; }


        [SerializeField] protected FootstepDataModel_SO model_SO;
        protected FootstepDataModel model;

        [InjectFromInstance] IAlive Owner { get; set; }
        protected override int OwnerId => Owner.Id;
        public override void Init()
        {
            base.Init();

            model = DInjector.Instantiate<FootstepDataModel_SO>(model_SO).model;
            anim.RegisterEvent(EnumEventReceiver.FootStep, FootStep);
            InitSfx(model.materialSfxes);
        }

        protected virtual void FootStep(List<string> param)
        {
            PlayFootStepSfx(param[0]);
        }


    }
}