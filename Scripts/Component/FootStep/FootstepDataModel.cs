﻿using RCPU.Audio;
using System;

namespace RCPU.Component
{
    [Serializable]
    public class FootstepDataModel
    {
        public MaterialSfx[] materialSfxes;
    }

    [Serializable]
    public class MaterialSfx
    {
        public EnumMaterialType materialType;
        public EnumAudioSFX audioClip;
    }
}