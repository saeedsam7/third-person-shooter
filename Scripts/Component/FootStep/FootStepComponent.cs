﻿using RCPU.Attributes;
using RCPU.Audio;
using RCPU.Helper;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Component
{
    public class FootStepComponent : ComponentBase, IFoorStep
    {

        [InjectFromSystem] protected IAudioManager AudioManager { get; set; }
        public Dictionary<EnumMaterialType, EnumAudioSFX> sfxes;
        [SerializeField] protected PairArray<string, Transform> foots;
        protected Dictionary<string, Transform> footsData;
        public override EnumComponentType ComponentType => EnumComponentType.FootStepper;
        public override void Init()
        {
            base.Init();
            footsData = foots.ToDictionary();
        }

        public void InitSfx(MaterialSfx[] materialSfxes)
        {
            sfxes = new Dictionary<EnumMaterialType, EnumAudioSFX>();
            for (int i = 0; i < materialSfxes.Length; i++)
            {
                sfxes.Add(materialSfxes[i].materialType, materialSfxes[i].audioClip);
            }
        }

        protected void PlayFootStepSfx(string footname)
        {
            if (DEBUG)
                Debug.Log(" PlayFootStepSfx  name " + name + " time " + TimeController.gameTime);

            if (footsData.ContainsKey(footname))
            {
                if (PhysicsHelper.Raycast(footsData[footname].position + (Vector3.up * .2f), Vector3.down, out RaycastHit hitInfo, 1, PhysicsHelper.Layer_Environment))
                {
                    if (DEBUG)
                        Debug.Log(" hit  name " + hitInfo.collider.name + " tag " + hitInfo.collider.tag + " point " + hitInfo.point);
                    AudioManager.PlaySfx(sfxes[TagsHelper.GetMaterialType(hitInfo.collider.tag)], footsData[footname].position);
                }
                else
               if (DEBUG)
                {
                    Debug.DrawLine(footsData[footname].position, footsData[footname].position + Vector3.down, Color.magenta, 9999);
                    //Debug.Log(" NOT hit  position " + transform.position);
                }
            }

        }


    }
}
