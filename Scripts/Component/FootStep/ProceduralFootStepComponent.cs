﻿using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Audio;
using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.NotificationSystem;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Component
{
    [InitOrder(EnumInitOrder.Leg)]
    [InjectToInstance(typeof(IProceduralLeg))]
    public class ProceduralFootStepComponent : FootStepComponent, IProceduralLeg
    {
        [InjectFromInstance] IAiPublic owner { get; set; }
        [InjectFromInstance] ILocomotionComponent locomotion { get; set; }
        [SerializeField] Transform body = default;
        [SerializeField] Transform targetTransform = default;
        [SerializeField] Vector3 footPositionOffset = default;
        [SerializeField] Vector3 footRotationOffset = default;
        [SerializeField] int group;
        [SerializeField] Color debugColor1;
        [SerializeField] Color debugColor2;
        [InjectFromInstance] IAlive Owner { get; set; }
        protected override int OwnerId => Owner.Id;

        protected Dictionary<EnumMoveRotateAbility, ProceduralLegDataModel> allDataModel;
        [SerializeField] protected ProceduralLegDataModel_SO model_SO;


        Vector3 footSpacing;
        Vector3 oldPosition, currentPosition, newPosition;
        Vector3 oldNormal, currentNormal, newNormal;
        float lerp;
        bool rotation;

        //public Vector3 Position => newPosition;
        public int Group => group;
        public bool IsMoving => (lerp < 1);

        [field: SerializeField] public EnumLegIndex LegIndex { get; protected set; }

        public float TargetOffset => IsMoving ? 0 : CommonHelper.Distance(targetTransform.position, transform.position);

        public override void Init()
        {
            base.Init();
            footSpacing = body.InverseTransformPoint(transform.position + footPositionOffset);
            currentPosition = newPosition = oldPosition = transform.position;
            currentNormal = newNormal = oldNormal = transform.up;
            lerp = 1;
            var modelInstance = DInjector.Instantiate<ProceduralLegDataModel_SO>(model_SO);
            allDataModel = new Dictionary<EnumMoveRotateAbility, ProceduralLegDataModel>
            {
                { EnumMoveRotateAbility.RotateOnly, modelInstance.rotateOnlyModel },
                { EnumMoveRotateAbility.StrafMove, modelInstance.strafMoveModel },
                { EnumMoveRotateAbility.SimpleMove, modelInstance.simpleMoveModel }
            };
            InitSfx(modelInstance.footstepData.materialSfxes);
            IsEnable = false;
        }

        protected override void OnOwnerDied(NotificationParameter obj)
        {
            base.OnOwnerDied(obj);
            IsEnable = false;
        }

        protected override void OnOwnerSpawned(NotificationParameter obj)
        {
            base.OnOwnerSpawned(obj);
            IsEnable = true;
        }

        protected Vector3 GetFootSpace()
        {
            var res = body.TransformPoint(footSpacing);
            res.y = body.position.y;
            return res;
        }

        public void Jump()
        {
            oldPosition = newPosition = currentPosition = GetFootSpace();
            transform.position = currentPosition;
        }

        public void UpdateLeg(bool canMove, bool hasLag)
        {
            if (!IsEnable || ownerShocked) return;
            transform.position = currentPosition;
            transform.up = currentNormal;
            transform.localEulerAngles += footRotationOffset;

            //if (CommonHelper.Distance(targetTransform.position, transform.position) > .3f)
            //{
            //    Debug.LogError(name + " -> " + (CommonHelper.Distance(targetTransform.position, transform.position).ToString("f2")));
            //}
            UpdateLeg(canMove, hasLag, allDataModel[locomotion.LastMoveType]);
        }

        bool IsRotating => locomotion.LastMoveType == EnumMoveRotateAbility.RotateOnly;


        void UpdateLeg(bool canMove, bool hasLag, ProceduralLegDataModel model)
        {
            if (DEBUG)
                Debug.DrawLine(owner.Position + Vector3.up, owner.Position + Vector3.up + (owner.Forward * 5f), Color.yellow);
            if (canMove && lerp == 1)
            {
                var velocity = locomotion.Velocity;
                Ray ray = new Ray(GetFootSpace(), Vector3.down);
                if (PhysicsHelper.Raycast(ray, out RaycastHit info, 5, PhysicsHelper.Layer_Environment))
                {
                    float targetDistance = CommonHelper.Distance(newPosition, info.point);
                    if (targetDistance > model.MinStepDistance)
                    {
                        Vector3 tempNewPos = info.point;
                        if (!IsRotating)
                        {
                            tempNewPos += (velocity.normalized * model.StepLength);
                        }
                        //if (DEBUG)
                        //    Debug.Log(name + " ? x " + CommonHelper.Distance(newPosition, owner.Position).ToString("f1") + " y " + CommonHelper.Distance(tempNewPos, owner.Position).ToString("f1"));
                        rotation = false;
                        lerp = 0;
                        newPosition = tempNewPos;
                        newNormal = info.normal;
                    }
                }
                if (lerp == 1 && velocity.magnitude < .1f)
                {
                    StandRotate(model);
                }
            }

            if (lerp == 1)
            {
                oldPosition = newPosition;
                oldNormal = newNormal;
            }
            else
            {
                Vector3 tempPosition = Vector3.Lerp(oldPosition, newPosition, model.MoveCurveHorizental.Evaluate(lerp));
                tempPosition.y += model.MoveCurveVertical.Evaluate(lerp) * model.StepHeight;

                currentPosition = tempPosition;
                currentNormal = Vector3.Lerp(oldNormal, newNormal, lerp);
                float boostSpeed = (rotation || hasLag || IsRotating) ? 2f : 1f;
                lerp = Mathf.Clamp(lerp + Time.deltaTime * model.SpeedCurve.Evaluate(boostSpeed * locomotion.Speed), 0, 1);

                if (canMove && lerp == 1)
                {
                    PlayFootStepSfx(name);
                }
            }
        }

        private void StandRotate(ProceduralLegDataModel model)
        {
            Vector3 targetPos = (body.forward * 100) + GetFootSpace();
            float myAngle = CommonHelper.Angle(transform, targetPos, true);
            if (myAngle > model.MinAngleToRotate)
            {
                var ray = new Ray(GetFootSpace(), Vector3.down);
                if (PhysicsHelper.Raycast(ray, out RaycastHit info, 5, PhysicsHelper.Layer_Environment))
                {
                    Vector3 tenNewPos = info.point;
                    if (CommonHelper.Distance(tenNewPos, transform.position) > model.MinStepDistance)
                    {
                        rotation = true;
                        lerp = 0;
                        newPosition = tenNewPos;
                        newNormal = info.normal;
                        PlaySfx(model.legSfx);
                    }
                }
            }
        }

        protected void PlaySfx(EnumAudioSFX sfx)
        {
            AudioManager.PlaySfx(sfx, transform.position);
        }


        private void OnDrawGizmos()
        {
            Gizmos.color = debugColor1;
            Gizmos.DrawSphere(newPosition, 0.2f);
            //Gizmos.color = Color.yellow;
            //Gizmos.DrawSphere(currentPosition, 0.4f);
            Gizmos.color = debugColor2;
            Gizmos.DrawSphere(oldPosition, 0.2f);

            //GizmosHelper.SetHandlesColor(Color.white, 1);
            //Vector3 targetPos = body.position + (body.forward * 10) + GetFootSpace();
            //targetPos.y = transform.position.y;


            //Vector3 target = transform.position;
            //Ray ray = new Ray(GetFootSpace(), Vector3.down);
            //if (PhysicsHelper.Raycast(ray, out RaycastHit info, 5, PhysicsHelper.Layer_Environment))
            //{
            //    target = info.point ;
            //}

            Debug.DrawLine(transform.position, body.position, debugColor1);
            Debug.DrawLine(transform.position + Vector3.forward * .1f, GetFootSpace(), debugColor1);
            Debug.DrawLine(transform.position, GetFootSpace(), debugColor1);
            Debug.DrawLine(transform.position - Vector3.forward * .1f, GetFootSpace(), debugColor1);

            //GizmosHelper.Label(transform.position, CommonHelper.SignedAngle(transform, targetPos, true).ToString("f0") + " - " + CommonHelper.SignedAngle(body, targetPos, true).ToString("f0"));

            //Gizmos.color = Color.white;
            //Gizmos.DrawLine(transform.position, targetPos);
            //Gizmos.color = Color.yellow;
            //Gizmos.DrawLine(transform.position, transform.forward * 10);

            //Gizmos.color = Color.white;

            //Vector3 pos = (body.position + parentOffset);
            //Vector3 forward = body.forward;
            //Vector3 right = body.right * Mathf.Sign(parentOffset.x);
            //Quaternion rotation = body.rotation;
            //if (parentOffset.x > 0)
            //{
            //    GizmosHelper.SetHandlesColor(Color.red, .5f); 
            //}
            //else
            //{
            //    GizmosHelper.SetHandlesColor(Color.blue, .5f); 
            //}
            //GizmosHelper.DrawCube(pos, rotation, 1f);
            //GizmosHelper.DrawCube(pos + forward, rotation, 1f);
            //GizmosHelper.DrawCube(pos - forward, rotation, 1f);
            //GizmosHelper.DrawCube(right + pos, rotation, 1f);
            //GizmosHelper.DrawCube(right + pos + forward, rotation, 1f);
            //GizmosHelper.DrawCube(right + pos - forward, rotation, 1f);

        }

        public void PutOnGround()
        {
            Ray ray = new Ray(GetFootSpace(), Vector3.down);
            if (PhysicsHelper.Raycast(ray, out RaycastHit info, 5, PhysicsHelper.Layer_Environment))
            {
                Vector3 tempNewPos = info.point;
                rotation = false;
                currentPosition = newPosition = tempNewPos;
                currentNormal = newNormal = info.normal;
            }
            transform.position = currentPosition;
            transform.up = currentNormal;
            transform.localEulerAngles += footRotationOffset;
            lerp = 1;
        }

        public void PutOnGround(float lerp, Vector3 fromPosition)
        {
            Ray ray = new Ray(GetFootSpace(), Vector3.down);
            if (PhysicsHelper.Raycast(ray, out RaycastHit info, 5, PhysicsHelper.Layer_Environment))
            {
                Vector3 tempNewPos = info.point;
                rotation = false;
                currentPosition = newPosition = tempNewPos;
                currentNormal = newNormal = info.normal;
            }
            transform.position = Vector3.Lerp(fromPosition, currentPosition, lerp);
        }
    }
}
