﻿using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Component;
using RCPU.NotificationSystem;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Ragdoll
{
    [InjectToInstance(typeof(IRagdollRecovery))]
    [InitOrder(EnumInitOrder.Ragdoll)]
    public class RagdollRecoveryComponent : ComponentBase, IRagdollRecovery
    {
        protected override int OwnerId => Owner.Id;
        public override EnumComponentType ComponentType => EnumComponentType.RagdollRecovery;
        [InjectFromInstance] private IAlive Owner { get; set; }
        [InjectFromInstance] private IAnimatorComponent Anim { get; set; }
        [InjectFromInstance] private IProceduralWalk ProceduralWalk { get; set; }
        [InjectFromInstance] private IIkHeightModifier IkHeightModifier { get; set; }



        [SerializeField] protected TransformDataModel[] rootBones;

        [SerializeField] protected GameObject[] gameObjects;

        protected Dictionary<string, TransformDataModel> allBones;
        protected float lerp;

        Dictionary<EnumLegIndex, TransformDataModel> legBones;
        public void Activate(Dictionary<string, TransformDataModel> targetState)
        {
            //Vector3 delta = allBones[rootBoneName].Position - targetState[rootBoneName].Position;
            //delta.y = 0;
            //Debug.Log("  delta " + delta);
            //allBones[rootBoneName].Update(delta);

            //Anim.SetActive(false);
            foreach (var bone in allBones)
            {
                if (targetState.ContainsKey(bone.Key))
                {
                    bone.Value.Update(targetState[bone.Key]);
                }
            }

            IkHeightModifier.Offset = -1.5f;

            lerp = 0;
            SetActive(true, true);
            //Debug.Break();
        }
        void SetActive(bool value, bool enable)
        {
            for (int i = 0; i < gameObjects.Length; i++)
            {
                gameObjects[i].SetActive(value);
            }
            IsEnable = enable;
        }

        public override void Init()
        {
            base.Init();
            legBones = new Dictionary<EnumLegIndex, TransformDataModel>();
            allBones = new Dictionary<string, TransformDataModel>();
            foreach (var bone in rootBones)
            {
                bone.Init();
                allBones.Add(bone.Name, bone);
                if (bone.LegIndex != EnumLegIndex.None)
                {
                    legBones.Add(bone.LegIndex, bone);
                }
            }
            SetActive(true, false);
        }

        public Dictionary<string, TransformDataModel> GetCurrentState()
        {
            //Debug.Log("1- " + allBones[rootBoneName].Position);
            ProceduralWalk.PutAllLegsOnGround();
            //Debug.Log("2- " + allBones[rootBoneName].Position);
            //Debug.Break();
            //Debug.Log("3- " + allBones[rootBoneName].Position);
            SetActive(false, false);
            return allBones;
        }

        void Update()
        {
            if (lerp < 1)
            {
                lerp = Mathf.Clamp01(lerp + TimeController.deltaTime * .33f);

                foreach (var bone in allBones)
                {
                    bone.Value.Update(lerp);
                }
                ProceduralWalk.PutAllLegsOnGround(lerp, legBones);
                IkHeightModifier.Offset = Mathf.Lerp(-1.5f, -.1f, lerp);
            }
            else
            {
                Notification.NotifyInstance(OwnerId, EnumInstanceNotifType.OwnerShockRestoredCompletly, new NotificationParameter());
                //Anim.SetActive(true);
                SetActive(true, false);
            }
        }
    }
}