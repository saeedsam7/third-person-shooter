﻿using RCPU.Component;
using System.Collections.Generic;

namespace RCPU.Ragdoll
{
    public interface IRagdollRecovery : IComponent
    {
        void Activate(Dictionary<string, TransformDataModel> curentBones);
        Dictionary<string, TransformDataModel> GetCurrentState();
    }
}