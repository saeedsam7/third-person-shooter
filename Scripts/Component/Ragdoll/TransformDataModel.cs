﻿using RCPU.Component;
using RCPU.Helper;
using System;
using UnityEngine;

namespace RCPU.Ragdoll
{
    [Serializable]
    public class TransformDataModel
    {
        [SerializeField] Transform transform;
        Rigidbody rigidbody;
        [SerializeField] float restoreSpeed = 1;
        //[SerializeField] bool isRootBone;
        Vector3 defaultPosition;
        Quaternion defaultRotation;
        public Vector3 Position { get; protected set; }
        Vector3 position;
        Quaternion rotation;
        bool exist;
        bool hasRigidbody;
        [field: SerializeField] public EnumLegIndex LegIndex { get; protected set; }

        public string Name => transform.name;



        public void Init()
        {
            rigidbody = transform.GetComponent<Rigidbody>();
            hasRigidbody = !CommonHelper.IsNull(rigidbody);
            position = defaultPosition = transform.localPosition;
            rotation = defaultRotation = transform.localRotation;
            exist = false;
        }

        public void Reset()
        {
            if (hasRigidbody)
            {
                rigidbody.velocity = Vector3.zero;
            }
            transform.localPosition = defaultPosition;
            transform.localRotation = defaultRotation;
        }

        public void Update(TransformDataModel target)
        {
            exist = true;
            position = target.transform.localPosition;
            rotation = target.transform.localRotation;
            transform.localPosition = position;
            transform.localRotation = rotation;
            Position = transform.position;
        }
        //public void Update(Vector3 delta)
        //{
        //    defaultPosition -= delta;
        //}

        public void Update(float lerp)
        {
            if (exist)
            {
                if (hasRigidbody)
                {
                    rigidbody.velocity = Vector3.zero;
                }
                //Debug.Log(transform.name + " lerp " + lerp + " localPosition " + transform.localPosition + "  position " + position + " defaultPosition " + defaultPosition);
                var pos = transform.localPosition;
                pos.y = Mathf.Lerp(position.y, defaultPosition.y, lerp);
                transform.localPosition = pos;
                //transform.localPosition = Vector3.Lerp(Position, defaultPosition, lerp);
                transform.localRotation = Quaternion.Lerp(rotation, defaultRotation, lerp);
            }
        }
    }

    //[Serializable]
    //public class RagdollBoneDataModel
    //{
    //    [SerializeField] Transform transform;
    //    [SerializeField] Transform transform;
    //}
}