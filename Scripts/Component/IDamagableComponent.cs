﻿
using RCPU.Damage;
using UnityEngine;

namespace RCPU.Component
{
    public interface IDamageableComponentPrivate : IDamageableComponentPublic
    {
        float Multiplier(EnumDamageType damageType);
        float ApplyDamage(EnumDamageType health, float finaldmg);
    }

    public interface IDamageableComponentPublic : IComponent
    {
        Vector3 WidgetPostion { get; }
        float GetCurrentCapacity(EnumDamageType damageType);
        float GetMaxCapacity(EnumDamageType damageType);
        bool IsAlive { get; }
        bool IsConnected { get; }
        bool IsWorking { get; }

    }
}
