﻿using System;
using UnityEngine;

namespace RCPU.Component
{
    [Serializable]
    public struct AnimationParamValue
    {
        public AnimationIntegerParamValue[] integerParamValue;
        public AnimationFloatParamValue[] floatParamValue;
        public AnimationTriggerParamValue[] triggerParamValue;
        public AnimationBooleanParamValue[] booleanParamValue;
        public AnimationFloatCurveParamValue[] floatCurveParamValue;

    }
    public class AnimationBaseParamValue
    {
        public AnimTagCondition[] AnimTagCompare;
        [HideInInspector] public bool HasCondition;
    }

    [Serializable]
    public class AnimationIntegerParamValue : AnimationBaseParamValue
    {
        public EnumIntegerParam param;
        public int value;
    }
    [Serializable]
    public class AnimationFloatParamValue : AnimationBaseParamValue
    {
        public EnumFloatParam param;
        public float value;
        public float time;
        [HideInInspector] public float startValue;
        [HideInInspector] public bool done;
    }
    [Serializable]
    public class AnimationFloatCurveParamValue : AnimationBaseParamValue
    {
        public EnumFloatParam param;
        public float time;
        public AnimationCurve curve;
        [HideInInspector] public bool done;
    }
    [Serializable]
    public class AnimationTriggerParamValue : AnimationBaseParamValue
    {
        public EnumTriggerParam param;
        public bool value;
    }
    [Serializable]
    public class AnimationBooleanParamValue : AnimationBaseParamValue
    {
        public EnumBooleanParam param;
        public bool value;
    }
}
