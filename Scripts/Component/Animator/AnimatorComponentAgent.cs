using RCPU.Attributes;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Component
{
    [InitOrder(EnumInitOrder.Animator)]
    [InjectToInstance(typeof(IAnimatorComponent))]
    public class AnimatorComponentAgent : AnimatorComponentBase, IAnimatorComponent
    {
        void InvokeCallback(EnumEventReceiver receiver, List<string> newParams)
        {
            if (animatorEvents.ContainsKey(receiver))
            {
                for (int i = 0; i < animatorEvents[receiver].Count; i++)
                {
                    animatorEvents[receiver][i](newParams);
                }
            }
        }
        public override void ExecuteEvent(string s)
        {
            //Logger.Log(" ExecuteEvent  "+s);
            string[] param = s.Split(splitor, StringSplitOptions.RemoveEmptyEntries);
            if (param.Length > 0)
            {
                List<string> newParams = new List<string>();
                for (int i = 1; i < param.Length; i++)
                {
                    newParams.Add(param[i]);
                }
                if (Enum.TryParse<EnumEventReceiver>(param[0], out EnumEventReceiver receiver))
                {
                    InvokeCallback(receiver, newParams);
                }
                else
                {
                    Debug.LogError(" wrong receiver " + s);
                }
            }
            else
            {
                Debug.LogError(" wrong param " + s);
            }
        }
    }
}
