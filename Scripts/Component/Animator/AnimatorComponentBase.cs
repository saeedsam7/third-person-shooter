using RCPU.Helper;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Component
{
    [RequireComponent(typeof(Animator))]
    public class AnimatorComponentBase : BaseObject, IAnimatorComponent
    {
        protected Animator anim;
        protected char[] splitor = new char[] { ',' };
        protected Dictionary<EnumEventReceiver, List<Action<List<string>>>> animatorEvents;

        private static bool isParamInited;

        private static Dictionary<EnumIntegerParam, string> integerParam;
        private static Dictionary<EnumBooleanParam, string> booleanParam;
        private static Dictionary<EnumTriggerParam, string> triggerParam;
        private static Dictionary<EnumFloatParam, string> floatParam;
        private static Dictionary<EnumAnimTag, string> tags;
        [Header("Debug")]
        [SerializeField] private bool debugFloatParameters;
        [SerializeField] private bool debugIntegerParameters;
        [SerializeField] private bool debugTriggerParameters;
        [SerializeField] private bool debugBooleanParameters;

        public EnumComponentType ComponentType => EnumComponentType.Animator;

        //EnumAnimLayer currentLayer;
        private static void InitParams()
        {
            if (!isParamInited)
            {
                isParamInited = true;
                integerParam = new Dictionary<EnumIntegerParam, string>();
                for (int i = 0; i < CommonHelper.GetEnumLength(typeof(EnumIntegerParam)); i++)
                {
                    integerParam.Add(((EnumIntegerParam)i), ((EnumIntegerParam)i).ToString());
                }
                tags = new Dictionary<EnumAnimTag, string>();
                for (int i = 0; i < CommonHelper.GetEnumLength(typeof(EnumAnimTag)); i++)
                {
                    tags.Add(((EnumAnimTag)i), ((EnumAnimTag)i).ToString());
                }
                floatParam = new Dictionary<EnumFloatParam, string>();
                for (int i = 0; i < CommonHelper.GetEnumLength(typeof(EnumFloatParam)); i++)
                {
                    floatParam.Add(((EnumFloatParam)i), ((EnumFloatParam)i).ToString());
                }
                booleanParam = new Dictionary<EnumBooleanParam, string>();
                for (int i = 0; i < CommonHelper.GetEnumLength(typeof(EnumBooleanParam)); i++)
                {
                    booleanParam.Add(((EnumBooleanParam)i), ((EnumBooleanParam)i).ToString());
                }
                triggerParam = new Dictionary<EnumTriggerParam, string>();
                for (int i = 0; i < CommonHelper.GetEnumLength(typeof(EnumTriggerParam)); i++)
                {
                    triggerParam.Add(((EnumTriggerParam)i), ((EnumTriggerParam)i).ToString());
                }
            }
        }

        public virtual void Init()
        {
            animatorEvents = new Dictionary<EnumEventReceiver, List<Action<List<string>>>>();
            this.anim = GetComponent<Animator>();
            InitParams();
        }
        //public bool IsAnimTag(string v)
        //{
        //    return anim.GetCurrentAnimatorStateInfo(0).IsTag(v);
        //}
        public bool IsStandOrMove(bool includeCrouch, bool acceptNext)
        {
            bool res = IsAnimTag(EnumAnimTag.Move, acceptNext) || IsAnimTag(EnumAnimTag.Sprint, acceptNext) /*|| IsAnimTag(AnimatorTags.MoveStart, acceptNext)*/ || IsAnimTag(EnumAnimTag.Stand, acceptNext);
            if (includeCrouch)
                res |= IsAnimTag(EnumAnimTag.Crouch, acceptNext) || IsAnimTag(EnumAnimTag.CrouchMove, acceptNext);
            return res;
        }
        public bool IsMove(bool includeCrouch, bool acceptNext)
        {
            bool res = IsAnimTag(EnumAnimTag.Move, acceptNext) || IsAnimTag(EnumAnimTag.Sprint, acceptNext) /*|| IsAnimTag(AnimatorTags.MoveStart, acceptNext)*/;
            if (includeCrouch)
                res |= IsAnimTag(EnumAnimTag.CrouchMove, acceptNext);
            return res;
        }
        public bool IsStand(bool includeCrouch, bool acceptNext)
        {
            bool res = IsAnimTag(EnumAnimTag.Stand, acceptNext);
            if (includeCrouch)
                res |= IsAnimTag(EnumAnimTag.Crouch, acceptNext);
            return res;
        }

        public bool IsRotate(bool includeCrouch, bool acceptNext)
        {
            bool res = IsAnimTag(EnumAnimTag.Rotate, acceptNext);
            if (includeCrouch)
                res |= IsAnimTag(EnumAnimTag.CrouchRotate, acceptNext);
            return res;
        }
        public bool IsAnimTag(EnumAnimTag tag, bool acceptNext)
        {
            return IsAnimTag(tag, acceptNext, EnumAnimLayer.Default);
        }
        //public bool IsAnimTag(string tag, bool acceptNext)
        //{
        //    return anim.GetCurrentAnimatorStateInfo(0).IsTag(tag) ||
        //        (acceptNext && anim.IsInTransition(0) && anim.GetNextAnimatorStateInfo(0).IsTag(tag));
        //}
        public bool IsAnimTag(EnumAnimTag tag, bool acceptNext, EnumAnimLayer layer)
        {
            int layerIndex = (int)layer;
            //if(layerIndex==1 && anim.GetCurrentAnimatorStateInfo(layerIndex).IsTag(tag))
            //{
            //    Debug.LogError(acceptNext+" tag" +tag + " Reload " + anim.GetCurrentAnimatorStateInfo(layerIndex).IsName("Aim") + " name " + anim.GetCurrentAnimatorStateInfo(layerIndex).IsName("Aim"));
            //    Debug.Break();
            //}
            return anim.GetCurrentAnimatorStateInfo(layerIndex).IsTag(tags[tag]) ||
                (acceptNext && anim.IsInTransition(layerIndex) && anim.GetNextAnimatorStateInfo(layerIndex).IsTag(tags[tag]));
        }

        public void SetTrigger(EnumTriggerParam name)
        {
            ////if (name == EnumTriggerParam.Rotate)
            if (debugTriggerParameters)
                Debug.LogError(" SetTrigger   " + name);
            anim.SetTrigger(triggerParam[name]);
        }
        //public void SetTrigger(EnumTriggerParam name, float AnimatorClipSpeedMultiplier)
        //{
        //    //Debug.LogError(" SetTrigger   " + name);
        //    SetAnimatorClipSpeedMultiplier(AnimatorClipSpeedMultiplier);
        //    anim.SetTrigger(triggerParam[name]);
        //}

        public void SetFloat(EnumFloatParam name, float value)
        {
            //if (name == EnumFloatParam.Aim)
            //    Debug.Log(" SetFloat " + floatParam[name] + "  - " + name + " : " + value);
            if (debugFloatParameters)
                Debug.Log(" SetFloat  " + name + " : " + value);
            anim.SetFloat(floatParam[name], value);
        }

        //public void SetFloat(string name, float value)
        //{
        //    anim.SetFloat(name, value);
        //}

        public void SetFloat(EnumFloatParam name, float from, float to, float normilizedTime)
        {
            //if (name == EnumFloatParam.Aim)
            //    Debug.Log(" SetFloat " + floatParam[name] + "  from " + from + " to " + to + " normilizedTime " + normilizedTime + " final : " + Mathf.Lerp(from, to, normilizedTime));
            if (debugFloatParameters)
                Debug.Log(" SetFloat  " + name + "  from " + from + " to " + to + " normilizedTime " + normilizedTime + " final : " + Mathf.Lerp(from, to, normilizedTime));
            anim.SetFloat(floatParam[name], Mathf.Lerp(from, to, normilizedTime));
            //StartCoroutine(Coroutine_SetFloat(name, value, time));
        }

        //public void SetFloatCoroutine(EnumFloatParam name, float value, float time)
        //{
        //    StartCoroutine(Coroutine_SetFloat(name, value, time));
        //}


        //IEnumerator Coroutine_SetFloat(EnumFloatParam name, float value, float time)
        //{
        //    float startTime = time;
        //    while (time > 0)
        //    {
        //        float val = CommonHelper.Linear(GetFloat(name), value, (time / startTime));
        //        //CLog.Log(" time " + time + " (time / startTime) " + (time / startTime)  + " AnimGetFloat(name) " + AnimGetFloat(name) + " val " + val);
        //        SetFloat(name, val);
        //        yield return new WaitForEndOfFrame();
        //        time -= TimeController.deltaTime;
        //    }
        //    SetFloat(name, value);
        //}


        public float GetFloat(EnumFloatParam name)
        {
            return anim.GetFloat(floatParam[name]);
        }
        public void SetBool(EnumTriggerParam name, bool value)
        {
            anim.SetBool(triggerParam[name], value);
        }
        public bool GetBool(EnumBooleanParam name)
        {
            return anim.GetBool(booleanParam[name]);
        }
        public bool GetBool(EnumTriggerParam name)
        {
            return anim.GetBool(triggerParam[name]);
        }

        public int GetInteger(EnumIntegerParam name)
        {
            return anim.GetInteger(integerParam[name]);
        }

        public AnimationTimeWeight GetCurrentNormalizedTime(string animName)
        {
            AnimatorClipInfo[] aci = anim.GetCurrentAnimatorClipInfo(0);
            if (aci.Length == 0 || !aci[0].clip.name.Equals(animName)) return AnimationTimeWeight.Zero;
            AnimatorStateInfo animationState = anim.GetCurrentAnimatorStateInfo(0);
            AnimationTimeWeight res = new AnimationTimeWeight((animationState.normalizedTime % 1), aci[0].weight);
            //if(aci[0].weight>0)
            //Debug.Log(" aci[0].weight " + aci[0].weight + " Length " + aci.Length);
            //Debug.Log(" length " + animationState.length + " normalizedTime " + animationState.normalizedTime + " name " + aci.clip.name + " IsName " + animationState.IsName("Run"));
            return res;
        }

        public float GetCurrentFrame(string animName)
        {
            AnimatorClipInfo[] aci = anim.GetCurrentAnimatorClipInfo(0);
            if (aci.Length == 0 || !aci[0].clip.name.Equals(animName)) return 0;

            AnimatorStateInfo animationState = anim.GetCurrentAnimatorStateInfo(0);
            Debug.Log(" length " + animationState.length + " normalizedTime " + animationState.normalizedTime + " F " + (animationState.normalizedTime % 1) * animationState.length * aci[0].clip.frameRate);
            return (animationState.normalizedTime % 1) * animationState.length * aci[0].clip.frameRate;
        }

        void SetAnimatorClipSpeedMultiplier(float val)
        {
            anim.SetFloat("ReloadSpeed", val);
        }

        //public void SetBool(EnumBooleanParam name, bool value, float AnimatorClipSpeedMultiplier)
        //{
        //    if (debugBooleanParameters)
        //        Debug.Log(" SetBool  " + name + " : " + value);
        //    SetAnimatorClipSpeedMultiplier(AnimatorClipSpeedMultiplier);
        //    anim.SetBool(booleanParam[name], value);
        //}
        public void SetBool(EnumBooleanParam name, bool value)
        {
            if (debugBooleanParameters)
                Debug.Log(" SetBool  " + name + " : " + value);
            //if (name == EnumBooleanParam.Aim)
            //    Debug.Log(" Aim " + value);
            anim.SetBool(booleanParam[name], value);
        }



        public void SwitchAnimLayer(EnumAnimLayer layer, float t)
        {
            if (t > 0)
                StartCoroutine(Coroutine_SwitchAnimLayerWithDelay(layer, t));
            else
                _SwitchAnimLayer(layer);
        }



        private void _SwitchAnimLayer(EnumAnimLayer layer)
        {
            //CLog.Error(" anim.layerCount "+ anim.layerCount+ " layer " + layer);
            //currentLayer = layer;
            if (anim.layerCount > (int)layer)
            {
                for (int i = 0; i < anim.layerCount; i++)
                {
                    anim.SetLayerWeight(i, 0);
                }
                anim.SetLayerWeight((int)EnumAnimLayer.Default, 1);
                anim.SetLayerWeight((int)layer, 1);
            }
        }

        private IEnumerator Coroutine_SwitchAnimLayerWithDelay(EnumAnimLayer layer, float t)
        {
            yield return new WaitForSeconds(t);
            _SwitchAnimLayer(layer);
        }
        public void ResetAnimLayer(float t)
        {
            if (t > 0)
                StartCoroutine(Coroutine_RestoreAnimLayerWithDelay(t));
            else
                _RestoreAnimLayer();
        }
        private IEnumerator Coroutine_RestoreAnimLayerWithDelay(float t)
        {
            yield return new WaitForSeconds(t);
            _RestoreAnimLayer();
        }
        private void _RestoreAnimLayer()
        {
            _SwitchAnimLayer(EnumAnimLayer.Default);
            for (int i = 0; i < anim.layerCount; i++)
            {
                anim.SetLayerWeight(i, 0);
            }
            anim.SetLayerWeight((int)EnumAnimLayer.Default, 1);
        }


        public void SetInteger(EnumIntegerParam name, int value)
        {
            if (debugIntegerParameters)
                Debug.Log(" SetInteger  " + name + " : " + value);
            anim.SetInteger(integerParam[name], value);
        }


        public void ApplyRootMotion(bool v)
        {
            anim.applyRootMotion = v;
        }
        public void RegisterEvent(EnumEventReceiver receiver, Action<List<string>> action)
        {
            if (!animatorEvents.ContainsKey(receiver))
            {
                animatorEvents.Add(receiver, new List<Action<List<string>>>());
            }
            animatorEvents[receiver].Add(action);
        }
        public virtual void ExecuteEvent(string s)
        {
        }

        public void SetActive(bool value)
        {
            anim.enabled = value;
        }
    }

}