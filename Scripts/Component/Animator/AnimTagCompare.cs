﻿

using RCPU.Agent;
using System;

namespace RCPU.Component
{
    [Serializable]
    public class AnimTagCondition
    {
        public EnumAnimLayer animLayer;
        public EnumCompareOperator CompareOperator;
        public EnumAnimTag tag; 
        public bool acceptNextState;
    }

    [Serializable]
    public class AnimTagChangeState : AnimTagCondition
    {
        public EnumAgentState State;
    }
}
