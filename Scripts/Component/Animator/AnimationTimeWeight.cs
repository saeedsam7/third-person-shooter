﻿

namespace RCPU.Component
{
    public struct AnimationTimeWeight
    {
        public float time;
        public float weight;
        public static AnimationTimeWeight Zero => new AnimationTimeWeight(0, 0);

        public AnimationTimeWeight(float time, float weight)
        {
            this.time = time;
            this.weight = weight;
        }
    }
}
