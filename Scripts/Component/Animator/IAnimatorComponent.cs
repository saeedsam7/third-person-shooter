
using System;
using System.Collections.Generic;

namespace RCPU.Component
{
    public interface IAnimatorComponent : IComponent
    {
        //bool IsAnimTag(string tag);
        //bool IsAnimTag(string tag, bool acceptNext);
        bool IsAnimTag(EnumAnimTag tag, bool acceptNext, EnumAnimLayer layer);
        void SetTrigger(EnumTriggerParam v);
        //void SetTrigger(EnumTriggerParam v, float AnimatorClipSpeedMultiplier);
        void SetFloat(EnumFloatParam name, float value);
        //void SetFloat(string name, float value);

        public void SetFloat(EnumFloatParam name, float from, float to, float normilizedTime);
        //public void SetFloatCoroutine(EnumFloatParam name, float value, float time);
        //void Init(Animator anim);
        float GetCurrentFrame(string animName);
        void ApplyRootMotion(bool v);
        //void SetBool(EnumBooleanParam name, bool value, float AnimatorClipSpeedMultiplier);
        void SetBool(EnumBooleanParam name, bool value);
        void SetBool(EnumTriggerParam name, bool value);
        void SetInteger(EnumIntegerParam name, int value);
        bool GetBool(EnumBooleanParam name);
        bool GetBool(EnumTriggerParam name);
        float GetFloat(EnumFloatParam name);
        void RegisterEvent(EnumEventReceiver receiver, Action<List<string>> action);
        int GetInteger(EnumIntegerParam name);
        void ExecuteEvent(string s);
        //void SwitchAnimLayer(EnumAnimLayer layer, float t);
        void ResetAnimLayer(float t);
        //MAnimationTimeWeight GetCurrentNormalizedTime(string animName);
        bool IsStandOrMove(bool includeCrouch, bool acceptNext);
        bool IsMove(bool includeCrouch, bool acceptNext);
        bool IsStand(bool includeCrouch, bool acceptNext);
        bool IsRotate(bool includeCrouch, bool acceptNext);
        void SetActive(bool value);
    }
}