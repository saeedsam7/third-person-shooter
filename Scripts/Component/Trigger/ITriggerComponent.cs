
using RCPU.Trigger;

namespace RCPU.Component
{
    public interface ITriggerComponent : IComponent
    {
        void TriggerEnter(string tag, int v);
        void TriggerExit(string tag, int v);
        void Interact();
        void AddPickup(IPickupTrigger triggerPickup);
        void RemovePickup(IPickupTrigger triggerPickup);
        int Pickup(IPickupItem pickupAmmo);
    }
}