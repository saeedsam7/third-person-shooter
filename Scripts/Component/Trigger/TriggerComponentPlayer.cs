﻿using RCPU.Attributes;
using RCPU.Helper;
using RCPU.Inventory;
using RCPU.Trigger;
using RCPU.UI;
using System.Collections.Generic;
using System.Linq;

namespace RCPU.Component
{
    public class TriggerComponentPlayer : TriggerComponentBase, ITriggerComponent
    {
        [InjectFromInstance] protected IInvontoryComponent InvontoryComponent { get; set; }
        [InjectFromSystem] protected IUIManager UIManager { get; set; }
        protected Dictionary<int, IPickupTrigger> allIPickupTriggers;
        int activePickup;
        public override void Init()
        {
            base.Init();
            allIPickupTriggers = new Dictionary<int, IPickupTrigger>();
        }

        public override void Interact()
        {
            if (allIPickupTriggers.Count > 0)
            {
                base.Interact();
                var next = allIPickupTriggers[allIPickupTriggers.Keys.First()];
                next.Interact(this);
            }
        }

        public override void TriggerExit(string tag, int id)
        {
            base.TriggerExit(tag, id);
            var trigger = triggersManager.GetTrigger(id);
            if (!CommonHelper.IsNull(trigger))
            {
                trigger.ExitTrigger(this);
            }
        }

        public override void TriggerEnter(string tag, int id)
        {
            base.TriggerEnter(tag, id);
            var trigger = triggersManager.GetTrigger(id);
            if (!CommonHelper.IsNull(trigger))
            {
                trigger.EnterTrigger(this);
            }
        }

        public override void AddPickup(IPickupTrigger triggerPickup)
        {
            if (!allIPickupTriggers.ContainsKey(triggerPickup.Id))
            {
                allIPickupTriggers.Add(triggerPickup.Id, triggerPickup);
                SetActivePickup(triggerPickup.Id);
            }
        }

        void SetActivePickup(int id)
        {
            activePickup = id;
            //UIManager.SetTextPosition(EnumUiTextIndex.Pickup, allIPickupTriggers[activePickup].Position + (Vector3.up * .5f));
            UIManager.SetText(EnumUiTextIndex.Pickup, allIPickupTriggers[activePickup].Message);
            //UIManager.SetImagePosition(EnumUiImageIndex.Pickup, allIPickupTriggers[activePickup].Position + (Vector3.up * 1f));
            UIManager.SetImageSprite(EnumUiImageIndex.Pickup, allIPickupTriggers[activePickup].Icon);
        }

        public override void RemovePickup(IPickupTrigger triggerPickup)
        {
            if (allIPickupTriggers.ContainsKey(triggerPickup.Id))
            {
                allIPickupTriggers.Remove(triggerPickup.Id);
                if (activePickup == triggerPickup.Id)
                {
                    if (allIPickupTriggers.Count > 0)
                    {
                        SetActivePickup(allIPickupTriggers.Keys.First());
                    }
                    else
                    {
                        activePickup = 0;
                        UIManager.SetText(EnumUiTextIndex.Pickup, "");
                        UIManager.SetImageSprite(EnumUiImageIndex.Pickup, EnumSpriteIndex.None);
                    }
                }
            }
        }

        public override int Pickup(IPickupItem pickupItem)
        {
            return InvontoryComponent.Add(pickupItem);
        }


    }
}
