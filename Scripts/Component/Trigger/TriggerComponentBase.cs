using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Trigger;
using UnityEngine;

namespace RCPU.Component
{
    [RequireComponent(typeof(Rigidbody), typeof(Collider))]
    [InjectToInstance(typeof(ITriggerComponent))]
    [InitOrder(EnumInitOrder.TriggerComponent)]
    public class TriggerComponentBase : ComponentBase, ITriggerComponent
    {
        [InjectFromInstance] protected IAnimatorComponent anim { get; set; }
        [InjectFromInstance] protected IAgent Owner { get; set; }
        [InjectFromSystem] protected ITriggersManager triggersManager { get; set; }


        public override EnumComponentType ComponentType => EnumComponentType.Trigger;
        protected override int OwnerId { get => Owner.Id; }



        //protected virtual void EnterTrigger(ITrigger region)
        //{
        //}
        //protected virtual void ExitTrigger(ITrigger region)
        //{
        //}

        public virtual void Interact()
        {
        }

        public virtual void TriggerExit(string tag, int id)
        {

        }

        public virtual void TriggerEnter(string tag, int id)
        {

        }

        //public virtual void CollisionEnter(string tag, int id)
        //{
        //}

        //public virtual void CollisionExit(string tag, int id)
        //{
        //}

        private void OnTriggerEnter(Collider collider)
        {
            TriggerEnter(collider.tag, collider.GetInstanceID());
        }

        private void OnTriggerExit(Collider collider)
        {
            TriggerExit(collider.tag, collider.GetInstanceID());
        }

        public virtual void AddPickup(IPickupTrigger triggerPickup)
        {
            throw new System.NotImplementedException();
        }

        public virtual void RemovePickup(IPickupTrigger triggerPickup)
        {
            throw new System.NotImplementedException();
        }

        public virtual int Pickup(IPickupItem pickupAmmo)
        {
            throw new System.NotImplementedException();
        }
    }
}
