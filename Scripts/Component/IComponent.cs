﻿

namespace RCPU.Component
{
    public interface IComponent : IInitializable
    {
        EnumComponentType ComponentType { get; }
    }
}
