﻿
using RCPU.Attributes;
using RCPU.NotificationSystem;
using RCPU.UI;

namespace RCPU.Damage
{
    [InjectToInstance(typeof(IDamageManager))]
    [InitOrder(EnumInitOrder.DamageReceiver)]
    public class PlayerDamageManager : DamageManager
    {
        [InjectFromSystem] protected IUiPlayerWidget PlayerWidget { get; set; }



        protected override void OnOwnerSpawned(NotificationParameter obj)
        {
            base.OnOwnerSpawned(obj);
            UpdateHealthUI();
            UpdateShieldUI();
        }

        protected override void OnDamageRecieved(IDamageModel damage)
        {
            base.OnDamageRecieved(damage);
            UiManager.BindIndicatorToDamage(damage.OwnerPosition);
            UpdateHealthUI();
            UpdateShieldUI();
        }

        void UpdateHealthUI()
        {
            PlayerWidget.UpdateHealth(damageCap[EnumDamageType.Health], damageMaxCap[EnumDamageType.Health]);
        }

        void UpdateShieldUI()
        {
            PlayerWidget.UpdateShield(damageCap[EnumDamageType.Shock], damageMaxCap[EnumDamageType.Shock]);
        }
    }
}