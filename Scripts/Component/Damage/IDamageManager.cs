﻿

using RCPU.Agent;
using RCPU.Component;

namespace RCPU.Damage
{
    public interface IDamageManager : IComponent, IDamageMangerPublicData
    {
        //int OwnerId { get; }
        IAlive GetOwner();
        void ApplyDamage(IDamageModel damageModel);
    }

    public interface IDamageMangerPublicData
    {
        EnumFamily Family { get; }
        public float GetCurrentCapacity(EnumDamageType damageType);
        public float GetMaxCapacity(EnumDamageType damageType);
        public IDamageableComponentPublic[] GetAllDamageableComponent();
    }
}