namespace RCPU.Damage
{
    public enum EnumBodyPart
    {
        Body, Weakness, Component, Weapon
    }

    //public enum EnumSocketType
    //{
    //    Health, Armor, Weapon, Component
    //}

    public enum EnumDamageType
    {
        Health, Shock, Link
    }

    public enum EnumDamageSource
    {
        Melee, Bullet, Explosive
    }
}