using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Component;
using RCPU.DependencyInjection;
using RCPU.NotificationSystem;
using RCPU.Pool;
using RCPU.Vfx;
using UnityEngine;

namespace RCPU.Damage
{
    [RequireComponent(typeof(Collider))]
    //[InjectToInstance(typeof(IDamageReceiver))]
    [InitOrder(EnumInitOrder.DamageReceiver)]
    public class DamageReceiver : DamageableComponent, IDamageReceiver
    {
        protected override int OwnerId { get => Owner.Id; }
        [InjectFromInstance] protected IAlive Owner { get; set; }
        [InjectFromSystem] protected IPool Pool { get; set; }
        protected DamageReceiverDataModel model;
        [SerializeField] protected DamageReceiverDataModel_SO model_SO;

        public override EnumComponentType ComponentType => EnumComponentType.DamageReceiver;

        protected EnumBodyPart BodyPart => model.bodyPart;
        public Vector3 Position => transform.position;
        public float Size => model.size;


        public override void Init()
        {
            base.Init();
            this.model = DInjector.Instantiate<DamageReceiverDataModel_SO>(model_SO).model;
            for (int i = 0; i < model.HealthProperties.Length; i++)
            {
                Properties[model.HealthProperties[i].damageType] = model.HealthProperties[i];
            }
            IsEnable = false;
        }


        protected override void OnOwnerSpawned(NotificationParameter obj)
        {
            base.OnOwnerSpawned(obj);
            for (int i = 0; i < model.HealthProperties.Length; i++)
            {
                currentCapacity[model.HealthProperties[i].damageType] = Property(model.HealthProperties[i].damageType).capacity;
            }
        }

        protected override void Destruct()
        {
            base.Destruct();
            MakeVfx(model.destructVfx);
        }

        protected override void Detach()
        {
            base.Detach();
            MakeVfx(model.detachVfx);
        }

        protected void MakeVfx(EnumVfxType vfxType)
        {
            if (vfxType != Vfx.EnumVfxType.None)
            {
                Pool.Activate(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.position, EnumNotifParam.rotation },
                                                   new object[] { Pool.GetEquivalentType(vfxType), transform.position, transform.rotation }));
            }
        }
    }
}