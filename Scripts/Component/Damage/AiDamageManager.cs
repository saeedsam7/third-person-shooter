﻿
using RCPU.Agent;
using RCPU.Attributes;
using UnityEngine;

namespace RCPU.Damage
{
    [InjectToInstance(typeof(IDamageManager))]
    [InitOrder(EnumInitOrder.DamageReceiver)]
    public class AiDamageManager : DamageManager
    {
        protected override void OnDamageRecieved(EnumDamageType damageType, float amount, float multiplier, Vector3 position)
        {
            base.OnDamageRecieved(damageType, amount, multiplier, position);
            UiManager.BindDamageText(Owner as IAiPublic, position, damageType, Mathf.FloorToInt(amount), multiplier);
        }

    }
}