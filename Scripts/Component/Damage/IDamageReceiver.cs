
using RCPU.Component;
using UnityEngine;

namespace RCPU.Damage
{
    public interface IDamageReceiver : IDamageableComponentPrivate
    {

        //float Size { get; }
        Vector3 Position { get; }
    }


}