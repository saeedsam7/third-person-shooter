﻿
using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Component;
using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.NotificationSystem;
using RCPU.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Damage
{
    public class DamageManager : ComponentBase, IDamageManager
    {
        [InjectFromInstance] protected IAlive Owner { get; set; }
        [InjectFromSystem] protected IUIManager UiManager { get; set; }
        [InjectFromInstance] protected IDamageableComponentPrivate[] injectedDamageableComponent { set; get; }
        //[InjectFromInstance] protected IDamageReceiver[] injectedDamageReceiver { set; get; }
        [InjectFromSystem] protected ILevelManager levelManager { set; get; }
        public override EnumComponentType ComponentType => EnumComponentType.DamageManager;
        protected override int OwnerId => Owner.Id;
        public IAlive GetOwner() => this.Owner;
        protected Dictionary<int, IDamageableComponentPrivate> damageReceiver { set; get; }
        protected Dictionary<EnumDamageType, float> damageMaxCap { set; get; }
        protected Dictionary<EnumDamageType, float> damageCap { set; get; }
        protected Dictionary<EnumDamageType, bool> damageApplied;
        protected Dictionary<EnumDamageType, bool> isRegenActive;
        protected Dictionary<EnumDamageType, Coroutine> coroutineRegen;

        protected Dictionary<EnumDamageType, HealthRegenDataModel> healthRegen;
        public float GetCurrentCapacity(EnumDamageType damageType) => damageCap[damageType];
        public float GetMaxCapacity(EnumDamageType damageType) => damageMaxCap[damageType];
        public EnumFamily Family => Owner.Family;
        protected HealthDataModel model;
        [SerializeField] protected HealthDataModel_SO model_SO;

        public override void Init()
        {
            base.Init();
            this.model = DInjector.Instantiate<HealthDataModel_SO>(model_SO).model;
            damageReceiver = new Dictionary<int, IDamageableComponentPrivate>();
            if (!CommonHelper.IsNull(injectedDamageableComponent))
            {
                //injectedDamageReceiver = injectedDamageReceiver.OrderBy(x => x.Size).ToArray();
                for (int i = 0; i < injectedDamageableComponent.Length; i++)
                {
                    levelManager.RegisterDamageManager(injectedDamageableComponent[i].Id, this);
                    damageReceiver.Add(injectedDamageableComponent[i].Id, injectedDamageableComponent[i]);
                }
            }
            damageMaxCap = new Dictionary<EnumDamageType, float>();
            damageCap = new Dictionary<EnumDamageType, float>();
            damageApplied = new Dictionary<EnumDamageType, bool>();
            isRegenActive = new Dictionary<EnumDamageType, bool>();
            coroutineRegen = new Dictionary<EnumDamageType, Coroutine>();
            healthRegen = new Dictionary<EnumDamageType, HealthRegenDataModel>();
            for (int i = 0; i < CommonHelper.GetEnumLength(typeof(EnumDamageType)); i++)
            {
                EnumDamageType damageType = (EnumDamageType)i;
                damageApplied.Add(damageType, false);
                isRegenActive.Add(damageType, false);
                damageCap.Add(damageType, 0);
                damageMaxCap.Add(damageType, 0);
                coroutineRegen.Add(damageType, null);
                //healthRegen.Add(damageType, HealthRegenDataModel.Empty);
            }
        }

        protected override void OnOwnerSpawned(NotificationParameter obj)
        {
            base.OnOwnerSpawned(obj);
            //foreach (var dReceiver in damageReceiver)
            //{
            //    for (int i = 0; i < CommonHelper.GetEnumLength(typeof(EnumDamageType)); i++)
            //    {
            //        EnumDamageType damageType = (EnumDamageType)i;
            //        damageCap[damageType] += dReceiver.Value.Capacity(damageType);
            //        damageMaxCap[damageType] += dReceiver.Value.Capacity(damageType);
            //    }
            //}
            for (int i = 0; i < CommonHelper.GetEnumLength(typeof(EnumDamageType)); i++)
            {
                EnumDamageType damageType = (EnumDamageType)i;
                damageApplied[damageType] = false;
                damageCap[damageType] = 0;
                damageMaxCap[damageType] = 0;
                isRegenActive[damageType] = false;
            }
            foreach (var capacity in model.capacity.data)
            {
                damageCap[capacity.key] += capacity.value;
                damageMaxCap[capacity.key] += capacity.value;
            }
            foreach (var regen in model.regen.data)
            {
                healthRegen[regen.key] = regen.value.Clone();
            }
        }

        public virtual void ApplyDamage(IDamageModel dmgFull)
        {
            if (damageCap[EnumDamageType.Health] > 0 && damageReceiver.ContainsKey(dmgFull.ReceiverId))
            {

                bool anyDamageApplied = false;
                IDamageableComponentPrivate dReceiver = damageReceiver[dmgFull.ReceiverId];
                if (dReceiver.IsConnected && dReceiver.IsAlive)
                {
                    bool dApplied = ApplyDamage(dmgFull, dReceiver, EnumDamageType.Health, dmgFull.HitPoint);
                    anyDamageApplied |= damageApplied[EnumDamageType.Health] = dApplied;

                    dApplied = ApplyDamage(dmgFull, dReceiver, EnumDamageType.Shock, dmgFull.HitPoint);
                    anyDamageApplied |= damageApplied[EnumDamageType.Shock] = dApplied;

                    dApplied = ApplyDamage(dmgFull, dReceiver, EnumDamageType.Link, dmgFull.HitPoint);
                    anyDamageApplied |= damageApplied[EnumDamageType.Link] = dApplied;

                }
                if (anyDamageApplied)
                {
                    OnDamageRecieved(dmgFull);
                }
            }
        }

        protected bool ApplyDamage(IDamageModel dmgFull, IDamageableComponentPrivate dReceiver, EnumDamageType damageType, Vector3 position)
        {
            var dmg = dmgFull.DamagePower(damageType);
            var multiplier = dReceiver.Multiplier(damageType);
            var finaldmg = dmg * multiplier;
            if (finaldmg > 0 && damageCap[damageType] > 0)
            {
                dReceiver.ApplyDamage(damageType, finaldmg);
                OnDamageRecieved(damageType, finaldmg, multiplier, position);
                damageCap[damageType] -= finaldmg;
                if (damageCap[damageType] < 0)
                    damageCap[damageType] = 0;

                return true;
            }
            return false;
        }

        protected virtual void OnDamageRecieved(EnumDamageType damageType, float amount, float multiplier, Vector3 position)
        {

        }

        protected virtual void OnDamageRecieved(IDamageModel damageModel)
        {
            if (damageApplied[EnumDamageType.Health] && damageCap[EnumDamageType.Health] <= 0)
            {
                Notification.NotifyInstance(OwnerId, EnumInstanceNotifType.OwnerDied, new NotificationParameter());
                StopAllRegens();
            }
            else
            {
                if (damageApplied[EnumDamageType.Shock] && damageCap[EnumDamageType.Shock] <= 0)
                {
                    Notification.NotifyInstance(OwnerId, EnumInstanceNotifType.OwnerShocked, new NotificationParameter());
                    StartRegen(EnumDamageType.Shock);
                }
            }
            NotifyDamageReceived(damageModel.OwnerId);
        }
        protected void StartRegen(EnumDamageType damageType)
        {
            if (healthRegen.ContainsKey(damageType) && healthRegen[damageType].amountPerSecond > 0 && !isRegenActive[damageType])
            {
                coroutineRegen[damageType] = StartCoroutine(CoroutineRegen(damageType, healthRegen[damageType].coolDown, healthRegen[damageType].amountPerSecond));
            }
        }
        protected void StopAllRegens()
        {
            for (int i = 0; i < CommonHelper.GetEnumLength<EnumDamageType>(); i++)
            {
                EnumDamageType damageType = (EnumDamageType)i;
                if (isRegenActive[damageType])
                {
                    StopCoroutine(coroutineRegen[damageType]);
                    isRegenActive[damageType] = false;
                }
            }

        }

        protected IEnumerator CoroutineRegen(EnumDamageType damageType, float coolDown, float amountPerSecond)
        {
            isRegenActive[damageType] = true;
            float t = coolDown;
            while (t > 0)
            {
                yield return new WaitForEndOfFrame();
                t -= TimeController.deltaTime;
            }
            while (damageCap[damageType] < damageMaxCap[damageType])
            {
                yield return new WaitForEndOfFrame();
                damageCap[damageType] = Mathf.Clamp(damageCap[damageType] + (amountPerSecond * TimeController.deltaTime), 0, damageMaxCap[damageType]);
                NotifyHealthHasChanged();
            }
            isRegenActive[damageType] = false;
            if (damageCap[EnumDamageType.Health] > 0 && damageType == EnumDamageType.Shock)
            {
                Notification.NotifyInstance(OwnerId, EnumInstanceNotifType.OwnerShockRestoredInitialy, new NotificationParameter());
            }

        }

        private void NotifyHealthHasChanged()
        {
            var param = new NotificationParameter(EnumNotifParam.health, damageCap[EnumDamageType.Health])
            {
                { EnumNotifParam.healthCap, damageMaxCap[EnumDamageType.Health] },
                { EnumNotifParam.shockCap,  damageMaxCap[EnumDamageType.Shock] },
                { EnumNotifParam.shock, damageCap[EnumDamageType.Shock] },
                { EnumNotifParam.linkCap,  damageMaxCap[EnumDamageType.Link] },
                { EnumNotifParam.link, damageCap[EnumDamageType.Link] },
            };
            Notification.NotifyInstance(OwnerId, EnumInstanceNotifType.HealthAmountHasChanged, param);
        }

        private void NotifyDamageReceived(int damageOwnerId)
        {
            var param = new NotificationParameter(EnumNotifParam.health, damageCap[EnumDamageType.Health])
            {
                { EnumNotifParam.healthCap, damageMaxCap[EnumDamageType.Health] },
                { EnumNotifParam.shockCap,  damageMaxCap[EnumDamageType.Shock] },
                { EnumNotifParam.shock, damageCap[EnumDamageType.Shock] },
                { EnumNotifParam.linkCap,  damageMaxCap[EnumDamageType.Link] },
                { EnumNotifParam.link, damageCap[EnumDamageType.Link] },
                { EnumNotifParam.ownerID, damageOwnerId },
            };
            Notification.NotifyInstance(OwnerId, EnumInstanceNotifType.HealthAmountHasChanged, param);
            Notification.NotifyInstance(OwnerId, EnumInstanceNotifType.DamageReceived, param);
        }

        public IDamageableComponentPublic[] GetAllDamageableComponent()
        {
            return injectedDamageableComponent;
        }
    }
}