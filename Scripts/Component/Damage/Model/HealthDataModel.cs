﻿using RCPU.Damage;
using System;

namespace RCPU.Component
{
    [Serializable]
    public class HealthDataModel
    {
        public PairArray<EnumDamageType, float> capacity;
        public PairArray<EnumDamageType, HealthRegenDataModel> regen;

    }

    [Serializable]
    public class HealthRegenDataModel
    {
        public static HealthRegenDataModel Empty => new HealthRegenDataModel() { amountPerSecond = 0, coolDown = 0 };
        public HealthRegenDataModel Clone() => new HealthRegenDataModel() { amountPerSecond = this.amountPerSecond, coolDown = this.coolDown };
        public float amountPerSecond;
        public float coolDown;


    }
}