﻿using System;

namespace RCPU.Damage
{

    [Serializable]
    public class DamagePowerModel
    {
        public PairArray<EnumDamageType, float> damage;
    }
}