using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Damage
{
    public class DamageModel : IDamageModel
    {
        //public int id;
        //public int ownerId;
        //public Vector3 ownerPosition;
        //public Vector3 hitPoint;  
        public Dictionary<EnumDamageType, float> damagePower;
        //public int receiverId;

        public int Id { get; protected set; }
        public int OwnerId { get; protected set; }
        public Vector3 HitPoint { get; protected set; }
        //public int damageOwner;
        public float DamagePower(EnumDamageType damageType) => damagePower[damageType];
        public int ReceiverId { get; protected set; }
        public EnumDamageSource DamageSource { get; protected set; }
        public Vector3 OwnerPosition { get; protected set; }

        public DamageModel(int id, int ownerId, int receiverId, Vector3 ownerPosition, PairArray<EnumDamageType, float> damagePower, Vector3 hitPoint, EnumDamageSource damageSource)
        {
            this.damagePower = new Dictionary<EnumDamageType, float>();
            for (int i = 0; i < damagePower.data.Length; i++)
            {
                this.damagePower.Add(damagePower.data[i].key, damagePower.data[i].value);
            }
            this.Id = id;
            this.HitPoint = hitPoint;
            this.OwnerId = ownerId;
            this.ReceiverId = receiverId;
            this.OwnerPosition = ownerPosition;
            this.DamageSource = damageSource;
        }
    }

}