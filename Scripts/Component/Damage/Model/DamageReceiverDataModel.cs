﻿

using RCPU.Vfx;
using System;

namespace RCPU.Damage
{

    [Serializable]
    public class DamageReceiverDataModel : DamageableComponentDataModel
    {
        public EnumBodyPart bodyPart;
        public float size;
        public EnumVfxType destructVfx;
        public EnumVfxType detachVfx;

    }

    public class DamageableComponentDataModel
    {
        public DamageReceiverProperty[] HealthProperties;

    }

    [Serializable]
    public class DamageReceiverProperty
    {
        public static DamageReceiverProperty Empty(EnumDamageType damageType) => new DamageReceiverProperty() { damageType = damageType, breakable = false, capacity = 0, multiplier = 0 };
        public EnumDamageType damageType;
        public float multiplier;
        public float capacity;
        public bool breakable;
    }
}