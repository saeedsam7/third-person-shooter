
using UnityEngine;

namespace RCPU.Damage
{
    public interface IDamageModel
    {
        int Id { get; }
        int OwnerId { get; }
        Vector3 OwnerPosition { get; }
        Vector3 HitPoint { get; }
        int ReceiverId { get; }
        EnumDamageSource DamageSource { get; }
        float DamagePower(EnumDamageType damageType);
    }
}