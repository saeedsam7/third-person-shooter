﻿using UnityEngine;

namespace RCPU.Component
{
    [CreateAssetMenu(fileName = "Health", menuName = "RCPU/DataModel/Component/Health", order = 1)]
    public class HealthDataModel_SO : ScriptableObject
    {
        public HealthDataModel model;
    }
}