﻿
using RCPU.Damage;
using UnityEngine;

namespace RCPU.Component
{
    [CreateAssetMenu(fileName = "DamageReceiver", menuName = "RCPU/DataModel/Component/DamageReceiver", order = 1)]
    public class DamageReceiverDataModel_SO : ScriptableObject
    {
        public DamageReceiverDataModel model;
    }
}