﻿
using RCPU.NotificationSystem;

namespace RCPU.Component
{
    public class ComponentBase : BaseObject, IComponent
    {
        public virtual EnumComponentType ComponentType => throw new System.NotImplementedException();
        protected bool ownerShocked;
        protected virtual int OwnerId { get => throw new System.Exception(); }
        public virtual void Init()
        {
            Notification.RegisterInstance(OwnerId, EnumInstanceNotifType.OwnerDied, Id, OnOwnerDied);
            Notification.RegisterInstance(OwnerId, EnumInstanceNotifType.OwnerSpawned, Id, OnOwnerSpawned);
            Notification.RegisterInstance(OwnerId, EnumInstanceNotifType.OwnerShocked, Id, OnOwnerShocked);
            Notification.RegisterInstance(OwnerId, EnumInstanceNotifType.OwnerShockRestoredCompletly, Id, OnOwnerShockRestoredCompletly);
        }

        protected virtual void OnOwnerDied(NotificationParameter obj)
        {

        }
        protected virtual void OnOwnerSpawned(NotificationParameter obj)
        {
            ownerShocked = false;
        }

        protected virtual void OnOwnerShocked(NotificationParameter parameter)
        {
            ownerShocked = true;
        }


        protected virtual void OnOwnerShockRestoredCompletly(NotificationParameter parameter)
        {
            ownerShocked = false;
        }
    }
}