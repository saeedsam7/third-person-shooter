﻿using RCPU.Attributes;
using RCPU.Component;
using RCPU.Helper;
using RCPU.NotificationSystem;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Damage
{
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(Rigidbody))]
    [InjectToInstance(typeof(IDamageableComponentPrivate))]
    [InitOrder(EnumInitOrder.DamageReceiver)]
    public class DamageableComponent : ComponentBase, IDamageableComponentPrivate
    {
        public override int Id { get { if (_Id == 0) _Id = GetComponent<Collider>().GetInstanceID(); return _Id; } protected set { _Id = value; } }


        protected Dictionary<EnumDamageType, DamageReceiverProperty> Properties;
        [SerializeField] protected Transform detachedPart;

        protected Quaternion defaultRotation;
        protected Vector3 defaultPosition;
        protected Transform defaultParent;
        protected Rigidbody rigid;

        public float Multiplier(EnumDamageType damageType)
        {
            return Properties[damageType].multiplier;
        }

        public float GetCurrentCapacity(EnumDamageType damageType)
        {
            return currentCapacity[damageType];
        }

        public float GetMaxCapacity(EnumDamageType damageType)
        {
            return Properties[damageType].capacity;
        }

        protected DamageReceiverProperty Property(EnumDamageType damageType)
        {
            return Properties[damageType];
        }


        protected Dictionary<EnumDamageType, float> currentCapacity;
        public bool IsAlive => (!Properties[EnumDamageType.Health].breakable || currentCapacity[EnumDamageType.Health] > 0);
        public bool IsWorking => (!Properties[EnumDamageType.Shock].breakable || currentCapacity[EnumDamageType.Shock] > 0);
        public bool IsConnected => (!Properties[EnumDamageType.Link].breakable || currentCapacity[EnumDamageType.Link] > 0);

        public Vector3 WidgetPostion => transform.position;

        public override void Init()
        {
            base.Init();
            rigid = GetComponent<Rigidbody>();
            Properties = new Dictionary<EnumDamageType, DamageReceiverProperty>();
            currentCapacity = new Dictionary<EnumDamageType, float>();
            for (int i = 0; i < CommonHelper.GetEnumLength<EnumDamageType>(); i++)
            {
                EnumDamageType damageType = (EnumDamageType)i;
                Properties.Add(damageType, DamageReceiverProperty.Empty(damageType));
                currentCapacity.Add(damageType, Properties[damageType].capacity);
            }
        }
        protected override void OnOwnerSpawned(NotificationParameter obj)
        {
            base.OnOwnerSpawned(obj);
            Attach();
        }


        public float ApplyDamage(EnumDamageType daamageType, float amount)
        {
            float totalDamage = 0;
            if (currentCapacity[daamageType] > 0)
            {
                if (currentCapacity[daamageType] >= amount)
                {
                    totalDamage = amount;
                }
                else
                {
                    totalDamage = currentCapacity[daamageType];
                }
                currentCapacity[daamageType] -= totalDamage;
                if (Property(daamageType).breakable && currentCapacity[daamageType] <= 0)
                {
                    Notification.NotifyInstance(OwnerId, EnumInstanceNotifType.DamageReceiverBroken, new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.Id, EnumNotifParam.damageType }, new object[] { this.Id, daamageType }));
                    switch (daamageType)
                    {
                        case EnumDamageType.Link: Detach(); break;
                        case EnumDamageType.Health: Destruct(); break;
                        case EnumDamageType.Shock: Shocked(); break;
                    }
                }

            }

            return totalDamage;
        }

        protected virtual void Shocked()
        {
        }

        protected virtual void Destruct()
        {
            gameObject.SetActive(false);
        }

        protected virtual void Detach()
        {
            if (!CommonHelper.IsNull(detachedPart))
            {
                detachedPart.gameObject.SetActive(true);
                gameObject.SetActive(false);

                detachedPart.position = transform.position;
                detachedPart.rotation = transform.rotation;
                detachedPart.parent = null;
            }
        }

        protected void Attach()
        {
            if (!CommonHelper.IsNull(detachedPart))
            {
                detachedPart.gameObject.SetActive(false);

                detachedPart.parent = transform.parent;
                detachedPart.position = transform.position;
                detachedPart.rotation = transform.rotation;
            }
            gameObject.SetActive(true);
        }
    }
}