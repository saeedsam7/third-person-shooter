﻿
using RCPU.Agent;
using RCPU.Attributes;
using RCPU.NotificationSystem;
using RCPU.Weapon;
using UnityEngine;

namespace RCPU.Component
{
    public class IkComponentPlayer : IkComponentBase
    {
        [SerializeField] protected bool DebugApplyWeight;

        [SerializeField] protected bool StopWeightOnDisable;
        //[SerializeField] protected EnumIkCondition WeaponAimState;
        [InjectFromInstance] protected IPlayer Owner { get; set; }
        protected override int OwnerId => Owner.Id;


        [Range(0.0f, 1.0f)][SerializeField] protected float DebugWeight;
        public override void Init()
        {
            base.Init();
            Notification.Register(EnumNotifType.PlayerStateChanged, Id, UpdateStatus);
            Notification.Register(EnumNotifType.PlayerChangeWeapon, Id, UpdateStatus);
        }
        private void Update()
        {
            UpdateWeight();
        }

        public float GetAimStateVAlue(EnumIkCondition weaponAimState) => weaponAimState switch
        {
            EnumIkCondition.Never => 0,
            EnumIkCondition.Always => 1,
            EnumIkCondition.Aimed => Owner.AimValue > .5f ? 1 : 0,
            EnumIkCondition.UnAimed => Owner.AimValue < .5f ? 1 : 0,
            EnumIkCondition.IkWeight => Owner.GetIkWeight(ikPart),
            EnumIkCondition.ReverseIkWeight => Mathf.Clamp01(1f - Owner.GetIkWeight(ikPart)),
            _ => 0,

        };


        protected virtual void UpdateWeight()
        {
            if (DEBUG)
            {
                if (Weight != Owner.GetIkWeight(ikPart))
                    Debug.Log(ikPart + " Weight " + Weight + " GetIkWeight " + Owner.GetIkWeight(ikPart));
            }

            if (DebugApplyWeight)
                Weight = DebugWeight;
            else
                Weight = Owner.GetIkWeight(ikPart) * DebugWeight;
        }

        protected virtual void UpdateStatus(NotificationParameter obj)
        {
            //bool isActive = (obj.Get<float>(EnumNotifParam.IkWeight) > 0);
            bool isWeaponMatch = (obj.Get<EnumWeaponType>(EnumNotifParam.currentWeapon) == weaponType);
            //bool isWeaponAimStatetMatch = (WeaponAimState == EnumWeaponAimState.All || obj.Get<EnumWeaponAimState>(EnumNotifParam.WeaponAimState) == WeaponAimState);
            //bool isStateMatch= (ikState & obj.Get<EnumIkState>(EnumNotifParam.IkState)) == ikState; 
            IsEnable = isWeaponMatch;
            if (!IsEnable && StopWeightOnDisable)
            {
                Weight = 0;
            }
        }
    }
}
