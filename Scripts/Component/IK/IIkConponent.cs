﻿

namespace RCPU.Component
{
    public interface IIkConponent : IInitializable
    {
    }

    public interface IIkHeightModifier : IInitializable
    {
        float Offset { get; set; }
    }
}
