﻿
using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Helper;
using RCPU.NotificationSystem;
using RCPU.Weapon;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace RCPU.Component
{
    [InitOrder(EnumInitOrder.Ik)]
    [RequireComponent(typeof(Rig))]
    //[DefaultExecutionOrder(-100000)]
    public class IkPlayerWeaponOffset : ComponentBase
    {
        [InjectFromInstance] protected IAgent owner { get; set; }

        [SerializeField] protected EnumWeaponType weaponType;
        [SerializeField] protected Transform target;
        //[SerializeField] protected EnumIkState ikState;
        [SerializeField] protected Transform follow;
        [SerializeField] protected Vector3 rotationOffset;
        [SerializeField] protected Vector3 positionOffset;

        protected override int OwnerId => owner.Id;

        public override void Init()
        {
            base.Init();
            Notification.Register(EnumNotifType.PlayerStateChanged, Id, UpdateStatus);
            Notification.Register(EnumNotifType.PlayerChangeWeapon, Id, UpdateStatus);
        }

        protected override void OnOwnerDied(NotificationParameter obj)
        {
            IsEnable = false;
        }
        protected override void OnOwnerSpawned(NotificationParameter obj)
        {
            IsEnable = true;
        }

        protected void Update()
        {
            if (ownerShocked)
                return;
            if (DEBUG)
            {
                Debug.Log(" weight " + GetComponent<Rig>().weight.ToString("f1") + " Distance " + CommonHelper.Distance(target.position, (follow.position + positionOffset)).ToString("f1"));
                Debug.Log(" target.position " + target.position + " follow.position " + follow.position);
            }
            target.position = (follow.position + positionOffset);
            target.rotation = follow.rotation;
            target.Rotate(rotationOffset);
        }

        protected void UpdateStatus(NotificationParameter obj)
        {
            //bool isActive = (obj.Get<float>(EnumNotifParam.IkWeight) > 0);
            bool isWeaponMatch = (weaponType == EnumWeaponType.None || obj.Get<EnumWeaponType>(EnumNotifParam.currentWeapon) == weaponType);
            //bool isStateMatch = (ikState & obj.Get<EnumIkState>(EnumNotifParam.IkState)) == ikState;
            IsEnable = isWeaponMatch/* && isStateMatch*/;
        }
    }
}
