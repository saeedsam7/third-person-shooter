using RCPU.Attributes;
using RCPU.NotificationSystem;
using RCPU.Weapon;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace RCPU.Component
{
    [InitOrder(EnumInitOrder.Ik)]
    [RequireComponent(typeof(Rig))]
    public class IkComponentBase : ComponentBase
    {
        protected Rig rig;
        [SerializeField] protected EnumIkPart ikPart;
        [SerializeField] protected EnumWeaponType weaponType;
        //[SerializeField] protected EnumIkState ikState;

        //protected bool canBeEnable;
        protected override bool IsEnable
        {
            get => base.IsEnable; set
            {
                base.IsEnable = value;
                //if (!value)
                //    Weight = 0;
            }
        }

        protected override void OnOwnerDied(NotificationParameter obj)
        {
            IsEnable = false;
            Weight = 0;
        }


        public EnumIkPart IkPart { get => ikPart; }
        public override void Init()
        {
            base.Init();
            rig = GetComponent<Rig>();
            Weight = 0;
        }

        public float Weight { get => rig.weight; set => rig.weight = value; }

    }
}