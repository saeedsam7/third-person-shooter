﻿

using RCPU.Attributes;
using RCPU.CameraFSM;
using RCPU.NotificationSystem;
using RCPU.Weapon;
using UnityEngine;

namespace RCPU.Component
{
    public class IkComponentPlayerRotation : IkComponentPlayer
    {
        [SerializeField] protected Transform target;
        [InjectFromSystem] protected ICameraAimTarget AimTarget { get; set; }
        private void Update()
        {
            UpdateWeight();
            // float lerpSpeed = Mathf.Clamp(TimeController.deltaTime * 100, 0, .8f);
            target.position = AimTarget.Center;// Vector3.Lerp(target.position, AimTarget.Center, lerpSpeed);

            //target.position = AimTarget.Position;
        }

        protected override void UpdateStatus(NotificationParameter obj)
        {
            //bool isActive = (obj.Get<float>(EnumNotifParam.IkWeight) > 0);
            bool isWeaponMatch = (weaponType == EnumWeaponType.None || obj.Get<EnumWeaponType>(EnumNotifParam.currentWeapon) == weaponType);
            //bool isPartMatch = (ikPart & obj.Get<EnumIkPart>(EnumNotifParam.IkParts)) == ikPart;
            //bool isStateMatch= (ikState & obj.Get<EnumIkState>(EnumNotifParam.IkState)) == ikState; 
            IsEnable = isWeaponMatch;
        }
    }
}
