﻿namespace RCPU.Component
{
    public class IkComponentPlayerAim : IkComponentPlayer
    {
        protected override void UpdateWeight()
        {
            base.UpdateWeight();
            Weight *= Owner.AimValue;
        }
    }
}
