﻿using RCPU.Agent;
using RCPU.Attributes;
using RCPU.NotificationSystem;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace RCPU.Component
{
    [InjectToInstance(typeof(IIkHeightModifier))]
    [InitOrder(EnumInitOrder.Ik)]
    [RequireComponent(typeof(MultiPositionConstraint))]
    public class IkComponentHeightModifier : ComponentBase, IIkHeightModifier
    {
        protected MultiPositionConstraint constraint;
        [InjectFromInstance] IAlive Owner { get; set; }
        protected override int OwnerId => Owner.Id;
        protected override bool IsEnable
        {
            get => base.IsEnable; set
            {
                base.IsEnable = value;
            }
        }

        protected override void OnOwnerDied(NotificationParameter obj)
        {
            IsEnable = false;
        }


        public override void Init()
        {
            base.Init();
            constraint = GetComponent<MultiPositionConstraint>();
        }

        public float Offset
        {
            get => constraint.data.offset.y;
            set
            {
                var offset = constraint.data.offset;
                offset.y = value;
                constraint.data.offset = offset;
            }
        }
    }
}