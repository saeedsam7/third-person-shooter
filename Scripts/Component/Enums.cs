

namespace RCPU.Component
{
    public enum EnumComponentType
    {
        VisionDetector, NightVisionDetector, TermalVisionDetector, NoiseDetector, Movement,
        Communication, DamageReceiver, WeaponManager, Animator, Locomotion,
        Trigger, Weapon, InputBinder, DecisionMaker, FootStepper,
        Walk, DamageManager, RagdollRecovery
    }
    public enum EnumMovementType { Ground, Flight };
    public enum EnumIntegerParam { CoverHeight, CoverDirection }
    public enum EnumBooleanParam { Unused01 }
    public enum EnumTriggerParam
    {
        Shoot, Die, Hit, Rotate, Revive,
        Teleport, GetOnVehicle, GetOffVehicle, Cover, ExitCover,
        Reload, Roll, Slide, UnequipWeapon, EquipWeapon
    }
    public enum EnumFloatParam
    {
        Speed, RotateAngle, HitAngle, ClipSpeed, Direction,
        RollAmount, InputSpeed, StopSpeed, RotateSpeed, Weapon,
        InputDirection, Multiplier_Rotation, Aim, PosState,
    }

    public enum EnumEventReceiver { StartAttack, ExitAttack, FinishAttack, FootStep };
    public enum EnumAnimLayer { Default, OverideWeapon, Ik };
    public enum EnumAnimTag { Move, Stand, Rotate, Sprint, Roll, Crouch, CrouchMove, CrouchRotate, Aim, Reload };


    //[System.Flags]
    public enum EnumIkPart { None, LeftFoot, RightFoot, LeftHand, RightHand, Root, UnArm };

    //public enum EnumIkState { None, Default, SwitchWeapon };

    public enum EnumShootMode { Aim, Blind };
    public enum EnumBlindShootState { None, AimDown, AimUp };
    public enum EnumLocomotionState { None, Stop, Walk, Jump };
}