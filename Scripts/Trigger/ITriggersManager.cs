namespace RCPU.Trigger
{
    public interface ITriggersManager : IInitializable
    {
        ITrigger GetTrigger(int id);
        //ITriggerVehicle AddVehicleTrigger(Vector3 position, IVehicle  cVehicle_Base);

        //void Remove(int triggerId);

        //bool GetCover(ITarget caller, List<ITarget> treats, ICoverResult coverResult);
        //ITriggerTeleportItem AddTelleportTrigger(Vector3 pos, ITeleportItem teleportItem);
        //ITriggerPickupItem AddPickupItem(Vector3 pos, IPickupItem pickupItem);
    }
}