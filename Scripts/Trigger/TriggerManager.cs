﻿using RCPU.Attributes;
using System.Collections.Generic;

namespace RCPU.Trigger
{
    [InjectToSystem(typeof(ITriggersManager))]
    [InitOrder(EnumInitOrder.Spawn)]
    public class TriggerManager : BaseObject, ITriggersManager
    {
        [InjectFromSystem] protected ITrigger[] injectedTriggers { get; set; }
        protected Dictionary<int, ITrigger> allITriggers;
        public ITrigger GetTrigger(int id)
        {
            if (allITriggers.ContainsKey(id) && allITriggers[id].IsActive)
                return allITriggers[id];
            return null;
        }

        public void Init()
        {
            allITriggers = new Dictionary<int, ITrigger>();
            for (int i = 0; i < injectedTriggers.Length; i++)
            {
                allITriggers.Add(injectedTriggers[i].Id, injectedTriggers[i]);
            }
        }




    }
}