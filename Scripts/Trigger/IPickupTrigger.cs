﻿

using UnityEngine;

namespace RCPU.Trigger
{
    public interface IPickupTrigger : ITrigger, IPickupItem
    {
        Vector3 Position { get; }
    }
}