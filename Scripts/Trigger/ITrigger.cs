
using RCPU.Component;
using RCPU.InputSystem;
using RCPU.UI;

namespace RCPU.Trigger
{
    public interface ITrigger : IInitializable
    {
        int Id { get; }
        EnumTriggerType TriggerType { get; }
        bool IsInteractable { get; }
        bool IsActive { get; }
        void EnterTrigger(ITriggerComponent agent);
        void ExitTrigger(ITriggerComponent agent);
        void Interact(ITriggerComponent agent);
        string Message { get; }
        EnumSpriteIndex Icon { get; }
        EnumButton InteractType { get; }
    }
}