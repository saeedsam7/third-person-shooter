﻿using RCPU.NotificationSystem;
using UnityEngine;

namespace RCPU.Trigger
{
    public class TriggerEvent : TriggerNotification
    {
        [SerializeField] protected EnumTriggerId eventToNotify;
        [SerializeField] protected bool notifyOnce;
        public override EnumTriggerType TriggerType => EnumTriggerType.Event;
        protected override void Activate()
        {
            Notification.Notify(EnumNotifType.EventTrigger, new NotificationParameter(EnumNotifParam.Id, eventToNotify));
            if (notifyOnce)
                IsActive = false;
        }
    }
}
