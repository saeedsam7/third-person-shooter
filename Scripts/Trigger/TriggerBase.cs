﻿using RCPU.Attributes;
using RCPU.Component;
using RCPU.InputSystem;
using RCPU.UI;
using UnityEngine;

namespace RCPU.Trigger
{
    [RequireComponent(typeof(Collider), typeof(Rigidbody))]
    [InjectToSystemArray(typeof(ITrigger))]
    [InitOrder(EnumInitOrder.Trigger)]
    public class TriggerBase : BaseObject, ITrigger
    {
        public override int Id { get { if (_Id == 0) _Id = GetComponent<Collider>().GetInstanceID(); return _Id; } protected set { _Id = value; } }

        public virtual EnumTriggerType TriggerType => throw new System.NotImplementedException();


        public virtual bool IsActive { get; protected set; }

        public virtual bool IsInteractable => false;

        public virtual string Message => "";

        public virtual EnumSpriteIndex Icon => EnumSpriteIndex.None;

        public virtual EnumButton InteractType => throw new System.NotImplementedException();

        public virtual void EnterTrigger(ITriggerComponent agent)
        {
        }

        public virtual void ExitTrigger(ITriggerComponent agent)
        {
        }

        public virtual void Init()
        {
        }

        public virtual void Interact(ITriggerComponent agent)
        {
        }
    }
}