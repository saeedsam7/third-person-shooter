﻿using RCPU.Attributes;
using RCPU.Component;
using RCPU.InputSystem;
using RCPU.Inventory;
using RCPU.Pickup;
using RCPU.UI;
using UnityEngine;

namespace RCPU.Trigger
{

    public class TriggerPickup : TriggerBase, IPickupTrigger
    {
        [SerializeField] GameObject[] skins;

        [InjectFromSystem] IPickupManager PickupManager { get; set; }

        //public EnumTriggerType TriggerType => EnumTriggerType.PickupItem;
        public override bool IsInteractable => true;
        public override string Message => "Press \'" + InteractType + "\' To Pickup '" + TriggerType + "\' ";

        public override EnumButton InteractType => EnumButton.Pickup;
        public override EnumTriggerType TriggerType => EnumTriggerType.PickupItem;


        bool isActive;
        public override bool IsActive { get { return isActive; } protected set { isActive = value; gameObject.SetActive(value); } }

        public Vector3 Position => transform.position;
        public override EnumSpriteIndex Icon => PickupManager.Get(PickupType).Icon;


        [field: SerializeField] public EnumPickupType PickupType { get; protected set; }
        [field: SerializeField] public int Count { get; set; }


        public override void EnterTrigger(ITriggerComponent agent)
        {
            if (IsActive)
                agent.AddPickup(this);
        }

        public override void ExitTrigger(ITriggerComponent agent)
        {
            agent.RemovePickup(this);
        }

        public override void Init()
        {
            base.Init();
            isActive = gameObject.activeSelf;
            SetSkin(PickupManager.Get(PickupType).Skin);
        }

        protected void SetSkin(EnumPickupIndicatorSkin index)
        {
            for (int i = 0; i < skins.Length; i++)
            {
                skins[i].SetActive(false);
            }
            skins[(int)index].SetActive(true);
        }

        public override void Interact(ITriggerComponent agent)
        {
            if (IsActive)
            {
                PickupByAgent(agent);
            }
        }

        protected virtual void PickupByAgent(ITriggerComponent agent)
        {
            Count = agent.Pickup(this);
            if (Count == 0)
            {
                agent.RemovePickup(this);
                IsActive = false;
            }
        }
    }
}