namespace RCPU.Trigger
{
    public enum EnumTriggerType
    {
        None,
        DayCycle,
        Door,
        PickupItem,
        TeleportItem,
        Vehicle,
        Cover,
        Spawn,
        Event,
    }




    public enum EnumTriggerId
    {
        None,
        Event01, Event02, Event03, Event04, Event05,
        Event06, Event07, Event08, Event09, Event10,
        Event11, Event12, Event13, Event14, Event15,
        Event16, Event17, Event18, Event19, Event20,
        Event21, Event22, Event23, Event24, Event25,
        Event26, Event27, Event28, Event29, Event30,
        Event31, Event32, Event33, Event34, Event35,
        Event36, Event37, Event38, Event39, Event40,
        Event41, Event42, Event43, Event44, Event45,
        Event46, Event47, Event48, Event49, Event50,
        Event51, Event52, Event53, Event54, Event55,
        Event56, Event57, Event58, Event59, Event60,
        Event61, Event62, Event63, Event64, Event65,
        Event66, Event67, Event68, Event69, Event70,
        Event71, Event72, Event73, Event74, Event75,
        Event76, Event77, Event78, Event79, Event80,
        Event81, Event82, Event83, Event84, Event85,
        Event86, Event87, Event88, Event89, Event90,
        Event91, Event92, Event93, Event94, Event95,
        Event96, Event97, Event98, Event99, Event100,

    }
}