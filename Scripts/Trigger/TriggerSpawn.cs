﻿using RCPU.NotificationSystem;
using RCPU.Spawn;
using UnityEngine;

namespace RCPU.Trigger
{
    public class TriggerSpawn : TriggerNotification
    {
        [SerializeField] protected EnumSpawnTrigger SpawnTrigger;
        public override EnumTriggerType TriggerType => EnumTriggerType.Spawn;

        protected override void Activate()
        {
            Notification.Notify(EnumNotifType.SpawnRequest, new NotificationParameter(EnumNotifParam.Id, SpawnTrigger));
            IsActive = false;

        }

    }
}