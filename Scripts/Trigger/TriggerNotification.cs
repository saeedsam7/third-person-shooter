﻿using RCPU.Component;
using RCPU.NotificationSystem;
using UnityEngine;

namespace RCPU.Trigger
{
    public class TriggerNotification : TriggerBase
    {

        [SerializeField] protected EnumTriggerId activateByNotification;

        //public override EnumTriggerType TriggerType => EnumTriggerType.Spawn;



        [field: SerializeField] public override bool IsActive { get; protected set; }

        public override void EnterTrigger(ITriggerComponent agent)
        {
            if (IsActive)
            {
                Activate();
            }
        }

        protected virtual void Activate()
        {
        }

        public override void Init()
        {
            base.Init();
            if ((IsActive && activateByNotification != EnumTriggerId.None) || (!IsActive && activateByNotification == EnumTriggerId.None))
                throw new System.Exception(" ERROR trigger IsActive : " + IsActive + " , activateByNotification: " + activateByNotification);

            if (!IsActive)
            {
                Notification.Register(EnumNotifType.EventTrigger, Id, OnEventTrigger);
            }
        }

        private void OnEventTrigger(NotificationParameter parameter)
        {
            if (parameter.Get<EnumTriggerId>(EnumNotifParam.Id) == activateByNotification)
            {
                IsActive = true;
                Notification.UnRegister(EnumNotifType.EventTrigger, Id);
            }
        }

    }
}