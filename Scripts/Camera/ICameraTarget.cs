﻿using UnityEngine;

namespace RCPU.CameraFSM
{
    public interface ICameraTarget : IInitializable
    {

        Vector3 Position { get; }
        Vector3 Center { get; }
        Vector3 WeaponPosition { get; }
        Vector3 Forward { get; }
        //Vector3 WeaponForward { get; }
        float AimValue { get; }
        float GetCrosshairSize();
    }
}