using RCPU.Attributes;
using RCPU.Game;
using RCPU.Helper;
using RCPU.InputSystem;
using RCPU.NotificationSystem;
using RCPU.StateMachine;
using UnityEngine;

namespace RCPU.CameraFSM
{
    [RequireInput]
    [InjectToInstance(typeof(ICameraState))]
    [InitOrder(EnumInitOrder.CameraState)]
    public class CameraStateBase : StateBase, ICameraState
    {
        [InjectFromInstance] protected ICamera owner { get; set; }
        [InjectFromSystem] protected IInGameDetailFSM InGameDetailFSM { get; set; }
        [InjectFromSystem] public ICameraTarget Target { get; set; }
        [InjectFromSystem] protected ICameraAimTarget AimTarget { get; set; }
        [InjectFromSystem] protected IGameFSM GameFSM { get; set; }

        public EnumCameraState State => model.State;
        protected CameraStateDataModel model;

        //protected Transform aimTarget;
        protected float maxCamDist = 1;
        public bool ApplyShake { get { return model.applyShake; } }
        protected float DefaultMaxCamDist = 3;

        protected CameraOffsetData offsetData => (CurrentShoulder == EnumCameraPosition.Left ? model.offsetLeft : model.offsetRight);
        protected Vector2 angle;
        protected float AngleX { get { return angle.x; } set { angle.x = value; } }
        protected float AngleY { get { return angle.y; } set { angle.y = value; } }

        protected EnumCameraPosition CurrentShoulder { get; set; }
        //public virtual void Init(StateCamera model, Transform aimTarget, EnumCameraPosition cameraPosition)
        public override void Init()
        {
            base.Init();
            CurrentShoulder = EnumCameraPosition.Right;
            maxCamDist = DefaultMaxCamDist;
            Notification.Register(EnumNotifType.GameInitializationDone, Id, OnGameInitializationDone);

        }
        protected void OnGameInitializationDone(NotificationParameter obj)
        {
            UpdatePositionAndRotation(1000, 1000);
        }

        [InputRegisterButtonDown(EnumInputPriority.ChangeShoulder, EnumUnityEvent.Update, "IsActive", EnumGameOveralState.InGame, EnumInGameUiState.HUD, EnumButton.ChangeShoulder)]
        protected virtual void ChangeShoulder()
        {
            CurrentShoulder = (CurrentShoulder == EnumCameraPosition.Left ? EnumCameraPosition.Right : EnumCameraPosition.Left);
        }


        public override void EnterState(StateChangeParameter param)
        {
            base.EnterState(param);
            this.model = param.Get<CameraStateDataModel>(EnumStateChangeParameter.model);
            this.model.EnterTime = TimeController.gameTime;
            this.model.EnterAngle = angle;
        }

        public void LookAt(Vector3 target)
        {
            AimTarget.LookAt(target);
            transform.position = Target.Position + (offsetData.camOffset.y * Vector3.up) + ((AimTarget.Position - Target.Position).normalized * offsetData.camOffset.z);
            transform.LookAt(target);
            AngleX = transform.eulerAngles.y;
            AngleY = transform.eulerAngles.x * -1;

        }


        //public override void UpdateState()
        //{
        //    base.UpdateState();
        //    UpdateAimTarget();
        //}

        //protected void UpdateAimTarget()
        //{
        //    RaycastHit raycastHit;
        //    //Collider hit; 
        //    if (PhysicsHelper.Raycast(transform.position, transform.forward, out raycastHit, 1000, PhysicsHelper.Layer_DamageReceiver_Environment))
        //    {
        //        //hit = raycastHit.collider;
        //        //CLog.Log(" transform.position "+ transform.position + " transform.forward " + transform.forward + " raycastHit.point " + raycastHit.point+ "  raycastHit.transform.name " + raycastHit.transform.name );

        //        Vector3 hitPoint = raycastHit.point;
        //        float dis1 = CommonHelper.Distance(hitPoint, transform.position);
        //        float dis2 = CommonHelper.Distance(hitPoint, Target.WeaponPosition);
        //        //float dis3 = CommonHelper.Distance(transform.position, Target.HeadPosition);


        //        float angle1 = Mathf.Abs(CommonHelper.SignedAngle(Target.Position, Target.Forward, hitPoint, true));
        //        //float angle2 = CommonHelper.SignedAngle(transform.position, transform.forward, raycastHit.point, true);

        //        if ((angle1 < 45 && dis2 > 1f) /*|| (angle1 < 45 && dis2 > 3)*/)
        //        {
        //            //Debug.Log(" dis1 " + dis1.ToString("f1") + " dis2 " + dis2.ToString("f1") + " angle1 : " + angle1);
        //            //UIManager.CLearShadowCursor();
        //            AimTarget.EnableShoot = true;


        //            ITarget alive = null;
        //            if (raycastHit.colliderInstanceID != Target.Id)
        //                levelController.GetAliveObject(raycastHit.colliderInstanceID);
        //            //CLog.Log(" UpdateState " + alive + " hit.name " + hit.name + " position " + aimIk.target.position, CLog.EnumPriority.Extremly);
        //            if (!CommonHelper.IsNull(alive))
        //            {
        //                //CLog.Log(" alive.Family " + alive.Family + " owner.Family " + owner.Family + " GetRelation " + owner.GetRelation(alive.Family), CLog.EnumPriority.Extremly);
        //                switch (relation.GetRelation(alive.Family, EnumFamily.Player))
        //                {
        //                    case EnumRelation.Enemy:
        //                    case EnumRelation.PotentialEnemy:
        //                        UiManager.SetCursorColor(EnumCursorColor.Enemy);
        //                        Notification.Notify(EnumNotifType.UpdateAgentWidget,
        //                                    new NotificationParameter(EnumNotifParam.agentUiUpdater, alive));
        //                        break;
        //                    case EnumRelation.Friend:
        //                        UiManager.SetCursorColor(EnumCursorColor.Friend);
        //                        break;
        //                }
        //            }
        //            else
        //            {
        //                UiManager.SetCursorColor(EnumCursorColor.Neutral);
        //            }
        //        }
        //        else
        //        {
        //            AimTarget.EnableShoot = false;
        //            Debug.LogError(" d1 " + dis1.ToString("f1") + " d2 " + dis2.ToString("f1") + " angle1 : " + angle1);
        //            //UIManager.SetShadowCursor(hitPoint + (Vector3.down * 2));
        //            UiManager.SetCursorColor(EnumCursorColor.Disable);
        //        }
        //        AimTarget.HitTarget(raycastHit.point, raycastHit.colliderInstanceID);
        //        return;
        //    }
        //    AimTarget.Clear(owner.GetForward(1000, false));
        //} 

        [InputRegisterAxis(EnumInputPriority.Camera, EnumUnityEvent.Update, "IsActive", EnumGameOveralState.InGame, EnumInGameUiState.HUD)]
        protected void ReadInput(float[] axis, bool[] btn)
        {
            //TODO 
            float multiply = 0;
            switch (InGameDetailFSM.CauciousState)
            {
                case EnumInGameCauciousState.Reflex:
                    multiply = 3;
                    break;
                default:
                    multiply = 1;
                    break;
            }
            AngleX += Mathf.Clamp(axis[(int)EnumAxis.AimX], -1, 1) * model.mouseSensitivity.x * multiply * TimeController.deltaTime;
            AngleY += Mathf.Clamp(axis[(int)EnumAxis.AimY], -1, 1) * model.mouseSensitivity.y * multiply * TimeController.deltaTime;

            //Debug.Log (" cam.eulerAngles " + cam.eulerAngles + " cam.position " + cam.position+" angleH " + angleH + " angleV " + angleV);
        }
        public override void LateUpdateState()
        {

            if (TimeController.deltaTime == 0 || TimeController.timeScale == 0)
                return;

            float moveSpeed = model.moveSpeed;
            if (TimeController.gameTime - model.EnterTime < .5f && (this.model.EnterAngle - angle).magnitude < 10)
                moveSpeed = model.TransitionSpeed;
            UpdatePositionAndRotation(moveSpeed, model.rotateSpeed);
        }

        protected void UpdatePositionAndRotation(float moveSpeed, float rotateSpeed)
        {
            AngleY = Mathf.Clamp(AngleY, model.VerticalAngle.min, model.VerticalAngle.max);
            var extraRotForShoulder = (Mathf.Min(-3f, AngleY) * offsetData.extraRotation);
            Quaternion aimRotation = Quaternion.Euler(-AngleY - owner.Recoil.y, AngleX + extraRotForShoulder + owner.Recoil.x, 0);
            Quaternion camYRotation = Quaternion.Euler(0, AngleX + extraRotForShoulder, 0);

            Vector3 farCamPoint = Target.Position + camYRotation * offsetData.pivotOffset + aimRotation * offsetData.camOffset;
            Vector3 closeCamPoint = Target.Position + camYRotation * offsetData.closeOffset;
            float farDist = Vector3.Distance(farCamPoint, closeCamPoint);

            Vector3 closeToFarDir = (farCamPoint - closeCamPoint) / farDist;

            if (DEBUG)
            {
                Debug.DrawLine(closeCamPoint + Vector3.up * .3f, closeCamPoint - Vector3.up * .3f, Color.green);
                Debug.DrawLine(closeCamPoint + Vector3.right * .3f, closeCamPoint - Vector3.right * .3f, Color.green);

                Debug.DrawLine(farCamPoint + Vector3.up * .3f, farCamPoint - Vector3.up * .3f, Color.green);
                Debug.DrawLine(farCamPoint + Vector3.right * .3f, farCamPoint - Vector3.right * .3f, Color.green);
            }


            if (PhysicsHelper.Raycast(closeCamPoint, closeToFarDir, out RaycastHit hit, farDist + model.Padding, model.mask, QueryTriggerInteraction.Ignore))
            {
                if (DEBUG)
                {
                    Debug.DrawLine(hit.point + Vector3.up * .3f, hit.point - Vector3.up * .3f, Color.black);
                    Debug.DrawLine(hit.point + Vector3.right * .3f, hit.point - Vector3.right * .3f, Color.black);
                    Debug.DrawLine(closeCamPoint, hit.point, Color.black);
                }

                maxCamDist = hit.distance - model.Padding;
                maxCamDist = Mathf.Clamp(maxCamDist, 0, 1000);
            }
            else
            {
                if (DEBUG)
                {
                    Debug.DrawLine(closeCamPoint, closeCamPoint + (closeToFarDir * farDist), Color.green);
                }
                var pad = Mathf.Clamp(Mathf.Abs(farDist - maxCamDist), 0, model.Padding);
                maxCamDist += Mathf.Sign(farDist - maxCamDist) * pad;
            }


            Vector3 finalPos = closeCamPoint + closeToFarDir * maxCamDist;
            transform.position = Vector3.Lerp(transform.position, finalPos, TimeController.GetNormilizeTime(moveSpeed));
            transform.rotation = Quaternion.Lerp(transform.rotation, aimRotation, TimeController.GetNormilizeTime(rotateSpeed));
        }

    }
}