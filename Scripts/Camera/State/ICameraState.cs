﻿using RCPU.StateMachine;
using UnityEngine;

namespace RCPU.CameraFSM
{
    public interface ICameraState : IState
    {
        EnumCameraState State { get; }
        bool ApplyShake { get; }
        void LookAt(Vector3 target);
        void LateUpdateState();
        //void GetAngles(StateChangeParameter param);
        //void ResetRecoil(Vector2 currentRecoil); 
    }
}
