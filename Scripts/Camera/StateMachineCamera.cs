
using RCPU.Attributes;
using RCPU.DependencyInjection;
using RCPU.Game;
using RCPU.Helper;
using RCPU.NotificationSystem;
using RCPU.StateMachine;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.CameraFSM
{
    [InitOrder(EnumInitOrder.CameraFSM)]
    [InjectToSystem(typeof(ICamera))]
    [InjectToInstance(typeof(ICamera))]
    [RequireComponent(typeof(Camera))]
    [DefaultExecutionOrder(1000)]
    public class StateMachineCamera : StateMachineBase, ICamera
    {
        [InjectFromSystem] public ICameraTarget Target { get; private set; }
        [InjectFromInstance] public ICameraShakeGenerator ShakeGenerator { get; private set; }
        [InjectFromSystem] protected IGameFSM gameFSM { get; set; }
        [InjectFromInstance] ICameraState ActiveState { get; set; }
        //protected Dictionary<EnumCameraState, ICameraState> allState;
        private EnumCameraState currentState;

        [Header("Model")][SerializeField] protected CameraDataModel_SO model_SO;
        //protected PlayerDataModel model;

        protected Dictionary<EnumCameraState, CameraStateDataModel> allState;
        protected CameraStateDataModel CurrentStateModel => allState[currentState];


        protected Camera cam;
        //public IAgent Target { get { return player; } }

        public void LookAt(Vector3 target)
        {
            ActiveState.LookAt(target);
        }
        public Vector3 Position => transform.position;
        public Quaternion Rotation => transform.rotation;
        public Vector3 Forward => transform.forward;
        public Vector3 Right => new Vector3(transform.right.x, 0, transform.right.z);
        public Vector2 Recoil { get; private set; }

        public Quaternion LocalRotation => transform.localRotation;


        //protected float angleH;
        //protected float angleV;

        //[SerializeField]
        //protected List<CameraDataModel_SO> statesModel;
        //[HideInInspector]
        // just for debug
        //protected List<EnumCameraState> temp;
        //public Vector3 AimTarget { get => aimTarget.position; }
        //Transform aimTarget;
        protected Transform cameraParent;

        float lastRecoil;
        [SerializeField] bool DEBUG_PATH;

        public Vector3 GetForward(float distance, bool ignoreY)
        {
            return transform.position + ((ignoreY ? new Vector3(transform.forward.x, 0, transform.forward.z) : Forward) * distance);
        }

        //public Vector3 GetForwardHit(float distance, int layerMask, out Collider hit)
        //{
        //    RaycastHit raycastHit;
        //    if (PhysicsHelper.Raycast(transform.position + (transform.forward * CommonHelper.Distance(transform.position, Target.Position)), transform.forward, out raycastHit, distance, layerMask))
        //    {
        //        hit = raycastHit.collider;
        //        Debug.DrawLine(transform.position + (transform.forward * CommonHelper.Distance(transform.position, Target.Position)), raycastHit.point, Color.green);
        //        //CLog.Log(" transform.position "+ transform.position + " transform.forward " + transform.forward + " raycastHit.point " + raycastHit.point+ "  raycastHit.transform.name " + raycastHit.transform.name );
        //        return raycastHit.point;
        //    }
        //    hit = null;
        //    return GetForward(distance, false);
        //}


        //public Vector3 GetForwardHit(float distance, int layerMask)
        //{
        //    RaycastHit raycastHit;
        //    Debug.DrawLine(transform.position + (transform.forward * CommonHelper.Distance(transform.position, Target.Position)), transform.position + (transform.forward * distance), Color.magenta, 3);
        //    if (PhysicsHelper.Raycast(transform.position + (transform.forward * CommonHelper.Distance(transform.position, Target.Position)), transform.forward, out raycastHit, distance, layerMask))
        //    {
        //        //CLog.Log("GetForwardHit transform.position " + transform.position + " transform.forward " + transform.forward + " raycastHit.point " + raycastHit.point + "  raycastHit.transform.name " + raycastHit.transform.name + "  layer " + LayerMask.LayerToName(raycastHit.transform.gameObject.layer));
        //        return raycastHit.point;
        //    }
        //    //else
        //    //{ 
        //    //    if (Helper.Raycast(transform.position, transform.forward, out raycastHit, distance))
        //    //    {
        //    //        CLog.Log("GetForwardHit transform.position " + transform.position + " transform.forward " + transform.forward + " raycastHit.point " + raycastHit.point + "  raycastHit.transform.name " + raycastHit.transform.name + raycastHit.point + "  layer " + LayerMask.LayerToName( raycastHit.transform.gameObject.layer));
        //    //        return raycastHit.point;
        //    //    }
        //    //}
        //    return GetForward(distance, false);
        //}



        public override void Init()
        {
            base.Init();
            cam = GetComponent<Camera>();

            Register(EnumNotifType.PlayerStateChanged, PlayerStateChanged);
            Notification.Register(EnumNotifType.Recoil, Id, UpdateRecoil);

            //aimTarget = new GameObject("Aim Target").transform;
            cameraParent = new GameObject("Camera Parent").transform;
            cameraParent.SetParent(transform.parent);
            cameraParent.position = transform.position;
            cameraParent.rotation = transform.rotation;
            transform.SetParent(cameraParent);

            allState = new Dictionary<EnumCameraState, CameraStateDataModel>();
            for (int i = 0; i < model_SO.states.Length; i++)
            {
                var temp = DInjector.Instantiate<CameraStateDataModel_SO>(model_SO.states[i]).model;
                allState.Add(temp.State, temp);
            }

            Spawn();
        }

        protected override void Spawn()
        {
            base.Spawn();
            ChangeState(new StateChangeParameter(EnumStateChangeParameter.newState, EnumCameraState.Normal));

            //Register(EnumNotifType.SetPlayer, SetPlayer);
            //instance = this;
            //allState = new Dictionary<EnumCameraState, ICameraState>();

            //temp = new List<EnumCameraState>(); 
            //for (int i = 0; i < statesModel.Count; i++)
            //{
            //    var model = statesModel[i].model;// ScriptableObject.CreateInstance<AgentState_Model>(statesModel[i].name);
            //                                     //Type t = Type.GetType(model.Controller);

            //    var controller = (IStateCamera)DInjector.CreateComponent(this, model.Controller);

            //    allState.Add(model.State, controller);
            //    temp.Add(model.State);
            //}


            //Notification.Register(EnumNotifType.ResetRecoil, Id, ResetRecoil);
        }



        private void UpdateRecoil(NotificationParameter param)
        {
            float value = param.Get<float>(EnumNotifParam.value);
            if (value == 0)
            {
                Debug.LogError("Recoil value  == 0 ");
                return;
            }

            //ShakeCamera(.1f, value, AnimationCurve.Linear(0, 1, 1, 1), true);
            lastRecoil = 1f;
            float x = value * RandomHelper.Range(-1f, 1f);
            float y = value * RandomHelper.Range(.9f, 1.1f);
            Recoil = new Vector2(x, Recoil.y + y);
            //allState[currentState].Recoil(x, y);
        }

        //public void ShakeCamera(NotificationParameter param)
        //{
        //    Vector3 shakePos = param.Get<Vector3>(EnumNotifParam.position);
        //    float shakePower = param.Get<float>(EnumNotifParam.shakePower);
        //    float dis = CommonHelper.Distance(transform.position, shakePos);
        //    if (shakePower == 0)
        //    {
        //        Debug.LogError("ShakeCamera shakePower==0 ");
        //        return;
        //    }

        //    float duration = shake_duration = shakePower * 2.5f;
        //    float amount = shakePower * (1 - (dis / 50f)); ;

        //    ShakeCamera(duration, amount, param.Get<AnimationCurve>(EnumNotifParam.curveData), false);
        //    //CLog.Log("ShakeCamera dis " + dis + " shakePower " + shakePower + " duration " + duration + " amount " + amount + " IsNull " + CommonHelper.IsNull(coroutine_Shake) + " == " + (coroutine_Shake == null), CLog.EnumPriority.Extremly);

        //}

        //private void ShakeCamera(float duration, float amount, AnimationCurve shakeCurve, bool twoD)
        //{
        //    if (shake_RemainDuration <= 0 || CommonHelper.IsNull(coroutine_Shake))
        //    {
        //        if (!CommonHelper.IsNull(coroutine_Shake))
        //            StopCoroutine(coroutine_Shake);

        //        shake_Curve = shakeCurve;
        //        shake_RemainDuration = shake_duration = duration;
        //        shake_Amount = amount;

        //        coroutine_Shake = StartCoroutine(Coroutine_Shake(twoD));
        //    }
        //    else
        //       if (shake_duration < duration || shake_Amount < amount)
        //    {
        //        shake_Curve = shakeCurve;
        //        if (shake_duration < duration)
        //            shake_RemainDuration = shake_duration = duration;
        //        if (shake_Amount < amount)
        //            shake_Amount = amount;
        //    }
        //}

        //Coroutine coroutine_Shake;
        //float shake_duration;
        //float shake_RemainDuration;
        //float shake_Amount;
        //AnimationCurve shake_Curve;
        //public IEnumerator Coroutine_Shake(bool twoD)
        //{
        //    while (shake_RemainDuration > 0 && allState[currentState].ApplyShake)
        //    {
        //        if (TimeController.timeScale != 0)
        //        {
        //            if (twoD)
        //            {
        //                transform.localPosition = RandomHelper.insideUnitCircle * shake_Curve.Evaluate(shake_RemainDuration / shake_duration) * shake_Amount;
        //            }
        //            else
        //            {
        //                transform.localPosition = RandomHelper.insideUnitSphere * shake_Curve.Evaluate(shake_RemainDuration / shake_duration) * shake_Amount;
        //            }
        //            shake_RemainDuration -= TimeController.deltaTime;
        //        }
        //        yield return null;
        //    }
        //    shake_RemainDuration = 0;
        //    transform.localPosition = Vector3.zero;
        //}



        //private void SetPlayer(NotificationParameter obj)
        //{
        //    player = obj.Get<IAgent>(EnumNotifParam.agent);
        //    //this.target = player.GetTrans();
        //    for (int i = 0; i < statesModel.Count; i++)
        //    {
        //        var model = statesModel[i].model;// ScriptableObject.CreateInstance<AgentState_Model>(statesModel[i].name);
        //                                         //Type t = Type.GetType(model.Controller);
        //        allState[model.State].Init(player, model, aimTarget, EnumCameraPosition.Right);

        //    }
        //    ChangeState(EnumCameraState.Normal);

        //}

        private void PlayerStateChanged(NotificationParameter obj)
        {
            var change = obj.Get<bool>(EnumNotifParam.ChangeCameraState);
            if (change)
            {
                var state = obj.Get<EnumCameraState>(EnumNotifParam.CameraState);
                ChangeState(new StateChangeParameter(EnumStateChangeParameter.newState, state));
            }

        }

        protected void ChangeState(StateChangeParameter param)
        {
            EnumCameraState newState = param.Get<EnumCameraState>(EnumStateChangeParameter.newState);
            if (currentState != newState && allState.ContainsKey(newState))
            {
                Logger.Log(" Camera " + currentState + " ---> " + newState, EnumLogColor.GrayLight);

                if (CommonHelper.IsNull(param))
                    param = new StateChangeParameter();

                if (allState.ContainsKey(currentState))
                {
                    //allState[currentState].GetAngles(param);
                    ActiveState.ExitState(new StateChangeParameter(EnumStateChangeParameter.newState, newState));
                }

                currentState = newState;
                param.Add(EnumStateChangeParameter.model, CurrentStateModel);
                ActiveState.EnterState(param);
            }
        }


        Vector3 prevPos;
        protected override void LateUpdateStateMachine()
        {
            if (DEBUG_PATH)
            {
                Debug.DrawLine(prevPos, transform.position, Color.white, 1f);
                Debug.DrawLine(transform.position, transform.position + (transform.forward * Vector3.Distance(Target.Position, transform.position)), Color.yellow, 3f);
                prevPos = transform.position;
            }
            //if (Input.GetKeyDown(KeyCode.F1))
            //{
            //    RegisterShake(new CameraShakeParam() { ShakeType = EnumCameraShakeType.Perlin, Power = 4, Duration_Bounce = .35f });

            //}

            //if (Input.GetKeyDown(KeyCode.F2))
            //{
            //    RegisterShake(new CameraShakeParam() { ShakeType = EnumCameraShakeType.Bounce, Power = 3, Duration_Bounce = 5 });

            //}

            //if (Input.GetKeyDown(KeyCode.F3))
            //{
            //    RegisterShake(new CameraShakeParam() { ShakeType = EnumCameraShakeType.Kick, Power = 3 });

            //}

            //allState[currentState].LateUpdateState();
            if (gameFSM.State == EnumGameOveralState.InGame)
            {
                if (lastRecoil > 0)
                {
                    lastRecoil -= TimeController.deltaTime;
                }
                if (Recoil.magnitude > 0 && lastRecoil <= 0.0f)
                {
                    Recoil = CommonHelper.Lerp(Recoil, Vector2.zero, TimeController.NormilizeDeltaTime);
                    //AngleH += Recoil.x;
                    //AngleV += Recoil.y;
                    //Recoil = Vector2.zero;
                }
                UpdateShaker();
                //allState[currentState].UpdateState();
            }
        }




        public Vector2 WorldToViewportPoint(Vector3 position)
        {
            return cam.WorldToViewportPoint(position);
        }



        public void RegisterShake(CameraShakeParam param)
        {
            //Vector3 shakePos = param.Get<Vector3>(EnumNotifParam.position);
            //float shakePower = param.Get<float>(EnumNotifParam.shakePower);
            //float dis = CommonHelper.Distance(transform.position, shakePos);

            RegisterShake(ShakeGenerator.Make(param));
            //CLog.Log("ShakeCamera dis " + dis + " shakePower " + shakePower + " duration " + duration + " amount " + amount + " IsNull " + CommonHelper.IsNull(coroutine_Shake) + " == " + (coroutine_Shake == null), CLog.EnumPriority.Extremly);

        }



        List<ICameraShake> activeShakes = new List<ICameraShake>();
        void RegisterShake(ICameraShake shake)
        {
            if (CommonHelper.IsNull(shake))
                return;
            shake.Initialize(transform.position, transform.rotation);
            activeShakes.Add(shake);
        }


        private void UpdateShaker()
        {

            Displacement cameraDisplacement = Displacement.Zero;
            for (int i = activeShakes.Count - 1; i >= 0; i--)
            {
                if (activeShakes[i].IsFinished)
                {
                    activeShakes.RemoveAt(i);
                }
                else
                {
                    activeShakes[i].Update(TimeController.deltaTime, transform.position, transform.rotation);
                    cameraDisplacement += activeShakes[i].CurrentDisplacement;
                }
            }
            transform.localPosition += cameraDisplacement.position;
            transform.localEulerAngles += (cameraDisplacement.eulerAngles);
        }
    }

}