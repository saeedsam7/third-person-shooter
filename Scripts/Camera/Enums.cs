namespace RCPU.CameraFSM
{
    public enum EnumCameraState
    {
        Ready, Normal, Aim, Menu, Riding, Cover, CoverAim,
        Sprint, Crouch, CrouchAim
    }
    public enum EnumCameraPosition
    {
        Right = -1, Left = 1
    }

    public enum EnumCameraShakeType { None, Perlin, Kick, Bounce }
}