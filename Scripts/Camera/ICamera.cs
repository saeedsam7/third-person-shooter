using UnityEngine;

namespace RCPU.CameraFSM
{
    public interface ICamera : IInitializable
    {
        Vector3 GetForward(float distance, bool ignoreY);
        //Vector3 GetForwardHit(float distance, int layerMask, out Collider hit);
        void LookAt(Vector3 target);
        Vector3 Position { get; }
        Quaternion Rotation { get; }
        //Vector3 GetForwardHit(float distance, int layerMask);
        Vector2 WorldToViewportPoint(Vector3 position);
        void RegisterShake(CameraShakeParam param);

        Vector3 Forward { get; }
        Vector3 Right { get; }
        Vector2 Recoil { get; }
        Quaternion LocalRotation { get; }
        //Vector3 AimTarget { get; }

    }
}