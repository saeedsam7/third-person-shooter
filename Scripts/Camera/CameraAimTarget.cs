﻿

using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Damage;
using RCPU.Helper;
using RCPU.UI;
using UnityEngine;

namespace RCPU.CameraFSM
{
    [InjectToSystem(typeof(ICameraAimTarget))]
    [InitOrder(EnumInitOrder.CameraAim)]
    public class CameraAimTarget : BaseObject, ICameraAimTarget
    {
        [InjectFromSystem] protected ICamera cam { get; set; }
        [InjectFromSystem] protected IUIManager UiManager { get; set; }
        [InjectFromSystem] protected ILevelManager levelManager { get; set; }
        [InjectFromSystem] protected IRelation relation { get; set; }
        [InjectFromSystem] public ICameraTarget Target { get; set; }


        public Vector3 Position
        {
            get
            {
                var dir = (Vector3.one - transform.forward);
                var up = dir;
                up.x = up.z = 0;
                dir.y = 0;
                up *= (distance * .03f) * RandomHelper.Range(-CrosshairSize, CrosshairSize);
                dir *= (distance * .03f) * RandomHelper.Range(-CrosshairSize, CrosshairSize);
                return transform.position + up + dir;
            }
            protected set { transform.position = value; }
        }
        public Vector3 Center { get { return transform.position; } }
        public int HitId { get; protected set; }
        public bool Hit { get; protected set; }
        public bool CanShoot { get; protected set; }
        protected float CrosshairSize { get; set; }
        float distance;
        //EnumAimState aimState;

        public void LookAt(Vector3 pos)
        {
            Position = pos;
            UpdateAimTarget();
        }

        protected void HitTarget(Vector3 hitPoint, int hitId)
        {
            //Debug.Log(transform.name + " 1 Position " + Position + " collider " + raycastHit.collider.name);
            Position = hitPoint;
            Hit = true;
            HitId = hitId;
            //Debug.Log(transform.name + " 2 Position " + Position + " collider " + raycastHit.collider.name);
        }

        protected void Clear(Vector3 position)
        {
            Position = position;
            Hit = false;
            HitId = 0;
            UiManager.CLearShadowCrosshair();
        }

        void Update()
        {
            UpdateAimTarget();
        }
        RaycastHit weaponRaycastHit;
        RaycastHit cameraRaycastHit;
        private bool CheckWeaponHitSameTarget(Vector3 hitPoint)
        {
            return !PhysicsHelper.Raycast(Target.WeaponPosition, (hitPoint - Target.WeaponPosition), out weaponRaycastHit, CommonHelper.Distance(Target.WeaponPosition, hitPoint) + 2f, PhysicsHelper.Layer_DamageReceiver_Environment)
                       || (weaponRaycastHit.colliderInstanceID != cameraRaycastHit.colliderInstanceID) || (CommonHelper.Distance(weaponRaycastHit.point, hitPoint) > .02f);
        }

        protected void UpdateAimTargetByCameraRaycastHit()
        {

            Vector3 hitPoint = cameraRaycastHit.point;
            distance = CommonHelper.Distance(hitPoint, Target.Center);
            float angle = Mathf.Abs(CommonHelper.SignedAngle(Target.Position, Target.Forward, hitPoint, true));

            float minDistance = 1f;
            float minAngle = 44;
            float maxAngle = 50;
            float acceptedAngle = Mathf.Clamp(minAngle + ((distance - minDistance) * 1), minAngle, maxAngle);

            //Logger.Log(" distance : " + distance.ToString("f2") + " angle : " + angle.ToString("f0") + " minDistance : " + minDistance.ToString("f2") + " acceptedAngle : " + acceptedAngle.ToString("f0"), 0);
            //Debug.Log("11");
            if (/*aimState != EnumAimState.IsAim*/   (angle < acceptedAngle && distance > minDistance))
            {
                //Debug.Log("12");
                //UIManager.CLearShadowCursor();
                if (!CanShoot)
                {
                    //Debug.Log("13");
                    UiManager.SetColor(EnumUiTextIndex.BulletCounter, EnumUiColor.UiElementActive);
                    UiManager.SetColor(EnumUiImageIndex.Weapom, EnumUiColor.UiElementActive);
                    CanShoot = true;
                }


                if (CheckWeaponHitSameTarget(hitPoint))
                {
                    if (weaponRaycastHit.colliderInstanceID != 0)
                    {
                        Logger.Error(" Distance " + CommonHelper.Distance(weaponRaycastHit.point, hitPoint).ToString("f2") + " my DIs " + CommonHelper.Distance(Target.Position, hitPoint).ToString("f2"), 3);
                        UiManager.SetShadowCrosshair(weaponRaycastHit.point);
                    }

                    UiManager.SetColor(EnumUiColor.CrosshairShadow);
                }
                else
                {
                    Logger.Error("", 3);
                    UiManager.CLearShadowCrosshair();

                    IDamageManager alive = null;
                    if (cameraRaycastHit.colliderInstanceID != Target.Id)
                        alive = levelManager.GetDamageManager(cameraRaycastHit.colliderInstanceID);
                    //CLog.Log(" UpdateState " + alive + " hit.name " + hit.name + " position " + aimIk.target.position, CLog.EnumPriority.Extremly);
                    if (!CommonHelper.IsNull(alive))
                    {
                        //Debug.Log("17");
                        //CLog.Log(" alive.Family " + alive.Family + " owner.Family " + owner.Family + " GetRelation " + owner.GetRelation(alive.Family), CLog.EnumPriority.Extremly);
                        switch (relation.GetRelation(alive.Family, EnumFamily.Player))
                        {
                            case EnumRelation.Enemy:
                            case EnumRelation.PotentialEnemy:
                                UiManager.SetColor(EnumUiColor.CrosshairEnemy);
                                UiManager.BindWidgetToAgent(alive.GetOwner() as IAiPublic);
                                //Notification.Notify(EnumNotifType.UpdateAgentWidget, new NotificationParameter(EnumNotifParam.agentUiUpdater, alive));
                                break;
                            case EnumRelation.Friend:
                                UiManager.SetColor(EnumUiColor.CrosshairFriend);
                                break;
                        }
                    }
                    else
                    {
                        //Debug.Log("18");
                        UiManager.SetColor(EnumUiColor.CrosshairNeutral);
                    }
                    HitTarget(cameraRaycastHit.point, cameraRaycastHit.colliderInstanceID);
                }
            }
            else
            {
                if (CanShoot)
                {
                    UiManager.SetColor(EnumUiColor.CrosshairDisable);
                    UiManager.SetColor(EnumUiTextIndex.BulletCounter, EnumUiColor.UiElementDisable);
                    UiManager.SetColor(EnumUiImageIndex.Weapom, EnumUiColor.UiElementDisable);
                }
                CanShoot = false;
                Clear(cam.GetForward(4, false));
            }
        }

        protected void UpdateAimTarget()
        {
            transform.LookAt(Target.Position);
            var newCrosshairSize = Target.GetCrosshairSize();
            CrosshairSize = Mathf.Lerp(CrosshairSize, newCrosshairSize, TimeController.GetNormilizeTime(newCrosshairSize > CrosshairSize ? 20f : 5f));
            //Logger.Log(" CrosshairSize : " + CrosshairSize.ToString("f2"), 2);
            //Collider hit; 
            //Debug.Log("00");
            if (Target.AimValue == 1f)
            {
                UiManager.SetCrosshairEnable(true);
                UiManager.SetCrosshairSize(CrosshairSize);

                if (PhysicsHelper.Raycast(cam.Position, cam.Forward, out cameraRaycastHit, 100, PhysicsHelper.Layer_DamageReceiver_Environment))
                {
                    //Debug.Log("10");
                    UpdateAimTargetByCameraRaycastHit();
                }
                else
                {
                    //Debug.Log("20");
                    distance = 100;
                    Clear(cam.GetForward(100, false));
                    CanShoot = true;
                }
            }
            else
            {
                //Debug.Log("30");
                UiManager.SetCrosshairEnable(false);
                distance = 100;
                Clear(cam.GetForward(100, false));
                CanShoot = true;
            }
            //Debug.Log("99");
        }

        public void Init()
        {
            //Notification.Register(EnumNotifType.PlayerStateChanged, Id, UpdateStatus);
        }

        //private void UpdateStatus(NotificationParameter obj)
        //{
        //    aimState = obj.Get<EnumAimState>(EnumNotifParam.AimState);

        //}
    }
}
