﻿

using RCPU.Attributes;
using UnityEngine;

namespace RCPU.CameraFSM
{
    [RequireComponent(typeof(Camera))]
    [InjectToSystem(typeof(IUiCamera))]
    [InitOrder(EnumInitOrder.UiCamera)]
    public class UiCamera : BaseObject, IUiCamera
    {
        public Camera cam { get; protected set; }

        public void Init()
        {
            cam = GetComponent<Camera>();
            enabled = false;
        }
    }
}
