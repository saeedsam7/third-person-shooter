﻿

using UnityEngine;

namespace RCPU.CameraFSM
{
    public interface ICameraAimTarget : IInitializable
    {
        Vector3 Position { get; }
        Vector3 Center { get; }
        int HitId { get; }
        bool Hit { get; }
        bool CanShoot { get; }
        void LookAt(Vector3 pos);
        //float CrosshairSize { get; }

    }
}
