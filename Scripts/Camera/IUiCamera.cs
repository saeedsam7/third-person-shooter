﻿using UnityEngine;

namespace RCPU.CameraFSM
{
    public interface IUiCamera : IInitializable
    {
        Camera cam { get; }
    }
}
