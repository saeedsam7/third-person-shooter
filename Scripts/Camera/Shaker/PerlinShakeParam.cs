﻿using System;
using UnityEngine;

namespace RCPU.CameraFSM
{


    [Serializable]
    public class PerlinShakeParam
    {
        /// <summary>
        /// Strength of the shake for each axis.
        /// </summary>
        [Tooltip("Strength of the shake for each axis.")]
        public Displacement strength = new Displacement(Vector3.zero, new Vector3(2, 2, 0.8f));

        /// <summary>
        /// Layers of perlin noise with different frequencies.
        /// </summary>
        [Tooltip("Layers of perlin noise with different frequencies.")]
        public NoiseMode[] noiseModes = { new NoiseMode(12, 1) };

        /// <summary>
        /// Strength over time.
        /// </summary>
        [Tooltip("Strength of the shake over time.")]
        public EnvelopeParams envelope;

        /// <summary>
        /// How strength falls with distance from the shake source.
        /// </summary>
        [Tooltip("How strength falls with distance from the shake source.")]
        public StrengthAttenuationParams attenuation;
    }


    [Serializable]
    public struct NoiseMode
    {
        public NoiseMode(float freq, float amplitude)
        {
            this.freq = freq;
            this.amplitude = amplitude;
        }

        /// <summary>
        /// Frequency multiplier for the noise.
        /// </summary>
        [Tooltip("Frequency multiplier for the noise.")]
        public float freq;

        /// <summary>
        /// Amplitude of the mode.
        /// </summary>
        [Tooltip("Amplitude of the mode.")]
        [Range(0, 1)]
        public float amplitude;
    }
}
