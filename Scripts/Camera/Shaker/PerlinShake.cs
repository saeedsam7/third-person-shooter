﻿
using RCPU.Helper;
using UnityEngine;
using Random = UnityEngine.Random;

namespace RCPU.CameraFSM
{
    internal class PerlinShake : ICameraShake
    {
        readonly PerlinShakeParam pars;
        readonly Envelope envelope;

        public IAmplitudeController AmplitudeController;

        Vector2[] seeds;
        float time;
        Vector3? sourcePosition;
        float norm;

        /// <summary>
        /// Creates an instance of PerlinShake.
        /// </summary>
        /// <param name="parameters">Parameters of the shake.</param>
        /// <param name="maxAmplitude">Maximum amplitude of the shake.</param>
        /// <param name="sourcePosition">World position of the source of the shake.</param>
        /// <param name="manualStrengthControl">Pass true if you want to control amplitude manually.</param>
        public PerlinShake(
            PerlinShakeParam parameters,
            float maxAmplitude = 1,
            Vector3? sourcePosition = null,
            bool manualStrengthControl = false)
        {
            pars = parameters;
            envelope = new Envelope(pars.envelope, maxAmplitude,
                manualStrengthControl ?
                    Envelope.EnvelopeControlMode.Manual : Envelope.EnvelopeControlMode.Auto);
            AmplitudeController = envelope;
            this.sourcePosition = sourcePosition;
        }

        public Displacement CurrentDisplacement { get; private set; }
        public bool IsFinished { get; private set; }

        public void Initialize(Vector3 cameraPosition, Quaternion cameraRotation)
        {
            seeds = new Vector2[pars.noiseModes.Length];
            norm = 0;
            for (int i = 0; i < seeds.Length; i++)
            {
                seeds[i] = Random.insideUnitCircle * 20;
                norm += pars.noiseModes[i].amplitude;
            }
        }

        public void Update(float deltaTime, Vector3 cameraPosition, Quaternion cameraRotation)
        {
            if (envelope.IsFinished)
            {
                IsFinished = true;
                return;
            }
            time += deltaTime;
            envelope.Update(deltaTime);

            Displacement disp = Displacement.Zero;
            for (int i = 0; i < pars.noiseModes.Length; i++)
            {
                disp += pars.noiseModes[i].amplitude / norm *
                    SampleNoise(seeds[i], pars.noiseModes[i].freq);
            }

            CurrentDisplacement = envelope.Intensity * Displacement.Scale(disp, pars.strength);
            if (sourcePosition != null)
                CurrentDisplacement *= CommonHelper.Strength(pars.attenuation, sourcePosition.Value, cameraPosition);
        }

        private Displacement SampleNoise(Vector2 seed, float freq)
        {
            Vector3 position = new Vector3(
                Mathf.PerlinNoise(seed.x + time * freq, seed.y),
                Mathf.PerlinNoise(seed.x, seed.y + time * freq),
                Mathf.PerlinNoise(seed.x + time * freq, seed.y + time * freq));
            position -= Vector3.one * 0.5f;

            Vector3 rotation = new Vector3(
                Mathf.PerlinNoise(-seed.x - time * freq, -seed.y),
                Mathf.PerlinNoise(-seed.x, -seed.y - time * freq),
                Mathf.PerlinNoise(-seed.x - time * freq, -seed.y - time * freq));
            rotation -= Vector3.one * 0.5f;

            return new Displacement(position, rotation);
        }


    }
}
