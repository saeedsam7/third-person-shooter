﻿
using RCPU.Helper;
using UnityEngine;

namespace RCPU.CameraFSM
{
    public class BounceShake : ICameraShake
    {
        readonly BounceShakeParam pars;
        readonly AnimationCurve moveCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
        readonly Vector3? sourcePosition = null;

        float attenuation = 1;
        Displacement direction;
        Displacement previousWaypoint;
        Displacement currentWaypoint;
        int bounceIndex;
        float t;

        /// <summary>
        /// Creates an instance of BounceShake.
        /// </summary>
        /// <param name="parameters">Parameters of the shake.</param>
        /// <param name="sourcePosition">World position of the source of the shake.</param>
        public BounceShake(BounceShakeParam parameters, Vector3? sourcePosition = null)
        {
            previousWaypoint = Displacement.Zero;
            this.sourcePosition = sourcePosition;
            pars = parameters;
            Displacement rnd = Displacement.InsideUnitSpheres();
            direction = Displacement.Scale(rnd, pars.axesMultiplier).Normalized;
        }

        /// <summary>
        /// Creates an instance of BounceShake.
        /// </summary>
        /// <param name="parameters">Parameters of the shake.</param>
        /// <param name="initialDirection">Initial direction of the shake motion.</param>
        /// <param name="sourcePosition">World position of the source of the shake.</param>
        public BounceShake(BounceShakeParam parameters, Displacement initialDirection, Vector3? sourcePosition = null)
        {
            previousWaypoint = Displacement.Zero;
            this.sourcePosition = sourcePosition;
            pars = parameters;
            direction = Displacement.Scale(initialDirection, pars.axesMultiplier).Normalized;
        }

        public Displacement CurrentDisplacement { get; private set; }
        public bool IsFinished { get; private set; }
        public void Initialize(Vector3 cameraPosition, Quaternion cameraRotation)
        {
            attenuation = sourcePosition == null ?
                1 : CommonHelper.Strength(pars.attenuation, sourcePosition.Value, cameraPosition);
            currentWaypoint = attenuation * direction.ScaledBy(pars.positionStrength, pars.rotationStrength);
        }

        public void Update(float deltaTime, Vector3 cameraPosition, Quaternion cameraRotation)
        {
            if (t < 1)
            {

                t += deltaTime * pars.freq;
                if (pars.freq == 0) t = 1;

                CurrentDisplacement = Displacement.Lerp(previousWaypoint, currentWaypoint, moveCurve.Evaluate(t));
            }
            else
            {
                t = 0;
                CurrentDisplacement = currentWaypoint;
                previousWaypoint = currentWaypoint;
                bounceIndex++;
                if (bounceIndex > pars.numBounces)
                {
                    IsFinished = true;
                    return;
                }

                Displacement rnd = Displacement.InsideUnitSpheres();
                direction = -direction + pars.randomness * Displacement.Scale(rnd, pars.axesMultiplier).Normalized;
                direction = direction.Normalized;
                float decayValue = 1 - (float)bounceIndex / pars.numBounces;
                currentWaypoint = decayValue * decayValue * attenuation
                    * direction.ScaledBy(pars.positionStrength, pars.rotationStrength);
            }
        }
    }
}
