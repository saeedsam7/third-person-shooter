﻿namespace RCPU.CameraFSM
{
    public interface ICameraShakeGenerator
    {
        ICameraShake Make(CameraShakeParam param);
    }
}
