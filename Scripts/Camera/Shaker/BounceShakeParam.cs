﻿using System;
using UnityEngine;

namespace RCPU.CameraFSM
{
    [Serializable]
    public class BounceShakeParam
    {
        /// <summary>
        /// Strength of the shake for positional axes.
        /// </summary>
        [Tooltip("Strength of the shake for positional axes.")]
        public float positionStrength = 0.05f;

        /// <summary>
        /// Strength of the shake for rotational axes.
        /// </summary>
        [Tooltip("Strength of the shake for rotational axes.")]
        public float rotationStrength = 0.1f;

        /// <summary>
        /// Preferred direction of shaking.
        /// </summary>
        [Tooltip("Preferred direction of shaking.")]
        public Displacement axesMultiplier = new Displacement(Vector2.one, Vector3.forward);

        /// <summary>
        /// Frequency of shaking.
        /// </summary>
        [Tooltip("Frequency of shaking.")]
        public float freq = 25;

        /// <summary>
        /// Number of vibrations before stop.
        /// </summary>
        [Tooltip("Number of vibrations before stop.")]
        public int numBounces = 5;

        /// <summary>
        /// Randomness of motion.
        /// </summary>
        [Range(0, 1)]
        [Tooltip("Randomness of motion.")]
        public float randomness = 0.5f;

        /// <summary>
        /// How strength falls with distance from the shake source.
        /// </summary>
        [Tooltip("How strength falls with distance from the shake source.")]
        public StrengthAttenuationParams attenuation;
    }
}
