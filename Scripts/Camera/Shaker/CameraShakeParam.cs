﻿

using System;
using UnityEngine;

namespace RCPU.CameraFSM
{
    [Serializable]
    public class CameraShakeParam
    {
        public EnumCameraShakeType ShakeType;
        public float Power;
        public float Duration_Bounce;
        [HideInInspector] public Vector3 position;
    }
}
