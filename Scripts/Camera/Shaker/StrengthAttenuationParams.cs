﻿

using UnityEngine;

namespace RCPU.CameraFSM
{
    [System.Serializable]
    public class StrengthAttenuationParams
    {
        /// <summary>
        /// Radius in which shake doesn't lose strength.
        /// </summary>
        [Tooltip("Radius in which shake doesn't lose strength.")]
        public float clippingDistance = 10;

        /// <summary>
        /// Defines how fast strength falls with distance.
        /// </summary>
        [Tooltip("How fast strength falls with distance.")]
        public float falloffScale = 50;

        /// <summary>
        /// Power of the falloff function.
        /// </summary>
        [Tooltip("Power of the falloff function.")]
        public EnumPower falloffDegree = EnumPower.Quadratic;

        /// <summary>
        /// Contribution of each axis to distance. E. g. (1, 1, 0) for a 2D game in XY plane.
        /// </summary>
        [Tooltip("Contribution of each axis to distance. E. g. (1, 1, 0) for a 2D game in XY plane.")]
        public Vector3 axesMultiplier = Vector3.one;
    }
}
