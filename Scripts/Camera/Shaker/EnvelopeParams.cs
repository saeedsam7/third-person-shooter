﻿

using UnityEngine;

namespace RCPU.CameraFSM
{
    [System.Serializable]
    public class EnvelopeParams
    {
        /// <summary>
        /// How fast the amplitude rises.
        /// </summary>
        [Tooltip("How fast the amplitude increases.")]
        public float attack = 10;

        /// <summary>
        /// How long in seconds the amplitude holds a maximum value.
        /// </summary>
        [Tooltip("How long in seconds the amplitude holds maximum value.")]
        public float sustain = 0;

        /// <summary>
        /// How fast the amplitude falls.
        /// </summary>
        [Tooltip("How fast the amplitude decreases.")]
        public float decay = 1f;

        /// <summary>
        /// Power in which the amplitude is raised to get intensity.
        /// </summary>
        [Tooltip("Power in which the amplitude is raised to get intensity.")]
        public EnumPower degree = EnumPower.Cubic;
    }
}
