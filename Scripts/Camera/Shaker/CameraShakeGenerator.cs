﻿

using RCPU.Attributes;
using UnityEngine;

namespace RCPU.CameraFSM
{
    [InitOrder(EnumInitOrder.CameraShake)]
    [InjectToInstance(typeof(ICameraShakeGenerator))]
    public class CameraShakeGenerator : BaseObject, ICameraShakeGenerator
    {
        public ICameraShake Make(CameraShakeParam param)
        {
            switch (param.ShakeType)
            {
                case EnumCameraShakeType.Bounce:
                    return MakeBounce(param);
                case EnumCameraShakeType.Perlin:
                    return MakePerlin(param);
                case EnumCameraShakeType.Kick:
                    return MakeKick(param);
            }
            return null;
        }

        private ICameraShake MakeKick(CameraShakeParam param)
        {
            KickShakeParam pars = new KickShakeParam
            {
                strength = new Displacement(Vector3.zero, new Vector3(1, 1, 0.5f) * param.Power),
            };
            return new KickShake(pars, param.position, true);
        }

        private ICameraShake MakePerlin(CameraShakeParam param)
        {

            NoiseMode[] modes =  {
                new NoiseMode(6, 1),
                new NoiseMode(20, 0.2f)  };

            EnvelopeParams envelopePars = new EnvelopeParams();
            envelopePars.decay = param.Duration_Bounce <= 0 ? 1 : 1 / param.Duration_Bounce;
            PerlinShakeParam pars = new PerlinShakeParam
            {
                strength = new Displacement(Vector3.zero, new Vector3(1, 1, 0.5f) * param.Power),
                noiseModes = modes,
                envelope = envelopePars,
            };
            return new PerlinShake(pars);
        }

        private ICameraShake MakeBounce(CameraShakeParam param)
        {
            float freq = 25;
            BounceShakeParam pars = new BounceShakeParam
            {
                axesMultiplier = new Displacement(Vector3.zero, new Vector3(1, 1, 0.5f)),
                rotationStrength = param.Power,
                freq = freq,
                numBounces = Mathf.RoundToInt(param.Duration_Bounce)
            };
            return new BounceShake(pars);
        }
    }
}
