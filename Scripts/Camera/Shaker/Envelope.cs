﻿

using RCPU.Helper;
using UnityEngine;

namespace RCPU.CameraFSM
{
    /// <summary>
    /// Controls strength of the shake over time.
    /// </summary>
    public class Envelope : IAmplitudeController
    {
        readonly EnvelopeParams pars;
        readonly EnvelopeControlMode controlMode;

        float amplitude;
        float targetAmplitude;
        float sustainEndTime;
        bool finishWhenAmplitudeZero;
        bool finishImmediately;
        EnvelopeState state;

        /// <summary>
        /// Creates an Envelope instance.
        /// </summary>
        /// <param name="pars">Envelope parameters.</param>
        /// <param name="controlMode">Pass Auto for a single shake, or Manual for controlling strength manually.</param>
        public Envelope(EnvelopeParams pars, float initialTargetAmplitude, EnvelopeControlMode controlMode)
        {
            this.pars = pars;
            this.controlMode = controlMode;
            SetTarget(initialTargetAmplitude);
        }

        /// <summary>
        /// The value by which you want to multiply shake displacement.
        /// </summary>
        public float Intensity { get; private set; }

        public bool IsFinished
        {
            get
            {
                if (finishImmediately) return true;
                return (finishWhenAmplitudeZero || controlMode == EnvelopeControlMode.Auto)
                    && amplitude <= 0 && targetAmplitude <= 0;
            }
        }

        public void Finish()
        {
            finishWhenAmplitudeZero = true;
            SetTarget(0);
        }

        public void FinishImmediately()
        {
            finishImmediately = true;
        }

        /// <summary>
        /// Update is called every frame by the shake.
        /// </summary>
        public void Update(float deltaTime)
        {
            if (IsFinished) return;

            if (state == EnvelopeState.Increase)
            {
                if (pars.attack > 0)
                    amplitude += deltaTime * pars.attack;
                if (amplitude > targetAmplitude || pars.attack <= 0)
                {
                    amplitude = targetAmplitude;
                    state = EnvelopeState.Sustain;
                    if (controlMode == EnvelopeControlMode.Auto)
                        sustainEndTime = Time.time + pars.sustain;
                }
            }
            else
            {
                if (state == EnvelopeState.Decrease)
                {

                    if (pars.decay > 0)
                        amplitude -= deltaTime * pars.decay;
                    if (amplitude < targetAmplitude || pars.decay <= 0)
                    {
                        amplitude = targetAmplitude;
                        state = EnvelopeState.Sustain;
                    }
                }
                else
                {
                    if (controlMode == EnvelopeControlMode.Auto && Time.time > sustainEndTime)
                    {
                        SetTarget(0);
                    }
                }
            }

            amplitude = Mathf.Clamp01(amplitude);
            Intensity = CommonHelper.Power(amplitude, pars.degree);
        }

        public void SetTargetAmplitude(float value)
        {
            if (controlMode == EnvelopeControlMode.Manual && !finishWhenAmplitudeZero)
            {
                SetTarget(value);
            }
        }

        private void SetTarget(float value)
        {
            targetAmplitude = Mathf.Clamp01(value);
            state = targetAmplitude > amplitude ? EnvelopeState.Increase : EnvelopeState.Decrease;
        }


        public enum EnvelopeControlMode { Auto, Manual }

        public enum EnvelopeState { Sustain, Increase, Decrease }
    }
}


