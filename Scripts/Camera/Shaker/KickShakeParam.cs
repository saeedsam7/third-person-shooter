﻿using System;
using UnityEngine;

namespace RCPU.CameraFSM
{
    [Serializable]
    public class KickShakeParam
    {
        /// <summary>
        /// Strength of the shake for each axis.
        /// </summary>
        [Tooltip("Strength of the shake for each axis.")]
        public Displacement strength = new Displacement(Vector3.zero, Vector3.one);

        /// <summary>
        /// How long it takes to move forward.
        /// </summary>
        [Tooltip("How long it takes to move forward.")]
        public float attackTime = 0.05f;
        public AnimationCurve attackCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);

        /// <summary>
        /// How long it takes to move back.
        /// </summary>
        [Tooltip("How long it takes to move back.")]
        public float releaseTime = 0.2f;
        public AnimationCurve releaseCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);

        /// <summary>
        /// How strength falls with distance from the shake source.
        /// </summary>
        [Tooltip("How strength falls with distance from the shake source.")]
        public StrengthAttenuationParams attenuation;
    }
}
