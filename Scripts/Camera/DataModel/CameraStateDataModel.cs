﻿

using RCPU.Models;
using System;
using UnityEngine;

namespace RCPU.CameraFSM
{
    [Serializable]
    public class CameraStateDataModel
    {

        public EnumCameraState State;
        //public EnumGameOveralState GameOveralState;
        //public bool AutoExit;
        //public AutoExitModel autoExitModel;
        [HideInInspector] public float EnterTime;
        [HideInInspector] public Vector2 EnterAngle;
        //public bool CanMove; 

        //public float smoothingTime = 10.0f;
        public float moveSpeed;
        public float rotateSpeed;

        public float Padding;

        public CameraOffsetData offsetLeft;
        public CameraOffsetData offsetRight;

        //public float horizontalAimingSpeed = 800f;

        //public float verticalAimingSpeed = 800f;

        //public float zoomSpeed = 5f;

        public float TransitionSpeed;
        public RangeFloat VerticalAngle;
         

        public Vector2 mouseSensitivity;

        public LayerMask mask;

        public bool applyShake;
    }
    [Serializable]
    public class CameraOffsetData
    {
        public Vector3 pivotOffset;
        public Vector3 camOffset;
        public Vector3 closeOffset;
        public float extraRotation;
    }
}
