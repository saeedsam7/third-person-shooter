﻿
using UnityEngine;

namespace RCPU.CameraFSM
{
    [CreateAssetMenu(fileName = "Camera", menuName = "RCPU/DataModel/Camera/Camera", order = 2)]
    public class CameraDataModel_SO : ScriptableObject
    { 
        public CameraStateDataModel_SO[] states;
    }
}

