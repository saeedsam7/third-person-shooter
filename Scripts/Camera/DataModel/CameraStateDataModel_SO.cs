﻿
using UnityEngine;

namespace RCPU.CameraFSM
{
    [CreateAssetMenu(fileName = "CameraState", menuName = "RCPU/DataModel/Camera/CameraState", order = 2)]
    public class CameraStateDataModel_SO : ScriptableObject
    {
        public CameraStateDataModel model;
    }
}

