﻿using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Component;
using RCPU.Helper;
using RCPU.NotificationSystem;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Noise
{
    [InjectToSystem(typeof(INoiseManager))]
    [InitOrder(EnumInitOrder.Noise)]
    public class NoiseManager : BaseObject, INoiseManager
    {
        protected Dictionary<int, INoiseDetector> allDetectors;
        protected List<Pair<float, Vector3>> allNoises;
        public void Init()
        {
            allDetectors = new Dictionary<int, INoiseDetector>();
            allNoises = new List<Pair<float, Vector3>>();
            Notification.Register(EnumNotifType.NoiseCreated, this.Id, OnNoiseCreated);
        }

        public void RegisterNoiseDetector(INoiseDetector detector)
        {
            allDetectors.Add(detector.Id, detector);
        }
        public void UnregisterNoiseDetector(INoiseDetector detector)
        {
            allDetectors.Remove(detector.Id);
        }

        private void OnNoiseCreated(NotificationParameter parameter)
        {
            var position = parameter.Get<Vector3>(EnumNotifParam.position);
            var noisePower = parameter.Get<float>(EnumNotifParam.noisePower);
            var target = parameter.Get<IAlive>(EnumNotifParam.target);

            for (int i = 0; i < allNoises.Count; i++)
            {
                if (allNoises[i].key < TimeController.gameTime)
                {
                    allNoises.RemoveAt(i);
                    i--;
                    continue;
                }
                if (CommonHelper.Distance(position, allNoises[i].value) < 1f)
                {
                    allNoises[i].key = TimeController.gameTime + 1f;
                    return;
                }
            }

            foreach (var detector in allDetectors.Values)
            {
                if (CommonHelper.Distance(position, detector.Position) < noisePower + detector.Radius)
                {
                    detector.NoiseDetected(position, target);

                }
            }
            allNoises.Add(new Pair<float, Vector3>(TimeController.gameTime + 1f, position));
        }
    }
}