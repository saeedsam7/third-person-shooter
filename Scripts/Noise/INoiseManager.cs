﻿

using RCPU.Component;

namespace RCPU.Noise
{
    public interface INoiseManager : IInitializable
    {
        void RegisterNoiseDetector(INoiseDetector detector);
        void UnregisterNoiseDetector(INoiseDetector detector);
    }
}