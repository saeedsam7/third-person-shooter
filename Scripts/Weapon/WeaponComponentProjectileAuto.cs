﻿
using RCPU.Attributes;
using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.NotificationSystem;
using UnityEngine;

namespace RCPU.Weapon
{
    [InjectToInstance(typeof(IWeaponComponentBulletAuto))]
    [InitOrder(EnumInitOrder.Weapon)]
    public class WeaponComponentProjectileAuto : WeaponComponentRange, IWeaponComponentBulletAuto
    {
        [SerializeField] protected WeaponDataModelProjectileAuto_SO model_SO;
        protected WeaponDataModelProjectileAuto model;

        public IWeaponDataModelBulletAuto DataModelBulletAuto => model;

        protected Vector3 target;

        protected override void LoadModel()
        {
            this.DataModelBulletBase = model = DInjector.Instantiate<WeaponDataModelProjectileAuto_SO>(model_SO).model;
        }
        //public override void Aim(bool value)
        //{
        //    base.Aim(value);
        //    if (value)
        //        DrawMovementLine();
        //    else
        //        lineRenderer.positionCount = 0;
        //}


        protected Vector3 GetVelocity()
        {

            Vector3[] solutions = new Vector3[2];
            int numSolutions;
            //if (curTarget.velocity.sqrMagnitude > 0)
            //    numSolutions = fts.solve_ballistic_arc(projPos, projSpeed, targetPos, curTarget.velocity, gravity, out solutions[0], out solutions[1]);
            //else
            numSolutions = fts.solve_ballistic_arc(BulletPos, model.bulletDataModel.speed.z, target, -PhysicsHelper.Gravity.y, out solutions[0], out solutions[1]);


            if (numSolutions > 1 && CommonHelper.Distance(BulletPos, target) > 15)
                return solutions[1];
            else
            if (numSolutions > 0)
                return solutions[0];

            return (BulletForward * BulletDataModel.Speed.z) + Vector3.up * BulletDataModel.Speed.y;
        }

        //private void DrawMovementLine()
        //{
        //    var res = rigidBody.CalculateMovement(model.stepCount, model.timeBeteenStep, GetVelocity(), Vector3.zero);

        //    lineRenderer.positionCount = res.Length + 1;
        //    lineRenderer.SetPosition(0, bulletPos.position);
        //    for (int i = 0; i < res.Length; ++i)
        //    {
        //        lineRenderer.SetPosition(i + 1, res[i]);
        //    }
        //}


        protected override void FireBullet(Vector3 target)
        {
            this.target = target;
            Pool.Activate(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.dataModel, EnumNotifParam.position, EnumNotifParam.targetPosition, EnumNotifParam.owner, EnumNotifParam.velocity },
                                                       new object[] { Pool.GetEquivalentType(bulletType), BulletDataModel, BulletPos, target, owner, GetVelocity() }));


            Pool.Activate(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.position, EnumNotifParam.rotation },
                                                       new object[] { Pool.GetEquivalentType(ShellType), ShellPos, ShellForward }));
        }


        //protected override void Fire()
        //{
        //    base.Fire();
        //    CrosshairSize = Mathf.Clamp(CrosshairSize + CrosshairSizeIncreament, 0, MaxCrosshairSize);

        //}





    }
}