﻿
using RCPU.Attributes;
using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.Models;
using RCPU.NotificationSystem;
using UnityEngine;

namespace RCPU.Weapon
{
    [InjectToInstance(typeof(IWeaponComponentBulletAuto))]
    [InitOrder(EnumInitOrder.Weapon)]
    internal class WeaponComponentBulletAuto : WeaponComponentRange, IWeaponComponentBulletAuto
    {

        [SerializeField] protected WeaponDataModelBulletAuto_SO model_SO;
        protected WeaponDataModelBulletAuto model;

        public RangeFloat AttackDistanceRange => model.AttackDistanceRange;
        public bool AutoControl => model.AutoControl;
        public float AutoRotateSpeed => model.AutoRotateSpeed;

        public IWeaponDataModelBulletAuto DataModelBulletAuto => model;

        protected override void LoadModel()
        {
            this.DataModelBulletBase = model = DInjector.Instantiate<WeaponDataModelBulletAuto_SO>(model_SO).model;
        }

        protected override void FireBullet(Vector3 target)
        {
            Pool.Activate(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.dataModel, EnumNotifParam.position, EnumNotifParam.targetPosition, EnumNotifParam.owner },
                                                       new object[] { Pool.GetEquivalentType(bulletType), BulletDataModel, BulletPos, target, owner }));

            Pool.Activate(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.position, EnumNotifParam.rotation },
                                                       new object[] { Pool.GetEquivalentType(ShellType), ShellPos, ShellForward }));
        }

        public override bool LookAt(Vector3 target)
        {
            if (model.AutoControl)
            {
                //aimTarget = target;
                //CLog.Log(" weapon LookAt " + target + " independentRotateSpeed " + model.independentRotateSpeed, CLog.EnumPriority.Normal); 
                CommonHelper.LinearLookAt(transform, target, model.AutoRotateSpeed, false);
            }
            return base.LookAt(target);
        }
    }
}
