﻿

using UnityEngine;

namespace RCPU.Weapon
{
    [CreateAssetMenu(fileName = "WeaponBulletAuto", menuName = "RCPU/DataModel/Weapon/WeaponBulletAuto", order = 1)]
    public class WeaponDataModelBulletAuto_SO : ScriptableObject
    {
        public WeaponDataModelBulletAuto model;
    }
}