﻿

using RCPU.Audio;
using RCPU.Models;
using System;
using UnityEngine;

namespace RCPU.Weapon
{
    [Serializable]
    public class WeaponDataModelBulletBase : WeaponDataModelBase, IWeaponDataModelBulletBase
    {
        [Header("Ammo Properties")]
        //public int maxAmmoCount;
        public int clipSize;

        [Header("Recoil")]
        public RangeFloat defaultRecoil;
        public RangeFloat aimRecoil;


        [Header("Crosshair")]
        //[Range(.01f, 10f)]
        public float maxCrosshairSize;
        //[Range(.01f, 10f)]
        public float crosshairSizeIncreament;
        //[Range(.01f, 10f)]
        public float crosshairCoolDown;

        [Header("Reload")]
        //public EnumAnimLayer AnimLayer;
        public float animatorReloadMultiplier;
        public float reloadTime;


        [Header("Bullet")]
        public EnumAmmoType bulletType;
        public BulletDataModel bulletDataModel;

        [Header("Sound")]
        public EnumAudioSFX sfxShoot;
        public EnumAudioSFX sfxReload;
        public EnumAudioSFX sfxEmpty;

        public EnumShellType shellType;

        public float MaxCrosshairSize => maxCrosshairSize;

        public float CrosshairSizeIncreament => crosshairSizeIncreament;

        public IBulletDataModel BulletDataModel => bulletDataModel;

        //public int MaxAmmoCount => maxAmmoCount;

        public int ClipSize => clipSize;

        public RangeFloat DefaultRecoil => defaultRecoil;

        public RangeFloat AimRecoil => aimRecoil;

        public float CrosshairCoolDown => crosshairCoolDown;

        public float AnimatorReloadMultiplier => animatorReloadMultiplier;

        public float ReloadTime => reloadTime;

        public EnumAmmoType BulletType => bulletType;



        public EnumAudioSFX SfxShoot => sfxShoot;

        public EnumAudioSFX SfxReload => sfxReload;

        public EnumAudioSFX SfxEmpty => sfxEmpty;

        public EnumShellType ShellType => shellType;

    }
}
