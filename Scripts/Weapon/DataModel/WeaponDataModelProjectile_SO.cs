﻿


using UnityEngine;

namespace RCPU.Weapon
{
    [CreateAssetMenu(fileName = "WeaponProjectile", menuName = "RCPU/DataModel/Weapon/WeaponProjectile", order = 1)]
    public class WeaponDataModelProjectileManual_SO : ScriptableObject
    {
        public WeaponDataModelProjectileManual model;
    }
}