﻿
using RCPU.Models;
using RCPU.UI;
using System;

namespace RCPU.Weapon
{
    [Serializable]
    public class WeaponDataModelManual : WeaponDataModelBase, IWeaponDataModelManual
    {
        public virtual EnumCrosshairType CrosshairType => throw new NotImplementedException();
        public virtual EnumSpriteIndex Icon => throw new NotImplementedException();

        public virtual RangeFloat DefaultRecoil => throw new NotImplementedException();

        public virtual RangeFloat AimRecoil => throw new NotImplementedException();

        public virtual float CrosshairSizeIncreament => throw new NotImplementedException();

        public virtual float CrosshairCoolDown => throw new NotImplementedException();


    }
}