﻿
using RCPU.CameraFSM;
using RCPU.InputSystem;
using System;
using UnityEngine;

namespace RCPU.Weapon
{
    [Serializable]
    public class WeaponDataModelBase : IWeaponDataModelBase
    {
        [Header("Gameplay Properties")]
        public int level;
        public float eachFireDelay;
        public float eachFireDuration;
        public float noisePower;

        public bool burstFire;
        public CameraShakeParam cameraShake;
        public EnumAttckType attckType;
        public EnumWeaponType weaponType;
        public EnumWeaponName weaponName;
        public EnumWeaponSlotIndex weaponSlotIndex;


        [Header("Equip")]
        public float unequipTime;
        public float equipTime;
        public float equipSelectTime;

        public int Level => level;
        public float EachFireDelay => eachFireDelay;
        public float EachFireDuration => eachFireDuration;

        public float NoisePower => noisePower;
        public bool BurstFire => burstFire;
        public CameraShakeParam CameraShake => cameraShake;
        public EnumWeaponName WeaponName => weaponName;
        public EnumWeaponSlotIndex WeaponSlotIndex => weaponSlotIndex;



        public EnumWeaponType WeaponType => weaponType;
        public EnumAttckType AttckType => attckType;
        public float EquipTime => equipTime;
        public float UnequipTime => unequipTime;
        public float EquipSelectTime => equipSelectTime;


    }
}
