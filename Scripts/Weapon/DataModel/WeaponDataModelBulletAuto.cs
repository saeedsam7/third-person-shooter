﻿using RCPU.Models;
using System;
using UnityEngine;

namespace RCPU.Weapon
{
    [Serializable]
    public class WeaponDataModelBulletAuto : WeaponDataModelBulletBase, IWeaponDataModelBulletAuto
    {
        [Header("Auto")]
        public bool autoControl;
        public float autoRotateSpeed;
        public RangeFloat attackDistanceRange;


        public virtual bool AutoControl => autoControl;
        public virtual float AutoRotateSpeed => autoRotateSpeed;
        public virtual RangeFloat AttackDistanceRange => attackDistanceRange;

    }
}
