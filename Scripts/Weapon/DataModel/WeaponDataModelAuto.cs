﻿

using RCPU.Models;
using System;

namespace RCPU.Weapon
{
    [Serializable]
    public class WeaponDataModelAuto : WeaponDataModelBase, IWeaponDataModelAuto
    {
        public virtual bool AutoControl => throw new NotImplementedException();

        public virtual float AutoRotateSpeed => throw new NotImplementedException();

        public virtual RangeFloat AttackDistanceRange => throw new NotImplementedException();
    }
}