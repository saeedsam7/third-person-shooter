﻿using System;

namespace RCPU.Weapon
{
    [Serializable]
    public class WeaponDataModelProjectileManual : WeaponDataModelBulletManual, IWeaponDataModelProjectileManual
    {
        public float timeBetweenStep;
        public int stepCount;

        public float TimeBetweenStep => timeBetweenStep;

        public int StepCount => stepCount;


    }
}