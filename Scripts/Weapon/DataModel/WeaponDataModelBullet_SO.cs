﻿
using UnityEngine;

namespace RCPU.Weapon
{
    [CreateAssetMenu(fileName = "WeaponBulletManual", menuName = "RCPU/DataModel/Weapon/WeaponBulletManual", order = 1)]
    public class WeaponDataModelBulletManual_SO : ScriptableObject
    {
        public WeaponDataModelBulletManual model;



    }
}