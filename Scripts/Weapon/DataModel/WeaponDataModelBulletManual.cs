﻿
using RCPU.UI;
using System;
using UnityEngine; 

namespace RCPU.Weapon
{
    [Serializable]
    public class WeaponDataModelBulletManual : WeaponDataModelBulletBase, IWeaponDataModelBulletManual
    {
        [Header("Ui")]
        public EnumCrosshairType crosshairType;
        public EnumSpriteIndex icon;


        public virtual EnumCrosshairType CrosshairType => crosshairType;
        public virtual EnumSpriteIndex Icon => icon;
         
    }
}
