﻿

using RCPU.Audio;
using RCPU.CameraFSM;
using RCPU.InputSystem;
using RCPU.Models;
using RCPU.UI;

namespace RCPU.Weapon
{
    public interface IWeaponDataModelBase
    {
        int Level { get; }
        EnumWeaponType WeaponType { get; }
        EnumAttckType AttckType { get; }

        float EachFireDelay { get; }
        float EachFireDuration { get; }
        float NoisePower { get; }
        bool BurstFire { get; }
        CameraShakeParam CameraShake { get; }
        EnumWeaponName WeaponName { get; }
        EnumWeaponSlotIndex WeaponSlotIndex { get; }

        float EquipTime { get; }
        float UnequipTime { get; }
        float EquipSelectTime { get; }

    }

    public interface IWeaponDataModelManual : IWeaponDataModelBase
    {
        EnumCrosshairType CrosshairType { get; }
        EnumSpriteIndex Icon { get; }
        RangeFloat DefaultRecoil { get; }
        RangeFloat AimRecoil { get; }
        float CrosshairSizeIncreament { get; }
        float CrosshairCoolDown { get; }
    }

    public interface IWeaponDataModelAuto : IWeaponDataModelBase
    {
        RangeFloat AttackDistanceRange { get; }
        bool AutoControl { get; }
        float AutoRotateSpeed { get; }
    }

    public interface IWeaponDataModelBulletBase : IWeaponDataModelBase
    {
        //int MaxAmmoCount { get; }
        int ClipSize { get; }
        float MaxCrosshairSize { get; }
        //public EnumAnimLayer AnimLayer;
        float AnimatorReloadMultiplier { get; }
        float ReloadTime { get; }
        EnumShellType ShellType { get; }

        EnumAmmoType BulletType { get; }
        IBulletDataModel BulletDataModel { get; }

        EnumAudioSFX SfxShoot { get; }
        EnumAudioSFX SfxReload { get; }
        EnumAudioSFX SfxEmpty { get; }
    }

    public interface IWeaponDataModelBulletAuto : IWeaponDataModelBulletBase, IWeaponDataModelAuto
    {
    }

    public interface IWeaponDataModelBulletManual : IWeaponDataModelBulletBase, IWeaponDataModelManual
    {
    }

    public interface IWeaponDataModelProjectileAuto : IWeaponDataModelBulletAuto
    {

    }

    public interface IWeaponDataModelProjectileManual : IWeaponDataModelBulletManual
    {
        float TimeBetweenStep { get; }
        int StepCount { get; }
    }

}
