﻿

using UnityEngine;

namespace RCPU.Weapon
{
    [CreateAssetMenu(fileName = "WeaponProjectileAuto", menuName = "RCPU/DataModel/Weapon/WeaponProjectileAuto", order = 1)]
    public class WeaponDataModelProjectileAuto_SO : ScriptableObject
    {
        public WeaponDataModelProjectileAuto model;
    }
}