﻿

using RCPU.InputSystem;
using RCPU.UI;

namespace RCPU.Weapon
{

    public interface IWeaponUiData
    {
        int TotalAmmo { get; }
        int AmmoInClip { get; }
        EnumSpriteIndex Icon { get; }
        EnumWeaponSlotIndex WeaponSlotIndex { get; }
    }

}