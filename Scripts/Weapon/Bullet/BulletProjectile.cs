﻿
using RCPU.Agent;
using RCPU.Helper;
using RCPU.NotificationSystem;
using UnityEngine;


namespace RCPU.Weapon
{
    [RequireComponent(typeof(Rigidbody))]
    public class BulletProjectile : BulletBase
    {

        public override void SetVelocity(Vector3 velocity)
        {
            rigidBody.velocity = velocity;
        }



        protected override void CheckCollision(Collider collider)
        {
            if (isAlive)
            {
                PhysicsHelper.Raycast(transform.position - transform.forward, transform.forward, 1, collider, PhysicsHelper.Layer_DamageReceiver_Environment, out RaycastHit hit);
                Die(collider, hit);
            }
        }

        public void MakeDamge()
        {
            Pool.Activate(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.position, EnumNotifParam.rotation },
                                                       new object[] { Pool.GetEquivalentType(model.ExplosionType), transform.position, transform.rotation }));



            foreach (var item in LevelManager.GetAgentsInArea(transform.position, model.Radius))
            {
                item.Key.ApplyDamage(model.MakeDamage(Id, transform.position, owner.Position, owner.Id, item.Value));
            }
        }

        public override void Die(Collider other, RaycastHit hit)
        {
            MakeDamge();
            SetEnable(false); ;
        }


        protected void Fire(IBulletDataModel model, Vector3 start, Vector3 target, IAlive owner, Vector3 velocity)
        {
            DEBUG_StartPos = start;
            this.owner = owner;
            this.model = model;
            transform.position = start;
            life = TimeController.gameTime + model.LifeTime;
            oldPos = start;
            transform.LookAt(target);
            SetVelocity(velocity);
            tr.Clear();
            tr.AddPosition(start);
            SetEnable(true);

        }

        public override void Activate(NotificationParameter param)
        {
            Fire(param.Get<IBulletDataModel>(EnumNotifParam.dataModel), param.Get<Vector3>(EnumNotifParam.position), param.Get<Vector3>(EnumNotifParam.targetPosition), param.Get<IAlive>(EnumNotifParam.owner), param.Get<Vector3>(EnumNotifParam.velocity));
        }
    }
}
