﻿using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Helper;
using RCPU.NotificationSystem;
using RCPU.Pool;
using UnityEngine;


namespace RCPU.Weapon
{
    [RequireComponent(typeof(Rigidbody))]
    public class BulletBase : BaseObject, IBullet
    {
        [InjectFromSystem] protected ILevelManager LevelManager { set; get; }
        [InjectFromSystem] protected IRelation Relation { set; get; }
        [InjectFromSystem] protected IPool Pool { set; get; }


        protected IBulletDataModel model;
        protected bool isAlive;
        protected float life;
        protected IAlive owner;
        protected Vector3 oldPos;

        protected Vector3 DEBUG_StartPos;

        [SerializeField] protected Rigidbody rigidBody;
        [SerializeField] protected TrailRenderer tr;
        [SerializeField] protected EnumAmmoType bulletType;

        public EnumAmmoType BulletType => bulletType;
        public bool IsReady => !isAlive;

        public virtual void Init(int index)
        {
            name += " " + index;
            SetEnable(false);
        }

        public void SetEnable(bool val)
        {
            //Debug.Log(name + " SetEnable " + val);
            gameObject.SetActive(val);
            isAlive = val;
        }

        protected void Fire(IBulletDataModel model, Vector3 start, Vector3 target, IAlive owner)
        {
            DEBUG_StartPos = start;
            this.owner = owner;
            this.model = model;
            transform.position = start;
            life = TimeController.gameTime + model.LifeTime;
            oldPos = start;
            transform.LookAt(target);
            SetVelocity(model.Speed);
            tr.Clear();
            tr.AddPosition(start);
            SetEnable(true);
        }
        public virtual void SetVelocity(Vector3 velocity)
        {
            rigidBody.velocity = (transform.forward * velocity.z);
        }

        public virtual void CopyRigidbody(Rigidbody rigid)
        {
            rigid.mass = rigidBody.mass;
            rigid.drag = rigidBody.drag;
            rigid.angularDrag = rigidBody.angularDrag;
        }

        public virtual void Move()
        {
            //transform.position += transform.forward * bullet_Base_Model.speed * TimeController.deltaTime;
            if (life < TimeController.gameTime)
            {
                Die(null, default(RaycastHit));
            }
        }
        void FixedUpdate()
        {
            if (isAlive)
            {
                Move();
                //if(DEBUG)
                Debug.DrawLine(DEBUG_StartPos, transform.position, Color.blue);
                if (isAlive)
                {
                    GizmosHelper.DrawArrow(oldPos, (transform.position - oldPos), CommonHelper.Distance(oldPos, transform.position) + .1f, Color.yellow, .2f);
                    if (PhysicsHelper.Raycast(oldPos, transform.position - oldPos, out RaycastHit hit, CommonHelper.Distance(oldPos, transform.position) + .1f, PhysicsHelper.Layer_DamageReceiver_Environment))
                    {
                        CheckCollision(hit.collider);

                    }
                    Debug.DrawLine(oldPos, transform.position, Color.blue);
                    Debug.DrawLine(oldPos + (Vector3.up * CommonHelper.Distance(oldPos, transform.position)), oldPos + (Vector3.down * CommonHelper.Distance(oldPos, transform.position)), Color.blue);
                    Debug.DrawLine(oldPos + (Vector3.up * CommonHelper.Distance(oldPos, transform.position)), transform.position, Color.blue);
                    Debug.DrawLine(oldPos + (Vector3.down * CommonHelper.Distance(oldPos, transform.position)), transform.position, Color.blue);
                    oldPos = transform.position;
                }
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (isAlive)
            {
                //Debug.LogError(" OnCollisionEnter " + collision.collider.name);
                CheckCollision(collision.collider);
            }

        }

        private void OnTriggerEnter(Collider other)
        {
            //CLog.Error(" OnTriggerEnter " + other.name);
            if (isAlive)
            {
                CheckCollision(other);
            }
        }

        protected virtual void CheckCollision(Collider collider)
        {
            if (isAlive)
            {
                if (PhysicsHelper.ShouldIgnoreCollision(collider.gameObject, gameObject))
                    return;
                int colId = collider.GetInstanceID();
                var alive = LevelManager.GetDamageManagerExcept(colId, owner.Id);
                // CLog.Error(" OnTriggerEnter other " + other.name + "  name " + name + " position " + transform.position + " other.layer " + LayerMask.LayerToName(other.gameObject.layer) + " gameObject.layer " + LayerMask.LayerToName(gameObject.layer));
                //CLog.Error(" OnTriggerEnter  other.layer " + LayerMask.LayerToName(other.gameObject.layer) + " gameObject.layer " + LayerMask.LayerToName(gameObject.layer)+
                //    " ? " + Physics.GetIgnoreCollision(other,GetCom ponent<Collider>()) + " ? " + Physics.GetIgnoreLayerCollision(other.gameObject.layer, gameObject.layer));

                PhysicsHelper.Raycast(transform.position - (transform.forward * 2.5f), transform.forward, 5, collider, PhysicsHelper.Layer_DamageReceiver_Environment, out RaycastHit hit);
                if (!CommonHelper.IsNull(alive) && Relation.IsEnemy(owner, alive))
                {
                    alive.ApplyDamage(model.MakeDamage(Id, hit.point, owner.Position, owner.Id, colId));
                }

                Die(collider, hit);
            }
        }

        public virtual void Die(Collider other, RaycastHit hit)
        {


            if (!CommonHelper.IsNull(other) && !CommonHelper.IsNull(hit.collider))
            {
                Pool.Activate(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.position, EnumNotifParam.rotation },
                                                           new object[] { Pool.GetEquivalentType(TagsHelper.GetMaterialType(hit.collider.tag)), hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal) }));

                //if (!CommonHelper.IsNull(other.GetComponent<Renderer>()))
                //{
                //    Notification.Notify(EnumNotifType.GetDecal, new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.type, EnumNotifParam.position, EnumNotifParam.rotation, EnumNotifParam.isTerrain, EnumNotifParam.target }, new object[] { EnumDecalType.Metal, hit.point, Quaternion.FromToRotation(-Vector3.forward, hit.normal), false, other.transform }));
                //}
                //else
                //{
                //    Terrain terrain;
                //    if (other.TryGetComponent<Terrain>(out terrain))
                //    {
                //        Notification.Notify(EnumNotifType.GetDecal, new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.type, EnumNotifParam.position, EnumNotifParam.isTerrain, EnumNotifParam.target }, new object[] { EnumDecalType.Wood, hit.point, true, terrain }));
                //        //Notification.Notify(EnumNotifType.GetPool, new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.position, EnumNotifParam.rotation }, new object[] { EnumPoolable.VfxImpactFoliage, hit.point, Quaternion.FromToRotation(-Vector3.forward, hit.normal) }));
                //    }

                //}

            }
            SetEnable(false);
        }

        public virtual void Activate(NotificationParameter param)
        {
            Fire(param.Get<IBulletDataModel>(EnumNotifParam.dataModel), param.Get<Vector3>(EnumNotifParam.position), param.Get<Vector3>(EnumNotifParam.targetPosition), param.Get<IAlive>(EnumNotifParam.owner));
        }
    }
}
