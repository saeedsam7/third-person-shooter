﻿using RCPU.Pool;
using UnityEngine;

namespace RCPU.Weapon
{
    public interface IBullet : IPoolable
    {
        public EnumAmmoType BulletType { get; }
        void Die(Collider other, RaycastHit hit);
        void CopyRigidbody(Rigidbody rb);
    }
}
