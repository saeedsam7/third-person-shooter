﻿

using RCPU.Damage;
using RCPU.Vfx;
using System;
using UnityEngine;

namespace RCPU.Weapon
{
    [Serializable]
    public class BulletDataModel : IBulletDataModel
    {
        //public EnumAmmoType bulletType; 
        public EnumVfxType explosionType;
        public Vector3 speed;
        public float radius;
        public PairArray<EnumDamageType, float> damagePower;
        public float lifeTime;

        public EnumVfxType ExplosionType => explosionType;
        public Vector3 Speed => speed;
        public float Radius => radius;
        //public float DamagePower(EnumDamageType damageType) => damagePower[damageType].capacity;
        public float LifeTime => lifeTime;

        [field: SerializeField] public EnumDamageSource DamageSource { get; protected set; }

        public IDamageModel MakeDamage(int Id, Vector3 hitPoint, Vector3 ownerPosition, int ownerId, int colidId)
        {
            return new DamageModel(Id, ownerId, colidId, ownerPosition, damagePower, hitPoint, DamageSource);
        }
    }

    public interface IBulletDataModel
    {
        //public EnumAmmoType bulletType;
        EnumVfxType ExplosionType { get; }
        Vector3 Speed { get; }
        float Radius { get; }
        //public float DamagePower(EnumDamageType damageType);
        float LifeTime { get; }
        IDamageModel MakeDamage(int id1, Vector3 point, Vector3 ownerPosition, int id2, int colId);
    }
}
