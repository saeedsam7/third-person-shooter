﻿using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Audio;
using RCPU.Component;
using RCPU.Helper;
using RCPU.InputSystem;
using RCPU.NotificationSystem;
using RCPU.VFX;
using System;
using UnityEngine;

namespace RCPU.Weapon
{
    [InitOrder(EnumInitOrder.Weapon)]
    public class WeaponComponentBase : BaseObject, IWeaponComponentBase
    {
        [HideInInspector] public virtual EnumWeaponSlotIndex weaponSlotIndex { get; }
        [InjectFromInstance] protected IAgent owner { get; set; }
        [InjectFromInstance] protected IAnimatorComponent anim { get; set; }
        protected IMuzzleFlash[] muzzle;
        [InjectFromSystem] protected IAudioManager AudioManager { set; get; }



        [SerializeField] protected Animator animator;
        [SerializeField] protected Collider rootCollider;
        [SerializeField] protected Rigidbody rigidBody;
        [SerializeField] protected Transform unarmParent;
        [SerializeField] protected Transform armParent;


        protected bool isActivated;
        protected int muzzleIndex;
        //protected Vector3 aimTarget;
        protected bool lastAimState;
        protected float lastTryShoot;


        public float CrosshairSize { get; protected set; }
        //public int TotalAmmo { get; protected set; }
        //public virtual int MaxAmmoCount { get => throw new Exception(); }
        public int AmmoInClip { get; protected set; }

        protected virtual EnumAmmoType bulletType { get => throw new Exception(); }
        public virtual float NoisePower { get => throw new Exception(); }
        //public virtual EnumCrosshairType CrosshairType { get; }
        //public virtual EnumSpriteIndex Icon { get; }
        //public virtual IWeaponDataModelBase Model { get; protected set; }
        public virtual Vector3 BulletPos { get => throw new Exception(); }
        public virtual Vector3 BulletForward { get => throw new Exception(); }
        public virtual EnumWeaponType WeaponType { get => throw new Exception(); }


        protected virtual void LoadModel()
        {
        }
        public virtual void Init()
        {
            LoadModel();
            Deselect();
            isActivated = false;
            muzzleIndex = 0;
            muzzle = GetComponentsInChildren<IMuzzleFlash>();
            //for (int i = 0; i < muzzle.Length; i++)
            //{
            //    muzzle[i].SetEnable(false);
            //}
        }

        public virtual void Aim(bool value)
        {
            if (value != this.lastAimState)
            {
                this.lastAimState = value;
            }
        }

        protected virtual void Fire(Vector3 target)
        {
        }

        public virtual void Activate()
        {
            isActivated = true;

            //anim.SetClipSpeed(AnimatorController_Base.AnimLayer.Pistol, weaponModel.AnimatorSpeedMultiplier * weaponModel.EachFireDelay);
        }

        public virtual void FinishAttack()
        {
        }

        public virtual void ExitAttack()
        {
        }
        public virtual void StartAttack()
        {
        }
        public virtual void Select()
        {
            //Logger.Log(name+" Select weapon  ");
            //gameObject.SetActive(true);
            IsEnable = true;
            SetParent(armParent);
        }

        protected void SetParent(Transform parent)
        {
            //Debug.Log(" SetParent " + model.weaponName + " - " + parent);
            transform.SetParent(parent, false);
            transform.localPosition = Vector3.zero;
            transform.localEulerAngles = Vector3.zero;
        }

        public virtual void ReloadCompleted(bool succeed)
        {
        }

        public virtual void Deselect()
        {
            //Ddebug.Log(name + " Deselect weapon  ");
            //gameObject.SetActive(false);

            SetParent(unarmParent);
            IsEnable = false;
        }



        public virtual bool CanAutoReload()
        {
            return false;
        }

        public virtual bool TryFire(bool keyDown)
        {
            return false;
        }



        //public int Pickup(IPickupAmmo pu)
        //{
        //    if (bulletType == pu.AmmoType && MaxAmmoCount > TotalAmmo)
        //    {
        //        //CLog.Error(" pu.Amount " + pu.Amount + " CurrentAmmoCount "+ CurrentAmmoCount);
        //        TotalAmmo += pu.Amount;
        //        if (TotalAmmo > MaxAmmoCount)
        //        {
        //            int unPicked = TotalAmmo - MaxAmmoCount;
        //            TotalAmmo = MaxAmmoCount;
        //            return pu.Amount - unPicked;
        //        }
        //        return pu.Amount;
        //    }
        //    return 0;
        //}

        public virtual bool LookAt(Vector3 target)
        {
            return CommonHelper.Angle(transform, target, false) < 10;
        }

        //public virtual Vector3 GetAimPosition()
        //{
        //    return Vector3.zero;
        //}

        protected void MakeNoise()
        {
            if (NoisePower > 0)
                Notification.Notify(EnumNotifType.NoiseCreated, new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.target, EnumNotifParam.noisePower, EnumNotifParam.position, },
                    new object[] { owner, NoisePower, transform.position }));
        }

        public virtual bool TryReload()
        {
            return false;
        }


    }
}