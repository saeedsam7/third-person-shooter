﻿using RCPU.Attributes;
using RCPU.Audio;
using RCPU.CameraFSM;
using RCPU.Component;
using RCPU.InputSystem;
using RCPU.Inventory;
using RCPU.Pool;
using UnityEngine;

namespace RCPU.Weapon
{
    public class WeaponComponentRange : WeaponComponentBase, IWeaponComponentBulletBase
    {
        [InjectFromSystem] protected IPool Pool { set; get; }
        [InjectFromSystem] protected ICamera cam { set; get; }
        [InjectFromInstance] protected IInvontoryComponent InvontoryComponent { set; get; }

        public override EnumWeaponSlotIndex weaponSlotIndex { get { return DataModelBulletBase.WeaponSlotIndex; } }
        //public override int MaxAmmoCount { get { return DataModelBulletBase.MaxAmmoCount; } }
        protected override EnumAmmoType bulletType { get { return DataModelBulletBase.BulletType; } }
        public override float NoisePower { get { return DataModelBulletBase.NoisePower; } }
        public virtual int ClipSize => DataModelBulletBase.ClipSize;
        public virtual IBulletDataModel BulletDataModel => DataModelBulletBase.BulletDataModel;
        public virtual EnumAudioSFX SfxReload => DataModelBulletBase.SfxReload;
        public virtual EnumAudioSFX SfxEmpty => DataModelBulletBase.SfxEmpty;
        public virtual EnumAudioSFX SfxShoot => DataModelBulletBase.SfxShoot;
        public virtual float EachFireDelay => DataModelBulletBase.EachFireDelay;
        public virtual float EachFireDuration => DataModelBulletBase.EachFireDuration;
        public virtual bool BurstFire => DataModelBulletBase.BurstFire;
        public virtual EnumShellType ShellType => DataModelBulletBase.ShellType;
        public override EnumWeaponType WeaponType => DataModelBulletBase.WeaponType;
        public virtual CameraShakeParam CameraShake => DataModelBulletBase.CameraShake;
        public virtual float MaxCrosshairSize => DataModelBulletBase.MaxCrosshairSize;
        public override Vector3 BulletPos => bulletPos.position;
        public override Vector3 BulletForward => bulletPos.forward;
        public virtual Vector3 ShellPos => shellPos.position;
        public virtual Quaternion ShellForward => shellPos.rotation;

        public float AnimatorReloadMultiplier => DataModelBulletBase.AnimatorReloadMultiplier;
        public float ReloadTime => DataModelBulletBase.ReloadTime;
        public EnumAmmoType BulletType => DataModelBulletBase.BulletType;
        public int Level => DataModelBulletBase.Level;
        public EnumAttckType AttckType => DataModelBulletBase.AttckType;
        public EnumWeaponName WeaponName => DataModelBulletBase.WeaponName;
        public EnumWeaponSlotIndex WeaponSlotIndex => DataModelBulletBase.WeaponSlotIndex;
        public float EquipTime => DataModelBulletBase.EquipTime;
        public float UnequipTime => DataModelBulletBase.UnequipTime;
        public float EquipSelectTime => DataModelBulletBase.EquipSelectTime;


        [SerializeField] protected Transform bulletPos;
        [SerializeField] protected LineRenderer lineRenderer;
        [SerializeField] protected Transform shellPos;

        public virtual IWeaponDataModelBulletBase DataModelBulletBase { get; protected set; }

        public EnumAmmoType AmmoType => DataModelBulletBase.BulletType;

        public override void Init()
        {
            base.Init();
            //TotalAmmo = MaxAmmoCount;
            AmmoInClip = ClipSize;
        }


        public override bool TryFire(bool keyDown)
        {
            if ((BurstFire && !keyDown) || keyDown)
            {
                if (TimeController.TimePassed(lastTryShoot + EachFireDelay))
                {
                    if (AmmoInClip > 0)
                    {
                        Fire();
                        return true;
                    }
                    else
                    {
                        AudioManager.PlaySfx(SfxEmpty, transform.position);
                    }
                }
            }

            return false;
        }

        protected virtual void Fire()
        {
            lastTryShoot = TimeController.gameTime;
            anim.SetTrigger(EnumTriggerParam.Shoot);

            Debug.DrawLine(BulletPos, owner.GetAimPosition(), Color.blue, 60);
            //CLog.Log(" Fire " + target + " CurrentClipSize " + CurrentClipSize);
            //if (AmmoInClip > 0)
            //{
            //this.target = target;
            //CLog.Error(" Fire ");
            FireBullet(owner.GetAimPosition());


            FireComplete();

            //Fire(param.Get<IBulletDataModel>(EnumNotifParam.dataModel), param.Get<Vector3>(EnumNotifParam.position), param.Get<Vector3>(EnumNotifParam.targetPosition), param.Get<ITarget>(EnumNotifParam.target));

        }
        public override void Select()
        {
            base.Select();
            lineRenderer.positionCount = 0;
            lineRenderer.enabled = true;
        }
        public override void Deselect()
        {
            base.Deselect();
            lineRenderer.enabled = false;
        }

        protected virtual void FireComplete()
        {
            //if (AmmoInClip > 0)
            //{
            //BulletSpeed= bullet.DataModel.speed;
            AmmoInClip--;
            //CLog.Error(" BulletReady CurrentClipSize " + CurrentClipSize);
            MakeNoise();
            //b.Fire(transform.position, CStateMachine_Camera.instance.GetRealForward(100),owner);
            //(bullet as IBullet).Fire(model.bulletDataModel, bulletPos, target, owner);
            //animator.SetTrigger(EnumTriggerParam.Shoot.ToString());


            muzzle[muzzleIndex].Activate();
            muzzleIndex++;
            if (muzzleIndex >= muzzle.Length)
                muzzleIndex = 0;


            AudioManager.PlaySfx(SfxShoot, transform.position);

        }

        protected virtual void FireBullet(Vector3 target)
        {

        }


        public override bool TryReload()
        {
            if (AmmoInClip < ClipSize && InvontoryComponent.AmmoCount(AmmoType) > 0)
            {
                AudioManager.PlaySfx(SfxReload, transform.position);
                return true;
            }
            return false;
        }
        public override bool CanAutoReload()
        {
            if (AmmoInClip == 0 && InvontoryComponent.AmmoCount(AmmoType) > 0)
            {
                return true;
            }
            return false;
        }



        public override void ReloadCompleted(bool succeed)
        {
            if (succeed)
            {
                int ammoCount = InvontoryComponent.AmmoCount(AmmoType);
                if (ammoCount > 0 && AmmoInClip < ClipSize)
                {

                    int totalAmmoCount = ammoCount + AmmoInClip;
                    if (totalAmmoCount >= ClipSize)
                    {
                        AmmoInClip = ClipSize;
                        totalAmmoCount -= ClipSize;
                    }
                    else
                    {
                        AmmoInClip = totalAmmoCount;
                        totalAmmoCount = 0;
                    }
                    InvontoryComponent.RemoveAmmo(AmmoType, ammoCount - totalAmmoCount);
                }
            }
            else
            {
                //TODO stop the reload sound
            }

        }

        //public override Vector3 GetAimPosition()
        //{
        //    return bulletPos.position + (bulletPos.forward * 100);
        //}



    }
}
