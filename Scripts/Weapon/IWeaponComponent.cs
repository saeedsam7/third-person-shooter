using RCPU.InputSystem;
using UnityEngine;

namespace RCPU.Weapon
{
    public interface IWeaponComponentBase : IInitializable
    {
        //int TotalAmmo { get; }
        int AmmoInClip { get; }
        EnumWeaponSlotIndex weaponSlotIndex { get; }
        Vector3 BulletPos { get; }
        Vector3 BulletForward { get; }
        float CrosshairSize { get; }
        EnumWeaponType WeaponType { get; }

        //IWeaponDataModelBase Model { get; }


        void Aim(bool value);
        void Activate();
        void FinishAttack();
        void ExitAttack();
        void StartAttack();
        void Select();
        void ReloadCompleted(bool succeed);
        void Deselect();
        bool TryReload();
        bool CanAutoReload();
        bool TryFire(bool keyDown);
        //int Pickup(IPickupAmmo pu);
        bool LookAt(Vector3 target);
        //Vector3 GetAimPosition();
    }

    public interface IWeaponComponentBulletBase : IWeaponComponentBase
    {
        EnumAmmoType AmmoType { get; }
    }

    public interface IWeaponComponentBulletManual : IWeaponComponentBulletBase, IWeaponUiData
    {
        IWeaponDataModelBulletManual DataModelBulletManual { get; }
    }

    public interface IWeaponComponentBulletAuto : IWeaponComponentBulletBase
    {
        IWeaponDataModelBulletAuto DataModelBulletAuto { get; }
    }
}