﻿
using RCPU.Attributes;
using RCPU.CameraFSM;
using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.Models;
using RCPU.NotificationSystem;
using RCPU.UI;
using UnityEngine;

namespace RCPU.Weapon
{
    [InjectToInstance(typeof(IWeaponComponentBulletManual))]
    [InitOrder(EnumInitOrder.Weapon)]
    public class WeaponComponentBulletManual : WeaponComponentRange, IWeaponComponentBulletManual
    {

        //protected Vector3 target;
        [SerializeField] protected WeaponDataModelBulletManual_SO model_SO;
        protected WeaponDataModelBulletManual model;

        public virtual EnumCrosshairType CrosshairType { get { return model.CrosshairType; } }
        public virtual EnumSpriteIndex Icon { get { return model.Icon; } }
        public virtual RangeFloat AimRecoil => model.aimRecoil;
        public virtual RangeFloat DefaultRecoil => model.defaultRecoil;
        public virtual float CrosshairCoolDown => model.CrosshairCoolDown;
        public virtual float CrosshairSizeIncreament => model.CrosshairSizeIncreament;
        [InjectFromInstance] protected IWeaponManagerComponentManual weaponGroup { get; set; }
        public IWeaponDataModelBulletManual DataModelBulletManual => model;

        public int TotalAmmo => InvontoryComponent.AmmoCount(AmmoType);

        //IBulletDataModel BulletDataModel => model.BulletDataModel;

        protected override void LoadModel()
        {
            this.DataModelBulletBase = model = DInjector.Instantiate<WeaponDataModelBulletManual_SO>(model_SO).model;
        }

        protected override void FireBullet(Vector3 target)
        {
            Pool.Activate(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.dataModel, EnumNotifParam.position, EnumNotifParam.targetPosition, EnumNotifParam.owner },
                                                       new object[] { Pool.GetEquivalentType(bulletType), BulletDataModel, BulletPos, target, owner }));

            Pool.Activate(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.position, EnumNotifParam.rotation },
                                                       new object[] { Pool.GetEquivalentType(ShellType), ShellPos, ShellForward }));
        }


        protected override void Fire()
        {
            base.Fire();
            CrosshairSize = Mathf.Clamp(CrosshairSize + CrosshairSizeIncreament, 0, MaxCrosshairSize);
            if (AimRecoil.max != 0)
                Notification.Notify(EnumNotifType.Recoil, new NotificationParameter(EnumNotifParam.value, RandomHelper.Range(AimRecoil)));
        }


        protected override void FireComplete()
        {
            base.FireComplete();
            weaponGroup.UpdateBulletUi();
            if (CameraShake.ShakeType != EnumCameraShakeType.None)
            {
                CameraShake.position = transform.position;
                cam.RegisterShake(CameraShake);
                //Notification.Notify(EnumNotifType.ShakeCamera, new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.position, EnumNotifParam.shakePower, EnumNotifParam.curveData },
                //                                                                                 new object[] { bulletPos.position, model.shakePower, model.shakeCurve }));
            }
        }

        void Update()
        {
            if (CrosshairSize > 0)
                CrosshairSize = Mathf.Clamp(CrosshairSize - TimeController.GetNormilizeTime(CrosshairCoolDown), 0, MaxCrosshairSize);
        }


    }
}
