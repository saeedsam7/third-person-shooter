﻿
using RCPU.Attributes;
using RCPU.CameraFSM;
using RCPU.DependencyInjection;
using RCPU.Helper;
using RCPU.Models;
using RCPU.NotificationSystem;
using RCPU.UI;
using UnityEngine;

namespace RCPU.Weapon
{
    [InjectToInstance(typeof(IWeaponComponentBulletManual))]
    [InitOrder(EnumInitOrder.Weapon)]
    public class WeaponComponentProjectileManual : WeaponComponentRange, IWeaponComponentBulletManual
    {
        public virtual EnumCrosshairType CrosshairType { get { return model.CrosshairType; } }
        public virtual EnumSpriteIndex Icon { get { return model.Icon; } }
        public virtual RangeFloat AimRecoil => model.aimRecoil;
        public virtual RangeFloat DefaultRecoil => model.defaultRecoil;
        public virtual float CrosshairCoolDown => model.CrosshairCoolDown;
        public virtual float CrosshairSizeIncreament => model.CrosshairSizeIncreament;
        public virtual float TimeBetweenStep { get { return model.timeBetweenStep; } }
        public virtual int StepCount { get { return model.stepCount; } }

        public int TotalAmmo => InvontoryComponent.AmmoCount(AmmoType);

        [SerializeField] protected WeaponDataModelProjectileManual_SO model_SO;
        protected WeaponDataModelProjectileManual model;

        public IWeaponDataModelBulletManual DataModelBulletManual => model;

        protected override void LoadModel()
        {
            DataModelBulletBase = model = DInjector.Instantiate<WeaponDataModelProjectileManual_SO>(model_SO).model;
        }
        public override void Aim(bool value)
        {
            base.Aim(value);
            if (value)
                DrawMovementLine();
            else
                lineRenderer.positionCount = 0;
        }


        protected Vector3 GetVelocity()
        {
            //if (model.independentControl)
            //{
            //    Vector3[] solutions = new Vector3[2];
            //    int numSolutions;
            //    //if (curTarget.velocity.sqrMagnitude > 0)
            //    //    numSolutions = fts.solve_ballistic_arc(projPos, projSpeed, targetPos, curTarget.velocity, gravity, out solutions[0], out solutions[1]);
            //    //else
            //    numSolutions = fts.solve_ballistic_arc(weaponConfig.bulletPos.position, BulletSpeed.z, target, -PhysicsHelper.Gravity.y, out solutions[0], out solutions[1]);


            //    if (numSolutions > 1 && CommonHelper.Distance(weaponConfig.bulletPos.position, target) > 15)
            //        return solutions[1];
            //    else
            //    if (numSolutions > 0)
            //        return solutions[0];
            //}

            return (BulletForward * BulletDataModel.Speed.z) + Vector3.up * BulletDataModel.Speed.y;
        }

        private void DrawMovementLine()
        {
            var res = rigidBody.CalculateMovement(BulletPos, model.stepCount, TimeBetweenStep, GetVelocity(), Vector3.zero);
            lineRenderer.positionCount = res.Count;
            for (int i = 0; i < res.Count; ++i)
            {
                lineRenderer.SetPosition(i, res[i]);
            }
        }


        protected override void FireBullet(Vector3 target)
        {
            Pool.Activate(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.dataModel, EnumNotifParam.position, EnumNotifParam.targetPosition, EnumNotifParam.owner, EnumNotifParam.velocity },
                                                       new object[] { Pool.GetEquivalentType(bulletType), BulletDataModel, BulletPos, target, owner, GetVelocity() }));

            Pool.Activate(new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.poolGroup, EnumNotifParam.position, EnumNotifParam.rotation },
                                                       new object[] { Pool.GetEquivalentType(ShellType), ShellPos, ShellForward }));
        }


        protected override void Fire()
        {
            base.Fire();
            CrosshairSize = Mathf.Clamp(CrosshairSize + CrosshairSizeIncreament, 0, MaxCrosshairSize);
            if (AimRecoil.max != 0)
                Notification.Notify(EnumNotifType.Recoil, new NotificationParameter(EnumNotifParam.value, RandomHelper.Range(AimRecoil)));

        }


        protected override void FireComplete()
        {
            base.FireComplete();
            if (CameraShake.ShakeType != EnumCameraShakeType.None)
            {
                CameraShake.position = transform.position;
                cam.RegisterShake(CameraShake);
                //Notification.Notify(EnumNotifType.ShakeCamera, new NotificationParameter(new EnumNotifParam[] { EnumNotifParam.position, EnumNotifParam.shakePower, EnumNotifParam.curveData },
                //                                                                                 new object[] { bulletPos.position, model.shakePower, model.shakeCurve }));
            }
        }

        void Update()
        {
            if (CrosshairSize > 0)
                CrosshairSize = Mathf.Clamp(CrosshairSize - TimeController.GetNormilizeTime(CrosshairCoolDown), 0, MaxCrosshairSize);
        }
    }
}