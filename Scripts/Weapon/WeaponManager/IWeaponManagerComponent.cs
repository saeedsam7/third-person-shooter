
using RCPU.Component;
using RCPU.InputSystem;
using UnityEngine;

namespace RCPU.Weapon
{
    public interface IWeaponManagerComponentBase : IComponent, IInitializable
    {
        //EnumWeaponType selectedWeaponType { get; }

        EnumWeaponType WeaponType { get; }
        Vector3 WeaponPosition { get; }
        Vector3 WeaponForward { get; }
        float ReloadSpeedMultiplier { get; }
        float ReloadTime { get; }
        float CrosshairSize { get; }
        float EquipTime { get; }
        float UnequipTime { get; }
        EnumAttckType AttckType { get; }
        bool IsReloading { get; }

        //void Init(List<MWeapon_Base_SO> availableWeaponModels, List<MWeaponType_Socket> weaponSockets);
        bool HasGrenade();
        bool HasBullet { get; }
        //int Pickup(IPickupAmmo pu);
        //void Pickup(IPickupWeapn PickupWeapn);
        bool LookAt(Vector3 target);
        //void UpdateBulletUI();
        bool TryReload();
        bool CanMustReload();
        //Vector3 GetAimPosition();
        void Aim(bool value);
        bool TryFire(bool keyDown);
        //bool HaveWeapon();
        //IWeaponBasicInfo CurrentWeapon { get; }
        bool Select(EnumWeaponSlotIndex index);
        void SelectActiveWeapon();
        bool HasWeaponOnSlot(EnumWeaponSlotIndex weaponSlotIndexToSelect);
        //bool HasAnyWeapon { get; }
        bool HasWeapon { get; }
    }

    public interface IWeaponManagerComponentAuto : IWeaponManagerComponentBase
    {
        float MinAttackDistance { get; }
        float MaxAttackDistance { get; }
    }

    public interface IWeaponManagerComponentManual : IWeaponManagerComponentBase
    {
        float EachFireDuration { get; }
        float EquipSelectTime { get; }

        void UpdateBulletUi();
    }
}