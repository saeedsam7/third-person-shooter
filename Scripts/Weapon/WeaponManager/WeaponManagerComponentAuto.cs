﻿using RCPU.Attributes;
using RCPU.Helper;
using RCPU.InputSystem;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Weapon
{
    [InitOrder(EnumInitOrder.WeaponManager)]
    [InjectToInstance(typeof(IWeaponManagerComponentAuto))]
    [InjectToInstance(typeof(IWeaponManagerComponentBase))]
    public class WeaponManagerComponentAuto : WeaponManagerComponentBase, IWeaponManagerComponentAuto
    {
        [InjectFromInstance] protected IWeaponComponentBulletAuto[] InjectedWeapon { get; set; }
        Dictionary<EnumWeaponSlotIndex, IWeaponComponentBulletAuto> AvailableAutoWeapon;

        protected IWeaponComponentBulletAuto CurrentAutoWeapon => AvailableAutoWeapon[current];
        protected IWeaponDataModelBulletAuto CurrentAutoModel => AvailableAutoWeapon[current].DataModelBulletAuto;

        public float MinAttackDistance { get { return (HasWeapon ? CurrentAutoModel.AttackDistanceRange.min : -1); } }
        public float MaxAttackDistance { get { return (HasWeapon ? CurrentAutoModel.AttackDistanceRange.max : Mathf.Infinity); } }

        protected override IWeaponComponentBulletBase CurrentWeapon => CurrentAutoWeapon;
        protected override IWeaponDataModelBulletBase CurrentModel => CurrentAutoModel;
        public override void Init()
        {
            base.Init();
            AvailableAutoWeapon = new Dictionary<EnumWeaponSlotIndex, IWeaponComponentBulletAuto>();
            if (!CommonHelper.IsNull(InjectedWeapon))
                for (int i = 0; i < InjectedWeapon.Length; i++)
                {
                    AvailableAutoWeapon.Add(InjectedWeapon[i].weaponSlotIndex, InjectedWeapon[i]);
                }
        }



        //public override int Pickup(IPickupAmmo pu)
        //{
        //    //foreach (var item in AvailableAutoWeapon.Values)
        //    //{
        //    //    if (item.WeaponType == pu.WeaponType)
        //    //    {
        //    //        return item.Pickup(pu);
        //    //    }
        //    //}
        //    return 0;
        //}

        public override bool HasWeaponOnSlot(EnumWeaponSlotIndex index)
        {
            return AvailableAutoWeapon.ContainsKey(index);
        }

    }
}