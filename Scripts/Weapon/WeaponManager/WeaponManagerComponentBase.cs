﻿
using RCPU.Agent;
using RCPU.Attributes;
using RCPU.Component;
using RCPU.InputSystem;
using RCPU.Inventory;
using RCPU.NotificationSystem;
using System.Collections.Generic;
using UnityEngine;

namespace RCPU.Weapon
{

    public class WeaponManagerComponentBase : ComponentBase, IWeaponManagerComponentBase
    {

        protected EnumWeaponSlotIndex current;
        [InjectFromInstance] protected IAnimatorComponent anim { get; set; }
        [InjectFromInstance] protected IAgent Owner { get; set; }
        [InjectFromInstance] protected IInvontoryComponent InvontoryComponent { set; get; }

        protected override int OwnerId => Owner.Id;
        public override EnumComponentType ComponentType => EnumComponentType.WeaponManager;
        public bool IsReloading { get; protected set; }
        //public int CurrentAmmoCount { get { return CurrentWeapon.TotalAmmo; } }
        public int CurrentClipSize { get { return CurrentWeapon.AmmoInClip; } }
        public EnumAttckType AttckType { get { return CurrentModel.AttckType; } }
        public EnumWeaponType WeaponType { get { return CurrentModel.WeaponType; } }
        public Vector3 WeaponPosition { get { return CurrentWeapon.BulletPos; } }
        public Vector3 WeaponForward { get { return CurrentWeapon.BulletForward; } }
        public float ReloadSpeedMultiplier { get { return CurrentModel.AnimatorReloadMultiplier; } }
        public float ReloadTime { get { return CurrentModel.ReloadTime; } }
        public float CrosshairSize { get { return CurrentWeapon.CrosshairSize; } }
        public float EquipTime { get { return CurrentModel.EquipTime; } }
        public float UnequipTime { get { return CurrentModel.UnequipTime; } }
        public float EquipSelectTime { get { return CurrentModel.EquipSelectTime; } }



        protected virtual IWeaponComponentBulletBase CurrentWeapon { get; }
        protected virtual IWeaponDataModelBulletBase CurrentModel { get; }


        public override void Init()
        {
            base.Init();
            //handle it by code instead of animator
            //owner.RegisterAnimatorEvent(CComponent_Animator_Base.EnumEventReceiver.Fire, Fire);
            //owner.RegisterAnimatorEvent(CComponent_Animator_Base.EnumEventReceiver.Reload, Reload);
            anim.RegisterEvent(EnumEventReceiver.StartAttack, StartAttack);
            anim.RegisterEvent(EnumEventReceiver.FinishAttack, FinishAttack);
            anim.RegisterEvent(EnumEventReceiver.ExitAttack, ExitAttack);

            Notification.RegisterInstance(Owner.Id, EnumInstanceNotifType.ReloadComplete, Id, OnReloadComplete);
            //AvailableWeapon = new Dictionary<EnumWeaponSlotIndex, IWeaponComponentBase>();
            //if (!CommonHelper.IsNull(InjectedWeapon))
            //    for (int i = 0; i < InjectedWeapon.Length; i++)
            //    {
            //        AvailableWeapon.Add(InjectedWeapon[i].weaponSlotIndex, InjectedWeapon[i]);
            //    } 
        }

        protected void ExitAttack(List<string> param)
        {
            if (AttckType == EnumAttckType.Melee)
            {
                CurrentWeapon.ExitAttack();
            }
        }

        protected void FinishAttack(List<string> param)
        {
            if (AttckType == EnumAttckType.Melee)
            {
                CurrentWeapon.FinishAttack();
            }
        }

        protected void StartAttack(List<string> param)
        {
            if (AttckType == EnumAttckType.Melee)
            {
                CurrentWeapon.StartAttack();
            }
        }



        public bool HasGrenade()
        {
            return false;
        }

        public bool HasBullet => HasWeapon && CurrentClipSize > 0;

        //public virtual int Pickup(IPickupAmmo pu)
        //{
        //    return 0;
        //}

        //public virtual void Pickup(IPickupWeapn pu)
        //{
        //}

        //public void Fire(List<string> param)
        //{
        //    //Logger.Log(" Shoot 5  ");
        //    if (isWeaponSelected && (owner.IsInState(CStateMachine_Agent.EnumState.Shoot) || anim.IsAnimTag(CComponent_Animator_Base.Tag_Move)))
        //    {
        //        //Logger.Log(" Shoot 6  ");
        //        CurrentWeapon.Fire(owner.GetAimPosition());
        //        //{
        //        //    //Logger.Log(" Shoot 7  ");
        //        //    UpdateBulletUI();
        //        //}
        //    }
        //}

        public bool LookAt(Vector3 target)
        {
            return CurrentWeapon.LookAt(target);
        }
        protected virtual void OnReloadComplete(NotificationParameter param)
        {
            IsReloading = false;
            if (param.Get<bool>(EnumNotifParam.Successful))
            {
                CurrentWeapon.ReloadCompleted(true);
            }
            else
            {
                CurrentWeapon.ReloadCompleted(false);
            }
        }

        public bool TryReload()
        {
            if (!IsReloading && AttckType == EnumAttckType.Ranged)
            {
                IsReloading = CurrentWeapon.TryReload();
                if (IsReloading)
                    anim.SetFloat(EnumFloatParam.Multiplier_Rotation, ReloadSpeedMultiplier);
                //Ddebug.Log(" TryReload   ");
                return IsReloading;
            }
            return false;
        }

        public bool CanMustReload()
        {
            if (!IsReloading && AttckType == EnumAttckType.Ranged)
            {
                //Ddebug.Log(" TryReload   ");
                return CurrentWeapon.CanAutoReload();
            }
            return false;
        }

        //public Vector3 GetAimPosition()
        //{
        //    return CurrentWeapon.GetAimPosition();
        //}
        public void Aim(bool value)
        {
            if (AttckType != EnumAttckType.None)
                CurrentWeapon.Aim(value);
        }
        public virtual bool TryFire(bool keyDown)
        {
            if (AttckType != EnumAttckType.None)
            {
                //Ddebug.Log(" TryShoot  ");
                return CurrentWeapon.TryFire(keyDown);
            }
            return false;
        }



        public virtual bool Select(EnumWeaponSlotIndex index)
        {
            //Ddebug.Log(" SwitchWeapon 2  index " + ((int)index) + " current " + current +
            //	" Length " + AvailableWeapon.Length + " AvailableWeapon " + AvailableWeapon[(int)index]); 
            if (HasWeaponOnSlot(index))
            {
                if (HasWeapon)
                    CurrentWeapon.Deselect();
                if (current == index)
                {
                    if (current != EnumWeaponSlotIndex.None)
                        return Select(EnumWeaponSlotIndex.None);
                }
                else
                {
                    //Ddebug.Log(" SwitchWeapon 2.5  " + index + "  " + AvailableWeapon[(int)index]);
                    //if (!CommonHelper.IsNull(AvailableWeapon[(int)index]))
                    //{
                    //Ddebug.Log(" SwitchWeapon 3  "); 
                    current = index;
                    return true;
                    //}
                }
            }
            return false;
        }

        public void SelectActiveWeapon()
        {
            CurrentWeapon.Select();
        }


        public bool HasWeapon => HasWeaponOnSlot(current);

        public virtual bool HasWeaponOnSlot(EnumWeaponSlotIndex index)
        {
            return false;
        }
    }
}