﻿
using RCPU.Attributes;
using RCPU.Helper;
using RCPU.InputSystem;
using RCPU.NotificationSystem;
using RCPU.UI;
using System.Collections.Generic;

namespace RCPU.Weapon
{
    [InitOrder(EnumInitOrder.WeaponManager)]
    [InjectToInstance(typeof(IWeaponManagerComponentManual))]
    [InjectToInstance(typeof(IWeaponManagerComponentBase))]
    public class WeaponManagerComponentManual : WeaponManagerComponentBase, IWeaponManagerComponentManual
    {
        [InjectFromSystem] IUIManager UiManager { get; set; }
        [InjectFromInstance] protected IWeaponComponentBulletManual[] InjectedWeapon { get; set; }
        Dictionary<EnumWeaponSlotIndex, IWeaponComponentBulletManual> AvailableManualWeapon;

        protected IWeaponComponentBulletManual CurrentManualWeapon => AvailableManualWeapon[current];
        protected IWeaponDataModelBulletManual CurrentManualModel => CurrentManualWeapon.DataModelBulletManual;
        protected override IWeaponComponentBulletBase CurrentWeapon => CurrentManualWeapon;
        protected override IWeaponDataModelBulletBase CurrentModel => CurrentManualModel;
        public float EachFireDuration => CurrentModel.EachFireDelay;

        public override void Init()
        {
            base.Init();
            AvailableManualWeapon = new Dictionary<EnumWeaponSlotIndex, IWeaponComponentBulletManual>();
            if (!CommonHelper.IsNull(InjectedWeapon))
                for (int i = 0; i < InjectedWeapon.Length; i++)
                {
                    AvailableManualWeapon.Add(InjectedWeapon[i].weaponSlotIndex, InjectedWeapon[i]);
                }
        }
        protected override void OnOwnerSpawned(NotificationParameter obj)
        {
            base.OnOwnerSpawned(obj);
            List<IWeaponUiData> all = new List<IWeaponUiData>();
            foreach (var weapon in AvailableManualWeapon)
            {
                if (weapon.Key != EnumWeaponSlotIndex.None)
                    all.Add(weapon.Value as IWeaponUiData);
            }
            Notification.Notify(EnumNotifType.PlayerWeaponsUpdated, new NotificationParameter(EnumNotifParam.dataModel, all));
            Notification.RegisterInstance(OwnerId, EnumInstanceNotifType.UpdateBulletUi, this.Id, OnUpdateBulletUi);
        }




        //public override int Pickup(IPickupAmmo pu)
        //{
        //    foreach (var item in AvailableManualWeapon.Values)
        //    {
        //        if (item.WeaponType == pu.WeaponType)
        //        {
        //            UpdateBulletUi();
        //            return item.Pickup(pu);
        //        }
        //    }
        //    return 0;
        //}

        protected override void OnReloadComplete(NotificationParameter param)
        {
            base.OnReloadComplete(param);
            if (param.Get<bool>(EnumNotifParam.Successful))
                UpdateBulletUi(); ;
        }



        public override bool Select(EnumWeaponSlotIndex index)
        {
            var res = base.Select(index);
            if (res)
            {
                UiManager.SetCrosshair(CurrentManualModel.CrosshairType);
                UiManager.SetImageSprite(EnumUiImageIndex.Weapom, CurrentManualModel.Icon);
                UpdateBulletUi();
            }
            return res;
        }

        public override bool TryFire(bool keyDown)
        {
            var res = base.TryFire(keyDown);
            if (res)
            {
                UpdateBulletUi();
            }
            return res;
        }
        private void OnUpdateBulletUi(NotificationParameter parameter)
        {
            UpdateBulletUi();
        }
        public void UpdateBulletUi()
        {
            if (AttckType != EnumAttckType.None)
                UiManager.SetText(EnumUiTextIndex.BulletCounter, CurrentClipSize + "/" + this.InvontoryComponent.AmmoCount(CurrentWeapon.AmmoType));
            else
                UiManager.SetText(EnumUiTextIndex.BulletCounter, "");
        }

        public override bool HasWeaponOnSlot(EnumWeaponSlotIndex index)
        {
            return AvailableManualWeapon.ContainsKey(index);
        }
    }
}
