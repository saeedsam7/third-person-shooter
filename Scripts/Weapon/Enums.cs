namespace RCPU.Weapon
{
    public enum EnumAmmoType { None, DamagePistol, EmpPistol, DetacherPistol, ExplosiveGrenadeLauncher, DamageAssultRifle, EmpAssultRifle, DetacherAssultRifle, DamageSMG, EmpSMG, DetacherSMG };

    public enum EnumWeaponName { None, Pistol01, Pistol02, FistKick, GrenadeLauncher, AssaultRifle };
    public enum EnumWeaponType { None, Pistol, AssaultRifle, Shotgun, Sniper, RocketLauncher, Grenade, FistKick, GrenadeLauncher };
    public enum EnumShellType { None, Pistol, AssaultRifle, Shotgun, Sniper, RocketLauncher, GrenadeLauncher };
    public enum EnumRagdollType { None, E01, E02, E03 };

    public enum EnumIkCondition
    {
        Never, Aimed, UnAimed, Always,
        IkWeight, ReverseIkWeight
    }
}