using RCPU.Attributes;
using RCPU.Component;
using RCPU.Helper;
using UnityEngine;

namespace RCPU.Patrol
{
    [InitOrder(EnumInitOrder.Patrol)]
    [InjectToSystem(typeof(IPatrol))]
    public class PatrolManager : BaseObject, IPatrol
    {
        [InjectFromSystem] IPatrolGroup[] allPoints { get; set; }

        public IPatrolGroup GetPatrolPoints(EnumMovementType movementType, Vector3 position)
        {
            float dis = Mathf.Infinity;
            int index = -1;
            for (int i = 0; i < allPoints.Length; i++)
            {
                if (CommonHelper.Distance(position, allPoints[i].Center) < dis)
                {
                    index = i;
                    dis = CommonHelper.Distance(position, allPoints[i].Center);
                }
            }
            return allPoints[index];
        }

        public void Init()
        {
        }
    }
}