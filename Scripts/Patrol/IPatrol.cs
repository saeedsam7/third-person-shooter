﻿

using RCPU.Component;
using UnityEngine;

namespace RCPU.Patrol
{
    public interface IPatrol : IInitializable
    {
        IPatrolGroup GetPatrolPoints(EnumMovementType movementType, Vector3 position);
    }
}
