

using System;

namespace RCPU
{
    [Flags]
    public enum EnumPatrolPointType { Walk = 1, IronMine = 2, MercuryField = 4, Scan = 8 }
}