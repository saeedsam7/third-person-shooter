﻿
using UnityEngine;

namespace RCPU.Patrol
{
    public interface IPatrolGroup : IInitializable
    {
        Vector3 Center { get; }

        int GetClosestIndex(Vector3 position);
        Vector3[] GetPoints(EnumPatrolPointType pointType);
    }
}
