﻿
using RCPU.Attributes;
using RCPU.Component;
using RCPU.Helper;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace RCPU.Patrol
{
    [InjectToSystemArray(typeof(IPatrolGroup))]
    [InitOrder(EnumInitOrder.Patrol)]
    public class PatrolGroup : BaseObject, IPatrolGroup
    {

        [SerializeField] PatrolPoint[] allPoints;
        [SerializeField] EnumMovementType movementType;

        Vector3 center;
        public Vector3 Center => center;
        protected Dictionary<EnumPatrolPointType, Vector3[]> points;

        public Vector3[] GetPoints(EnumPatrolPointType pointType)
        {
            return points[pointType];
        }


        public void Init()
        {
            center = Vector3.zero;
            int counter = 0;
            points = new Dictionary<EnumPatrolPointType, Vector3[]>();
            for (int j = 0; j < CommonHelper.GetEnumLength(typeof(EnumPatrolPointType)); j++)
            {
                EnumPatrolPointType tpy = CommonHelper.GetEnumMember<EnumPatrolPointType>(j);
                var sub = Array.FindAll(allPoints, (x => (x.pointType & tpy) == tpy));
                Vector3[] positions = new Vector3[sub.Length];
                for (int i = 0; i < sub.Length; i++)
                {
                    if (NavMeshHelper.SamplePosition(sub[i].point.position + Vector3.up, out NavMeshHit hit, 1000, NavMeshHelper.GetNavmehsLayer(movementType)))
                    {
                        //CLog.Error("2 CPatrolPoints GroundPatrolPoint " + hit.position + " distance " + hit.distance + " mask " + hit.mask);
                        sub[i].point.position = hit.position;
                    }
                    positions[i] = sub[i].point.position;
                    center += positions[i];
                    counter++;
                }
                points.Add(tpy, positions);
            }
            center /= counter;
        }

        public void OnDrawGizmos()
        {
#if UNITY_EDITOR

            if (points != null && points.Count > 0)
            {
                for (int j = 0; j < CommonHelper.GetEnumLength(typeof(EnumPatrolPointType)); j++)
                {
                    var tpy = CommonHelper.GetEnumMember<EnumPatrolPointType>(j);
                    if (points[tpy].Length > 0)
                        GizmosHelper.DrawLines(points[tpy], true, Color.white);
                }
            }
            else
            {
                for (int j = 0; j < allPoints.Length - 1; j++)
                {
                    Debug.DrawLine(allPoints[j].point.position, allPoints[j + 1].point.position, Color.white);
                }
                for (int j = 0; j < allPoints.Length; j++)
                {
                    if (allPoints[j].pointType != EnumPatrolPointType.Walk)
                        Debug.DrawLine(allPoints[j].point.position, allPoints[j].point.position + Vector3.up, Color.red);
                }
            }
#endif
        }

        public int GetClosestIndex(Vector3 pos)
        {
            var walkPoints = points[EnumPatrolPointType.Walk];
            var closestDis = Mathf.Infinity;
            var closestIndex = -1;
            for (int i = 0; i < walkPoints.Length; i++)
            {
                var dis = CommonHelper.Distance(walkPoints[i], pos);
                if (dis < closestDis)
                {
                    closestDis = dis;
                    closestIndex = i;
                }
            }

            return ((closestIndex + 1) % walkPoints.Length);
        }
    }

    [Serializable]
    public class PatrolPoint
    {
        public Transform point;
        public EnumPatrolPointType pointType;
    }

}
