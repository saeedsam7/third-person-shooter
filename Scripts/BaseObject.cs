﻿using UnityEngine;

namespace RCPU
{
    public class BaseObject : MonoBehaviour
    {
        [SerializeField] protected bool DEBUG;
        protected int _Id;
        public virtual int Id { get { if (_Id == 0) _Id = GetInstanceID(); return _Id; } protected set { _Id = value; } }
        protected virtual bool IsEnable { get { return this.enabled /*&& (model.EnterFrame != TimeController.frameCount)*/; } set { this.enabled = value; } }

    }
}
