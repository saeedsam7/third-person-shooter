﻿
using RCPU.Helper;
using RCPU.InputSystem;
using System;
using System.Reflection;
using UnityEngine;

namespace RCPU.Attributes
{
    public abstract class InputRegisterAttribute : MethodBaseAttribute
    {
        public EnumInputPriority priority;
        protected string checkMethod;
        protected bool allways;
        public Func<bool> checkFunction;
        public EnumGameOveralState[] gameState;
        public EnumInGameUiState inGameUIState;
        public EnumUnityEvent unityEvent;
        protected InputRegisterAttribute(EnumInputPriority priority, string checkMethod, EnumUnityEvent unityEvent, EnumGameOveralState[] gameState, EnumInGameUiState inGameUIState)
        {
            this.priority = priority;
            this.gameState = gameState;
            this.unityEvent = unityEvent;
            this.inGameUIState = inGameUIState;
            this.checkMethod = checkMethod;
            this.allways = false;
        }
        protected InputRegisterAttribute(EnumInputPriority priority, string checkMethod, EnumUnityEvent unityEvent, EnumGameOveralState gameState, EnumInGameUiState inGameUIState)
        {
            this.priority = priority;
            this.gameState = new EnumGameOveralState[] { gameState };
            this.unityEvent = unityEvent;
            this.inGameUIState = inGameUIState;
            this.checkMethod = checkMethod;
            this.allways = false;
        }



        bool AlwaysTrue()
        {
            return true;
        }

        public bool GenerateCheckFunction(object owner)
        {
            if (!string.IsNullOrEmpty(checkMethod))
            {
                var objectProperty = owner.GetType().GetMethod(checkMethod, BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.NonPublic | BindingFlags.Public);
                if (CommonHelper.IsNull(objectProperty))
                {
                    Debug.LogError("objectProperty " + objectProperty);
                }
                else
                {
                    if (objectProperty.ReturnType == typeof(bool))
                    {
                        checkFunction = (Func<bool>)objectProperty.CreateDelegate(typeof(Func<bool>), owner);
                        return true;
                    }
                }

            }
            else
            {
                checkFunction = (Func<bool>)Delegate.CreateDelegate(typeof(Func<bool>), this, "AlwaysTrue");
                return true;
                //throw new Exception(checkMethod + " invalid method name for input check ");
            }
            throw new Exception(checkMethod + " something went wrong ");

        }
    }
}