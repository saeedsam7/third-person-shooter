﻿
using RCPU.InputSystem;
using System;
using System.Reflection;
namespace RCPU.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]

    public class InputRegisterButtonHoldAttribute : InputRegisterAttribute
    {
        public EnumButton btn;
        public InputRegisterButtonHoldAttribute(EnumInputPriority priority, EnumUnityEvent unityEvent, string checkMethod, EnumGameOveralState[] gameState, EnumInGameUiState inGameUIState, EnumButton btn) : base(priority, checkMethod, unityEvent, gameState, inGameUIState)
        {
            this.btn = btn;
        }

        public InputRegisterButtonHoldAttribute(EnumInputPriority priority, EnumUnityEvent unityEvent, string checkMethod, EnumGameOveralState gameState, EnumInGameUiState inGameUIState, EnumButton btn) : base(priority, checkMethod, unityEvent, gameState, inGameUIState)
        {
            this.btn = btn;
        }

        public /*override*/ Action<T> GetDelegate<T>(object owner, MethodInfo method)
        {
            if (CheckMethod(owner) && HaveParameters(method.GetParameters()) && GenerateCheckFunction(owner))
                return Delegate.CreateDelegate(typeof(Action<T>), owner, method) as Action<T>;
            return null;
        }

        public override bool HaveParameters(ParameterInfo[] parameters)
        {
            return parameters.Length == 1 && parameters[0].ParameterType == typeof(float);
        }
    }
}