﻿using System;

namespace RCPU.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class InjectFromInstanceAttribute : ClassBaseAttribute
    {
        public InjectFromInstanceAttribute()
        {
        }
    }
}
