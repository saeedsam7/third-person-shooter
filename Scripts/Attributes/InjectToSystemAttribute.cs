﻿
using System;

namespace RCPU.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InjectToSystemAttribute : ClassBaseAttribute
    {
        public Type interfaceType;
        public InjectToSystemAttribute(Type interfaceType)
        {
            this.interfaceType = interfaceType;
        }
    }
}
