﻿using System;

namespace RCPU.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InitOrderAttribute : ClassBaseAttribute
    {
        public EnumInitOrder order;
        public InitOrderAttribute(EnumInitOrder order)
        {
            this.order = order;
        }

    }
}
