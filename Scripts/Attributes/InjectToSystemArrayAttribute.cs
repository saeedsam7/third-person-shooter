﻿
using System;

namespace RCPU.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InjectToSystemArrayAttribute : ClassBaseAttribute
    {
        public Type interfaceType;
        public InjectToSystemArrayAttribute(Type interfaceType)
        {
            this.interfaceType = interfaceType;
        }
    }
}
