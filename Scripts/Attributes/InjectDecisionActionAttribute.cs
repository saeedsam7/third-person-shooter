﻿
using RCPU.Agent;
using System;
using System.Reflection;

namespace RCPU.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class InjectDecisionActionAttribute : Attribute
    {
        public EnumDecision decision;
        public InjectDecisionActionAttribute(EnumDecision decision)
        {
            this.decision = decision;
        }

        public Action Value(object instance, MethodInfo method)
        {
            return Delegate.CreateDelegate(typeof(Action), instance, method) as Action;
        }
    }

}
