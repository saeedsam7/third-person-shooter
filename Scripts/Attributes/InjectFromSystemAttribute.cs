﻿
using System;

namespace RCPU.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class InjectFromSystemAttribute : ClassBaseAttribute
    {

        public InjectFromSystemAttribute()
        {
        }
    }
}
