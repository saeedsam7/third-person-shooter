﻿

using System;

namespace RCPU.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class InjectToInstanceAttribute : ClassBaseAttribute
    {
        public Type interfaceType;
        public InjectToInstanceAttribute(Type interfaceType)
        {
            this.interfaceType = interfaceType;
        }
    }
}
