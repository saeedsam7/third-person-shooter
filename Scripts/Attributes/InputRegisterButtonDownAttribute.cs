﻿
using RCPU.InputSystem;
using System;
using System.Reflection;
namespace RCPU.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class InputRegisterButtonDownAttribute : InputRegisterAttribute
    {
        public EnumButton btn;

        public InputRegisterButtonDownAttribute(EnumInputPriority priority, EnumUnityEvent unityEvent, string checkMethod, EnumGameOveralState[] gameState, EnumInGameUiState inGameUIState, EnumButton btn) : base(priority, checkMethod, unityEvent, gameState, inGameUIState)
        {
            this.btn = btn;
        }

        public InputRegisterButtonDownAttribute(EnumInputPriority priority, EnumUnityEvent unityEvent, string checkMethod, EnumGameOveralState gameState, EnumInGameUiState inGameUIState, EnumButton btn) : base(priority, checkMethod, unityEvent, gameState, inGameUIState)
        {
            this.btn = btn;
        }



        public /*override*/ Action GetDelegate(object owner, MethodInfo method)
        {
            if (CheckMethod(owner) && HaveParameters(method.GetParameters()) && GenerateCheckFunction(owner))
                return Delegate.CreateDelegate(typeof(Action), owner, method) as Action;
            return null;
        }

        public override bool HaveParameters(ParameterInfo[] parameters)
        {
            return parameters.Length == 0;
        }
    }

}