﻿
using System;

namespace RCPU.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class RequireInputAttribute : ClassBaseAttribute
    {
        public RequireInputAttribute()
        {
        }
    }
}