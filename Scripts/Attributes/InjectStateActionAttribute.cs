﻿

using RCPU.Agent;
using System;
using System.Reflection;

namespace RCPU.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class InjectStateActionAttribute : Attribute
    {
        public EnumAgentStateEvent state;
        public InjectStateActionAttribute(EnumAgentStateEvent state)
        {
            this.state = state;
        }

        public Delegate Value(object instance, MethodInfo method)
        {
            return Delegate.CreateDelegate(typeof(Action), instance, method);
        }
    }


}
