﻿using System;
using System.Reflection;
using UnityEngine;

namespace RCPU.Attributes
{
    public abstract class MethodBaseAttribute : Attribute
    {
        protected MethodBaseAttribute()
        {
        }

        public abstract bool HaveParameters(ParameterInfo[] parameters);
        public virtual bool CheckMethod(object owner)
        {
            return true;
        }

        public static int GetAttributeOrder(Type monoType)
        {
            int order = 0;
            //var monoType = mono.GetType();
            var attributes = monoType.GetCustomAttributes(typeof(InitOrderAttribute), true);
            if (attributes.Length > 0)
            {
                order = (int)((attributes[0] as InitOrderAttribute).order);
                //Debug.Log(mono.name + "  Order " + order);
            }
            else
            {
                Debug.LogError(monoType.Name + " Dont have Attribute_InitOrder ");
            }
            return order;
        }

    }
}