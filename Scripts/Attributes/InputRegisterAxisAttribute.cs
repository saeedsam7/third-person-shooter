﻿
using RCPU.InputSystem;
using System;
using System.Reflection;
namespace RCPU.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class InputRegisterAxisAttribute : InputRegisterAttribute
    {
        public InputRegisterAxisAttribute(EnumInputPriority priority, EnumUnityEvent unityEvent, string checkMethod, EnumGameOveralState[] gameState, EnumInGameUiState inGameUIState) : base(priority, checkMethod, unityEvent, gameState, inGameUIState)
        {
        }
        public InputRegisterAxisAttribute(EnumInputPriority priority, EnumUnityEvent unityEvent, string checkMethod, EnumGameOveralState gameState, EnumInGameUiState inGameUIState) : base(priority, checkMethod, unityEvent, gameState, inGameUIState)
        {
        }

        public Action<T, K> GetDelegate<T, K>(object owner, MethodInfo method)
        {
            if (CheckMethod(owner) && HaveParameters(method.GetParameters()) && GenerateCheckFunction(owner))
                return Delegate.CreateDelegate(typeof(Action<T, K>), owner, method) as Action<T, K>;

            return null;
        }

        public override bool HaveParameters(ParameterInfo[] parameters)
        {
            return parameters.Length == 2
                && parameters[0].ParameterType == typeof(float[])
                && parameters[1].ParameterType == typeof(bool[]);
        }
    }
}

